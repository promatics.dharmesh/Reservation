package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WriteReviewRestaurant extends Fragment implements View.OnClickListener, ServiceCallback {

    private Spinner spNoiseLvl, spFood, spService, spAmbience;
    private EditText etReview;
    private TextView tvSubmit, tvCancel,tvrview,tvservice,tvfood,tvNoise,tvambience;
    private RatingBar ratingbar;
    private ArrayList<String> rating = new ArrayList<>();
    private ArrayList<String> noiseList = new ArrayList<>();
    private boolean check=false;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rating_review, container, false);
        getActivity().setTitle("Rating & Review");
        ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
        spNoiseLvl = (Spinner) v.findViewById(R.id.spNoiseLvl);
        spFood = (Spinner) v.findViewById(R.id.spFood);
        spService = (Spinner) v.findViewById(R.id.spService);
        spAmbience = (Spinner) v.findViewById(R.id.spAmbience);
        etReview = (EditText) v.findViewById(R.id.etReview);
        tvSubmit = (TextView) v.findViewById(R.id.tvSubmit);
        tvCancel = (TextView) v.findViewById(R.id.tvCancel);
        tvrview = (TextView) v.findViewById(R.id.tvrview);
        tvNoise = (TextView) v.findViewById(R.id.tvNoise);
        tvfood = (TextView) v.findViewById(R.id.tvfood);
        tvservice = (TextView) v.findViewById(R.id.tvservice);
        tvambience = (TextView) v.findViewById(R.id.tvambience);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        Drawable stars = ratingbar.getProgressDrawable();
        DrawableCompat.setTint(stars, Color.rgb(162, 138, 60));
        for (int i = 1; i <= 5; i++) {
            rating.add("" + i);
        }
        rating.add(0, "Please select");
        noiseList.add(0, "Please select");
        noiseList.add("Low");
        noiseList.add("Moderate");
        noiseList.add("High");
        spFood.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, rating));
        spService.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, rating));
        spAmbience.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, rating));
        spNoiseLvl.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, noiseList));
//        android:progressTint="@color/ColorPrimary" -->to change the color of the stars when clicked
//        stars.setColorFilter(Color.parseColor("#A28A3C"), PorterDuff.Mode.SRC_ATOP);
        check=getArguments().getBoolean("check");
        Log.e("check",""+check);
        if(check){
            tvrview.setText("Your Reviews");
            tvSubmit.setVisibility(View.GONE);
            tvCancel.setVisibility(View.GONE);
            tvambience.setVisibility(View.VISIBLE);
            tvNoise.setVisibility(View.VISIBLE);
            tvfood.setVisibility(View.VISIBLE);
            tvservice.setVisibility(View.VISIBLE);
            spNoiseLvl.setVisibility(View.GONE);
            spFood.setVisibility(View.GONE);
            spService.setVisibility(View.GONE);
            spAmbience.setVisibility(View.GONE);
            ratingbar.setClickable(false);
            etReview.setEnabled(false);

            ratingbar.setIsIndicator(true);
            if(getArguments().getString("noise_level").equalsIgnoreCase("1")){
                tvNoise.setText("Low");
            }else if(getArguments().getString("noise_level").equalsIgnoreCase("2")){
                tvNoise.setText("Moderate");
            }else if(getArguments().getString("noise_level").equalsIgnoreCase("3")){
                tvNoise.setText("High");
            }
            tvfood.setText(getArguments().getString("food_rating"));
            tvservice.setText(getArguments().getString("service_rating"));
            tvambience.setText(getArguments().getString("ambience_rating"));
            etReview.setText(getArguments().getString("review"));
            ratingbar.setRating(Float.parseFloat(getArguments().getString("overall_rating")));
        }
        else{
            tvrview.setText("Write Your Review");
            ratingbar.setClickable(true);
            ratingbar.setIsIndicator(false);
            tvSubmit.setVisibility(View.VISIBLE);
            tvCancel.setVisibility(View.VISIBLE);
            spNoiseLvl.setVisibility(View.VISIBLE);
            spFood.setVisibility(View.VISIBLE);
            spService.setVisibility(View.VISIBLE);
            spAmbience.setVisibility(View.VISIBLE);
            tvambience.setVisibility(View.GONE);
            tvNoise.setVisibility(View.GONE);
            tvfood.setVisibility(View.GONE);
            tvservice.setVisibility(View.GONE);
        }
        tvSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        return v;
    }

    private void Validation() {
        if(ratingbar.getRating()<=0){
            CommonUtils.showToast(getActivity(), "Please Give OverAll Rating");
        }
        else if (spNoiseLvl.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please Select Noise Level");
        } else if (spFood.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please Select Food Rating");
        } else if (spService.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please Select Service Rating");
        } else if (spAmbience.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please Select Ambiance Rating");
        } else if (etReview.getText().toString().trim().length() == 0) {
            etReview.setError("Write your reviews here");
            etReview.requestFocus();
        } else {

            giveReview();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                getActivity().onBackPressed();
                break;

            case R.id.tvSubmit:
                etReview.setError(null);
                Validation();
                break;
        }
    }

    private void giveReview() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        map.put("restaurant_id", getArguments().getString("rest_id"));
        map.put("rest_book_id", getArguments().getString("booking_id"));
        map.put("noise_level", String.valueOf(spNoiseLvl.getSelectedItemPosition()));
        map.put("review", etReview.getText().toString().trim());
        map.put("overall_rating", String.valueOf(ratingbar.getRating()));
        map.put("food_rating", String.valueOf(spFood.getSelectedItem()));
        map.put("service_rating", String.valueOf(spService.getSelectedItem()));
        map.put("ambience_rating", String.valueOf(spAmbience.getSelectedItem()));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_GIVE_REVIEW_RESTAURANT, map).execute(Constants.GIVE_REVIEW_RESTAURANT);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_GIVE_REVIEW_RESTAURANT:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("Review Given successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().onBackPressed();
                                llayout.setVisibility(View.VISIBLE);
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
