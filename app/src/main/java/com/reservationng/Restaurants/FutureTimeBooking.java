package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CMS.PrivacyPolicy;
import com.reservationng.CMS.TermsnConditions1;
import com.reservationng.ClickSpan;
import com.reservationng.ConfirmationPage;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.CommonUtils;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

public class FutureTimeBooking extends android.support.v4.app.Fragment implements ServiceCallback, View.OnClickListener {

    private TextView txtName, txtPlace;
    private ImageView imgview;
    private TextView tvMobileNo, tvRestNote, tvpoints, tvoffer, txtRestName, tvPeople, tvdte, tvtime,tvagree_terms;
    private String MyDate1, dt, day, mnth, date, noOfpeople, showDate;
    private Button btnReserve;
    private EditText etSpecialReq, etFname, etLname, etEmail, etPhone;
    private boolean check = false;
    private AQuery aQuery = new AQuery(getActivity());
    View v;
    private LinearLayout llayout;
    private FrameLayout framelayout;
    private ImageView ivImg;
    private CircleImageView ivRestImg;
    public static boolean FutureBookFlag;
    Dialog d1;


    @Nullable

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.restaurant_book_screen, container, false);
            getActivity().setTitle("Reservation");
            ivImg = (ImageView) v.findViewById(R.id.ivImg);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            tvpoints = (TextView) v.findViewById(R.id.tvpoints);
            tvoffer = (TextView) v.findViewById(R.id.tvoffer);
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            tvagree_terms = (TextView) v.findViewById(R.id.tvagree_terms);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            tvMobileNo = (TextView) v.findViewById(R.id.tvMobileNo);
            tvRestNote = (TextView) v.findViewById(R.id.tvRestNote);
            btnReserve = (Button) v.findViewById(R.id.btnReserveHotel);
            etSpecialReq = (EditText) v.findViewById(R.id.etSpecialReq);
            etFname = (EditText) v.findViewById(R.id.etFname);
            etLname = (EditText) v.findViewById(R.id.etLname);
            etEmail = (EditText) v.findViewById(R.id.etEmail);
            etPhone = (EditText) v.findViewById(R.id.etPhone);

            llayout = (LinearLayout) v.findViewById(R.id.llayout);
            tvMobileNo.setText(getArguments().getString("mobile_number"));
            txtRestName.setText(getArguments().getString("rest_name"));
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dt = getArguments().getString("date");
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMMM d");
            MyDate1 = newDateFormat.format(MyDate);
            Log.e("date", MyDate1 + dt);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            check = getArguments().getBoolean("check");
            tvRestNote.setText(getArguments().getString("rest_desc"));
            noOfpeople = getArguments().getString("noOfPeople");

            tvPeople.setText(noOfpeople);
            tvdte.setText(day + ", " + mnth + " " + date);
            tvtime.setText(DataHolder.getInstance().slottiming);
            if (User.getInstance() != null) {
                tvpoints.setVisibility(View.VISIBLE);
                tvoffer.setVisibility(View.VISIBLE);
                tvpoints.setText(getArguments().getString("reward_point") + " points");
                etFname.setFocusableInTouchMode(false);
                etLname.setFocusableInTouchMode(false);
                etEmail.setFocusableInTouchMode(false);
                etFname.setText(User.getInstance().getFname());
                etLname.setText(User.getInstance().getLname());
                etEmail.setText(User.getInstance().getEmail());
                if(User.getInstance().getMobileNo().length()>10) {
                    etPhone.setText(User.getInstance().getMobileNo().substring(3,User.getInstance().getMobileNo().length()));
                }else{
                    etPhone.setText(User.getInstance().getMobileNo());
                }
            } else {
                tvpoints.setVisibility(View.GONE);
                tvoffer.setVisibility(View.GONE);
                etEmail.setFocusableInTouchMode(true);
                etFname.setFocusableInTouchMode(true);
                etLname.setFocusableInTouchMode(true);
            }
            aQuery.id(ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));
            Log.e("future time booking ",DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng);
            tvagree_terms.setOnClickListener(this);

            CommonUtils.clickify(tvagree_terms, "Terms and Conditions", new ClickSpan.OnClickListener() {
                @Override
                public void onClick() {
                    Intent i=new Intent(getActivity(), TermsnConditions1.class);
                    startActivity(i);
//                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new TermsnConditions(), TermsnConditions.class.getSimpleName());
                }
            });

            CommonUtils.clickify(tvagree_terms, "Privacy Statements", new ClickSpan.OnClickListener() {
                @Override
                public void onClick() {
                    Intent i = new Intent(getActivity(), PrivacyPolicy.class);
                    startActivity(i);
//                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new PrivacyPolicy(), PrivacyPolicy.class.getSimpleName());
                }
            });

            btnReserve.setOnClickListener(this);
            tvMobileNo.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    private void booking() {
        DataHolder.getInstance().time = DataHolder.getInstance().slottiming;
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("restaurant_id", getArguments().getString("rest_id"));
        map.put("timing_id", DataHolder.getInstance().slotId);
        map.put("no_of_person", noOfpeople);
        if (User.getInstance() != null) {
            map.put("member_id", User.getInstance().getUserId());
        } else {
            map.put("member_id","");
        }
        DataHolder.getInstance().efname= etFname.getText().toString().trim();
        DataHolder.getInstance().elname= etLname.getText().toString().trim();
        DataHolder.getInstance().emailuser= etEmail.getText().toString().trim();
        DataHolder.getInstance().ephn= etPhone.getText().toString().trim();

        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("email", etEmail.getText().toString().trim());
        map.put("mobile_number", "234"+etPhone.getText().toString().trim());
        map.put("special_request", etSpecialReq.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_TABLE_BOOKING, map).execute(Constants.RESTAURANT_TABLE_BOOKING);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_RESTAURANT_TABLE_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);

                        JSONObject booking_info=object.getJSONObject("booking_info");
                        JSONObject RestaurantBooking=booking_info.getJSONObject("RestaurantBooking");
                        JSONObject Restaurant=booking_info.getJSONObject("Restaurant");

                        Fragment fragment = new ConfirmationPage();
                        DataHolder.getInstance().bookingId = RestaurantBooking.getString("id");
                        DataHolder.getInstance().tableTimeId = RestaurantBooking.getString("table_time_id");
                        DataHolder.getInstance().id = RestaurantBooking.getString("restaurant_id");
                        DataHolder.getInstance().noOfPeople = RestaurantBooking.getString("no_of_person");
                        DataHolder.getInstance().date = RestaurantBooking.getString("date");
                        DataHolder.getInstance().specReq = RestaurantBooking.getString("special_request");
                        StringBuilder ssb = new StringBuilder();
                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().name = Restaurant.getString("rest_name");
                        DataHolder.getInstance().img = Restaurant.getString("image1");
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
                        DataHolder.getInstance().address = ssb.toString();
                        DataHolder.getInstance().desc = Restaurant.getString("description");
//                        DataHolder.getInstance().curLng = getArguments().getString("curlong");
//                        DataHolder.getInstance().curLati = getArguments().getString("curlat");
//                        DataHolder.getInstance().time=DataHolder.getInstance().slottiming;
                        Log.e("with login", DataHolder.getInstance().id+" "+DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng);
                        if (User.getInstance() != null) {
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ConfirmationPage.class.getSimpleName());
                        } else {
                            if (WithoutSignIn.ResvGuestFlag) {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ConfirmationPage.class.getSimpleName());
                            }else{
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new WithoutSignIn(), WithoutSignIn.class.getSimpleName());

                            }
                        }
                        }
                    else if(code == 2)
                    {
                        llayout.setVisibility(View.VISIBLE);

                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
                                View vvv = LayoutInflater.from(getActivity()).inflate(R.layout.signin_dialog, null);
                                d.setView(vvv);
                                d1 = d.create();
                                final EditText etEmail = (EditText) vvv.findViewById(R.id.etEmail);
                                final EditText etPwd = (EditText) vvv.findViewById(R.id.etPwd);
                                TextView txtSignin = (TextView) vvv.findViewById(R.id.txtSignin);
                                TextView txtCancel = (TextView) vvv.findViewById(R.id.txtCancel);
                                TextView txtForgotPwd = (TextView) vvv.findViewById(R.id.txtForgotPwd);
                                CheckBox chkshowPwd = (CheckBox) vvv.findViewById(R.id.chkshowPwd);
                                etEmail.setText(DataHolder.getInstance().email);
                                txtForgotPwd.setVisibility(View.GONE);
                                chkshowPwd.setVisibility(View.GONE);
                                txtSignin.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put("email", etEmail.getText().toString().trim());
                                        map.put("password", etPwd.getText().toString().trim());
                                        Log.e("map values", map.toString());
                                        new CallService(FutureTimeBooking.this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
                                    }
                                });
                                txtCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        d1.dismiss();
                                    }
                                });
                                d1.show();
                            }
                        });
                        builder.show();
                    }
                     /*   DataHolder.getInstance().email=object.getString("email");
                        JSONObject restaurant_info = object.getJSONObject("restaurant_info");
                        JSONObject Restaurant = restaurant_info.getJSONObject("Restaurant");
                        StringBuilder ssb=new StringBuilder();
                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
//                                Log.e("with login", DataHolder.getInstance().id+DataHolder.getInstance().mobile+DataHolder.getInstance().address);
                        DataHolder.getInstance().address = ssb.toString();
                        DataHolder.getInstance().desc = Restaurant.getString("description");
//                        DataHolder.getInstance().curLng = getArguments().getString("curlong");
//                        DataHolder.getInstance().curLati = getArguments().getString("curlat");
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(new
                                android.support.v7.internal.view.ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment fragment1=new WithoutSignIn();
                                DataHolder.getInstance().name=txtRestName.getText().toString();
                                DataHolder.getInstance().date=dt;
                                DataHolder.getInstance().time=tvtime.getText().toString();
                                DataHolder.getInstance().img=getArguments().getString("img");
                                DataHolder.getInstance().id=getArguments().getString("rest_id");
                                DataHolder.getInstance().noOfPeople=tvPeople.getText().toString();
                                DataHolder.getInstance().tableTimeId = DataHolder.getInstance().slotId;
                                DataHolder.getInstance().specReq = etSpecialReq.getText().toString().trim();
                                Log.e("name",DataHolder.getInstance().name+"\n");
                                Log.e("dte",DataHolder.getInstance().date+"\n");
                                Log.e("tym",DataHolder.getInstance().time+"\n");
                                Log.e("img",DataHolder.getInstance().img+"\n");
                                Log.e("people",DataHolder.getInstance().noOfPeople+"\n");
                                Log.e("tableTimeId",DataHolder.getInstance().tableTimeId+"\n");
                                Log.e("curLng",DataHolder.getInstance().curLng+"\n");
                                Log.e("curLati",DataHolder.getInstance().curLati+"\n");

                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutSignIn.class.getSimpleName());
                            }
                        });
                        builder.show();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);

                        d1.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMobileNo:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getArguments().getString("mobile_number")));
                startActivity(intent);
                break;
            case R.id.btnReserveHotel:
                if (etFname.getText().toString().trim().length()==0) {
                    etFname.setError("Please enter first name");
                    etFname.requestFocus();
                } else if (etLname.getText().toString().trim().length()==0) {
                    etLname.setError("Please enter last name");
                    etLname.requestFocus();
                } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter valid email");
                    etEmail.requestFocus();
                } else if (etPhone.getText().toString().length() != 10) {
                    etPhone.setError("Please enter 10 digits Phone Number");
                    etPhone.requestFocus();
                } else {
                    etFname.setError(null);
                    etLname.setError(null);
                    etEmail.setError(null);
                    etPhone.setError(null);
                    llayout.setVisibility(View.GONE);
                    booking();
                }

                break;
        }
    }

}
