package com.reservationng.Restaurants;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.PrevRestAdapter;
import com.reservationng.models.ReservationRestaurants;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PrevRestaurantResrv extends Fragment implements ServiceCallback {

    private ListView mRecyclerView;
    private ArrayList<ReservationRestaurants> list = new ArrayList<ReservationRestaurants>();
    private PrevRestAdapter adapter;
    private TextView tvNoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.listview_res, container, false);
        getActivity().setTitle("Restaurants");
        mRecyclerView = (ListView) v.findViewById(R.id.mRecyclerView);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        myReservations();
        return v;
    }

    public void myReservations() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "2");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKING_RESTAURANT, map).execute(Constants.VIEW_BOOKING_RESTAURANT);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_VIEW_BOOKING_RESTAURANT:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReservationRestaurants model = new ReservationRestaurants();
                            JSONObject RestaurantBooking = info.getJSONObject(i).getJSONObject("RestaurantBooking");
                            model.setBookingId(RestaurantBooking.getString("id"));
                            model.setTimings(RestaurantBooking.getString("time").substring(0, 5));
                            model.setDate(RestaurantBooking.getString("date"));
                            model.setNoOfPeople(RestaurantBooking.getString("no_of_person"));
                            JSONObject Restaurant = info.getJSONObject(i).getJSONObject("Restaurant");
                            model.setResid(RestaurantBooking.getString("restaurant_id"));
                            model.setImg(Restaurant.getString("image1"));
                            model.setName(Restaurant.getString("rest_name"));
                            model.setRewardPt(Restaurant.getString("reward_point"));

                            JSONArray RestaurantReview = info.getJSONObject(i).getJSONArray("RestaurantReview");
                            if (RestaurantReview.length() > 0) {
                                for (int j = 0; j < RestaurantReview.length(); j++) {
                                    model.setNoiseLvl(RestaurantReview.getJSONObject(j).getString("noise_level"));
                                    model.setReview(RestaurantReview.getJSONObject(j).getString("review"));
                                    model.setRating(RestaurantReview.getJSONObject(j).getString("overall_rating"));
                                    model.setFood(RestaurantReview.getJSONObject(j).getString("food_rating"));
                                    model.setService(RestaurantReview.getJSONObject(j).getString("service_rating"));
                                    model.setAmbience(RestaurantReview.getJSONObject(j).getString("ambience_rating"));
                                    model.setFlag(true);  //view a review
                                }
                            } else {
                                model.setFlag(false);  //write a review
                            }
                            list.add(model);
                        }
                        adapter = new PrevRestAdapter(getActivity(), list);
                        mRecyclerView.setAdapter(adapter);
                    } else if (code == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("No data Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }
}
