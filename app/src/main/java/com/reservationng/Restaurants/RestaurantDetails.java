package com.reservationng.Restaurants;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RestaurantDetails extends Fragment implements ServiceCallback {
    TabLayout tabLayout;
    private boolean flag;
    ViewPager viewPager;
    public static Menu menu;
    private LinearLayout llayout;
    // Bundle bundle;
    Fragment fragment;
    Fragment fragment1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(DataHolder.getInstance().name);
        View v = inflater.inflate(R.layout.restaurant_details, container, false);
        tabLayout = (TabLayout) v.findViewById(R.id.tab1);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager1);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setHasOptionsMenu(true);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        llayout.setVisibility(View.VISIBLE);
        fragment = new ReserveScreen();

            Bundle bundle = new Bundle();
            bundle.putString("id", DataHolder.getInstance().id);
            bundle.putString("date", DataHolder.getInstance().date);
            bundle.putString("time", DataHolder.getInstance().time);
            bundle.putString("curlong", DataHolder.getInstance().curLng);
            bundle.putString("curlat", DataHolder.getInstance().curLati);
            bundle.putString("resName", DataHolder.getInstance().name);
            bundle.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
            bundle.putString("img", DataHolder.getInstance().img);
            fragment.setArguments(bundle);
            Log.e("bundle in rest det srch", bundle.toString());
        fragment1 = new ReviewScreen();
        Bundle bundle1 = new Bundle();
        bundle1.putString("id", DataHolder.getInstance().id);
        fragment1.setArguments(bundle1);
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(fragment, "Reserve");
        adapter.addFrag(fragment1, "Reviews");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_ADD_FAVOURITE:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 2) {
                        flag = true;
                        CommonUtils.showToast(getActivity(), "Added to Favorites.");
                        menu.getItem(0).setIcon(R.drawable.favorite_fill);
                    } else if (code == 1) {
                        CommonUtils.showToast(getActivity(), "successfully unfavorite.");
                        menu.getItem(0).setIcon(R.drawable.fav);
                    } else {
                        CommonUtils.showToast(getActivity(), "Something went wrong.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        inflater.inflate(R.menu.rest_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_to_fav:
                if (User.getInstance() != null) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("rest_id", DataHolder.getInstance().id);
                    map.put("user_id", User.getInstance().getUserId());
                    Log.e("map values", map.toString());
                    new CallService(this, getActivity(), Constants.REQ_ADD_FAVOURITE, map).execute(Constants.ADD_FAVOURITE);
                } else {
                    CommonUtils.showDialog(getActivity(), "Please sign in first to favourite a restaurant.");
                }
                break;
            case R.id.sharing:
                try {
                    Uri bmpUri = CommonUtils.getLocalBitmapUri(ReserveScreen.img);
                    if (bmpUri != null) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, DataHolder.getInstance().name);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, DataHolder.getInstance().name);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.setType("image/*");
                        startActivity(Intent.createChooser(shareIntent, "Share Image"));
                    } else {
                        CommonUtils.showToast(getActivity(), "Sharing failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            /*case R.id.makeCall:
                Intent i = new Intent(Intent.ACTION_CALL);
                Log.e("mobile no in rest detl",ReserveScreen.mobile);
                i.setData(Uri.parse("tel:" + ReserveScreen.mobile));
                startActivity(i);

                break;*/

        }
        return super.onOptionsItemSelected(item);
    }
}
