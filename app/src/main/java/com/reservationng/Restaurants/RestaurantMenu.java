package com.reservationng.Restaurants;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.RestMenuAdapter;
import com.reservationng.models.RestMenuCategory;
import com.reservationng.models.RestMenuItems;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RestaurantMenu extends Fragment implements ServiceCallback {

    private ExpandableListView listView;
    private RestMenuAdapter restMenuAdapter;
    private List<RestMenuCategory> listHeader;
    private TextView txtNoData;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.restaurant_menu, container, false);
        getActivity().setTitle("Menu");
        listView = (ExpandableListView) v.findViewById(R.id.expanded_listmenu);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        listHeader = new ArrayList<RestMenuCategory>();

        menuDetails();
        return v;
    }

    private void menuDetails() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("restaurant_id", getArguments().getString("restaurant_id"));
       // map.put("restaurant_id","10");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_GET_MENU, map).execute(Constants.RESTAURANT_GET_MENU);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {

            case Constants.REQ_RESTAURANT_GET_MENU:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    String message = object.getString("message");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        listHeader.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            RestMenuCategory model = new RestMenuCategory();
                            JSONObject RestaurantCategory = info.getJSONObject(i).getJSONObject("RestaurantCategory");
                            model.setCatId(RestaurantCategory.getString("id"));
                            model.setCatName(RestaurantCategory.getString("category_name"));
                            JSONArray MenuItem = info.getJSONObject(i).getJSONArray("MenuItem");
                            RestMenuItems[] restMenuItems = new RestMenuItems[MenuItem.length()];
                            for (int j = 0; j < MenuItem.length(); j++) {
                                JSONObject jsonObject = MenuItem.getJSONObject(j);
                                RestMenuItems items = new RestMenuItems();
                                items.setMenuItemId(jsonObject.getString("id"));
                                items.setMenuItemName(jsonObject.getString("item_name"));
                                items.setMenuItemCom(jsonObject.getString("comment"));
                                items.setMenuItemPrice(jsonObject.getString("price"));
                                restMenuItems[j] = items;
                            }
                            model.setMenuItems(restMenuItems);
                            listHeader.add(model);
                        }
                        restMenuAdapter = new RestMenuAdapter(getActivity(), listHeader);
                        listView.setAdapter(restMenuAdapter);
                        Log.e("listheader size", "" + listHeader.size() + listHeader.get(0).getCatName());
                    } else if (code == 0) {
                        llayout.setVisibility(View.VISIBLE);
                        Log.e("list size", "" + listHeader.size());
                        txtNoData.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        txtNoData.setText("Menu coming soon");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
    }
}
