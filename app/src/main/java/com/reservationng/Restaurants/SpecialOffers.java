package com.reservationng.Restaurants;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.adapters.RestaurantOffersAdapter;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestOffers;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpecialOffers extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView;
    private RestaurantOffersAdapter adapter;
    private TextView txtNoData;
    private List<RestOffers> list;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rest_offers, container, false);
        getActivity().setTitle("Special Offers");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        list=new ArrayList<RestOffers>();
        restOffers();
        return v;
    }

    private void restOffers() {
        HashMap<String, String> map = new HashMap<String, String>();
          map.put("restaurant_id", getArguments().getString("restaurant_id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_GET_OFFERS, map).execute(Constants.RESTAURANT_GET_OFFERS);

    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_RESTAURANT_GET_OFFERS:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    String message = object.getString("message");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        list.clear();
                        JSONArray info=object.getJSONArray("info");
                        for (int i = 0; i <info.length() ; i++) {
                            RestOffers restOffers=new RestOffers();
                            JSONObject RestaurantOffer=info.getJSONObject(i).getJSONObject("RestaurantOffer");
                            restOffers.setOfferId(RestaurantOffer.getString("id"));
                            restOffers.setOfferName(RestaurantOffer.getString("offer_name"));
                            restOffers.setOfferDesc(RestaurantOffer.getString("offer_description"));
                            list.add(restOffers);
                        }
                        adapter = new RestaurantOffersAdapter(getActivity(),list);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new RestaurantOffersAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Fragment fragment = new SpecialOffersDetails();
                                Bundle bundle = new Bundle();
                                bundle.putString("id", list.get(position).getOfferId());
                                bundle.putString("name", list.get(position).getOfferName());
                                bundle.putString("desc", list.get(position).getOfferDesc());
                                fragment.setArguments(bundle);
                                Log.e("bundle ofr ", "" + bundle.toString()+ DataHolder.getInstance().curLati+" "+
                                        DataHolder.getInstance().curLng);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, SpecialOffersDetails.class.getSimpleName());
                            }
                        });
                        Log.e("list size",""+list.size());
                    }else if(code==0){
                        llayout.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No Offers Available");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
