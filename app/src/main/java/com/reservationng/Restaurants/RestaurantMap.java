package com.reservationng.Restaurants;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CommonUtils;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.models.RestaurantLists;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class RestaurantMap extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private Double myLat, myLng;
    static ArrayList list = new ArrayList();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.restaurant_map, container, false);

//        Toast.makeText(getActivity(),"on cre view",Toast.LENGTH_LONG).show();
//        Log.e("arraylist", "" + Restaurants.restaurantLists.size());
        return view;
    }

    private void initMap() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == resultCode) {
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
//            setUpMap();
        }
    }

    private void setUpMap() {
        try {
            showMarkers();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showMarkers() {
        map.clear();
        for (RestaurantLists restaurantList : Restaurants.restaurantLists) {
            Log.e("res lat n lng", restaurantList.getResLati() + ".." + restaurantList.getResLng() + " name" + restaurantList.getResName());
            myLat = Double.valueOf(restaurantList.getResLati());
            myLng = Double.valueOf(restaurantList.getResLng());
            map.addMarker(new MarkerOptions()
                    .position(
                            new LatLng(Double.valueOf(restaurantList.getResLati()), Double
                                    .valueOf(restaurantList.getResLng())))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker))
                    .title(restaurantList.getResName())
                    .snippet(restaurantList.getResCityName() + ", " + restaurantList.getResStateName()));
        }
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLat, myLng), 10.0f));
        //map.setOnMarkerClickListener(this);
        //map.setOnInfoWindowClickListener(this);
    }


    @Override
    public void onMapLoaded() {
        setUpMap();
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //send res id,date for part res details
        LatLng pos;
        String pos1="";
        Log.e("pos", marker.getPosition().toString());
        StringTokenizer stringTokenizer=new StringTokenizer( marker.getPosition().toString()," ");
        while (stringTokenizer.hasMoreTokens())
        {
            pos1=stringTokenizer.nextToken();
            Log.e("pos1", pos1);
        }
        for (int i = 0; i < Restaurants.restaurantLists.size(); i++) {
            String latLng="("+Restaurants.restaurantLists.get(i).getResLati()+","+Restaurants.restaurantLists.get(i).getResLng()+")";
            if(pos1.equals(latLng)){
                Log.e("res lat n lng", Restaurants.restaurantLists.get(i).getResId() + ".." + Restaurants.restaurantLists.get(i).getResDate() + " name" + Restaurants.restaurantLists.get(i).getResName());
                Fragment fragment=new RestaurantDetails();
                Bundle bundle=new Bundle();
                bundle.putString("id", Restaurants.restaurantLists.get(i).getResId());
                bundle.putString("date", Restaurants.restaurantLists.get(i).getResDate());
                bundle.putString("resName", Restaurants.restaurantLists.get(i).getResName());
                bundle.putString("curlong",getArguments().getString("curlong"));
                bundle.putString("curlat",getArguments().getString("curlat"));
                fragment.setArguments(bundle);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantDetails.class.getSimpleName());
            }

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        initMap();

    }


}

