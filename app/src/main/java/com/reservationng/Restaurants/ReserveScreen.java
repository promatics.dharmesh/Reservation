package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CommonUtils;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.Dashboard;
import com.reservationng.FullscreenImage;
import com.reservationng.R;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestaurantLists;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReserveScreen extends Fragment implements ServiceCallback, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener, View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private TextView txtRestName, txtDesc, tvTimings, txtAddress, tvpriceRange, tvReviews, tvCuisine,
            tvRestMenu, tvSpecOfferes, tvNoSlots, txtRestAddress, tvfutureTimes;

    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private String dLati = "", dLng = "";
    private ImageView img1, img2, img3, img4;
    private AQuery aQuery;
    private View v;
    private RestaurantLists model;
    private int Favorite = 0;
    public static ImageView img;
    private LinearLayout llayout, llimage, linearSlots;
    private ArrayList<String> list = new ArrayList<String>();
    private RatingBar ratingbar;
    public static String mobile;
    private CircleImageView ivRestImg;
    private TextView tvcuisines, tvphoneNumber, tvdining, tvstylewebsite, tvPriceRange, tvadditional, tvpaymentoptions,tvOpening;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.restaurant_reserve, container, false);
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            txtDesc = (TextView) v.findViewById(R.id.txtDesc);
            txtAddress = (TextView) v.findViewById(R.id.txtAddress);
            tvRestMenu = (TextView) v.findViewById(R.id.tvRestMenu);
            tvSpecOfferes = (TextView) v.findViewById(R.id.tvSpecOfferes);
            txtRestAddress = (TextView) v.findViewById(R.id.txtRestAddress);
            tvfutureTimes = (TextView) v.findViewById(R.id.tvfutureTimes);
            tvTimings = (TextView) v.findViewById(R.id.tvTimings);
            tvpriceRange = (TextView) v.findViewById(R.id.tvpriceRange);
            tvReviews = (TextView) v.findViewById(R.id.tvReviews);
            tvOpening = (TextView) v.findViewById(R.id.tvOpening);
            tvNoSlots = (TextView) v.findViewById(R.id.tvNoSlots);
            tvCuisine = (TextView) v.findViewById(R.id.tvCuisine);
            linearSlots = (LinearLayout) v.findViewById(R.id.linearSlots);
            llayout = (LinearLayout) v.findViewById(R.id.llayout);
            llimage = (LinearLayout) v.findViewById(R.id.llimage);
            ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
            img = (ImageView) v.findViewById(R.id.img);
            img1 = (ImageView) v.findViewById(R.id.img1);
            img2 = (ImageView) v.findViewById(R.id.img2);
            img3 = (ImageView) v.findViewById(R.id.img3);
            img4 = (ImageView) v.findViewById(R.id.img4);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            tvRestMenu.setOnClickListener(this);
            tvSpecOfferes.setOnClickListener(this);
            tvfutureTimes.setOnClickListener(this);

            tvcuisines = (TextView) v.findViewById(R.id.tvcuisines);
            tvphoneNumber = (TextView) v.findViewById(R.id.tvphoneNumber);
            tvdining = (TextView) v.findViewById(R.id.tvdining);
            tvstylewebsite = (TextView) v.findViewById(R.id.tvstylewebsite);
            tvPriceRange = (TextView) v.findViewById(R.id.tvPriceRange);
            tvadditional = (TextView) v.findViewById(R.id.tvadditional);
            tvpaymentoptions = (TextView) v.findViewById(R.id.tvpaymentoptions);

            String date = getArguments().getString("date");
            tvTimings.setText("Table for " + getArguments().getString("noOfPeople") + " people" + ", " + CommonUtils.source2.format(CommonUtils.target.parse(date)) + " at " + getArguments().getString("time"));
            aQuery = new AQuery(getActivity());
            aQuery.id(img).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));

//          TODO on 16 march make changes on this class for favrite
            DataHolder.getInstance().curLng=getArguments().getString("curlong");
            DataHolder.getInstance().curLati=getArguments().getString("curlat");

            setHasOptionsMenu(true);
            initMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }


    private void initMap() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == resultCode) {
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }

    private void restaurantDetails() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", getArguments().getString("id"));
        map.put("date", getArguments().getString("date"));
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            final Date dateObj = sdf.parse(getArguments().getString("time"));
            map.put("time", new SimpleDateFormat("H:mm").format(dateObj));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        if (User.getInstance() != null) {
            map.put("member_id", User.getInstance().getUserId());
        }
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_DETAIL, map).execute(Constants.RESTAURANT_DETAIL);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_RESTAURANT_DETAIL:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
                        mobile = "";

                        llayout.setVisibility(View.VISIBLE);
                        TimeSlots[] slots;
                        JSONObject info = object.getJSONObject("info");
                        JSONObject ratings = object.getJSONObject("ratings");

                        Favorite = info.getInt("Favorite");
                        model = new RestaurantLists();
                        JSONObject restaurant = info.getJSONObject("Restaurant");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        JSONObject Cuisine = info.getJSONObject("Cuisine");
                        model.setResDesc(restaurant.getString("description"));
                        model.setResName(restaurant.getString("rest_name"));
                        model.setResMobileNo(restaurant.getString("mobile_number"));
                        model.setRewardPt(restaurant.getString("reward_point"));
                        DataHolder.getInstance().rewardPt=model.getRewardPt();
                        if(restaurant.getString("opening_hours").equalsIgnoreCase("")) {
                            model.setOpeningHours(restaurant.getString("opening_hours"));
                        }else {
                            model.setOpeningHours("N/A");
                        }

                        if (!restaurant.getString("website").equalsIgnoreCase("")) {
                            model.setResWebSite(restaurant.getString("website"));
                        } else {
                            model.setResWebSite("N/A");
                        }
                        if (!restaurant.getString("additional").equalsIgnoreCase("")) {
                            model.setSetResAdditional(restaurant.getString("additional"));
                        } else {
                            model.setSetResAdditional("N/A");
                        }
                        model.setPriceRange(restaurant.getString("price_range"));

                        if (restaurant.getString("payment_options").equalsIgnoreCase("1")) {
                            model.setResPaymentOptions("Cash");
                        } else if (restaurant.getString("payment_options").equalsIgnoreCase("2")) {
                            model.setResPaymentOptions("Card");
                        } else if (restaurant.getString("payment_options").equalsIgnoreCase("3")) {
                            model.setResPaymentOptions("Cash and Card");
                        } else if (restaurant.getString("payment_options").equalsIgnoreCase("0")) {
                            model.setResPaymentOptions("N/A");
                        }
                        if (!restaurant.getString("phone_number").equalsIgnoreCase("")) {
                            model.setResPhoneno(restaurant.getString("phone_number"));
                        } else {
                            model.setResPhoneno("N/A");
                        }

                        if (restaurant.getString("dining_style").equalsIgnoreCase("1")) {
                            model.setResDining("Fine Dining");
                        } else if (restaurant.getString("dining_style").equalsIgnoreCase("2")) {
                            model.setResDining("Casual Dining");
                        } else if (restaurant.getString("dining_style").equalsIgnoreCase("3")) {
                            model.setResDining("Family Style Dining");
                        } else if (restaurant.getString("dining_style").equalsIgnoreCase("4")) {
                            model.setResDining("Buffet");
                        } else if (restaurant.getString("dining_style").equalsIgnoreCase("5")) {
                            model.setResDining("Fast Casual");
                        } else if (restaurant.getString("dining_style").equalsIgnoreCase("0")) {
                            model.setResDining("N/A");
                        }
                        if (!Cuisine.isNull("cuisine_name")) {
                            model.setResCuisine(Cuisine.getString("cuisine_name"));
                        } else {
                            model.setResCuisine("N/A");
                        }
                        if (!ratings.isNull("overall")) {
                            model.setResOverallRating(ratings.getString("overall"));
                        } else {
                            model.setResOverallRating("0");
                        }
                        tvCuisine.setText(model.getResCuisine());
                        tvcuisines.setText(Html.fromHtml("<b><font color='black'>Cuisines :</font></b>"));
                        tvcuisines.append("\n" + model.getResCuisine());
                        tvphoneNumber.setText(Html.fromHtml("<b><font color='black'>Phone Number :</font></b>"));
                        tvphoneNumber.append("\n" + model.getResMobileNo());
                        tvadditional.setText(Html.fromHtml("<b><font color='black'>Additional :</font></b>"));
                        tvadditional.append("\n" + model.getSetResAdditional());
                        tvstylewebsite.setText(Html.fromHtml("<b><font color='black'>Website :</font></b>"));
                        tvstylewebsite.append("\n" + model.getResWebSite());
                        tvdining.setText(Html.fromHtml("<b><font color='black'>Dining :</font></b>"));
                        tvdining.append("\n" + model.getResDining());
                        tvpaymentoptions.setText(Html.fromHtml("<b><font color='black'>Payment Options :</font></b>"));
                        tvpaymentoptions.append("\n" + model.getResPaymentOptions());

                        tvOpening.setText(Html.fromHtml("<b><font color='black'>Opening Hours :</font></b>"));
                        tvOpening.append("\n" + model.getOpeningHours());


                        if (model.getPriceRange().equalsIgnoreCase("1")) {
                            tvPriceRange.setText(Html.fromHtml("<b><font color='black'>Price Range :</font></b>"));
                            tvPriceRange.append("\n₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("2")) {
                            tvPriceRange.setText(Html.fromHtml("<b><font color='black'>Price Range :</font></b>"));
                            tvPriceRange.append("\n₦₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("3")) {
                            tvPriceRange.setText(Html.fromHtml("<b><font color='black'>Price Range :</font></b>"));
                            tvPriceRange.append("\n₦₦₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("4")) {
                            tvPriceRange.setText(Html.fromHtml("<b><font color='black'>Price Range :</font></b>"));
                            tvPriceRange.append("\n₦₦₦₦");
                        }

                        ratingbar.setRating(Float.parseFloat(model.getResOverallRating()));
                        LayerDrawable layerDrawable = (LayerDrawable) ratingbar.getProgressDrawable();
                        layerDrawable.getDrawable(2).setColorFilter(getActivity().getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                        if (!ratings.isNull("total_no_of_reviews")) {
                            model.setResTotReviews(ratings.getString("total_no_of_reviews"));
                        } else {
                            model.setResTotReviews("0");
                        }
                        model.setResFavStaus(String.valueOf(Favorite));
                        if (model.getResDesc().equalsIgnoreCase("")) {
                            txtDesc.setText("N/A");
                        } else {
                            txtDesc.setText(model.getResDesc());
                        }
                        if (model.getPriceRange().equalsIgnoreCase("1")) {
                            tvReviews.setText(model.getResTotReviews() + " reviews" + "  " + "₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("2")) {
                            tvReviews.setText(model.getResTotReviews() + " reviews" + "  " + "₦₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("3")) {
                            tvReviews.setText(model.getResTotReviews() + " reviews" + "  " + "₦₦₦");
                        } else if (model.getPriceRange().equalsIgnoreCase("4")) {
                            tvReviews.setText(model.getResTotReviews() + " reviews" + "  " + "₦₦₦₦");
                        } else {
                            tvpriceRange.setText("");
                        }

                        StringBuilder ssb = new StringBuilder();
                        StringBuilder ssb1 = new StringBuilder();
                        if (!restaurant.getString("address1").equalsIgnoreCase("null") || !restaurant.getString("address1").isEmpty()) {
                            ssb.append(restaurant.getString("address1"));
                            ssb1.append(restaurant.getString("address1"));
                        }
                        if (!restaurant.getString("address2").equalsIgnoreCase("null") || !restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0 || ssb1.length() > 0) {
                                ssb.append(", " + restaurant.getString("address2"));
                                ssb1.append(", " + restaurant.getString("address2"));
                            } else {
                                ssb.append(restaurant.getString("address2"));
                                ssb1.append(restaurant.getString("address2"));
                            }
                        }
                        if (!City.getString("city_name").equalsIgnoreCase("null") || !City.getString("city_name").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + City.getString("city_name"));
                            } else {
                                ssb.append(City.getString("city_name"));
                            }

                        }
                        if (!State.getString("state_name").equalsIgnoreCase("null") || !State.getString("state_name").isEmpty()) {

                            if (ssb.length() > 0) {
                                ssb.append(", " + State.getString("state_name"));
                            } else {
                                ssb.append(State.getString("state_name"));
                            }
                        }
                        txtAddress.setText(ssb.toString());
                        txtRestAddress.setText(ssb1.toString());
                        txtRestName.setText(model.getResName());
                        if(restaurant.isNull("lat")){
                            dLati = "0";
                        }else{
                            dLati = restaurant.getString("lat");
                        }
                        if(restaurant.isNull("long")){
                            dLng = "0";
                        }else{
                            dLng = restaurant.getString("long");
                        }
                        DataHolder.getInstance().destLati=dLati;
                        DataHolder.getInstance().destLng=dLng;
                        Log.e("fav value", model.getResFavStaus() + "///" + Favorite);
                        if (Favorite == 1) {
                            RestaurantDetails.menu.getItem(0).setIcon(R.drawable.favorite_fill);
                        } else {
                            RestaurantDetails.menu.getItem(0).setIcon(R.drawable.fav);
                        }
                        aQuery.id(img1).image(Constants.BASE_REST_IMG + restaurant.getString("image1"));
                        aQuery.id(img2).image(Constants.BASE_REST_IMG + restaurant.getString("image2"));
                        aQuery.id(img3).image(Constants.BASE_REST_IMG + restaurant.getString("image3"));
                        aQuery.id(img4).image(Constants.BASE_REST_IMG + restaurant.getString("image4"));

                        if (!restaurant.getString("image1").equalsIgnoreCase("")) {
                            img1.setVisibility(View.VISIBLE);
                            list.add(restaurant.getString("image1"));
                            img1.setOnClickListener(this);
                        } else {
                            img1.setVisibility(View.GONE);
                        }
                        if (!restaurant.getString("image2").equalsIgnoreCase("")) {
                            img2.setVisibility(View.VISIBLE);
                            img2.setOnClickListener(this);
                            list.add(restaurant.getString("image2"));
                        } else {
                            img2.setVisibility(View.GONE);
                        }
                        if (!restaurant.getString("image3").equalsIgnoreCase("")) {
                            img3.setVisibility(View.VISIBLE);
                            img3.setOnClickListener(this);
                            list.add(restaurant.getString("image3"));
                        } else {
                            img3.setVisibility(View.GONE);
                        }

                        if (!restaurant.getString("image4").equalsIgnoreCase("")) {
                            img4.setVisibility(View.VISIBLE);
                            img4.setOnClickListener(this);
                            list.add(restaurant.getString("image4"));
                        } else {
                            img4.setVisibility(View.GONE);
                        }
                        Log.e("list size", "" + list.size());

                        showMarkers();

                        JSONArray RestaurantTable = info.getJSONArray("RestaurantTable");
                        if (!RestaurantTable.isNull(0)) {
                            for (int k = 0; k < RestaurantTable.length(); k++) {
                                JSONArray TableTime = RestaurantTable.getJSONObject(k).getJSONArray("TableTime");
                                if (!TableTime.isNull(0)) {
                                    tvNoSlots.setVisibility(View.GONE);
                                    linearSlots.setVisibility(View.VISIBLE);
                                    slots = new TimeSlots[TableTime.length()];
                                    for (int l = 0; l < TableTime.length(); l++) {
                                        JSONObject ob = TableTime.getJSONObject(l);
                                        TimeSlots slot = new TimeSlots();
                                        slot.setRestTableId(ob.getString("restaurant_table_id"));
                                        slot.setTiming(ob.getString("timing"));
                                        slot.setSlotId(ob.getString("id"));
                                        slots[l] = slot;
                                        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        buttonLayoutParams.setMargins(5,5,10,5);

                                        final Button btn = new Button(getActivity());
                                        final TimeSlots slot1 = slots[l];
                                        btn.setLayoutParams(buttonLayoutParams);
                                        String time = slot1.getTiming().substring(0, 5);
                                        try {
                                            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                            final Date dateObj = sdf.parse(time);
                                            btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                                        } catch (final Exception e) {
                                            e.printStackTrace();
                                        }
                                        btn.setTextColor(Color.WHITE);
                                        btn.setBackgroundResource(R.drawable.slots_bg);
                                        btn.setPadding(5,0,5,0);
                                        btn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                WithoutSignIn.SpecGuestFlag=false;
                                                SpecialOffersDetails.SprcOfferGuestFlag=false;

                                                //TODO chnges on 23 april
                                                WithoutSignIn.ResvGuestFlag=false;
                                                if(User.getInstance()!=null) {
                                                    Fragment fragment = new RestaurantBookScreen();
                                                    Bundle bundle = new Bundle();
                                                    bundle.putString("rest_id", DataHolder.getInstance().id);
                                                    bundle.putString("rest_name", model.getResName());
                                                    bundle.putString("rest_desc", model.getResDesc());
                                                    bundle.putString("date", DataHolder.getInstance().date);
                                                    DataHolder.getInstance().slotId = slot1.getSlotId();
                                                    DataHolder.getInstance().slottiming = btn.getText().toString();
                                                    bundle.putString("slotId", DataHolder.getInstance().slotId);
                                                    bundle.putString("slotTiming", DataHolder.getInstance().slottiming);
                                                    bundle.putString("restTableId", slot1.getRestTableId());
                                                    bundle.putString("mobile_number", model.getResMobileNo());
                                                    bundle.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
                                                    bundle.putString("img", getArguments().getString("img"));
                                                    bundle.putString("reward_point", model.getRewardPt());
                                                    bundle.putString("curlat", getArguments().getString("curlat"));
                                                    bundle.putString("curlong", getArguments().getString("curlong"));
                                                    fragment.setArguments(bundle);
                                                    Log.e("rest book screen", bundle.toString());
                                                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantBookScreen.class.getSimpleName());
                                                }else{

                                                    DataHolder.getInstance().mobile = model.getResMobileNo();
                                                    DataHolder.getInstance().address = txtAddress.getText().toString().trim();
                                                    DataHolder.getInstance().desc = model.getResDesc();
                                                    DataHolder.getInstance().curLng = getArguments().getString("curlong");
                                                    DataHolder.getInstance().curLati = getArguments().getString("curlat");

                                                            Fragment fragment1=new WithoutSignIn();
                                                            DataHolder.getInstance().name=model.getResName();
//                                                            DataHolder.getInstance().date=dt;
//                                                            DataHolder.getInstance().slotId
                                                            DataHolder.getInstance().time=btn.getText().toString();
                                                            DataHolder.getInstance().img=getArguments().getString("img");
                                                            DataHolder.getInstance().id=DataHolder.getInstance().id;
//                                                            DataHolder.getInstance().noOfPeople=tvPeople.getText().toString();
                                                            DataHolder.getInstance().tableTimeId = slot1.getSlotId() ;
//                                                            DataHolder.getInstance().specReq = etSpecialReq.getText().toString().trim();
                                                            Log.e("name",DataHolder.getInstance().name+"\n");
                                                            Log.e("dte",DataHolder.getInstance().date+"\n");
                                                            Log.e("tym",DataHolder.getInstance().time+"\n");
                                                            Log.e("img",DataHolder.getInstance().img+"\n");
                                                            Log.e("people",DataHolder.getInstance().noOfPeople+"\n");
                                                            Log.e("tableTimeId",DataHolder.getInstance().tableTimeId+"\n");
                                                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutSignIn.class.getSimpleName());
                                                        }
                                                }
                                        });
                                        linearSlots.addView(btn);
                                    }
                                } else {
                                    tvNoSlots.setVisibility(View.VISIBLE);
                                    linearSlots.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            tvNoSlots.setVisibility(View.VISIBLE);
                            linearSlots.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);
        }
    }

    private void showMarkers() {
//        map.clear();
//        Log.e("curlat", getArguments().getString("curlat"));
//        Log.e("curlng", getArguments().getString("curlong"));
        LatLng latLng = new LatLng(Double.parseDouble(DataHolder.getInstance().curLati), Double.parseDouble(DataHolder.getInstance().curLng));
        LatLng dlatLng = new LatLng(Double.parseDouble(dLati), Double.parseDouble(dLng));
        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 13.0f));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 13.0f));
    }

    @Override
    public void onMapLoaded() {
//        showMarkers();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new
                ContextThemeWrapper(getActivity(), android.R.style.
                Theme_Holo_Light));
        builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
        builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMap();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("On resume","on resume");
        restaurantDetails();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvRestMenu:
                Fragment fragment = new RestaurantMenu();
                Bundle bundle = new Bundle();
                bundle.putString("restaurant_id", getArguments().getString("id"));
                fragment.setArguments(bundle);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantMenu.class.getSimpleName());
                break;
            case R.id.tvSpecOfferes:
                Fragment fragment1 = new SpecialOffers();
                Bundle bundle1 = new Bundle();
                bundle1.putString("restaurant_id", getArguments().getString("id"));
                DataHolder.getInstance().curLati=getArguments().getString("curlat");
                DataHolder.getInstance().curLng=getArguments().getString("curlong");
                fragment1.setArguments(bundle1);
                Log.e("offers..",bundle1.toString());
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, SpecialOffers.class.getSimpleName());
                break;

            case R.id.tvfutureTimes:
                Fragment fragment2 = new FutureTimes();
                Bundle bundle2 = new Bundle();
                bundle2.putString("restaurant_id", DataHolder.getInstance().id);
                bundle2.putString("rest_name", txtRestName.getText().toString());
                bundle2.putString("date",  DataHolder.getInstance().date); //show date of homescreen
                bundle2.putString("time", DataHolder.getInstance().time);  //show time of home screen
                bundle2.putString("address", txtAddress.getText().toString());
                bundle2.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
                bundle2.putString("rating", model.getResOverallRating());
                bundle2.putString("review", tvReviews.getText().toString());
                bundle2.putString("cuisine", model.getResCuisine());
                bundle2.putString("img", DataHolder.getInstance().img);
                bundle2.putString("desc", model.getResDesc());
                bundle2.putString("number", model.getResMobileNo());
                bundle2.putString("reward_point", model.getRewardPt());
                DataHolder.getInstance().curLati=getArguments().getString("curlat");
                DataHolder.getInstance().curLng=getArguments().getString("curlong");
                bundle2.putString("curlat", DataHolder.getInstance().curLati);
                bundle2.putString("curlong", DataHolder.getInstance().curLng);
                fragment2.setArguments(bundle2);
                Log.e("bundle future contains", bundle2.toString());
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment2, FutureTimes.class.getSimpleName());
                break;

            case R.id.img1:

                Intent i1 = new Intent(getActivity(), FullscreenImage.class);
                i1.putExtra("pos", 0);
                i1.putStringArrayListExtra("list", list);
                Log.e("list pos", "" + list.get(0));
                startActivity(i1);
                break;

            case R.id.img2:

                Intent i2 = new Intent(getActivity(), FullscreenImage.class);
                i2.putExtra("pos", 1);
                i2.putStringArrayListExtra("list", list);
                Log.e("list pos", "" + list.get(1));
                startActivity(i2);
                break;

            case R.id.img3:

                Intent i3 = new Intent(getActivity(), FullscreenImage.class);
                Log.e("list size", "" + list.size());
                i3.putExtra("pos", 2);
                i3.putStringArrayListExtra("list", list);
                startActivity(i3);
                break;

            case R.id.img4:

                Intent i4 = new Intent(getActivity(), FullscreenImage.class);
                Log.e("list size", "" + list.size());
                i4.putExtra("pos", 3);
                i4.putStringArrayListExtra("list", list);
                startActivity(i4);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.rest_menu_mobile, menu);
//        menu.findItem(R.id.makeCall).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.makeCall:
                Intent i = new Intent(Intent.ACTION_CALL);
                Log.e("mobile no in rest detl", model.getResMobileNo());
                i.setData(Uri.parse("tel:" + model.getResMobileNo()));
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equalsIgnoreCase(DataHolder.getInstance().name)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new
                    ContextThemeWrapper(getActivity(), android.R.style.
                    Theme_Holo_Light));
            builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
            builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showMap();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return false;
    }

    private void showMap() {
        try {
            Double slati = Double.valueOf(DataHolder.getInstance().curLati);
            Double slng = Double.valueOf(DataHolder.getInstance().curLng);
            Double dlati = Double.parseDouble(dLati);
            Double dlng = Double.parseDouble(dLng);

            Log.e("on map", "dest" + dlati + "..." + dlng);
            Log.e("on map", "src" + slati + "..." + slng);

            String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", slati, slng, "Current Location", dlati, dlng, txtAddress.getText().toString());
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
