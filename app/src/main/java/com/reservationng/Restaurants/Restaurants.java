package com.reservationng.Restaurants;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.LocResult;
import com.reservationng.R;
import com.reservationng.adapters.RestaurantListAdapter;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestaurantLists;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class Restaurants extends Fragment implements ServiceCallback, View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    private RecyclerView mRecyclerView;
    private RestaurantListAdapter adapter1;
    private TextView txtNoData, tvSrchbyDateTime, tvtotalRest, tvdate, tvtime;
    private EditText tvSrchbyLoc;
    public static List<RestaurantLists> restaurantLists;
    private int year, month, day;
    private String time, citystateId, date, noOfPeople, curLat, curLong, searchType, twelvehrtime, x, y, timeTvlve, timeSet, timeIs, abc;
    private boolean check = true, checkFilter = true, flag1 = false, flag2;
    private LinearLayout llayout;
    RestaurantLists model;
    public static boolean checkLocHome;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.restaurant_layout, container, false);
        getActivity().setTitle("Restaurants");
        checkLocHome=true;
        setHasOptionsMenu(true);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        tvSrchbyLoc = (EditText) v.findViewById(R.id.tvSrchbyLoc);
        tvSrchbyDateTime = (TextView) v.findViewById(R.id.tvSrchbyDateTime);
        tvtotalRest = (TextView) v.findViewById(R.id.tvtotalRest);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        restaurantLists = new ArrayList<>();
        check = getArguments().getBoolean("check");
        checkFilter = getArguments().getBoolean("checkFilter");
        Log.e("check", "" + check + checkFilter);

        tvSrchbyLoc.setOnClickListener(this);
        tvSrchbyDateTime.setOnClickListener(this);
        time = DataHolder.getInstance().time24;
        twelvehrtime = DataHolder.getInstance().time24;
        date = getArguments().getString("date");

        noOfPeople = getArguments().getString("people");
        curLong = getArguments().getString("long");
        curLat = getArguments().getString("lat");

//      TODO changes on march 17
        DataHolder.getInstance().curLati=curLat;
        DataHolder.getInstance().curLng=curLong;

//        searchType = getArguments().getString("search_type");
        try {
            String myFormat = "H:mm";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            final Date dateObj = sdf.parse(twelvehrtime);
            twelvehrtime = new SimpleDateFormat("hh:mm a").format(dateObj);
            DataHolder.getInstance().time=twelvehrtime;
            tvSrchbyDateTime.setText("Table For " + DataHolder.getInstance().noOfPeople + " people" + ", " + CommonUtils.source2.format(CommonUtils.target.parse(DataHolder.getInstance().date)) + " at " + DataHolder.getInstance().time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return v;
    }

    private void searchRestaurant() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("people", DataHolder.getInstance().noOfPeople);
        map.put("date", DataHolder.getInstance().date);
        map.put("time", time);
        if (DataHolder.getInstance().searchtype.equalsIgnoreCase("2")) {
            map.put("search_type", "2");
            map.put("long", curLong);
            map.put("lat", curLat);
            map.put("city", "0");
            map.put("distance", String.valueOf(RestaurantFilter.dis));
        } else if (DataHolder.getInstance().searchtype.equalsIgnoreCase("3")) {
            map.put("search_type", "3");
            map.put("city", DataHolder.getInstance().locId);
            map.put("long", curLong);
            map.put("lat", curLat);
            map.put("distance", "0");
        }else if(DataHolder.getInstance().searchtype.equalsIgnoreCase("4")){
            map.put("search_type", "4");
            map.put("city", "");
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        }else if(DataHolder.getInstance().searchtype.equalsIgnoreCase("5")){
            map.put("search_type", "5");
            map.put("city", DataHolder.getInstance().locId);
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        }
        else {
            map.put("search_type", "1");
            map.put("city", DataHolder.getInstance().locId);
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        }
        map.put("short_type", String.valueOf(RestaurantFilter.sort));
        if (RestaurantFilter.priceRange > 0) {
            map.put("price_range", String.valueOf(RestaurantFilter.priceRange));
        } else {
            map.put("price_range", "0");
        }
        if (RestaurantFilter.specialOffers > 0) {
            map.put("special_offer", String.valueOf(RestaurantFilter.specialOffers));
        } else {
            map.put("special_offer", "0");
        }
        if (RestaurantFilter.cuisineId > 0) {
            map.put("cuisines", String.valueOf(RestaurantFilter.cuisineId));
        } else {
            map.put("cuisines", "0");
        }
        if (RestaurantFilter.specFeaturesId > 0) {
            map.put("best_for", String.valueOf(RestaurantFilter.specFeaturesId));
        } else {
            map.put("best_for", "0");
        }
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_SEARCH_RESTAURANT, map).execute(Constants.SEARCH_RESTAURANT);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.restaurant_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_map:
                Fragment fragment = new RestaurantMap();
                Bundle bundle = new Bundle();
                bundle.putString("curlong", getArguments().getString("long"));
                bundle.putString("curlat", getArguments().getString("lat"));
                fragment.setArguments(bundle);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "Restaurants map");
                break;
            case R.id.ic_filter:
                Fragment fragment1 = new RestaurantFilter();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, RestaurantFilter.class.getSimpleName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_SEARCH_RESTAURANT:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    String message = object.getString("message");
                    if (code == 1) {
                        restaurantLists.clear();
                        Log.e("list clear >>>>", "" + restaurantLists.size());
                        llayout.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        final int disInAdapter = RestaurantFilter.dis;

                        //TODO changes on 29 Apr
                     /*   RestaurantFilter.cuisineId = 0;
                        RestaurantFilter.specFeaturesId = 0;
                        RestaurantFilter.priceRange = 0;
                        RestaurantFilter.specialOffers = 0;
                        RestaurantFilter.dis = 0;
                        RestaurantFilter.sort = 0;
                        */

                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            model= new RestaurantLists();
                            JSONObject restaurant = info.getJSONObject(j).getJSONObject("Restaurant");
                            JSONObject City = info.getJSONObject(j).getJSONObject("City");
                            JSONObject State = info.getJSONObject(j).getJSONObject("State");
                            JSONObject Cuisine = info.getJSONObject(j).getJSONObject("Cuisine");

                            JSONObject overall = info.getJSONObject(j);

                            if (overall.isNull("overall")) {
                                model.setResOverallRating("0");
                            } else {
                                model.setResOverallRating(String.valueOf(overall.getInt("overall")));
                            }

                            if (overall.isNull("total_no_of_reviews")) {
                                model.setResTotReviews("0");
                            } else {
                                model.setResTotReviews(String.valueOf(overall.getInt("total_no_of_reviews")));
                            }
                            if (disInAdapter > 0) {
                                model.setDistance(String.valueOf(disInAdapter));
                            } else {
                                model.setDistance("0");
                            }
                            model.setResId(restaurant.getString("id"));
                            model.setResName(restaurant.getString("rest_name"));
                            model.setPriceRange(restaurant.getString("price_range"));
                            model.setResCuisine(Cuisine.getString("cuisine_name"));
                            model.setResStateId(restaurant.getString("state"));
                            model.setResCityId(restaurant.getString("city"));
                            model.setResCityName(City.getString("city_name"));

                            StringBuilder ssb=new StringBuilder();
                            if (!City.getString("city_name").equalsIgnoreCase("null") || !City.getString("city_name").isEmpty()) {
                                    ssb.append(City.getString("city_name"));
                                    model.setResCityName(City.getString("city_name"));
                            }

                            if (!State.getString("state_name").equalsIgnoreCase("null") || !State.getString("state_name").isEmpty()) {
                                model.setResStateName(State.getString("state_name"));
                                if (ssb.length() > 0) {
                                    ssb.append(", " + State.getString("state_name"));
                                } else {
                                    ssb.append(State.getString("state_name"));
                                }
                            }else{
                                model.setResStateName("null");
                            }
                            if(restaurant.getString("long").equalsIgnoreCase("null") || restaurant.getString("long").isEmpty()) {
                                model.setResLng("0.0");
                            }else{
                                model.setResLng(restaurant.getString("long"));
                            }
                            if(restaurant.getString("lat").equalsIgnoreCase("null") || restaurant.getString("lat").isEmpty()) {
                                model.setResLati("0.0");
                            }else {

                                model.setResLati(restaurant.getString("lat"));
                            }
                            model.setResDate(date);
                            model.setCuisineId(Cuisine.getString("id"));

                            if (DataHolder.getInstance().searchtype.equalsIgnoreCase("2") || DataHolder.getInstance().searchtype.equalsIgnoreCase("4")) {

                                tvSrchbyLoc.setText("All Restaurants");
//                                DataHolder.getInstance().loc=tvSrchbyLoc.getText().toString();

                            } else if (DataHolder.getInstance().searchtype.equalsIgnoreCase("3") || DataHolder.getInstance().searchtype.equalsIgnoreCase("5")) {
                                tvSrchbyLoc.setText(DataHolder.getInstance().loc);
                                DataHolder.getInstance().loc=tvSrchbyLoc.getText().toString();
                            }
                            else {
                                tvSrchbyLoc.setText(model.getResCityName() + ", " + model.getResStateName());
                                DataHolder.getInstance().loc=tvSrchbyLoc.getText().toString();
                            }
                            if (restaurant.has("image1"))
                                model.setResImg(restaurant.getString("image1"));

                            JSONArray RestaurantTable = info.getJSONObject(j).getJSONArray("RestaurantTable");
                            if (!RestaurantTable.isNull(0)) {

                                for (int k = 0; k < RestaurantTable.length(); k++) {
                                    JSONArray TableTime = RestaurantTable.getJSONObject(k).getJSONArray("TableTime");
                                    if(!TableTime.isNull(0)) {
                                        model.setIsSlotsAvail("1");
                                        TimeSlots[] slots = new TimeSlots[TableTime.length()];

                                        for (int l = 0; l < TableTime.length(); l++) {
                                            JSONObject ob = TableTime.getJSONObject(l);
                                            TimeSlots slot = new TimeSlots();
                                            slot.setRestTableId(ob.getString("restaurant_table_id"));
                                            slot.setTiming(ob.getString("timing"));
                                            slot.setSlotId(ob.getString("id"));
                                            slots[l] = slot;

                                      /*  if(k==0&&l==0&&j==0){
                                                try {
                                                    Log.e("l value is",""+l);
                                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                                    final Date dateObj1 = sdf.parse(ob.getString("timing").substring(0,5));
                                                    Log.e("time>>>>",DataHolder.getInstance().time);
                                                    DataHolder.getInstance().time=new SimpleDateFormat("hh:mm a").format(dateObj1);
                                                    tvSrchbyDateTime.setText("Table For " + DataHolder.getInstance().noOfPeople + " people" + ", " + CommonUtils.source2.format(CommonUtils.target.parse(DataHolder.getInstance().date)) + " at " + DataHolder.getInstance().time);
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }
                                            }*/
                                        }
                                        model.setSlots(slots);
                                    }else{
                                        model.setIsSlotsAvail("0");
                                    }
                                }
                            }else{
                                model.setIsSlotsAvail("0");
                            }
                            restaurantLists.add(model);
                            tvtotalRest.setText(restaurantLists.size() + " Restaurants Available");
                        }
                        adapter1 = new RestaurantListAdapter(getActivity(), restaurantLists);
                        mRecyclerView.setAdapter(adapter1);
                        adapter1.setOnItemClickListener(new RestaurantListAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Log.e("position clicked", "" + position + "id " + restaurantLists.get(position).getResId());
                                Fragment fragment = new RestaurantDetails();
                                DataHolder.getInstance().id=restaurantLists.get(position).getResId();
                                DataHolder.getInstance().name=restaurantLists.get(position).getResName();
                                DataHolder.getInstance().date=date;
                                DataHolder.getInstance().time=twelvehrtime;
                                DataHolder.getInstance().curLati=getArguments().getString("lat");
                                DataHolder.getInstance().curLng=getArguments().getString("long");
                                DataHolder.getInstance().noOfPeople=noOfPeople;
                                DataHolder.getInstance().img=restaurantLists.get(position).getResImg();
                                Log.e("curlat",DataHolder.getInstance().curLati);
                                Log.e("curlng",DataHolder.getInstance().curLng);
                         ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantDetails.class.getSimpleName());
                            }
                        });

                    } else if (code == 0) {
                        Log.e("list clear >>>>",""+restaurantLists.size());

                        restaurantLists.clear();
                        llayout.setVisibility(View.VISIBLE);
                        RestaurantFilter.cuisineId = 0;
                        RestaurantFilter.specFeaturesId = 0;
                        RestaurantFilter.priceRange = 0;
                        RestaurantFilter.specialOffers = 0;
                        RestaurantFilter.dis = 0;
                        RestaurantFilter.sort = 0;

                        Log.e("list size", "" + restaurantLists.size());
                        if (DataHolder.getInstance().searchtype.equalsIgnoreCase("2")) {
                            tvSrchbyLoc.setText("All Restaurants");
                            DataHolder.getInstance().loc=tvSrchbyLoc.getText().toString();
                        }else if(DataHolder.getInstance().searchtype.equalsIgnoreCase("4")) {
                            tvSrchbyLoc.setText("All Restaurants");
                        }
                        else {
                            tvSrchbyLoc.setText(DataHolder.getInstance().loc);
                            DataHolder.getInstance().loc=tvSrchbyLoc.getText().toString();
                        }
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText(message);
                        tvtotalRest.setText("No Restaurant Available");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;

        }
    }

    private void showdialog() {
        String[] people = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.multidialog, null);
        final Spinner spPeople = (Spinner) view.findViewById(R.id.spPeople);
         tvdate = (TextView) view.findViewById(R.id.tvdate);
         tvtime = (TextView) view.findViewById(R.id.tvtime);
        final Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        final Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        spPeople.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, people));
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        try{
            tvdate.setText(CommonUtils.source2.format(CommonUtils.target.parse(DataHolder.getInstance().date)));
        }catch (Exception e){
            e.printStackTrace();
        }
            Log.e("flag1,flag2", flag1 + "..." + flag2);
        if (!flag1) {
            tvtime.setText(twelvehrtime); //from homescreen
        } else if (flag1) {
            Log.e("timesetn is ", timeSet + timeIs + abc);
            if (!flag2) {
                Log.e("timesetn is ", timeSet + timeIs + abc);
                //TODO on 29 apr for time change
                tvtime.setText(timeSet);   //comment it donot to show curtime
            } else {
                Log.e("timeset flag2 true", timeIs + abc);
                tvtime.setText(abc); //set abc here
                flag2 = false;
                timeSet = tvtime.getText().toString();
            }
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        tvdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDatePicker();
            }
        });
        tvtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customTimePicker();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    noOfPeople = String.valueOf(spPeople.getSelectedItemPosition() + 1);
                    //TODO changes 17 march
                    DataHolder.getInstance().noOfPeople=noOfPeople;
                    date = CommonUtils.target.format(CommonUtils.source2.parse(tvdate.getText().toString().trim()));
                    DataHolder.getInstance().date=date;
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
                    final Date dateObj = sdf.parse(tvtime.getText().toString());
                    time = new SimpleDateFormat("H:mm").format(dateObj);
                    DataHolder.getInstance().time24=time;
                    twelvehrtime = tvtime.getText().toString().trim();
                    DataHolder.getInstance().time=twelvehrtime;
                    Log.e("time in dialog",DataHolder.getInstance().time+" "+ DataHolder.getInstance().date);
                    tvSrchbyDateTime.setText("Table For " + DataHolder.getInstance().noOfPeople + " people" + ", " + tvdate.getText().toString().trim() + " at " + tvtime.getText().toString().trim());
                    if (tvSrchbyLoc.getText().toString().length() == 0) {
                        tvSrchbyLoc.setHint("Search by location");
                        CommonUtils.showToast(getActivity(), "Please select the location");
                    } else {
                        Log.e("on Restaurant", DataHolder.getInstance().loc + " " + DataHolder.getInstance().locId);
                        searchRestaurant();
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }


    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                 Restaurants.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void customDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                Restaurants.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSrchbyDateTime:
                RestaurantFilter.cuisineId = 0;
                RestaurantFilter.specFeaturesId = 0;
                RestaurantFilter.priceRange = 0;
                RestaurantFilter.specialOffers = 0;
                RestaurantFilter.dis = 0;
                RestaurantFilter.sort = 0;
                if (!flag1) {
                    timeIs = twelvehrtime; //from homescreen
                } else {
                    timeIs = timeSet;
                }
                showdialog();
                break;

            case R.id.tvSrchbyLoc:
                checkLocHome=false;
                ((Dashboard)getActivity()).beginTransactions(R.id.content_frame, new LocResult(), LocResult.class.getSimpleName());
                break;
        }
    }


    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minute, int second) {
        flag1 = true;
        String AM_PM = " AM";
        String mm_precede = "";
        int hourOfDay1 = 0;
        if (minute < 10) {
            mm_precede = "0";
        }
        if (hourOfDay >= 12) {
            AM_PM = " PM";
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay1 = hourOfDay - 12;
            } else {
                hourOfDay1 = 12;
            }
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            timeSet = tvtime.getText().toString();
        } else if (hourOfDay == 0) {
            hourOfDay1 = 12;
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            timeSet = tvtime.getText().toString();
        } else {
            tvtime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
            timeSet = tvtime.getText().toString();
        }
        String curDate = "", curTime = "";
        int curHour = 0, curMin = 0;
        StringTokenizer stringTokenizer = new StringTokenizer(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()), " ");
        while (stringTokenizer.hasMoreTokens()) {
            curDate = stringTokenizer.nextToken();
            curTime = stringTokenizer.nextToken();
        }
        if (tvdate.getText().toString().equalsIgnoreCase(curDate)) {
            StringTokenizer s1 = new StringTokenizer(curTime, ":");
            while (s1.hasMoreTokens()) {
                curHour = Integer.parseInt(s1.nextToken());
                curMin = Integer.parseInt(s1.nextToken());
                Log.e("a1  n a2", curHour + ".." + curMin);
            }
            if (hourOfDay < curHour) {
                CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
                //TODO on 29 apr for time change
                flag2 = true;
                tvtime.setText(timeIs);
                abc = tvtime.getText().toString();
                Log.e("in flag2", timeIs);
            } else if (hourOfDay == curHour) {
                if (minute < curMin) {
                    CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
                    //TODO on 29 apr for time change
                    flag2 = true;
                    tvtime.setText(timeIs);
                    abc = tvtime.getText().toString();
                    Log.e("in flag2", timeIs);
                }
            }
        }
        Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
        String min1="";
        time = hourOfDay + ":" + minute;
        DataHolder.getInstance().time24=time;

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        final long current = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        } else {
            x = String.valueOf(monthOfYear);
        }
        if (dayOfMonth < 10) {
            y = "0" + dayOfMonth;
        } else {
            y = String.valueOf(dayOfMonth);
        }
        if (calendar.getTimeInMillis() < current) {
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            tvdate.setText(y + "/" + x + "/" + year);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.e("on Restaurant", DataHolder.getInstance().loc + " " + DataHolder.getInstance().locId+" "+DataHolder.getInstance().searchtype);
        tvSrchbyLoc.setText(DataHolder.getInstance().loc);
        searchRestaurant();
    }
}
