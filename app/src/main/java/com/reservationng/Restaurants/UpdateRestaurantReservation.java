package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.models.DataHolder;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class UpdateRestaurantReservation extends Fragment implements View.OnClickListener, NumberPicker.OnValueChangeListener, ServiceCallback {


    private TextView txtRestName, tvMobileNo, tvAddress, tvdate, tvdte, tvtime, tvPeople, tvTick, txtNoSlot;
    private String MyDate1, day, mnth, date, noOfpeople, dt, dateToSend, timeToSend, x, showTime, slotId = "";
    private ImageView ivRestImg;
    private AQuery aQuery = new AQuery(getActivity());
    private ArrayList<String> peopleList = new ArrayList<>();
    private LinearLayout linearSlots;
    private boolean check = false, flag = false;
    private HorizontalScrollView horzscroll;
    private Button btnUpdate, btnCancel;
    private EditText etSpecialReq;
    private ScrollView sv;
    private String keyLoginHome;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.rest_future_times, container, false);
        getActivity().setTitle("You’re all set!");
        try {
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            txtNoSlot = (TextView) v.findViewById(R.id.txtNoSlot);
            tvMobileNo = (TextView) v.findViewById(R.id.tvMobileNo);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            tvdate = (TextView) v.findViewById(R.id.tvdate);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            tvTick = (TextView) v.findViewById(R.id.tvTick);
            ivRestImg = (ImageView) v.findViewById(R.id.ivRestImg);
            linearSlots = (LinearLayout) v.findViewById(R.id.linearSlots);
            horzscroll = (HorizontalScrollView) v.findViewById(R.id.horzscroll);
            btnCancel = (Button) v.findViewById(R.id.btnCancel);
            btnUpdate = (Button) v.findViewById(R.id.btnUpdate);
            etSpecialReq = (EditText) v.findViewById(R.id.etSpecialReq);
            sv = (ScrollView) v.findViewById(R.id.sv);

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dt = DataHolder.getInstance().date;
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMM d");
            MyDate1 = newDateFormat.format(MyDate);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            for (int i = 1; i <= 20; i++) {
                peopleList.add("" + i);
            }
            check = true;   //from hotdeals of restaurant
            txtRestName.setText(DataHolder.getInstance().name);
            tvMobileNo.setText(DataHolder.getInstance().mobile);
            tvAddress.setText(DataHolder.getInstance().address);
            etSpecialReq.setText(DataHolder.getInstance().specReq);
            tvPeople.setText(DataHolder.getInstance().noOfPeople);
            tvdte.setText(day + ", " + mnth + " " + date);
            showTime = DataHolder.getInstance().time;
            Log.e("shiwwwwwww",showTime);
            SimpleDateFormat dateFormat = new SimpleDateFormat("H:mm");
            Date date1 = dateFormat.parse(showTime);
            tvtime.setText(new SimpleDateFormat("hh:mm a").format(date1));
            tvtime.setText(showTime);
            if (check) {
                dateToSend = DataHolder.getInstance().date;
                timeToSend = DataHolder.getInstance().time;
                tvdate.setText("Table for " + DataHolder.getInstance().noOfPeople + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
//                tvdate.setText("Table for " + DataHolder.getInstance().noOfPeople + ", " + day + ", " + mnth + " " + date + " at " + showTime);
                DataHolder.getInstance().time=tvtime.getText().toString();
            } else {
                dateToSend = DataHolder.getInstance().date;
                timeToSend = DataHolder.getInstance().time;
                tvdate.setText("Table for " + DataHolder.getInstance().noOfPeople + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
//                tvdate.setText("Table for " + DataHolder.getInstance().noOfPeople + ", " + day + ", " + mnth + " " + date + " at " + showTime);
                DataHolder.getInstance().time=tvtime.getText().toString();
            }

            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + DataHolder.getInstance().img);
            tvdte.setOnClickListener(this);
            tvtime.setOnClickListener(this);
            tvPeople.setOnClickListener(this);
            tvTick.setOnClickListener(this);
            btnUpdate.setOnClickListener(this);
            btnCancel.setOnClickListener(this);
            tvMobileNo.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    private void timePicker() {
        final Calendar mcurrentTime = Calendar.getInstance();
        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                check = false;
                String AM_PM = " AM";
                String mm_precede = "";
                int hourOfDay1 = 0;
                if (minute < 10) {
                    mm_precede = "0";
                }
                if (hourOfDay >= 12) {
                    AM_PM = " PM";
                    if (hourOfDay >= 13 && hourOfDay < 24) {
                        hourOfDay1 = hourOfDay - 12;
                    } else {
                        hourOfDay1 = 12;
                    }
                    tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
                    tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                    DataHolder.getInstance().time=tvtime.getText().toString();
                    tvTick.setVisibility(View.VISIBLE);
                } else if (hourOfDay == 0) {
                    hourOfDay1 = 12;
                    tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
                    tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                    DataHolder.getInstance().time=tvtime.getText().toString();
                    tvTick.setVisibility(View.VISIBLE);
                } else {
                    tvtime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
                    tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                    DataHolder.getInstance().time=tvtime.getText().toString();
                    tvTick.setVisibility(View.VISIBLE);
                }
                Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
                timeToSend = hourOfDay + ":" + minute;
            }
        }, hour, minute, false);
        mTimePicker.show();
    }

    private void showDatePickerDialog() {
        final long current = System.currentTimeMillis();
        final Calendar calendarTwoWeeksInFuture = Calendar.getInstance();
        int year = calendarTwoWeeksInFuture.get(Calendar.YEAR);
        int month = calendarTwoWeeksInFuture.get(Calendar.MONTH);
        int day1 = calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                check = false;
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                if (monthOfYear < 10) {
                    monthOfYear = monthOfYear + 1;
                    x = "0" + monthOfYear;
                } else {
                    monthOfYear = monthOfYear + 1;
                    x = String.valueOf(monthOfYear);
                }
                if (calendar.getTimeInMillis() < current) {
                    showDatePickerDialog();
                    Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        String setDate = dayOfMonth + "/" + x + "/" + year;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date1 = dateFormat.parse(setDate);
                        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date2 = newDateFormat.parse(setDate);
                        dateToSend = CommonUtils.target.format(date1);
                        newDateFormat.applyPattern("EEE MMM d");
                        String MyDate1 = newDateFormat.format(date2);
                        StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                        while (tokenizer.hasMoreTokens()) {
                            day = tokenizer.nextToken();
                            mnth = tokenizer.nextToken();
                            date = tokenizer.nextToken();
                            tvdte.setText(day + ", " + mnth + " " + date);
                            tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                            DataHolder.getInstance().time=tvtime.getText().toString();
                            tvTick.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, calendarTwoWeeksInFuture.get(Calendar.YEAR), calendarTwoWeeksInFuture.get(Calendar.MONTH), calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMinDate(current);
        mDatePickerDialog.show();
    }


    public void peopleShow() {
        final Dialog d = new Dialog(getActivity());
        d.setTitle("Select People");
        d.setContentView(R.layout.number_picker);
        Button btnSet = (Button) d.findViewById(R.id.btnSet);
        Button btncancel = (Button) d.findViewById(R.id.btncancel);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(20);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check = false;
                tvPeople.setText(String.valueOf(np.getValue()));
//                tvdate.setText("Table for " +  tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + getArguments().getString("time"));
                tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString().trim());
                DataHolder.getInstance().time=tvtime.getText().toString();
                tvTick.setVisibility(View.VISIBLE);
                d.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPeople.setText(tvPeople.getText().toString());
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvMobileNo:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + DataHolder.getInstance().mobile));
                startActivity(intent);
                break;
            case R.id.tvTick:
                timeSlot();
                break;
            case R.id.tvdte:
                showDatePickerDialog();
                break;

            case R.id.tvtime:
                timePicker();
                break;

            case R.id.tvPeople:
                peopleShow();
                break;
            case R.id.btnCancel:
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", DataHolder.getInstance().bookingId);
                Log.e("map values", map.toString());
                sv.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
                new CallService(this, getActivity(), Constants.REQ_CANCEL_RESTAURANT_BOOKING, map).execute(Constants.CANCEL_RESTAURANT_BOOKING);
                break;

            case R.id.btnUpdate:
                updateBooking();
                break;
        }
    }

    private void updateBooking() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", DataHolder.getInstance().bookingId);
        map.put("date", dateToSend);
        if (slotId.equalsIgnoreCase("")) {
            map.put("table_time_id", DataHolder.getInstance().tableTimeId);
        } else {
            map.put("table_time_id", slotId);
        }
        if (tvPeople.getText().toString().trim().equalsIgnoreCase(DataHolder.getInstance().noOfPeople)) {
            map.put("no_of_person", DataHolder.getInstance().noOfPeople);
        } else {
            map.put("no_of_person", tvPeople.getText().toString().trim());
        }
        map.put("special_request", etSpecialReq.getText().toString().trim());
        Log.e("map values", map.toString());
        sv.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_MODIFY_BOOKING, map).execute(Constants.RESTAURANT_MODIFY_BOOKING);
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void timeSlot() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("rest_id", DataHolder.getInstance().id);
        map.put("date", dateToSend);
        map.put("time", timeToSend);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_TIME_SLOT, map).execute(Constants.REST_TIME_SLOT);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_REST_TIME_SLOT:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        linearSlots.removeAllViews();
                        TimeSlots[] slots;
                        horzscroll.setVisibility(View.VISIBLE);
                        txtNoSlot.setVisibility(View.GONE);
                        tvTick.setVisibility(View.GONE);
                        JSONObject info = object.getJSONObject("info");
                        JSONArray TableTime = info.getJSONArray("TableTime");
                        slots = new TimeSlots[TableTime.length()];
                        for (int l = 0; l < TableTime.length(); l++) {
                            JSONObject ob = TableTime.getJSONObject(l);
                            TimeSlots slot = new TimeSlots();
                            slot.setRestTableId(ob.getString("restaurant_table_id"));
                            slot.setTiming(ob.getString("timing"));
                            slot.setSlotId(ob.getString("id"));
                            slots[l] = slot;
                            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            buttonLayoutParams.setMargins(5, 5, 20, 5);
                            final Button btn = new Button(getActivity());
                            final TimeSlots slot1 = slots[l];
                            btn.setLayoutParams(buttonLayoutParams);
                            String time = slot1.getTiming().substring(0, 5);
                            try {
                                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                final Date dateObj = sdf.parse(time);
                                btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                                DataHolder.getInstance().time = btn.getText().toString().trim();
                            } catch (final Exception e) {
                                e.printStackTrace();
                            }
                            btn.setTextColor(Color.WHITE);
                            btn.setBackgroundResource(R.drawable.btn_reser_login);
                            btn.setPadding(5, 0, 5, 0);
                            btn.setFocusableInTouchMode(true);
                            btn.setFocusable(true);
                            btn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        btn.setBackgroundResource(R.drawable.btn_bg_orange);
                                        slotId = slot1.getSlotId();
                                        Log.e("slot id", slot1.getSlotId());
                                        tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + btn.getText().toString());
                                        DataHolder.getInstance().time=btn.getText().toString();
                                    } else {
                                        btn.setBackgroundResource(R.drawable.btn_reser_login);
                                    }
                                }
                            });
                            linearSlots.addView(btn);
                        }
                        Log.e("tme slts in rsrve ", "" + slots.length);
                    } else if (code == 0) {
                        horzscroll.setVisibility(View.GONE);
                        txtNoSlot.setVisibility(View.VISIBLE);
                        txtNoSlot.setText("No slots available");
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;

            case Constants.REQ_CANCEL_RESTAURANT_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thankyou");
                        builder.setMessage("Your Booking has been Cancelled Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sv.setVisibility(View.VISIBLE);
//                                getActivity().onBackPressed();
                                DataHolder.getInstance().date = dateToSend;
                                DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
                                Fragment fragment = new RestaurantDetails();
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_RESTAURANT_MODIFY_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thankyou");
                        builder.setMessage("Your Booking has been made Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                sv.setVisibility(View.VISIBLE);
                                DataHolder.getInstance().date = dateToSend;
//                                DataHolder.getInstance().time=timeToSend;
                                DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
                                Log.e("time",DataHolder.getInstance().time);
//                                    DataHolder.getInstance().img = getArguments().getString("image1");
//
                                    Fragment fragment = new RestaurantDetails();

                          ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
//                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,fragment).commit();

//                                } else {
//                                    getActivity().onBackPressed();
//                                }
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
