package com.reservationng.Restaurants;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.adapters.ReservationAdapter;
import com.reservationng.models.ReservationRestaurants;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class UpcomingRestaurants extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView;
    private TextView tvNoData;
    private ReservationAdapter adapter;
    private ArrayList<ReservationRestaurants> list = new ArrayList<ReservationRestaurants>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rest_list, container, false);
        getActivity().setTitle("Restaurants");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        myReservations();
        return v;
    }


    public void myReservations() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "1");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKING_RESTAURANT, map).execute(Constants.VIEW_BOOKING_RESTAURANT);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_VIEW_BOOKING_RESTAURANT:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReservationRestaurants model = new ReservationRestaurants();
                            JSONObject RestaurantBooking = info.getJSONObject(i).getJSONObject("RestaurantBooking");
                            model.setBookingId(RestaurantBooking.getString("id"));
                            model.setTimings(RestaurantBooking.getString("time").substring(0, 5));
                            model.setDate(RestaurantBooking.getString("date"));
                            model.setNoOfPeople(RestaurantBooking.getString("no_of_person"));
                            model.setSpcReq(RestaurantBooking.getString("special_request"));
                            model.setTabletimeId(RestaurantBooking.getString("table_time_id"));
                            JSONObject Restaurant = info.getJSONObject(i).getJSONObject("Restaurant");
                            model.setResid(Restaurant.getString("id"));
                            model.setImg(Restaurant.getString("image1"));
                            model.setName(Restaurant.getString("rest_name"));
                            model.setPhoneNo(Restaurant.getString("mobile_number"));
                            String add="";

                            if(Restaurant.getString("address1").equalsIgnoreCase("")){
                                add=Restaurant.getString("address1") ;
                            }else if(Restaurant.getString("address2").equalsIgnoreCase("")){
                                add= Restaurant.getString("address2");
                            }else{
                                add=Restaurant.getString("address1") + ", " + Restaurant.getString("address2");
                            }
                            if(Restaurant.getString("address1").equalsIgnoreCase("") && Restaurant.getString("address1").equalsIgnoreCase("")){
                                add="N/A";
                            }
                            model.setAddress(add);
                            model.setDesc(Restaurant.getString("description"));
                            model.setRewardPt(Restaurant.getString("reward_point"));
                            list.add(model);
                        }
                        adapter = new ReservationAdapter(getActivity(), list, this);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new ReservationAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Fragment fragment=new UpdateRestaurantReservation();
                                Bundle bundle=new Bundle();
                                bundle.putBoolean("check",true);
                                bundle.putString("table_time_id", list.get(position).getTabletimeId());
                                bundle.putString("rest_name", list.get(position).getName());
                                bundle.putString("noOfPeople", list.get(position).getNoOfPeople());
                                bundle.putString("date",list.get(position).getDate());
                                bundle.putString("image1",list.get(position).getImg());
                                bundle.putString("booking_id",list.get(position).getBookingId());
                                bundle.putString("time",list.get(position).getTimings());
                                bundle.putString("mobile_number",list.get(position).getPhoneNo());
                                bundle.putString("address",list.get(position).getAddress());
                                bundle.putString("rest_id",list.get(position).getResid());
                                bundle.putString("desc",list.get(position).getDesc());
                                bundle.putString("special_request",list.get(position).getSpcReq());
                                fragment.setArguments(bundle);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, UpdateRestaurantReservation.class.getSimpleName());
                            }
                        });
                    } else if (code == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("No data Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
