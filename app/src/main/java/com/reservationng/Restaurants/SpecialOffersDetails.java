package com.reservationng.Restaurants;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestOffers;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

public class SpecialOffersDetails extends Fragment implements View.OnClickListener,ServiceCallback, TimePickerDialog.OnTimeSetListener {

    private ImageView img;
    private CircleImageView ivRestImg;
    private TextView tvRestOwnerName,txtRestName,tvOfferName,tvOfferdesc,tvTime,tvFind;
    private HorizontalScrollView scrollview;
    private String time,curTime,sendDate,oname;
    private Spinner spPeople,spDate;
    private String[] people = {"1 people", "2 people", "3 people", "4 people", "5 people", "6 people", "7 people", "8 people", "9 people", "10 people", "11 people", "12 people", "13 people", "14 people", "15 people", "16 people", "17 people", "18 people", "19 people", "20 people", "Larger Party"};
    private ArrayList<String> dateList=new ArrayList<>();
    private AQuery aQuery=new AQuery(getActivity());
    private int year, month, day;
    private LinearLayout linearSlots,main,llayout;
    RestOffers restOffers;
    String hourday;
    StringBuilder ssb = new StringBuilder();
    public static boolean SprcOfferGuestFlag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.special_offers_details, container, false);
        getActivity().setTitle("Special Offers");

        img=(ImageView)v.findViewById(R.id.img);
        tvRestOwnerName=(TextView)v.findViewById(R.id.tvRestOwnerName);
        ivRestImg=(CircleImageView)v.findViewById(R.id.ivRestImg);
        linearSlots=(LinearLayout)v.findViewById(R.id.linearSlots);
        main=(LinearLayout)v.findViewById(R.id.main);
        llayout=(LinearLayout)v.findViewById(R.id.llayout);
        txtRestName=(TextView)v.findViewById(R.id.txtRestName);
        tvOfferName=(TextView)v.findViewById(R.id.tvOfferName);
        tvOfferdesc=(TextView)v.findViewById(R.id.tvOfferdesc);
        spPeople=(Spinner)v.findViewById(R.id.spPeople);
        spDate=(Spinner)v.findViewById(R.id.spDate);
        tvTime=(TextView)v.findViewById(R.id.tvTime);
        tvFind=(TextView)v.findViewById(R.id.tvFind);
        scrollview=(HorizontalScrollView)v.findViewById(R.id.scrollview);
        tvTime.setOnClickListener(this);
        tvFind.setOnClickListener(this);
        spPeople.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, people));
        spPeople.setSelection(1);
        Log.e("offers..",DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng+" "+DataHolder.getInstance().destLati+" "+DataHolder.getInstance().destLng);
        offerDetails();

        return v;
    }

    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                SpecialOffersDetails.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void offerDetails(){
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", getArguments().getString("id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_GET_SPECIAL_OFFERS, map).execute(Constants.REST_GET_SPECIAL_OFFERS);

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvTime:
                customTimePicker();
                break;

            case R.id.tvFind:
                findTable();
                break;
        }

    }

    private void findTable() {
        try
        {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("offer_id", getArguments().getString("id"));
        sendDate=String.valueOf(spDate.getSelectedItem());
        map.put("date", CommonUtils.target.format(CommonUtils.target1.parse(sendDate)));
        map.put("time_hours",hourday);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_GET_OFFERS_TIME_SLOTS, map).execute(Constants.REST_GET_OFFERS_TIME_SLOTS);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_REST_GET_SPECIAL_OFFERS:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    String message = object.getString("message");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        dateList.clear();
                        JSONArray info = object.getJSONArray("info");
                        JSONArray offer_dates = object.getJSONArray("offer_dates");
                        for (int j = 0; j < offer_dates.length(); j++) {
                            String date = offer_dates.getString(j);
                            dateList.add(CommonUtils.target2.format(CommonUtils.target.parse(date)));
                        }
                        spDate.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, dateList));
                        for (int i = 0; i < info.length(); i++) {
                           restOffers= new RestOffers();
                            JSONObject RestaurantOffer = info.getJSONObject(i).getJSONObject("RestaurantOffer");
                            JSONObject Restaurant = info.getJSONObject(i).getJSONObject("Restaurant");
                            JSONObject City = Restaurant.getJSONObject("City");
                            JSONObject State = Restaurant.getJSONObject("State");
                            StringBuilder ssb1 = new StringBuilder();
                            if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                                ssb.append(Restaurant.getString("address1"));
                                ssb1.append(Restaurant.getString("address1"));
                            }
                            if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                                if (ssb.length() > 0 || ssb1.length() > 0) {
                                    ssb.append(", " + Restaurant.getString("address2"));
                                    ssb1.append(", " + Restaurant.getString("address2"));
                                } else {
                                    ssb.append(Restaurant.getString("address2"));
                                    ssb1.append(Restaurant.getString("address2"));
                                }

                            }
                            if (!City.getString("city_name").equalsIgnoreCase("null") || !City.getString("city_name").isEmpty()) {
                                if (ssb.length() > 0) {
                                    ssb.append(", " + City.getString("city_name"));
                                } else {
                                    ssb.append(City.getString("city_name"));
                                }
                            }
                            if (!State.getString("state_name").equalsIgnoreCase("null") || !State.getString("state_name").isEmpty()) {

                                if (ssb.length() > 0) {
                                    ssb.append(", " + State.getString("state_name"));
                                } else {
                                    ssb.append(State.getString("state_name"));
                                }
                            }
                            restOffers.setOfferId(RestaurantOffer.getString("id"));

                            DataHolder.getInstance().offer_id=restOffers.getOfferId();
                            restOffers.setRestId(RestaurantOffer.getString("restaurant_id"));
                            restOffers.setOfferName(RestaurantOffer.getString("offer_name"));
                            DataHolder.getInstance().offerName=restOffers.getOfferName();
                            oname=RestaurantOffer.getString("offer_name");
                            restOffers.setOfferDesc(RestaurantOffer.getString("offer_description"));
                            restOffers.setRestName(Restaurant.getString("rest_name"));
                            restOffers.setRestImg(Restaurant.getString("image1"));
                            restOffers.setRestMobile(Restaurant.getString("mobile_number"));
                            restOffers.setRestDesc(Restaurant.getString("description"));


                            tvOfferName.setText(Html.fromHtml("<b>Offer Name : </b>"));
                            tvOfferName.append("\n" + restOffers.getOfferName());
                            tvOfferdesc.setText(Html.fromHtml("<b>Offer Description : </b>"));
                            tvOfferdesc.append("\n" + restOffers.getOfferDesc());
                            txtRestName.setText(restOffers.getRestName());
                            aQuery.id(img).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + restOffers.getRestImg());
                            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + restOffers.getRestImg());
                            DataHolder.getInstance().img=restOffers.getRestImg();
                            Calendar calendar = Calendar.getInstance();
                            year = calendar.get(Calendar.YEAR);
                            month = calendar.get(Calendar.MONTH);
                            day = calendar.get(Calendar.DAY_OF_MONTH);
                            String myFormat = "hh:mm a";
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                            curTime = sdf.format(calendar.getTime());
                            tvTime.setText(sdf.format(calendar.getTime()));
                            SimpleDateFormat displayFormat = new SimpleDateFormat("HH");
                            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                            Date date = parseFormat.parse(tvTime.getText().toString());
                            hourday=displayFormat.format(date);
                            Log.e("HOUR>>>>>>>>",hourday);

                            DataHolder.getInstance().time=tvTime.getText().toString();
                        }
                    } else if (code == 0) {
                    }
                    main.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            case Constants.REQ_REST_GET_OFFERS_TIME_SLOTS:

                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        linearSlots.removeAllViews();
                        scrollview.setVisibility(View.VISIBLE);
                        linearSlots.setVisibility(View.VISIBLE);
                        llayout.setVisibility(View.VISIBLE);

                        TimeSlots[] slots;
                        JSONArray info = object.getJSONArray("info");

                            for (int k = 0; k < info.length(); k++) {
                                JSONObject OfferSchedule = info.getJSONObject(k).getJSONObject("OfferSchedule");
                                 slots = new TimeSlots[info.length()];
                                    final TimeSlots slot = new TimeSlots();
                                    slot.setTiming(OfferSchedule.getString("time"));
                                    slot.setSlotId(OfferSchedule.getString("id"));
                                    LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    buttonLayoutParams.setMargins(5, 5, 20, 5);
                                    final Button btn = new Button(getActivity());
                                    btn.setLayoutParams(buttonLayoutParams);
                                    String time = slot.getTiming().substring(0, 5);
                                    try {
                                        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                        final Date dateObj = sdf.parse(time);
                                        btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                    btn.setTextColor(Color.WHITE);
                                    btn.setBackgroundResource(R.drawable.btn_reser_login);
                                    btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            WithoutSignIn.ResvGuestFlag=false;
                                            if (User.getInstance() != null) {
                                                DataHolder.getInstance().time = btn.getText().toString();
                                                Fragment fragment = new SpecialOfferConfirm();
                                                Bundle bundle = new Bundle();
                                                bundle.putString("restaurant_id", restOffers.getRestId());
                                                DataHolder.getInstance().slotId = slot.getSlotId();
                                                bundle.putString("book_time_slot_id", DataHolder.getInstance().slotId);
                                                bundle.putString("book_date", sendDate);
                                                bundle.putString("offer_id", DataHolder.getInstance().offer_id);
                                                bundle.putString("rest_name", restOffers.getRestName());
                                                bundle.putString("offer_name", oname);
                                                bundle.putString("no_of_person", String.valueOf(spPeople.getSelectedItemPosition() + 1));
                                                bundle.putString("timee", DataHolder.getInstance().time);
                                                fragment.setArguments(bundle);
                                                Log.e(" book screen offers..", bundle.toString() + DataHolder.getInstance().curLng + " " + DataHolder.getInstance().curLati + DataHolder.getInstance().img);
                                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, SpecialOfferConfirm.class.getSimpleName());
                                            }
                                        else {
                                                SprcOfferGuestFlag=true;
                                                DataHolder.getInstance().time = btn.getText().toString();
                                                DataHolder.getInstance().mobile = restOffers.getRestMobile();
                                                DataHolder.getInstance().address = ssb.toString();
                                                DataHolder.getInstance().desc = restOffers.getRestDesc();
                                                Log.e("cnf",DataHolder.getInstance().curLati+DataHolder.getInstance().curLng);

                                                        Fragment fragment1=new WithoutSignIn();
                                                        DataHolder.getInstance().name=restOffers.getRestName();
                                                        try {
                                                            DataHolder.getInstance().date = CommonUtils.target.format(CommonUtils.target1.parse(sendDate));
                                                        }catch (ParseException e){
                                                            e.printStackTrace();
                                                        }
                                                        DataHolder.getInstance().id=restOffers.getRestId();
                                                        DataHolder.getInstance().noOfPeople=String.valueOf(spPeople.getSelectedItemPosition() + 1);
                                                        DataHolder.getInstance().slotId = slot.getSlotId();
                                                        DataHolder.getInstance().tableTimeId = DataHolder.getInstance().slotId;
//                                                        DataHolder.getInstance().specReq = etSpecialReq.getText().toString().trim();
                                                        Log.e("name",DataHolder.getInstance().name+"\n");
                                                        Log.e("dte",DataHolder.getInstance().date+"\n");
                                                        Log.e("tym",DataHolder.getInstance().time+"\n");
                                                        Log.e("img",DataHolder.getInstance().img+"\n");
                                                        Log.e("people",DataHolder.getInstance().noOfPeople+"\n");
                                                        Log.e("tableTimeId",DataHolder.getInstance().tableTimeId+"\n");
                                                        Log.e("offerid",DataHolder.getInstance().offer_id+"\n");
                                                        Log.e("offername",DataHolder.getInstance().offerName+"\n");
                                                        ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutSignIn.class.getSimpleName());
                                                    }
                                        }

                                    });
                                    linearSlots.addView(btn);
                            }

                    }
                    llayout.setVisibility(View.VISIBLE);
                    main.setVisibility(View.VISIBLE);
                }catch (Exception e){
                    e.printStackTrace();
                }
                    break;
        }
        }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

        String AM_PM = " AM";

        String mm_precede = "";
        int hourOfDay1 = 0;
        if (minute < 10) {
            mm_precede = "0";
        }
        if (hourOfDay >= 12) {
            AM_PM = " PM";
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay1 = hourOfDay - 12;
            } else {
                hourOfDay1 = 12;
            }
            tvTime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time=tvTime.getText().toString();
        } else if (hourOfDay == 0) {
            hourOfDay1 = 12;
            tvTime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time=tvTime.getText().toString();
        } else {
            tvTime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time=tvTime.getText().toString();
        }
        String curDate = "", curTime = "";
        int curHour = 0, curMin = 0;
        StringTokenizer stringTokenizer = new StringTokenizer(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()), " ");
        while (stringTokenizer.hasMoreTokens()) {
            curDate = stringTokenizer.nextToken();
            curTime = stringTokenizer.nextToken();
        }
        Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
        time = hourOfDay + ":" + minute;
        hourday=String.valueOf(hourOfDay);
    }
}