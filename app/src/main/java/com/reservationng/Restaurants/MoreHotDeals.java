package com.reservationng.Restaurants;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.MoreHotDealsAdapter;
import com.reservationng.models.Offers;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

public class MoreHotDeals extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<String> list=new ArrayList<>();
    private TextView txtNoData;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.hotel_room_list,container,false);
        getActivity().setTitle("All Deals");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        llayout.setVisibility(View.VISIBLE);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                size(10).build());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Offers arr[] = (Offers[]) getArguments().getParcelableArray("array");
//        Log.e("arr lngth", "" + arr.length + arr[0].getName()+arr.length+arr[3].getName());
        mRecyclerView.setAdapter(new MoreHotDealsAdapter(getActivity(),arr));
        return v;
    }
}
