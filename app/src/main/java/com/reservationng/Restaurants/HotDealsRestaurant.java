package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.HotDealsLoc;
import com.reservationng.R;
import com.reservationng.adapters.HotDealsAdapterRestaurant;
import com.reservationng.models.DataHolder;
import com.reservationng.models.Offers;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class HotDealsRestaurant extends Fragment implements View.OnClickListener, ServiceCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private RecyclerView mRecyclerView;
    private HotDealsAdapterRestaurant adapter;
    private TextView  txtFindDeal,txtNoData;
    private GoogleApiClient googleApiClient;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public  double latitude, longitude;
    private ArrayList<RestaurantLists> offerList=new ArrayList<RestaurantLists>();
    private String time,x,y;
    private String[] people = {"1 people", "2 people", "3 people", "4 people", "5 people", "6 people", "7 people", "8 people", "9 people", "10 people", "11 people", "12 people", "13 people", "14 people", "15 people", "16 people", "17 people", "18 people", "19 people", "20 people", "Larger Party"};
    private View view;
    private LinearLayout llayout;
    RestaurantLists model;
    public static boolean dealsFlag=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            view= inflater.inflate(R.layout.hot_deals, container, false);
            getActivity().setTitle("Hot Deals");
            mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
            txtFindDeal = (TextView) view.findViewById(R.id.txtFindDeal);
            llayout = (LinearLayout) view.findViewById(R.id.llayout);
            txtNoData = (TextView) view.findViewById(R.id.txtNoData);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            txtFindDeal.setText("Location");
            txtFindDeal.setOnClickListener(this);
            if (checkPlayServices()) {
                buildGoogleApiClient();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
       hotDeals();
        return view;
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                CommonUtils.showToast(getActivity(), "This device is not supported the Google Play Services");
            }
            return false;
        }
        return true;
    }


    private void fetchCurrentLoc() {
        try {
            Location location = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                DataHolder.getInstance().curLati= String.valueOf(latitude);
                DataHolder.getInstance().curLng= String.valueOf(longitude);
                Log.e("Latitude", latitude + " Longitude" + longitude);
//               hotDeals();
            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                final String message = "Do you want open GPS setting?";
                builder.setMessage(message)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface d, int id) {
                                        startActivity(new Intent(action));
                                        d.dismiss();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface d, int id) {
                                        d.cancel();
                                    }
                                });
                builder.create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hotDeals() {

        try {
            HashMap<String, String> map = new HashMap<>();
            if(!dealsFlag) {
                map.put("type", "1");
            }else {
                map.put("type", "2");
                map.put("city_id", DataHolder.getInstance().hotDealLocId);
            }
            Log.e("map values are", map.toString()+dealsFlag);
            new CallService(this, getActivity(), Constants.REQ_RESTAURANT_HOT_DEALS, map).execute(Constants.RESTAURANT_HOT_DEALS);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                HotDealsRestaurant.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.e("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void customDatePicker(){
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                HotDealsRestaurant.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txttime:
              customTimePicker();
                break;

            case R.id.txtdate:
               customDatePicker();
                break;

            case R.id.txtFindDeal:
                ((Dashboard)getActivity()).beginTransactions(R.id.content_frame, new HotDealsLoc(), HotDealsLoc.class.getSimpleName());
//                fetchCurrentLoc();
                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_RESTAURANT_HOT_DEALS:
                try {
                    Log.e(" response is.....", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if(object.has("message")){
                        llayout.setVisibility(View.VISIBLE);
                            txtNoData.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            txtNoData.setText("No Data Found");
                    }else{
                        llayout.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }
                    if (code == 0) {
                        offerList.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            model = new RestaurantLists();
                            JSONObject restaurant = info.getJSONObject(j).getJSONObject("Restaurant");
                            JSONObject City = info.getJSONObject(j).getJSONObject("City");
                            JSONObject State = info.getJSONObject(j).getJSONObject("State");
                            JSONObject Cuisine = info.getJSONObject(j).getJSONObject("Cuisine");
                            model.setResId(restaurant.getString("id"));
                            model.setResName(restaurant.getString("rest_name"));
                            model.setResCuisine(Cuisine.getString("cuisine_name"));
                            model.setResStateId(restaurant.getString("state"));
                            model.setResCityId(restaurant.getString("city"));
                            model.setResCityName(City.getString("city_name"));
                            model.setResStateName(State.getString("state_name"));
                            model.setResLng(restaurant.getString("long"));
                            model.setResLati(restaurant.getString("lat"));
                            DataHolder.getInstance().destLati=model.getResLati();
                            DataHolder.getInstance().destLng=model.getResLng();
                            model.setResImg(restaurant.getString("image1"));
                            model.setResAddress1(restaurant.getString("address1"));
                            model.setResAddress2(restaurant.getString("address2"));
                            model.setRewardPt(restaurant.getString("reward_point"));
                            model.setResDesc(restaurant.getString("description"));
                            model.setResMobileNo(restaurant.getString("mobile_number"));

                           /* JSONArray RestaurantTable = info.getJSONObject(j).getJSONArray("RestaurantTable");
                            if (!RestaurantTable.isNull(0)) {
                                for (int k = 0; k < RestaurantTable.length(); k++) {
                                    JSONArray TableTime = RestaurantTable.getJSONObject(k).getJSONArray("TableTime");
                                    for (int l = 0; l < TableTime.length(); l++) {
                                        JSONObject ob = TableTime.getJSONObject(l);
                                        model.setHotdealsTimingId(ob.getString("id"));
                                    }
                                    Log.e("time slots ", "" + model.getHotdealsTimingId());
                                }
                            }*/

                            JSONArray RestaurantOffer=info.getJSONObject(j).getJSONArray("RestaurantOffer");
                            Offers[] offers = new Offers[RestaurantOffer.length()];
                            for (int l = 0; l <RestaurantOffer.length() ; l++) {
//                                JSONArray OfferSchedule=RestaurantOffer.getJSONObject(l).getJSONArray("OfferSchedule");
//                                Log.e("os length",""+OfferSchedule.length()+" l val"+l+"j val"+j);
//                                if(OfferSchedule.length()>0) {
                                    JSONObject object1 = RestaurantOffer.getJSONObject(l);
                                    Offers offers1 = new Offers();
                                    offers1.setId(object1.getString("id"));
                                    offers1.setName(object1.getString("offer_name"));
                                    offers[l] = offers1;
                                    Log.e("offers lngth >>.",offers1.getId()+" "+offers1.getName());
//                                }
                            }
                            model.setOffers(offers);
                            offerList.add(model);
                        }
                        Log.e("list size",""+offerList.size());
                        adapter = new HotDealsAdapterRestaurant(getActivity(),offerList);
                        mRecyclerView.setAdapter(adapter);
//                        spPeople.setSelection(1);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        fetchCurrentLoc();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("Connection failed", "" + connectionResult.getErrorCode());
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minute, int second) {

      /*  String AM_PM = " AM";
        String mm_precede = "";
        int hourOfDay1=0;
        if (minute < 10) {
            mm_precede = "0";
        }
        if (hourOfDay >= 12) {
            AM_PM = " PM";
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay1 =hourOfDay- 12;
            } else {
                hourOfDay1 = 12;
            }
            txttime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
        } else if (hourOfDay == 0) {
            hourOfDay1 = 12;
            txttime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
        }else{
            txttime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
        }
        Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
        time = hourOfDay + ":" + minute;

        String curDate = "", curTime = "";
        int curHour = 0, curMin = 0;
        StringTokenizer stringTokenizer = new StringTokenizer(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()), " ");
        while (stringTokenizer.hasMoreTokens()) {
            curDate = stringTokenizer.nextToken();
            curTime = stringTokenizer.nextToken();
        }
        if (txtdate.getText().toString().equalsIgnoreCase(curDate)) {
            Log.e("cur dt n time", txtdate.getText().toString() + curDate + ".." + curTime);
            StringTokenizer s1 = new StringTokenizer(curTime, ":");
            while (s1.hasMoreTokens()) {
                curHour = Integer.parseInt(s1.nextToken());
                curMin = Integer.parseInt(s1.nextToken());
                Log.e("a1  n a2", curHour + ".." + curMin);
            }
            if (hourOfDay < curHour) {
                customTimePicker();
                CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
            } else if (hourOfDay == curHour) {
                if (minute < curMin) {
                    customTimePicker();
                    CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
                }
            }
        }*/
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog,int year, int monthOfYear, int dayOfMonth) {
      /*  final long current = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);

        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        } else {
            x = String.valueOf(monthOfYear);
        }
        if (dayOfMonth < 10) {
            y = "0" + dayOfMonth;
        } else {
            y = String.valueOf(dayOfMonth);
        }
        if (calendar.getTimeInMillis() < current) {
            customDatePicker();
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            txtdate.setText(y + "/" + x + "/" + year);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DataHolder.getInstance().hotDealLoc.equalsIgnoreCase("")){
            txtFindDeal.setText("Location");
        }else {
            txtFindDeal.setText(DataHolder.getInstance().hotDealLoc);
        }
    }
}
