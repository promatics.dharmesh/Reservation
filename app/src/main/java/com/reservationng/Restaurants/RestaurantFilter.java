package com.reservationng.Restaurants;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.models.Cuisine;
import com.reservationng.models.SpecFeatures;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class RestaurantFilter extends Fragment implements View.OnClickListener, ServiceCallback {
    private TextView tvBestMatch, tvDistance, tvAZ, tvRating, tvAuto, txtSpecFeatures, txtSpecOffers, txtCuisines,
            txtPrice, tvDis2, tvDis5, tvDis8, tvDis15, tvDis1,tvReset,tvCancel;
    private ArrayList<String> price = new ArrayList<String>();
    private ArrayList<String> featuresName = new ArrayList<String>();
    private ArrayList<SpecFeatures> features = new ArrayList<SpecFeatures>();
    private ArrayList<String> specOffers = new ArrayList<String>();
    private ArrayList<Cuisine> cuisines = new ArrayList<Cuisine>();
    private ArrayList<String> cuisinesName = new ArrayList<String>();
    public static int specialOffers=0,cuisineId = 0, specFeaturesId = 0,priceRange=0,sort=0,dis=0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_restaurants, container, false);
        tvBestMatch = (TextView) v.findViewById(R.id.tvBestMatch);
        tvDistance = (TextView) v.findViewById(R.id.tvDistance);
        tvAZ = (TextView) v.findViewById(R.id.tvAZ);
        tvRating = (TextView) v.findViewById(R.id.tvRating);
        tvAuto = (TextView) v.findViewById(R.id.tvAuto);
        txtPrice = (TextView) v.findViewById(R.id.txtPrice);
        txtCuisines = (TextView) v.findViewById(R.id.txtCuisines);
        txtSpecOffers = (TextView) v.findViewById(R.id.txtSpecOffers);
        txtSpecFeatures = (TextView) v.findViewById(R.id.txtSpecFeatures);
        tvDis1 = (TextView) v.findViewById(R.id.tvDis1);
        tvDis2 = (TextView) v.findViewById(R.id.tvDis2);
        tvDis5 = (TextView) v.findViewById(R.id.tvDis5);
        tvDis8 = (TextView) v.findViewById(R.id.tvDis8);
        tvDis15 = (TextView) v.findViewById(R.id.tvDis15);
        tvReset = (TextView) v.findViewById(R.id.tvReset);
        tvCancel = (TextView) v.findViewById(R.id.tvCancel);

        tvBestMatch.setSelected(true);
        tvAuto.setSelected(true);
        tvBestMatch.setTextColor(Color.WHITE);
        tvAuto.setTextColor(Color.WHITE);

        txtPrice.setOnClickListener(this);
        txtCuisines.setOnClickListener(this);
        txtSpecOffers.setOnClickListener(this);
        txtSpecFeatures.setOnClickListener(this);
        tvBestMatch.setOnClickListener(this);
        tvDistance.setOnClickListener(this);
        tvAZ.setOnClickListener(this);
        tvRating.setOnClickListener(this);
        tvDis1.setOnClickListener(this);
        tvDis2.setOnClickListener(this);
        tvDis5.setOnClickListener(this);
        tvDis8.setOnClickListener(this);
        tvDis15.setOnClickListener(this);
        tvAuto.setOnClickListener(this);
        tvReset.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        cuisinesList();
        bestForList();
        price.add("₦");
        price.add("₦₦");
        price.add("₦₦₦");
        price.add("₦₦₦₦");
        specOffers.add("1000 point tables");

        cuisineId=0;
        specFeaturesId=0;
        priceRange=0;
        specialOffers=0;
        dis=0;
        sort=0;
        return v;

    }

    private void cuisinesList() {
        new CallService(this, getActivity(), Constants.REQ_CUISINES).execute(Constants.CUISINES);
    }

    private void bestForList() {
        new CallService(this, getActivity(), Constants.REQ_BEST_FOR_LIST).execute(Constants.BEST_FOR_LIST);
    }

    void Price() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Price");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vi = inflater.inflate(R.layout.listview_layout, null, false);
        dialog.setContentView(vi);
        ListView lv = (ListView) vi.findViewById(R.id.lvView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, price));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    txtPrice.setText("Prices " + "(" + price.get(0).toString() + ")");
                } else {
                    txtPrice.setText(price.get(i).toString());
                }
                priceRange= i+1;
                Log.e("Price selected", "" + price.get(i).toString());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void specFeatures() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Best For");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vi = inflater.inflate(R.layout.listview_layout, null, false);
        dialog.setContentView(vi);
        ListView lv = (ListView) vi.findViewById(R.id.lvView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, featuresName));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SpecFeatures data = features.get(i);
                txtSpecFeatures.setText(featuresName.get(i).toString());
                specFeaturesId = Integer.parseInt(data.getId());
                Log.e("id", "" + data.getId());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void specOffers() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Special Offers");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vi = inflater.inflate(R.layout.listview_layout, null, false);
        dialog.setContentView(vi);
        ListView lv = (ListView) vi.findViewById(R.id.lvView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, specOffers));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    txtSpecOffers.setText("Special Offers " + "(" + specOffers.get(0).toString() + ")");
                } else {
                    txtSpecOffers.setText(specOffers.get(i).toString());
                }
                Log.e("spec offers selected", "" + specOffers.get(i).toString());
                specialOffers=1000;
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void cuisines() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Cuisines");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vi = inflater.inflate(R.layout.listview_layout, null, false);
        dialog.setContentView(vi);
        ListView lv = (ListView) vi.findViewById(R.id.lvView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, cuisinesName));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == 0) {
//                    txtCuisines.setText("Cuisines " + "(" + cuisines.get(0).toString() + ")");
//                } else {
                Cuisine data = cuisines.get(i);
                txtCuisines.setText(cuisinesName.get(i).toString());
                cuisineId = Integer.parseInt(data.getId());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvCancel:
                cuisineId=0;
                specFeaturesId=0;
                priceRange=0;
                specialOffers=0;
                dis=0;
                sort=0;
                getActivity().onBackPressed();
                break;
            case R.id.tvReset:
                Fragment fragment=new Restaurants();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, Restaurants.class.getSimpleName());
                break;

            case R.id.txtPrice:
                Price();
                break;
            case R.id.txtCuisines:
                cuisines();
                break;
            case R.id.txtSpecOffers:
                specOffers();
                break;
            case R.id.txtSpecFeatures:
                specFeatures();
                break;
            case R.id.tvBestMatch:
                stateSelected(tvBestMatch, 0);
                break;
            case R.id.tvDistance:
                stateSelected(tvDistance, 1);
                break;
            case R.id.tvRating:
                stateSelected(tvRating, 3);
                break;
            case R.id.tvAZ:
                stateSelected(tvAZ, 2);
                break;
            case R.id.tvAuto:
                stateDisSelected(tvAuto, 0);
                break;
            case R.id.tvDis1:
                stateDisSelected(tvDis1, 1);
                break;
            case R.id.tvDis2:
                stateDisSelected(tvDis2, 2);
                break;
            case R.id.tvDis5:
                stateDisSelected(tvDis5, 5);
                break;
            case R.id.tvDis8:
                stateDisSelected(tvDis8, 8);
                break;
            case R.id.tvDis15:
                stateDisSelected(tvDis15, 15);
                break;
        }
    }

    private void stateSelected(TextView tv, int i) {
        tvBestMatch.setSelected(false);
        tvDistance.setSelected(false);
        tvAZ.setSelected(false);
        tvRating.setSelected(false);
        tvBestMatch.setTextColor(Color.BLACK);
        tvDistance.setTextColor(Color.BLACK);
        tvAZ.setTextColor(Color.BLACK);
        tvRating.setTextColor(Color.BLACK);
        tv.setSelected(true);
        tv.setTextColor(getResources().getColor(R.color.white));
        sort=i;
        Log.e("value of i", "" + i);
    }

    private void stateDisSelected(TextView tv, int i) {

        tvAuto.setSelected(false);
        tvDis1.setSelected(false);
        tvDis2.setSelected(false);
        tvDis5.setSelected(false);
        tvDis8.setSelected(false);
        tvDis15.setSelected(false);

        tvAuto.setTextColor(Color.BLACK);
        tvDis1.setTextColor(Color.BLACK);
        tvDis2.setTextColor(Color.BLACK);
        tvDis5.setTextColor(Color.BLACK);
        tvDis8.setTextColor(Color.BLACK);
        tvDis15.setTextColor(Color.BLACK);
        tv.setSelected(true);
        tv.setTextColor(getResources().getColor(R.color.white));
        dis=i;
        Log.e("value of i", "" + i);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_CUISINES:
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getInt("code") == 1) {
                        cuisines.clear();
                        cuisinesName.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            JSONObject cuisine = info.getJSONObject(i).getJSONObject("Cuisines");
                            Cuisine model = new Cuisine();
                            model.setId(cuisine.getString("id"));
                            model.setName(cuisine.getString("cuisine_name"));
                            cuisinesName.add(model.getName());
                            cuisines.add(model);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            case Constants.REQ_BEST_FOR_LIST:
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getInt("code") == 1) {
                        features.clear();
                        featuresName.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            JSONObject BestForManage = info.getJSONObject(i).getJSONObject("BestForManage");
                            SpecFeatures model = new SpecFeatures();
                            model.setId(BestForManage.getString("id"));
                            model.setName(BestForManage.getString("best_for"));
                            featuresName.add(model.getName());
                            features.add(model);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
