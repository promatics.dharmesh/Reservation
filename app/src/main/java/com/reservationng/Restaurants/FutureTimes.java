package com.reservationng.Restaurants;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.reservationng.R.color;

public class FutureTimes extends Fragment implements ServiceCallback, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private TextView txtRestName, tvNoSlots, txtRestAddress, tvReviews, tvCuisine, tvpeople, tvTime;
    private Spinner spDate;
    private ImageView img;
    private AQuery aQuery;
    private RatingBar ratingbar;
    private ArrayList<String> dateList = new ArrayList();
    private String dateToShow;
    private LinearLayout linearSlots;
    String dateToSend;
    private ScrollView sv;
    private CircleImageView ivRestImg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.restaurant_future_times, container, false);
        getActivity().setTitle("Future Times");
        try {
            sv = (ScrollView) v.findViewById(R.id.sv);
            img = (ImageView) v.findViewById(R.id.img);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            tvCuisine = (TextView) v.findViewById(R.id.tvCuisine);
            txtRestAddress = (TextView) v.findViewById(R.id.txtRestAddress);
            tvpeople = (TextView) v.findViewById(R.id.tvpeople);
            tvReviews = (TextView) v.findViewById(R.id.tvReviews);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvNoSlots = (TextView) v.findViewById(R.id.tvNoSlots);
            ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
            linearSlots = (LinearLayout) v.findViewById(R.id.linearSlots);
            spDate = (Spinner) v.findViewById(R.id.spDate);
            tvReviews.setText(getArguments().getString("review"));
            txtRestName.setText(getArguments().getString("rest_name"));
            txtRestAddress.setText(getArguments().getString("address"));
            tvCuisine.setText(getArguments().getString("cuisine"));
            try {
                tvpeople.setText("Table for " + getArguments().getString("noOfPeople") + " people");
                tvTime.setText(" at " + getArguments().getString("time"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            aQuery = new AQuery(getActivity());
            aQuery.id(img).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("img"));
            ratingbar.setRating(Float.parseFloat(getArguments().getString("rating")));
            LayerDrawable layerDrawable = (LayerDrawable) ratingbar.getProgressDrawable();
            layerDrawable.getDrawable(2).setColorFilter(getActivity().getResources().getColor(color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
            futureDates();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    private void futureDates() {
//        sv.setBackgroundColor(Color.parseColor("#ccc"));
//        sv.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("rest_id", getArguments().getString("restaurant_id"));
        map.put("date", getArguments().getString("date"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_FUTURE_DATES, map).execute(Constants.REST_FUTURE_DATES);
    }


    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {

            case Constants.REQ_REST_FUTURE_DATES:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
//                        sv.setBackgroundColor(Color.parseColor("#ccc"));
//                        sv.setVisibility(View.VISIBLE);
                        dateList.clear();
                        JSONArray dates = object.getJSONArray("dates");
                        if (!dates.isNull(0)) {
                            tvNoSlots.setVisibility(View.GONE);
                            linearSlots.setVisibility(View.VISIBLE);
                            for (int i = 0; i < dates.length(); i++) {
                                dateList.add(CommonUtils.source2.format(CommonUtils.target.parse(dates.getString(i))));
                            }
                            spDate.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, dateList));
                        } else {
                            dateList.add(CommonUtils.source2.format(CommonUtils.target.parse(getArguments().getString("date"))));
                            spDate.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, dateList));
                            linearSlots.setVisibility(View.GONE);
                            tvNoSlots.setVisibility(View.VISIBLE);
                        }
                        Log.e("datelist size", "" + dateList.size());
                        if (dateList.size() > 1) {
                            tvNoSlots.setVisibility(View.GONE);
                            linearSlots.setVisibility(View.VISIBLE);
                        } else {
                            tvNoSlots.setVisibility(View.VISIBLE);
                            linearSlots.setVisibility(View.GONE);
                        }
                        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                try {
//                                    if (spDate.getSelectedItemPosition() > 1) {
                                    sv.setVisibility(View.GONE);
                                    dateToSend = String.valueOf(spDate.getSelectedItem());
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put("rest_id", getArguments().getString("restaurant_id"));
                                    map.put("date", CommonUtils.target.format(CommonUtils.source2.parse(dateToSend)));
                                    Log.e("map values", map.toString());
                                    new CallService(FutureTimes.this, getActivity(), Constants.REQ_REST_FUTURE_DATES_TIMINGS, map).execute(Constants.REST_FUTURE_DATES_TIMINGS);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.REQ_REST_FUTURE_DATES_TIMINGS:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        sv.setVisibility(View.VISIBLE);
                        linearSlots.removeAllViews();
                        TimeSlots[] slots;
                        JSONArray dates = object.getJSONArray("dates");
                        Log.e("dates length", "" + dates.length());
                        if (!dates.isNull(0)) {
                            tvNoSlots.setVisibility(View.GONE);
                            linearSlots.setVisibility(View.VISIBLE);
                            slots = new TimeSlots[dates.length()];
                            for (int l = 0; l < dates.length(); l++) {
                                JSONObject ob = dates.getJSONObject(l).getJSONObject("TableTime");
                                TimeSlots slot = new TimeSlots();
                                slot.setTiming(ob.getString("timing"));
                                slot.setSlotId(ob.getString("id"));
                                slots[l] = slot;
                                LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                buttonLayoutParams.setMargins(5, 5, 10, 5);
                                final Button btn = new Button(getActivity());
                                final TimeSlots slot1 = slots[l];
                                btn.setLayoutParams(buttonLayoutParams);
                                String time = slot1.getTiming().substring(0, 5);
                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(time);
                                    btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                                } catch (final Exception e) {
                                    e.printStackTrace();
                                }
                                btn.setTextColor(Color.WHITE);
                                btn.setBackgroundResource(R.drawable.slots_bg);
                                btn.setPadding(5,0,5,0);
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (User.getInstance() != null) {
                                            Fragment fragment = new FutureTimeBooking();
                                            Bundle bundle = new Bundle();
                                            bundle.putString("rest_id", getArguments().getString("restaurant_id"));
                                            bundle.putString("rest_name", txtRestName.getText().toString().trim());
                                            bundle.putString("rest_desc", getArguments().getString("desc"));
                                            try {
                                                if (dateList.size() > 1) {
                                                    bundle.putString("date", CommonUtils.target.format(CommonUtils.source2.parse(dateToSend)));
                                                } else {
                                                    bundle.putString("date", getArguments().getString("date"));
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            DataHolder.getInstance().slotId = slot1.getSlotId();
                                            DataHolder.getInstance().slottiming = btn.getText().toString();
                                            bundle.putString("slotId", DataHolder.getInstance().slotId);
                                            bundle.putString("slotTiming", DataHolder.getInstance().slottiming);
                                            bundle.putString("restTableId", slot1.getRestTableId());  //null here
                                            bundle.putString("mobile_number", getArguments().getString("number"));
                                            bundle.putString("noOfPeople", getArguments().getString("noOfPeople"));
                                            bundle.putString("img", getArguments().getString("img"));
                                            bundle.putString("reward_point", getArguments().getString("reward_point"));
                                            fragment.setArguments(bundle);
                                            Log.e("bundle contains..", bundle.toString() + " " + DataHolder.getInstance().curLati + " " + DataHolder.getInstance().curLng);
                                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, FutureTimeBooking.class.getSimpleName());
                                        }else{
                                            DataHolder.getInstance().mobile =  getArguments().getString("number");
                                            DataHolder.getInstance().address = txtRestAddress.getText().toString().trim();
                                            DataHolder.getInstance().desc = getArguments().getString("desc");
                                            DataHolder.getInstance().curLng = getArguments().getString("curlong");
                                            DataHolder.getInstance().curLati = getArguments().getString("curlat");
                                            try {
                                                if (dateList.size() > 1) {
                                                    DataHolder.getInstance().date= CommonUtils.target.format(CommonUtils.source2.parse(dateToSend));
                                                } else {
                                                   DataHolder.getInstance().date= getArguments().getString("date");
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            Fragment fragment1=new WithoutSignIn();
                                            DataHolder.getInstance().name=txtRestName.getText().toString().trim();
//                                                            DataHolder.getInstance().date=dt;
//                                                    DataHolder.getInstance().slotId
                                            DataHolder.getInstance().time=btn.getText().toString();
                                            DataHolder.getInstance().img=getArguments().getString("img");
                                            DataHolder.getInstance().id=getArguments().getString("restaurant_id");
//                                                            DataHolder.getInstance().noOfPeople=tvPeople.getText().toString();
                                            DataHolder.getInstance().tableTimeId = slot1.getSlotId() ;
//                                                            DataHolder.getInstance().specReq = etSpecialReq.getText().toString().trim();
                                            Log.e("name",DataHolder.getInstance().name+"\n");
                                            Log.e("dte",DataHolder.getInstance().date+"\n");
                                            Log.e("tym",DataHolder.getInstance().time+"\n");
                                            Log.e("img",DataHolder.getInstance().img+"\n");
                                            Log.e("people",DataHolder.getInstance().noOfPeople+"\n");
                                            Log.e("tableTimeId",DataHolder.getInstance().tableTimeId+"\n");
                                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutSignIn.class.getSimpleName());








                                        }
                                    }
                                });
                                linearSlots.addView(btn);
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i2, int i3) {

    }
}