package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class FutureTimeBookingGuestUser extends Fragment implements View.OnClickListener, ServiceCallback {

    private TextView tvRestName, tvBookingDate, tvBookingTime, tvsignin;
    private EditText etFname, etLname, etEmail, etPhone, etSpecialReq;
    private Button btnconfirm, btnCancel;
    private String bookingdate, bookingTime;
    private ScrollView sv;
    private String MyDate1, dt, day, mnth, date, noOfpeople, showDate;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            v = inflater.inflate(R.layout.restaurant_guest_user, container, false);
            tvRestName = (TextView) v.findViewById(R.id.tvRestName);
            tvBookingDate = (TextView) v.findViewById(R.id.tvBookingDate);
            tvBookingTime = (TextView) v.findViewById(R.id.tvBookingTime);
            tvsignin = (TextView) v.findViewById(R.id.tvsignin);
            sv = (ScrollView) v.findViewById(R.id.sv);
            etFname = (EditText) v.findViewById(R.id.etFname);
            etLname = (EditText) v.findViewById(R.id.etLname);
            etEmail = (EditText) v.findViewById(R.id.etEmail);
            etPhone = (EditText) v.findViewById(R.id.etPhone);
            btnconfirm = (Button) v.findViewById(R.id.btnconfirm);
            btnCancel = (Button) v.findViewById(R.id.btnCancel);
            etSpecialReq = (EditText) v.findViewById(R.id.etSpecialReq);
            btnconfirm.setOnClickListener(this);
            btnCancel.setOnClickListener(this);
            tvsignin.setOnClickListener(this);
            //from spec offers also
//            if (User.getInstance() != null) {
//
//                etFname.setText(User.getInstance().getFname());
//                etLname.setText(User.getInstance().getLname());
//                etEmail.setText(User.getInstance().getEmail());
//                etPhone.setText(User.getInstance().getMobileNo());
//                etEmail.setFocusableInTouchMode(false);
//            } else {
                etSpecialReq.setVisibility(View.VISIBLE);
                 tvBookingTime.setVisibility(View.VISIBLE);
//                etEmail.setFocusableInTouchMode(true);
//            }

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dt = getArguments().getString("date");
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMM d");
            MyDate1 = newDateFormat.format(MyDate);
            Log.e("date", MyDate1 + dt);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            showDate = day + ", " + mnth + ", " + date;


//            bookingdate = getArguments().getString("showdate");
            bookingTime = getArguments().getString("slotTiming") + " for " + getArguments().getString("noOfpeople") + " people";
            tvRestName.setText(getArguments().getString("rest_name"));
            tvBookingDate.setText(showDate);
            tvBookingTime.setText(bookingTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    private void validation() {
        if (etFname.getText().toString().trim().isEmpty()) {
            etFname.setError("Please enter first name");
            etFname.requestFocus();
        } else if (etLname.getText().toString().trim().isEmpty()) {
            etLname.setError("Please enter last name");
            etLname.requestFocus();
        } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            etEmail.setError("Please enter valid email");
            etEmail.requestFocus();
        } else if (etPhone.getText().toString().length() != 11) {
            etPhone.setError("Please enter 11 digits Phone Number");
            etPhone.requestFocus();
        } else {
            etFname.setError(null);
            etLname.setError(null);
            etEmail.setError(null);
            etPhone.setError(null);
            sv.setBackgroundColor(getResources().getColor(R.color.light_grey));
            sv.setVisibility(View.GONE);
            booking();
        }
    }

    private void booking() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("restaurant_id", getArguments().getString("rest_id"));
        //map.put("date", getArguments().getString("date"));
        map.put("timing_id", getArguments().getString("slotId"));
        map.put("no_of_person", getArguments().getString("noOfpeople"));
        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("email", etEmail.getText().toString().trim());
        map.put("mobile_number", etPhone.getText().toString().trim());
        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("email", etEmail.getText().toString().trim());
        map.put("mobile_number", etPhone.getText().toString().trim());
        map.put("special_request", getArguments().getString("special_request"));
        map.put("member_id", "");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_TABLE_BOOKING, map).execute(Constants.RESTAURANT_TABLE_BOOKING);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnconfirm:
                validation();

                break;
            case R.id.btnCancel:
                getActivity().onBackPressed();
                break;
            case R.id.tvsignin:

                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_RESTAURANT_TABLE_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        sv.setVisibility(View.VISIBLE);
                        etFname.setText("");
                        etLname.setText("");
                        etEmail.setText("");
                        etPhone.setText("");
                        etSpecialReq.setText("");
                        JSONObject booking_info = object.getJSONObject("booking_info");
                        JSONObject RestaurantBooking = booking_info.getJSONObject("RestaurantBooking");
                        JSONObject Restaurant = booking_info.getJSONObject("Restaurant");
                            Fragment fragment = new UpdateRestaurantReservation();
//                            Bundle bundle = new Bundle();
//                            bundle.putString("FROM", "TOSET");
//                            bundle.putString("booking_id", RestaurantBooking.getString("id"));
//                            bundle.putString("table_time_id", RestaurantBooking.getString("table_time_id"));
//                            bundle.putString("rest_id", RestaurantBooking.getString("restaurant_id"));
//                            bundle.putString("noOfPeople", RestaurantBooking.getString("no_of_person"));
//                            bundle.putString("date", RestaurantBooking.getString("date"));
//                            bundle.putString("time", RestaurantBooking.getString("time"));
//                            bundle.putString("special_request", RestaurantBooking.getString("special_request"));

                            DataHolder.getInstance().bookingId=RestaurantBooking.getString("id");
                            DataHolder.getInstance().tableTimeId=RestaurantBooking.getString("table_time_id");
                            DataHolder.getInstance().id=RestaurantBooking.getString("restaurant_id");
                            DataHolder.getInstance().noOfPeople=RestaurantBooking.getString("no_of_person");
                            DataHolder.getInstance().date=RestaurantBooking.getString("date");
                            DataHolder.getInstance().time=getArguments().getString("slotTiming");
                            DataHolder.getInstance().specReq=RestaurantBooking.getString("special_request");

                            StringBuilder ssb = new StringBuilder();
                            if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                                ssb.append(Restaurant.getString("address1"));
                            }
                            if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                                if (ssb.length() > 0) {
                                    ssb.append(", " + Restaurant.getString("address2"));
                                } else {
                                    ssb.append(Restaurant.getString("address2"));
                                }
                            }
                            DataHolder.getInstance().name= Restaurant.getString("rest_name");
                            DataHolder.getInstance().img= Restaurant.getString("image1");
                            DataHolder.getInstance().mobile= Restaurant.getString("mobile_number");
                            DataHolder.getInstance().address= ssb.toString();
                            DataHolder.getInstance().desc= Restaurant.getString("description");
                            DataHolder.getInstance().curLng= DataHolder.getInstance().curLng;
                            DataHolder.getInstance().curLati=  DataHolder.getInstance().curLati;

                            Log.e("future without login",DataHolder.getInstance().id);
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, UpdateRestaurantReservation.class.getSimpleName());

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservaion");
                        builder.setMessage("Something went wrong");
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }
}
