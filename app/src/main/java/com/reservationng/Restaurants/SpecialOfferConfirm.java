package com.reservationng.Restaurants;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.ConfirmationPage;
import com.reservationng.Dashboard;
import com.reservationng.Login.SignInScreen;
import com.reservationng.R;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class SpecialOfferConfirm extends Fragment implements View.OnClickListener, ServiceCallback {

    private TextView tvRestName, tvBookingDate, tvBookingTime, tvsignin;
    private EditText etFname, etLname, etEmail, etPhone, etSpecialReq;
    private Button btnconfirm, btnCancel;
    private String bookingdate, bookingTime,MyDate1, dt, day, mnth, date;
    private ScrollView sv;
    RelativeLayout IsSignIn;
    public static boolean OffersBookFlag;
    Dialog d1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.restaurant_guest_user, container, false);
        getActivity().setTitle("Reservation");
        tvRestName = (TextView) v.findViewById(R.id.tvRestName);
        tvBookingDate = (TextView) v.findViewById(R.id.tvBookingDate);
        tvBookingTime = (TextView) v.findViewById(R.id.tvBookingTime);
        tvsignin = (TextView) v.findViewById(R.id.tvsignin);
        sv = (ScrollView) v.findViewById(R.id.sv);
        IsSignIn = (RelativeLayout) v.findViewById(R.id.IsSignIn);
        etFname = (EditText) v.findViewById(R.id.etFname);
        etLname = (EditText) v.findViewById(R.id.etLname);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etPhone = (EditText) v.findViewById(R.id.etPhone);
        btnconfirm = (Button) v.findViewById(R.id.btnconfirm);
        btnCancel = (Button) v.findViewById(R.id.btnCancel);
        etSpecialReq = (EditText) v.findViewById(R.id.etSpecialReq);
        btnconfirm.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        tvsignin.setOnClickListener(this);
        Log.e("offers..",DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng+" "+DataHolder.getInstance().destLati+" "+DataHolder.getInstance().destLng);

        if (User.getInstance() != null) {
            etFname.setText(User.getInstance().getFname());
            etLname.setText(User.getInstance().getLname());
            etEmail.setText(User.getInstance().getEmail());
            if(User.getInstance().getMobileNo().length()>10) {
                etPhone.setText(User.getInstance().getMobileNo().substring(3,User.getInstance().getMobileNo().length()));
            }else{
                etPhone.setText(User.getInstance().getMobileNo());
            }
            etFname.setFocusableInTouchMode(false);
            etLname.setFocusableInTouchMode(false);
            etEmail.setFocusableInTouchMode(false);
            IsSignIn.setVisibility(View.GONE);

        } else {
            etEmail.setFocusableInTouchMode(true);
            etFname.setFocusableInTouchMode(true);
            etLname.setFocusableInTouchMode(true);
            IsSignIn.setVisibility(View.VISIBLE);
            tvsignin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInScreen(), SignInScreen.class.getSimpleName());
                }
            });
        }
        etSpecialReq.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etSpecialReq.setHintTextColor(Color.WHITE);
                }else{
                    etSpecialReq.setHintTextColor(Color.BLACK);
                }
            }
        });

        bookingdate = getArguments().getString("showdate");
        bookingTime = getArguments().getString("slotTiming") + " for " + getArguments().getString("noOfpeople") + " people";
//        tvRestName.setText(getArguments().getString("rest_name")+"\n"+getArguments().getString("offer_name"));
        tvRestName.setText(getArguments().getString("offer_name"));
        try{
        Log.e("booking date", CommonUtils.target.format(CommonUtils.target2.parse(getArguments().getString("book_date"))));
          dt=  CommonUtils.target.format(CommonUtils.target2.parse(getArguments().getString("book_date")));

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            dt = getArguments().getString("date");
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMMM d");
            MyDate1 = newDateFormat.format(MyDate);
            Log.e("date", MyDate1 + dt);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            tvBookingDate.setText(day + " " + date + Html.fromHtml(" <sup><small>th</small></sup>") + " of " + mnth);
            tvBookingTime.setText(DataHolder.getInstance().time+ " for "+ getArguments().getString("no_of_person")+ " people");
        }catch (Exception e){
            e.printStackTrace();
        }
            return v;
    }

    private void validation() {
        if (etFname.getText().toString().trim().isEmpty()) {
            etFname.setError("Please enter first name");
            etFname.requestFocus();
        } else if (etLname.getText().toString().trim().isEmpty()) {
            etLname.setError("Please enter last name");
            etLname.requestFocus();
        } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            etEmail.setError("Please enter valid email");
            etEmail.requestFocus();
        } else if (etPhone.getText().toString().length() != 10) {
            etPhone.setError("Please enter 10 digits Phone Number");
            etPhone.requestFocus();
        } else {
            etFname.setError(null);
            etLname.setError(null);
            etEmail.setError(null);
            etPhone.setError(null);
            sv.setBackgroundColor(getResources().getColor(R.color.light_grey));
            sv.setVisibility(View.GONE);
            booking();
        }
    }

    private void booking() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("email", etEmail.getText().toString().trim());
        map.put("mobile_number", "234"+etPhone.getText().toString().trim());
        map.put("special_request", etSpecialReq.getText().toString().trim());
        if(User.getInstance()!=null) {
            map.put("member_id", User.getInstance().getUserId());
        }
        map.put("book_time_slot_id", getArguments().getString("book_time_slot_id"));
        map.put("book_date", dt);
        map.put("restaurant_id", getArguments().getString("restaurant_id"));
        map.put("no_of_person", getArguments().getString("no_of_person"));
        map.put("offer_id", getArguments().getString("offer_id"));
        Log.e("map values", map.toString()+" "+DataHolder.getInstance().slotId);
        new CallService(this, getActivity(), Constants.REQ_TABLE_BOOKING_OFFER, map).execute(Constants.TABLE_BOOKING_OFFER);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnconfirm:
                validation();

                break;
            case R.id.btnCancel:
                getActivity().onBackPressed();
                break;
            case R.id.tvsignin:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInScreen(), SignInScreen.class.getSimpleName());

                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_TABLE_BOOKING_OFFER:
               /* try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        sv.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thankyou");
                        builder.setMessage("Your Booking has been made successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().onBackPressed();
                            }
                        });
                        builder.show();
                    }
                    else if(code==2){
                        DataHolder.getInstance().email=object.getString("email");
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                OffersBookFlag=true;
//                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInScreen(), SignInScreen.class.getSimpleName());
                            }
                        })
                        ;
                        builder.show();
                    }

                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thankyou");
                        builder.setMessage("Something went wrong");
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                try {
                    Log.e("response booking", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        sv.setVisibility(View.VISIBLE);
                        JSONObject booking_info = object.getJSONObject("booking_info");
                        JSONObject RestaurantBooking = booking_info.getJSONObject("RestaurantBooking");
                        JSONObject Restaurant = booking_info.getJSONObject("Restaurant");
                        Fragment fragment = new ConfirmationPage();
                        DataHolder.getInstance().bookingId = RestaurantBooking.getString("id");
                        DataHolder.getInstance().tableTimeId = RestaurantBooking.getString("table_time_id");
                        DataHolder.getInstance().id = RestaurantBooking.getString("restaurant_id");
                        DataHolder.getInstance().noOfPeople = RestaurantBooking.getString("no_of_person");
                        DataHolder.getInstance().date = RestaurantBooking.getString("date");
                        DataHolder.getInstance().specReq = RestaurantBooking.getString("special_request");

                        StringBuilder ssb = new StringBuilder();

                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().name = Restaurant.getString("rest_name");
                        DataHolder.getInstance().img = Restaurant.getString("image1");
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
                        Log.e("with login", DataHolder.getInstance().id + DataHolder.getInstance().mobile + DataHolder.getInstance().address + DataHolder.getInstance().time);
                        DataHolder.getInstance().address = ssb.toString();
                        Log.e("cnf", DataHolder.getInstance().curLati + DataHolder.getInstance().curLng);
                        DataHolder.getInstance().desc = Restaurant.getString("description");
//                        DataHolder.getInstance().curLng = getArguments().getString("curlong");
//                        DataHolder.getInstance().curLati = getArguments().getString("curlat");
                        if (User.getInstance() != null) {
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ConfirmationPage.class.getSimpleName());
                        } else {
                            if (WithoutSignIn.SpecGuestFlag) {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ConfirmationPage.class.getSimpleName());
                            } else {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new WithoutSignIn(), WithoutSignIn.class.getSimpleName());
                            }
                        }
                    } else if (code == 2) {
                        sv.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
                                View vvv = LayoutInflater.from(getActivity()).inflate(R.layout.signin_dialog, null);
                                d.setView(vvv);
                                d1 = d.create();
                                final EditText etEmail = (EditText) vvv.findViewById(R.id.etEmail);
                                final EditText etPwd = (EditText) vvv.findViewById(R.id.etPwd);
                                TextView txtSignin = (TextView) vvv.findViewById(R.id.txtSignin);
                                TextView txtCancel = (TextView) vvv.findViewById(R.id.txtCancel);
                                TextView txtForgotPwd = (TextView) vvv.findViewById(R.id.txtForgotPwd);
                                CheckBox chkshowPwd = (CheckBox) vvv.findViewById(R.id.chkshowPwd);
                                etEmail.setText(DataHolder.getInstance().email);
                                txtForgotPwd.setVisibility(View.GONE);
                                chkshowPwd.setVisibility(View.GONE);
                                txtSignin.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put("email", etEmail.getText().toString().trim());
                                        map.put("password", etPwd.getText().toString().trim());
                                        Log.e("map values", map.toString());
                                        new CallService(SpecialOfferConfirm.this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
                                    }
                                });
                                txtCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        d1.dismiss();
                                    }
                                });
                                d1.show();
                            }
                        });
                        builder.show();
                    }

                      /*
                        DataHolder.getInstance().email=object.getString("email");
                        JSONObject restaurant_info = object.getJSONObject("rest_info");
                        JSONObject Restaurant = restaurant_info.getJSONObject("Restaurant");
                        StringBuilder ssb=new StringBuilder();
                        DataHolder.getInstance().img = Restaurant.getString("image1");
                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
                        DataHolder.getInstance().address = ssb.toString();
                        DataHolder.getInstance().desc = Restaurant.getString("description");
                        Log.e("cnf",DataHolder.getInstance().curLati+DataHolder.getInstance().curLng);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment fragment1=new WithoutSignIn();
                                DataHolder.getInstance().name=tvRestName.getText().toString();
                                DataHolder.getInstance().date=dt;
                                DataHolder.getInstance().id=getArguments().getString("rest_id");
                                DataHolder.getInstance().noOfPeople=getArguments().getString("no_of_person");
                                DataHolder.getInstance().tableTimeId = DataHolder.getInstance().slotId;
                                DataHolder.getInstance().specReq = etSpecialReq.getText().toString().trim();
                                Log.e("name",DataHolder.getInstance().name+"\n");
                                Log.e("dte",DataHolder.getInstance().date+"\n");
                                Log.e("tym",DataHolder.getInstance().time+"\n");
                                Log.e("img",DataHolder.getInstance().img+"\n");
                                Log.e("people",DataHolder.getInstance().noOfPeople+"\n");
                                Log.e("tableTimeId",DataHolder.getInstance().tableTimeId+"\n");
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutSignIn.class.getSimpleName());
                            }
                        });
                        builder.show();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);

                        d1.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
