package com.reservationng;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.reservationng.Hotels.HotDealsHotel;
import com.reservationng.Restaurants.HotDealsRestaurant;
import com.reservationng.utils.ServiceCallback;

import java.util.ArrayList;
import java.util.List;

public class HotDeals extends Fragment implements ServiceCallback {
    TabLayout tabLayout;
    ViewPager viewPager;
    public static Menu menu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Hot Deals");
        View v = inflater.inflate(R.layout.restaurant_details, container, false);
        tabLayout = (TabLayout) v.findViewById(R.id.tab1);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager1);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setHasOptionsMenu(true);

        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new HotDealsRestaurant(), "Restaurant");
        adapter.addFrag(new HotDealsHotel(), "Resort");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
}
