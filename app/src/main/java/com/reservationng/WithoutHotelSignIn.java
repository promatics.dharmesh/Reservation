package com.reservationng;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Hotels.HotelBooking;
import com.reservationng.Login.CreateAccount;
import com.reservationng.Login.SignInScreen;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by android2 on 4/12/16.
 */
public class WithoutHotelSignIn extends Fragment implements View.OnClickListener, ServiceCallback {


    private TextView txtRestName, tvPeople, tvdte, tvtime;
    private Button btnSignIn, btnGuest, btnCreateAcc;
    private AQuery aQuery = new AQuery(getActivity());
    private ImageView ivImg;
    private CircleImageView ivRestImg;
    public static boolean GuestFlag;
    private String MyDate1,Mydate2, chkin,chkout, day, mnth, date, day1, mnth1, date1;
    public static boolean withoutSignHotelInback = false;
    Dialog d1;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.without_hotel_signin, container, false);
        try {
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            btnSignIn = (Button) v.findViewById(R.id.btnSignIn);
            btnGuest = (Button) v.findViewById(R.id.btnGuest);
            btnCreateAcc = (Button) v.findViewById(R.id.btnCreateAcc);
            ivImg = (ImageView) v.findViewById(R.id.ivImg);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            chkin = HotelDataHolder.getInstance().checkIn;
            Date MyDate = newDateFormat.parse(chkin);
            newDateFormat.applyPattern("EEEE MMMM d");
            MyDate1 = newDateFormat.format(MyDate);
            Log.e("date1", MyDate1 + chkin);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            SimpleDateFormat newDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

            chkout = HotelDataHolder.getInstance().checkOut;
            Date dd = newDateFormat1.parse(chkout);
            newDateFormat1.applyPattern("EEEE MMMM d");
            Mydate2 = newDateFormat1.format(dd);
            Log.e("date2", Mydate2 + chkout+" "+HotelDataHolder.getInstance().checkOut);
            StringTokenizer tokenizer1 = new StringTokenizer(Mydate2, " ");
            while (tokenizer1.hasMoreTokens()) {
                day1 = tokenizer1.nextToken();
                mnth1 = tokenizer1.nextToken();
                date1 = tokenizer1.nextToken();
            }


            txtRestName.setText(DataHolder.getInstance().name);
            tvPeople.setText(HotelDataHolder.getInstance().room);
            tvdte.setText(day + ", " + mnth+ " " + date);
            tvtime.setText(day1 + ", " + mnth1+ " " + date1);
            aQuery.id(ivImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().RoomImg);
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().RoomImg);
            btnSignIn.setOnClickListener(this);
            btnGuest.setOnClickListener(this);
            btnCreateAcc.setOnClickListener(this);

            Log.e("name", HotelDataHolder.getInstance().name + "\n");
            Log.e("dte", HotelDataHolder.getInstance().checkOut + "\n");
            Log.e("tym", HotelDataHolder.getInstance().checkIn + "\n");
            Log.e("img", HotelDataHolder.getInstance().img + "\n");
            Log.e("people", HotelDataHolder.getInstance().room + "\n");


        }catch (Exception e){
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignIn:
                SignInScreen.Bookflag = true;

                final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
//                d.setTitle("Check-In");
                View vvv = LayoutInflater.from(getActivity()).inflate(R.layout.signin_dialog, null);
                d.setView(vvv);
                d1 = d.create();
                final EditText etEmail = (EditText) vvv.findViewById(R.id.etEmail);
                final EditText etPwd = (EditText) vvv.findViewById(R.id.etPwd);
                TextView txtSignin = (TextView) vvv.findViewById(R.id.txtSignin);
                TextView txtCancel = (TextView) vvv.findViewById(R.id.txtCancel);
                TextView txtForgotPwd = (TextView) vvv.findViewById(R.id.txtForgotPwd);
                CheckBox chkshowPwd = (CheckBox) vvv.findViewById(R.id.chkshowPwd);
                etEmail.setText(DataHolder.getInstance().email);
                txtForgotPwd.setVisibility(View.GONE);
                chkshowPwd.setVisibility(View.GONE);
                txtSignin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("email", etEmail.getText().toString().trim());
                        map.put("password", etPwd.getText().toString().trim());
                        Log.e("map values", map.toString());
                        new CallService(WithoutHotelSignIn.this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
                    }
                });
                txtCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d1.dismiss();
                    }
                });
                d1.show();
                break;
            case R.id.btnGuest:
                if(User.getInstance()!=null) {
                    withoutSignHotelInback = false;
                }else{
                    withoutSignHotelInback = true;
                }

                GuestFlag=true;
                Fragment fragment = new HotelBooking();
                Bundle bundle = new Bundle();
                bundle.putBoolean("check", false);
                bundle.putString("checkout_date", HotelDataHolder.getInstance().checkOut);
                bundle.putString("checkin_date", HotelDataHolder.getInstance().checkIn);
                bundle.putString("tot_price", HotelDataHolder.getInstance().price);

                bundle.putString("room_img",HotelDataHolder.getInstance().RoomImg );
                bundle.putString("room_id",  HotelDataHolder.getInstance().bookingId);
                bundle.putString("no_room", HotelDataHolder.getInstance().room);
                bundle.putString("hotel_id", HotelDataHolder.getInstance().hotelId);
                bundle.putString("room_name",  HotelDataHolder.getInstance().roomName);
                fragment.setArguments(bundle);
                Log.e("bundle contains", bundle.toString());
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelBooking.class.getSimpleName());
                break;

            case R.id.btnCreateAcc:
                CreateAccount.createFlag = true;
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new CreateAccount(), "Create Account");
                break;
        }
    }



    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);

                        d1.dismiss();

                        Fragment fragment = new HotelBooking();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("check", false);
                        bundle.putString("checkout_date", HotelDataHolder.getInstance().checkOut);
                        bundle.putString("checkin_date", HotelDataHolder.getInstance().checkIn);
                        bundle.putString("tot_price", HotelDataHolder.getInstance().price);

                        bundle.putString("room_img",HotelDataHolder.getInstance().RoomImg );
                        bundle.putString("room_id",  HotelDataHolder.getInstance().bookingId);
                        bundle.putString("no_room", HotelDataHolder.getInstance().room);
                        bundle.putString("hotel_id", HotelDataHolder.getInstance().hotelId);
                        bundle.putString("room_name",  HotelDataHolder.getInstance().roomName);
                        fragment.setArguments(bundle);
                        Log.e("bundle contains", bundle.toString());
                        ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelBooking.class.getSimpleName());


                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
        }
    }
}
