package com.reservationng;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.reservationng.adapters.FullScreenImageAdapter;

import java.util.ArrayList;

public class FullscreenImage extends Activity {

    FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_gallery);
        viewPager = (ViewPager) findViewById(R.id.pager);
        ArrayList<String> list = getIntent().getStringArrayListExtra("list");
        pos=getIntent().getExtras().getInt("pos");
        Log.e("position: ", String.valueOf(pos));
        adapter = new FullScreenImageAdapter(FullscreenImage.this, list);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos,false);

    }
}
