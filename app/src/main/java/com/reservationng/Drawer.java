package com.reservationng;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.plus.Plus;
import com.reservationng.Login.CreateAccount;
import com.reservationng.Login.GooglePlusSignIn;
import com.reservationng.Login.Login;
import com.reservationng.Login.SignInDrawer;
import com.reservationng.Login.SignInScreen;
import com.reservationng.MyProfile.MyProfile;
import com.reservationng.MyProfile.ProfileDetail;
import com.reservationng.MyProfile.PurchasedGiftVoucher;
import com.reservationng.Restaurants.RestaurantFilter;
import com.reservationng.Restaurants.SpecialOffersDetails;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.User;

public class Drawer extends Fragment implements View.OnClickListener {
    public static LinearLayout search, signin, createaccount, llhotDeals, al, bl;
    public static View view;
    public static TextView txtLogout,  txtMyDetails, txtSearch, txtHotDeal, tvPurGiftVoucher,rlDetail,review;

    //txtusername, txtemail
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_layout_after_login, container, false);
        al = (LinearLayout) view.findViewById(R.id.al);
        bl = (LinearLayout) view.findViewById(R.id.bl);
        al.setVisibility(View.GONE);
        bl.setVisibility(View.GONE);
        search = (LinearLayout) view.findViewById(R.id.search);
        signin = (LinearLayout) view.findViewById(R.id.signin);
        rlDetail = (TextView) view.findViewById(R.id.rlDetail);
        review = (TextView) view.findViewById(R.id.review);
//        txtemail = (TextView) view.findViewById(R.id.txtemail);
//        txtusername = (TextView) view.findViewById(R.id.txtusername);
        txtLogout = (TextView) view.findViewById(R.id.txtLogout);
        txtMyDetails = (TextView) view.findViewById(R.id.txtMyDetails);
        txtSearch = (TextView) view.findViewById(R.id.txtSearch);
        txtHotDeal = (TextView) view.findViewById(R.id.txtHotDeal);
        tvPurGiftVoucher = (TextView) view.findViewById(R.id.tvPurGiftVoucher);
        search = (LinearLayout) view.findViewById(R.id.search);
        signin = (LinearLayout) view.findViewById(R.id.signin);
        createaccount = (LinearLayout) view.findViewById(R.id.create);
        llhotDeals = (LinearLayout) view.findViewById(R.id.llhotDeals);

        rlDetail.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        txtMyDetails.setOnClickListener(this);
        txtHotDeal.setOnClickListener(this);
        txtSearch.setOnClickListener(this);
        tvPurGiftVoucher.setOnClickListener(this);
        createaccount.setOnClickListener(this);
        signin.setOnClickListener(this);
        search.setOnClickListener(this);
        llhotDeals.setOnClickListener(this);

        return view;
    }

    public void abc() {
        if (User.getInstance() != null) {
            al.setVisibility(View.VISIBLE);
            bl.setVisibility(View.GONE);
//            txtusername.setText(User.getInstance().getFname() + " " + User.getInstance().getLname());
//            txtemail.setText(User.getInstance().getEmail());
        } else {
            bl.setVisibility(View.VISIBLE);
            al.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvPurGiftVoucher:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new PurchasedGiftVoucher(), PurchasedGiftVoucher.class.getSimpleName());
                break;
            case R.id.txtHotDeal:
                Dashboard.Drawer.closeDrawers();
                DataHolder.getInstance().hotDealLoc="";
                DataHolder.getInstance().hotDealLocId="";
                DataHolder.getInstance().resLoc="";
                DataHolder.getInstance().resLocId="";
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotDeals1(), HotDeals1.class.getSimpleName());
                break;
            case R.id.llhotDeals:
                Dashboard.Drawer.closeDrawers();
                DataHolder.getInstance().hotDealLoc="";
                DataHolder.getInstance().hotDealLocId="";
                DataHolder.getInstance().resLoc="";
                DataHolder.getInstance().resLocId="";
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotDeals1(), HotDeals1.class.getSimpleName());
                break;
            case R.id.txtSearch:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HomeScreen(), "home screen");
                break;
            case R.id.search:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HomeScreen(), "home screen");
                break;
            case R.id.create:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Login(), Login.class.getSimpleName());
                break;
            case R.id.signin:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInDrawer(), "Sign In");
                break;
            case R.id.rlDetail:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new ProfileDetail(), "profile_details");
                break;
            case R.id.txtLogout:
                Dashboard.Drawer.closeDrawers();
                CreateAccount.createFlag=false;
                SignInScreen.Bookflag=false;
                SpecialOffersDetails.SprcOfferGuestFlag=false;
                WithoutSignIn.ResvGuestFlag=false;
                WithoutSignIn.SpecGuestFlag=false;
                WithoutSignIn.withoutSignInback=false;
                WithoutHotelSignIn.withoutSignHotelInback=false;

                RestaurantFilter.cuisineId = 0;
                RestaurantFilter.specFeaturesId = 0;
                RestaurantFilter.priceRange = 0;
                RestaurantFilter.specialOffers = 0;
                RestaurantFilter.dis = 0;
                RestaurantFilter.sort = 0;


                try {
                    if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null) {
                        Log.e("logout facebook", "you logout");
                        LoginManager.getInstance().logOut();
                        User.getInstance().logout();
                        Intent intentLog = new Intent(getActivity(), MainActivity.class);
                        intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLog);
                        Toast.makeText(getActivity(), "you have successfully logout.",
                                Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                        Log.e("Token After Logout ", "" + AccessToken.getCurrentAccessToken());
                        Log.e("Profile After Logout", "" + com.facebook.Profile.getCurrentProfile());
                    } else if (GooglePlusSignIn.mClient.isConnected()) {
                        Plus.AccountApi.clearDefaultAccount(GooglePlusSignIn.mClient);
                        GooglePlusSignIn.mClient.disconnect();
                        User.getInstance().logout();
                        Intent intentLog = new Intent(getActivity(), MainActivity.class);
                        intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLog);
                        Toast.makeText(getActivity(), "you have successfully logout",
                                Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        User.getInstance().logout();
                        Intent intentLog = new Intent(getActivity(), MainActivity.class);
                        intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLog);
                        getActivity().finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    User.getInstance().logout();
                    Intent intentLog = new Intent(getActivity(), MainActivity.class);
                    intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentLog);
                    getActivity().finish();
                }
                break;
            case R.id.txtMyDetails:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new MyProfile(), "my profile");
                break;

        }
    }
}