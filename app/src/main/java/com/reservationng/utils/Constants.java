package com.reservationng.utils;

public class Constants {
    public static final String KEY_PREF = "user_data";

    public static final String BASE = "http://reservation.ng/dev/WebServices/";

    public static final String BASE_REST_IMG = "http://reservation.ng/dev/img/front/restimage/";
    public static final String BASE_HOTEL_IMG = "http://reservation.ng/dev/img/front/hotelimage/";
    public static final String BASE_CMS = "http://reservation.ng/dev/WebServices/cms/";
    public static final String BASE_PVT_DINING = "http://reservation.ng/dev/Homes/private_dining";

    public static final String REGISTER = BASE + "register";
    public static final int REQ_REGISTER = 1;

    public static final String LOGIN = BASE + "login";
    public static final int REQ_LOGIN = 2;

    public static final String FORGOT_PASSWORD = BASE + "forgot_password";
    public static final int REQ_FORGOT_PASSWORD = 3;

    public static final String ALL_STATE = BASE + "state_list";
    public static final int REQ_ALL_STATE = 4;

    public static final String CITY_ACC_STATE = BASE + "city_according_state";
    public static final int REQ_CITY_ACC_STATE = 5;

    public static final String ALL_CITY = BASE + "city_list";
    public static final int REQ_ALL_CITY = 6;

    public static final String UPDATE_PROFILE = BASE + "update_profile";
    public static final int REQ_UPDATE_PROFILE = 7;

    public static final String CITY_KEYWORD_LIST = BASE + "city_keyword_list_revised";
    public static final int REQ_CITY_KEYWORD_LIST = 8;

    public static final String SEARCH_RESTAURANT = BASE + "search_restaurant";
    public static final int REQ_SEARCH_RESTAURANT = 9;

    public static final String RESTAURANT_DETAIL = BASE + "restaurant_detail";
    public static final int REQ_RESTAURANT_DETAIL = 10;

    public static final String RESTAURANT_GET_MENU = BASE + "get_menu";
    public static final int REQ_RESTAURANT_GET_MENU = 11;

    public static final String RESTAURANT_GET_OFFERS = BASE + "get_offers";
    public static final int REQ_RESTAURANT_GET_OFFERS = 12;

    public static final String FB_LOGIN = BASE + "facebook_login";
    public static final int REQ_FB_LOGIN = 13;

    public static final String GOOGLE_LOGIN = BASE + "google_login";
    public static final int REQ_GOOGLE_LOGIN = 14;

    public static final String RESTAURANT_TABLE_BOOKING = BASE + "table_booking";
    public static final int REQ_RESTAURANT_TABLE_BOOKING = 15;

    public static final String ADD_FAVOURITE = BASE + "add_favorite";
    public static final int REQ_ADD_FAVOURITE = 16;

    public static final String FAVOURITE_LIST = BASE + "favourite_list";
    public static final int REQ_FAVOURITE_LIST = 17;

    public static final String MY_RESTAURANT_BOOKING = BASE + "my_restaurant_bookings";
    public static final int REQ_MY_RESTAURANT_BOOKING = 18;

    public static final String SEARCH_HOTEL = BASE + "search_hotel";
    public static final int REQ_SEARCH_HOTEL = 19;

    public static final String FAVORITE_HOTEL = BASE + "hotel_favorite";
    public static final int REQ_FAVORITE_HOTEL = 20;

    public static final String HOTEL_FAVORITE_LIST = BASE + "hotel_favourite_list";
    public static final int REQ_HOTEL_FAVORITE_LIST = 21;

    public static final String HOTEL_ROOM_DETAIL = BASE + "room_detail";
    public static final int REQ_HOTEL_ROOM_DETAIL = 22;

    public static final String HOTEL_OVERVIEW = BASE + "hotel_detail";
    public static final int REQ_HOTEL_OVERVIEW = 23;

    public static final String HOTEL_FACILITIES = BASE + "hotel_facilities";
    public static final int REQ_HOTEL_FACILITIES = 24;

    public static final String HOTEL_NEAR_BY_ACTIVITIES = BASE + "hotel_nearby";
    public static final int REQ_HOTEL_NEAR_BY_ACTIVITIES = 25;

    public static final String HOTEL_QUERY = BASE + "hotel_query";
    public static final int REQ_HOTEL_QUERY = 26;

    public static final String RESTAURANT_HOT_DEALS = BASE + "deal_restaurants_revised";
    public static final int REQ_RESTAURANT_HOT_DEALS = 27;

    public static final String HOTEL_HOT_DEALS = BASE + "deal_hotels_revised";
    public static final int REQ_HOTEL_HOT_DEALS = 28;

    public static final String HOTEL_BOOKING = BASE + "hotel_booking";
    public static final int REQ_HOTEL_BOOKING = 29;

    public static final String VIEW_BOOKING_RESTAURANT = BASE + "view_bookings_rest";
    public static final int REQ_VIEW_BOOKING_RESTAURANT = 30;

    public static final String VIEW_BOOKING_HOTEL = BASE + "view_bookings_hotel";
    public static final int REQ_VIEW_BOOKING_HOTEL = 31;

    public static final String REST_TIME_SLOT = BASE + "rest_time_slot";
    public static final int REQ_REST_TIME_SLOT = 32;

    public static final String RESTAURANT_MODIFY_BOOKING = BASE + "modify_booking";
    public static final int REQ_RESTAURANT_MODIFY_BOOKING = 33;

    public static final String CANCEL_RESTAURANT_BOOKING = BASE + "cancel_rest_booking";
    public static final int REQ_CANCEL_RESTAURANT_BOOKING = 34;

    public static final String CANCEL_HOTEL_BOOKING = BASE + "cancel_hotel_booking";
    public static final int REQ_CANCEL_HOTEL_BOOKING = 35;

    public static final String HOTEL_BOOKING_MODIFY = BASE + "hotel_booking_modify";
    public static final int REQ_HOTEL_BOOKING_MODIFY = 36;

    public static final String GIVE_REVIEW_RESTAURANT = BASE + "give_review_restaurant";
    public static final int REQ_GIVE_REVIEW_RESTAURANT = 37;

    public static final String SHOW_USER_REVIEW_RESTAURANT = BASE + "show_user_review_rest";
    public static final int REQ_SHOW_USER_REVIEW_RESTAURANT = 38;

    public static final String SHOW_BOOKING_REVIEW_RESTAURANT = BASE + "show_booking_review_rest";
    public static final int REQ_SHOW_BOOKING_REVIEW_RESTAURANT = 39;

    public static final String SHOW_USER_REVIEW_HOTEL = BASE + "show_user_review_hotel";
    public static final int REQ_SHOW_USER_REVIEW_HOTEL = 40;

    public static final String GIVE_REVIEW_HOTEL = BASE + "give_review_hotel";
    public static final int REQ_GIVE_REVIEW_HOTEL = 40;

    public static final String REST_FULL_REVIEW = BASE + "restaurant_full_reviews";
    public static final int REQ_REST_FULL_REVIEW = 41;

    public static final String HOTEL_FULL_REVIEW = BASE + "hotel_full_reviews";
    public static final int REQ_HOTEL_FULL_REVIEW = 42;

    public static final String POINTS = BASE + "points";
    public static final int REQ_POINTS = 43;

    public static final String GIFT_CARD_LIST = BASE + "gift_card_list";
    public static final int REQ_GIFT_CARD_LIST = 44;

    public static final String PURCHASED_GIFT_VOUCHER = BASE + "purchased_gift_voucher";
    public static final int REQ_PURCHASED_GIFT_VOUCHER = 46;

    public static final String BUY_VOUCHER_APPS = BASE + "buy_voucher_android";
    public static final int REQ_BUY_VOUCHER_APPS = 47;

    public static final String CUISINES = BASE + "cuisines";
    public static final int REQ_CUISINES = 48;

    public static final String BEST_FOR_LIST = BASE + "best_for_list";
    public static final int REQ_BEST_FOR_LIST = 49;

    public static final String CITY_KEYWORD_LIST_HOTEL = BASE + "city_keyword_list_hotel_revised";
    public static final int REQ_CITY_KEYWORD_LIST_HOTEL = 50;

    public static final String REST_GET_SPECIAL_OFFERS = BASE + "get_offers_details";
    public static final int REQ_REST_GET_SPECIAL_OFFERS = 51;


    public static final String REST_GET_OFFERS_TIME_SLOTS = BASE + "get_offers_time_slots";
    public static final int REQ_REST_GET_OFFERS_TIME_SLOTS = 52;

    public static final String TABLE_BOOKING_OFFER = BASE + "table_booking_offer";
    public static final int REQ_TABLE_BOOKING_OFFER = 53;

    public static final String REST_FUTURE_DATES = BASE + "future_dates";
    public static final int REQ_REST_FUTURE_DATES = 54;

    public static final String REST_FUTURE_DATES_TIMINGS = BASE + "future_dates_timings";
    public static final int REQ_REST_FUTURE_DATES_TIMINGS= 55;


    public static final String VIEW_BOOKINGS_COMBINE = BASE + "view_bookings_combine";
    public static final int REQ_VIEW_BOOKINGS_COMBINE= 56;

    public static final String REVIEW_ON_LAST_BOOKING = BASE + "review_on_last_booking";
    public static final int REQ_REVIEW_ON_LAST_BOOKING= 59;

    public static final String GENERATE_OTP = BASE + "generate_otp";
    public static final int REQ_GENERATE_OTP= 61;

    public static final int REQ_VIEW_BOOKINGS_COMBINE_PREV= 57;
    public static final int REQ_BASE_CMS = 45;

}
