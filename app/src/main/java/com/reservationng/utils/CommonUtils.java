package com.reservationng.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.reservationng.ClickSpan;
import com.reservationng.Dashboard;
import com.reservationng.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;

public class CommonUtils {

	public static final SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static final SimpleDateFormat source1 = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    public static final SimpleDateFormat source2 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    public static final SimpleDateFormat target = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat target1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat target4 = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    public static final SimpleDateFormat target2 = new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat target3 = new SimpleDateFormat("yyyy-MMM-dd", Locale.getDefault());

	public static void clickify(TextView view, final String clickableText,
								final ClickSpan.OnClickListener listener) {

		CharSequence text = view.getText();
		String string = text.toString();
		ClickSpan span = new ClickSpan(listener);

		int start = string.indexOf(clickableText);
		int end = start + clickableText.length();
		if (start == -1) return;

		if (text instanceof Spannable) {
			((Spannable)text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			SpannableString s = SpannableString.valueOf(text);
			s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			view.setText(s);
		}

		MovementMethod m = view.getMovementMethod();
		if ((m == null) || !(m instanceof LinkMovementMethod)) {
			view.setMovementMethod(LinkMovementMethod.getInstance());
		}
	}


    public static View tabsInflator(final Context context, FragmentManager fm,
			LayoutInflater inflater, @Nullable ViewGroup container,
			Map<String, Class<?>> map) {

		View v = inflater.inflate(R.layout.tabfrag, container, false);
		final FragmentTabHost host = (FragmentTabHost) v
				.findViewById(android.R.id.tabhost);
		host.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				((Dashboard) context).hidekeyboard(context);
			}
		});
		host.setup(context, fm, android.R.id.tabcontent);
		for (String key : map.keySet()) {
			host.addTab(host.newTabSpec(key).setIndicator(key), map.get(key),
					null);
		}
		return v;
	}

	@SuppressLint("InflateParams")
	private static View indicator(Context ctx, String txt) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.tab_indicator, null);
		((TextView) v.findViewById(R.id.txtTab)).setText(txt);
		return v;
	}

	public static void showDialog(Context ctx, String msg) {

		AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
		alert.setMessage(msg);
		alert.setTitle("Reservation");
		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		try {
			alert.show();
		} catch (Exception e) {
			Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
					.show();
		}
	}


    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static Spannable changeTextColor(Context context, String str) {
		Spannable spanstr = new SpannableString(str);
		spanstr.setSpan(new ForegroundColorSpan(context.getResources()
				.getColor(R.color.black)), 0, spanstr.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return spanstr;

	}

	public static void setSwipeColor(SwipeRefreshLayout swiperefreshlayout) {
		swiperefreshlayout.setColorSchemeResources(
				android.R.color.holo_blue_dark, android.R.color.holo_red_dark,
				android.R.color.holo_green_dark,
				android.R.color.holo_orange_dark);

	}

    public static Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".jpg");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }



}
