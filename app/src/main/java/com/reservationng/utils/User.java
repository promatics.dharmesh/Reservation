package com.reservationng.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class User {
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String userId;
    private String Fname;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    private String Lname;
    private String email;
    private String cityId;
    private String cityName;
    private String countryId;
    private String countryName;
    private String countryCode;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    private String address1;
    private String address2;
    private String mobileNo;
    private String title;
    private String stateId,stateName;

    private SharedPreferences prefs;
    private static User instance = null;

    private User() {
        super();
        try {
            prefs = App.getInstance().getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE);
            userId = prefs.getString("KEY_USER_ID", null);
            Fname = prefs.getString("KEY_FNAME", null);
            Lname = prefs.getString("KEY_LNAME", null);
            email = prefs.getString("KEY_EMAILID", null);
            cityId = prefs.getString("KEY_CITYID", null);
            cityName = prefs.getString("KEY_CITYNAME", null);
       //     countryId = prefs.getString("KEY_COUNTRYID", null);
           // countryName = prefs.getString("KEY_COUNTRYNAME", null);
            address1 = prefs.getString("KEY_ADDRESS1", null);
            address2 = prefs.getString("KEY_ADDRESS2", null);
            mobileNo = prefs.getString("KEY_MOBILE", null);
            title = prefs.getString("KEY_TITLE", null);
            stateId = prefs.getString("KEY_STATEID", null);
            stateName = prefs.getString("KEY_STATENAME", null);
            countryCode = prefs.getString("KEY_COUNTRYCODE", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized User getInstance() {
        instance = new User();
        if (instance.userId != null)
            return instance;
        else
            return null;
    }


    public static void storeUserData(String userID, String Fname, String Lname, String emailId,String cityId,String cityName,
                                     String address1,String address2,String mobileNo,String title,String stateId,String stateName,String countryCode) {
        SharedPreferences.Editor editor = App.getInstance().getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE).edit();
        editor.putString("KEY_USER_ID", userID);
        editor.putString("KEY_FNAME", Fname);
        editor.putString("KEY_LNAME", Lname);
        editor.putString("KEY_EMAILID", emailId);
        editor.putString("KEY_CITYID", cityId);
        editor.putString("KEY_CITYNAME", cityName);
     // editor.putString("KEY_COUNTRYID", countryId);
        editor.putString("KEY_ADDRESS1", address1);
        editor.putString("KEY_ADDRESS2", address2);
        editor.putString("KEY_MOBILE", mobileNo);
        editor.putString("KEY_TITLE", title);
        editor.putString("KEY_STATEID", stateId);
        editor.putString("KEY_STATENAME", stateName);
        editor.putString("KEY_COUNTRYCODE", countryCode);
        editor.apply();
        instance = new User();
    }

    public void logout() {
        try {
            App.getInstance().getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE).edit()
                    .clear().commit();
            instance = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

}
