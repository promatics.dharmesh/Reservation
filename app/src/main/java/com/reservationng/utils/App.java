package com.reservationng.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

public class App extends MultiDexApplication {
    private static App instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/Questrial-Regular.ttf");
        FontsOverride.setDefaultFont(this, "DEFAULT_BOLD", "fonts/Questrial-Regular.ttf");
        instance = this;
    }

    public static synchronized App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static SharedPreferences getGlobalPrefs() {
        return getContext().getSharedPreferences("APP_SETTINGS", 0);
    }
}
