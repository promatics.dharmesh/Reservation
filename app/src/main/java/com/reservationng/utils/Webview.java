package com.reservationng.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.reservationng.R;


public class Webview extends Activity {
    private WebView webview;
    private String url;
    private Context context = this;
    private CustomProgressDialog progressdialog;
    public static boolean pvtDining=false;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.webview);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        WebView();
    }

    @SuppressWarnings("deprecation")
    public void WebView() {
        try {
            webview = (WebView) findViewById(R.id.webview);
            if (getIntent().getExtras().getString("url") != null) {
                url = getIntent().getExtras().getString("url");
            }
            Log.e("url", url);
            progressdialog = new CustomProgressDialog(context);
            progressdialog.setCancelable(false);
            webview.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                    if (url.equalsIgnoreCase(" http://Reservation.com/WebServices/paymentConfirm/app_redirect")) {
                    if (url.equalsIgnoreCase("http://reservation.ng/dev/WebServices/paymentConfirm")) {
                        Log.e("redirected...", "successfully");
                        finish();
                    } else {
                        view.loadUrl(url);
                    }
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    try {
                        if (isInternetConnected()) {
                            progressdialog.show();
                            Log.e("redirected on pge start","" + url);
//                            if (url.equalsIgnoreCase("http://Reservation.com/WebServices/paymentConfirm/app_redirect")) {
                            if (url.equalsIgnoreCase("http://reservation.ng/dev/WebServices/paymentConfirm")) {
                                progressdialog.dismiss();
                                finish();
//                            } else if (url.equalsIgnoreCase("http://mercury.promaticstechnologies.com/Reservation/WebServices/PrivateDining")) {
                            } else if (url.equalsIgnoreCase("http://reservation.ng/dev/WebServices/PrivateDining")) {
                                pvtDining=true;
                                finish();
                            } else {
                                progressdialog.show();
                            }
                        } else {
                            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
                            progressdialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if (progressdialog != null && progressdialog.isShowing()) {
                        progressdialog.cancel();
                        Log.e("if page finished", url + "hostname" + Uri.parse(url).getHost());
                    }
                }
            });

            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setPluginState(WebSettings.PluginState.ON);
            webview.getSettings().setAppCacheEnabled(true);
            webview.getSettings().setDomStorageEnabled(true);
//            if (url.equalsIgnoreCase("http://Reservation.com/WebServices/paymentConfirm/app_redirect")) {
            if (url.equalsIgnoreCase("http://reservation.ng/dev/WebServices/paymentConfirm")) {
                finish();
//            } else if (url.equalsIgnoreCase("http://mercury.promaticstechnologies.com/Reservation/WebServices/PrivateDining")) {
            } else if (url.equalsIgnoreCase("http://reservation.ng/dev/WebServices/PrivateDining")) {
                finish();
            } else {
                webview.loadUrl(url);
                Log.e("url is", webview.getOriginalUrl() + webview.getUrl());
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    private boolean isInternetConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}

