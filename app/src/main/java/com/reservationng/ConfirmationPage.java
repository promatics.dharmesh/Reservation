package com.reservationng;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.Restaurants.RestaurantDetails;
import com.reservationng.models.DataHolder;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConfirmationPage extends Fragment implements ServiceCallback, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener, View.OnClickListener, GoogleMap.OnMarkerClickListener, NumberPicker.OnValueChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private TextView tvRestNote, txtRestName, tvPeople, tvdte, tvtime, tvCall, tvRestAddress, txtemail, txtCalendar, btnUpdate, btnCancel, txtNoSlot, tvdetails, tvTick;
    private String MyDate1, dt, day, mnth, date, noOfpeople, showDate, x;
    private AQuery aQuery = new AQuery(getActivity());
    View v;
    private LinearLayout llayout;
    private ImageView ivImg;
    private CircleImageView ivRestImg;
    private boolean check = false;
    public static boolean hasFocus = false;
    private String dateToSend, timeToSend;
    private Button btnAnother;
    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LinearLayout linearSlots;
    private HorizontalScrollView horzscroll;


    @Nullable

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.confirmation_page, container, false);
            getActivity().setTitle("Reservation Confirmed!");
            ivImg = (ImageView) v.findViewById(R.id.ivImg);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            tvRestNote = (TextView) v.findViewById(R.id.tvRestNote);
            llayout = (LinearLayout) v.findViewById(R.id.llayout);
            btnUpdate = (TextView) v.findViewById(R.id.btnUpdate);
            btnCancel = (TextView) v.findViewById(R.id.btnCancel);
            btnAnother = (Button) v.findViewById(R.id.btnAnother);
            tvCall = (TextView) v.findViewById(R.id.tvCall);
            tvRestAddress = (TextView) v.findViewById(R.id.tvRestAddress);
            txtemail = (TextView) v.findViewById(R.id.txtemail);
            txtCalendar = (TextView) v.findViewById(R.id.txtCalendar);
            tvTick = (TextView) v.findViewById(R.id.tvTick);
            tvdetails = (TextView) v.findViewById(R.id.tvdetails);
            txtNoSlot = (TextView) v.findViewById(R.id.txtNoSlot);
            horzscroll = (HorizontalScrollView) v.findViewById(R.id.horzscroll);
            linearSlots = (LinearLayout) v.findViewById(R.id.linearSlots);
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dt = DataHolder.getInstance().date;
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMMM d");
            MyDate1 = newDateFormat.format(MyDate);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            check = true;   //from hotdeals of restaurant
            if (check) {
                dateToSend = DataHolder.getInstance().date;
                timeToSend = DataHolder.getInstance().time;
            } else {
                dateToSend = DataHolder.getInstance().date;
                timeToSend = DataHolder.getInstance().time;
            }

            txtRestName.setText(DataHolder.getInstance().name);
            tvPeople.setText(DataHolder.getInstance().noOfPeople);
            tvdte.setText(day + ", " + mnth + " " + date);
            String showTime = DataHolder.getInstance().time;  //12 hr time
            Log.e("showw", showTime);
            SimpleDateFormat dateFormat = new SimpleDateFormat("H:mm");
            Date date1 = dateFormat.parse(showTime);
            tvtime.setText(new SimpleDateFormat("hh:mm a").format(date1));
            tvtime.setText(showTime);
            aQuery.id(ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + DataHolder.getInstance().img);
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + DataHolder.getInstance().img);
            tvRestNote.setText(DataHolder.getInstance().desc);
            tvCall.setText(DataHolder.getInstance().mobile);
            tvRestAddress.setText(DataHolder.getInstance().address);
            tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            Log.e("no", DataHolder.getInstance().address + DataHolder.getInstance().mobile);
            if (User.getInstance() != null) {
                btnCancel.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.VISIBLE);
                btnAnother.setVisibility(View.GONE);
                btnUpdate.setOnClickListener(this);
                btnCancel.setOnClickListener(this);
                tvPeople.setOnClickListener(this);
                tvdte.setOnClickListener(this);
                tvtime.setOnClickListener(this);
                tvTick.setOnClickListener(this);
            } else {
                btnCancel.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
                btnAnother.setVisibility(View.VISIBLE);
                btnAnother.setOnClickListener(this);
            }
            tvCall.setOnClickListener(this);
            txtemail.setOnClickListener(this);
            txtCalendar.setOnClickListener(this);
            initMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    private void initMap() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == resultCode) {
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);
        }
    }

    private void showMarkers() {
//        map.clear();
        String slat = DataHolder.getInstance().curLati;
        String slng = DataHolder.getInstance().curLng;
        LatLng latLng = new LatLng(Double.valueOf(slat), Double.valueOf(slng));
        LatLng dlatLng = new LatLng(Double.parseDouble(DataHolder.getInstance().destLati), Double.parseDouble(DataHolder.getInstance().destLng));
//      map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 12.0f));
    }

    @Override
    public void onMapLoaded() {
        Log.e("cnf",DataHolder.getInstance().curLati+DataHolder.getInstance().curLng);
        String slat = DataHolder.getInstance().curLati;
        String slng = DataHolder.getInstance().curLng;
        LatLng latLng = new LatLng(Double.valueOf(slat), Double.valueOf(slng));
        if(DataHolder.getInstance().destLati.equalsIgnoreCase("null") || DataHolder.getInstance().destLati==null){
            DataHolder.getInstance().destLati="0";
            DataHolder.getInstance().destLng="0";
        }
        LatLng dlatLng = new LatLng(Double.parseDouble(DataHolder.getInstance().destLati), Double.parseDouble(DataHolder.getInstance().destLng));
//        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));

    }

    @Override
    public void onMapClick(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new
                ContextThemeWrapper(getActivity(), android.R.style.
                Theme_Holo_Light));
        builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
        builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMap();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equalsIgnoreCase(DataHolder.getInstance().name)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new
                    ContextThemeWrapper(getActivity(), android.R.style.
                    Theme_Holo_Light));
            builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
            builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showMap();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return false;
    }

    private void showMap() {

        Double slati = Double.valueOf(DataHolder.getInstance().curLati);
        Double slng = Double.valueOf(DataHolder.getInstance().curLng);
        Double dlati = Double.parseDouble(DataHolder.getInstance().destLati);
        Double dlng = Double.parseDouble(DataHolder.getInstance().destLng);

        Log.e("on map", "dest" + dlati + "..." + dlng);
        Log.e("on map", "src" + slati + "..." + slng);

        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", slati, slng, "Current Location", dlati, dlng, DataHolder.getInstance().address);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);

    }


    private void customDatePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                ConfirmationPage.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    public void peopleShow() {
        final Dialog d = new Dialog(getActivity());
        d.setTitle("Select People");
        d.setContentView(R.layout.number_picker);
        Button btnSet = (Button) d.findViewById(R.id.btnSet);
        Button btncancel = (Button) d.findViewById(R.id.btncancel);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(20);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                check=false;
                tvPeople.setText(String.valueOf(np.getValue()));
                tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + DataHolder.getInstance().time);
                tvTick.setVisibility(View.VISIBLE);
                d.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPeople.setText(tvPeople.getText().toString());
                d.dismiss();
            }
        });
        d.show();
    }

    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                ConfirmationPage.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.e("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void updateBooking() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", DataHolder.getInstance().bookingId);
        DataHolder.getInstance().date = dateToSend;
        map.put("date", dateToSend);
        map.put("table_time_id", DataHolder.getInstance().tableTimeId);
        DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
        map.put("no_of_person", tvPeople.getText().toString().trim());
        map.put("special_request", DataHolder.getInstance().specReq);
        Log.e("map values", map.toString() + " " + DataHolder.getInstance().noOfPeople + " " + DataHolder.getInstance().date + " slotid" + DataHolder.getInstance().tableTimeId+DataHolder.getInstance().time);
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_MODIFY_BOOKING, map).execute(Constants.RESTAURANT_MODIFY_BOOKING);
    }


    private void timeSlot() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("rest_id", DataHolder.getInstance().id);
        map.put("date", DataHolder.getInstance().date);
        map.put("time", timeToSend);
        Log.e("time slot values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_TIME_SLOT, map).execute(Constants.REST_TIME_SLOT);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvTick:
                timeSlot();
                break;
            case R.id.tvdte:
                customDatePicker();
                break;

            case R.id.tvPeople:
                peopleShow();
                break;

            case R.id.tvtime:
                customTimePicker();
                break;

            case R.id.btnAnother:
                WithoutSignIn.ResvGuestFlag=false;
                WithoutSignIn.SpecGuestFlag=false;
                DataHolder.getInstance().date = dateToSend;
                DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
                Log.e("time", DataHolder.getInstance().time);
                Fragment fragment = new RestaurantDetails();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
                break;
            case R.id.btnUpdate:
//                WithoutSignIn.ResvGuestFlag=false;
                Log.e("working", "working...");
                updateBooking();
                break;

            case R.id.tvCall:
                Intent i = new Intent(Intent.ACTION_CALL);
                Log.e("mobile no in rest detl", DataHolder.getInstance().mobile);
                i.setData(Uri.parse("tel:" + DataHolder.getInstance().mobile));
                startActivity(i);

                break;
            case R.id.btnCancel:
//                WithoutSignIn.ResvGuestFlag=false;
                cancelBooking();
                break;

            case R.id.txtemail:
                Intent testIntent = new Intent(Intent.ACTION_VIEW);
                String msg = "Let's Eat ! I Reserved a table for " + DataHolder.getInstance().noOfPeople + " people at " + DataHolder.getInstance().name + " on " + tvdte.getText().toString() + " at " + tvtime.getText().toString();
                Uri data = Uri.parse("mailto:?subject=" + "RNG Booking at " + DataHolder.getInstance().name + "&body=" + msg);
                testIntent.setData(data);
                startActivity(testIntent);
                break;

            case R.id.txtCalendar:
                long id = 0;
                String[] projection =
                        new String[]{
                                CalendarContract.Calendars._ID,
                                CalendarContract.Calendars.NAME,
                                CalendarContract.Calendars.ACCOUNT_NAME,
                                CalendarContract.Calendars.ACCOUNT_TYPE};
                Cursor calCursor = getActivity().getContentResolver().
                        query(CalendarContract.Calendars.CONTENT_URI,
                                projection,
                                CalendarContract.Calendars.VISIBLE + " = 1",
                                null,
                                CalendarContract.Calendars._ID + " ASC");
                if (calCursor.moveToFirst()) {
                    do {
                        id = calCursor.getLong(0);
                        String displayName = calCursor.getString(1);
                        Log.e("id is....", id + "   " + displayName + " ...2..." + calCursor.getString(2) + "...3....  " + calCursor.getString(3));
                        // …
                    } while (calCursor.moveToNext());
                }
                int y = 0, m = 0, d = 0;
//                long calID = id;
                long calID = 1;
                long startMillis = 0;
                long endMillis = 0;
                Calendar beginTime = Calendar.getInstance();
                dt = DataHolder.getInstance().date;
                StringTokenizer tokenizer = new StringTokenizer(dt, "-");
                while (tokenizer.hasMoreTokens()) {
                    y = Integer.parseInt(tokenizer.nextToken());
                    m = Integer.parseInt(tokenizer.nextToken());
                    d = Integer.parseInt(tokenizer.nextToken());
                }
                Log.e("click on calendar", "" + y + " " + m + " " + d);
                beginTime.set(y, m - 1, d);// set(int year, int month, int day, int hourOfDay, int minute)
                startMillis = beginTime.getTimeInMillis();
                Calendar endTime = Calendar.getInstance();
                endTime.set(y, m - 1, d);
                endMillis = endTime.getTimeInMillis();
                TimeZone tz = TimeZone.getDefault();
                String msg1 = " Let's Eat ! I Reserved a table for " + DataHolder.getInstance().noOfPeople + " people at " + DataHolder.getInstance().name + " on " + tvdte.getText().toString() + " at " + tvtime.getText().toString();
                ContentResolver cr = getActivity().getContentResolver();
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, startMillis);
                values.put(CalendarContract.Events.DTEND, endMillis);
                values.put(CalendarContract.Events.TITLE, "RNG Booking at " + DataHolder.getInstance().name);
                values.put(CalendarContract.Events.DESCRIPTION, msg1);
//                values.put(CalendarContract.Events.ALL_DAY, true);
                values.put(CalendarContract.Events.CALENDAR_ID, calID);
                values.put(CalendarContract.Events.EVENT_TIMEZONE, tz.getID());

                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                long eventID = Long.parseLong(uri.getLastPathSegment());
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, startMillis);
                Intent intent1 = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                startActivity(intent1);
                break;
        }
    }

    private void cancelBooking() {
        llayout.setVisibility(View.GONE);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", DataHolder.getInstance().bookingId);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_CANCEL_RESTAURANT_BOOKING, map).execute(Constants.CANCEL_RESTAURANT_BOOKING);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }


    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_REST_TIME_SLOT:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        linearSlots.removeAllViews();
                        horzscroll.setVisibility(View.VISIBLE);
                        txtNoSlot.setVisibility(View.GONE);
                        tvTick.setVisibility(View.GONE);
                        JSONObject info = object.getJSONObject("info");
                        JSONArray TableTime = info.getJSONArray("TableTime");

                        if(TableTime.length()>0) {
                            TimeSlots[] slots = new TimeSlots[TableTime.length()];
                            for (int l = 0; l < TableTime.length(); l++) {
                                JSONObject ob = TableTime.getJSONObject(l);
                                TimeSlots slot = new TimeSlots();
                                slot.setRestTableId(ob.getString("restaurant_table_id"));
                                slot.setTiming(ob.getString("timing"));
                                slot.setSlotId(ob.getString("id"));
                                slots[l] = slot;
                                LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                buttonLayoutParams.setMargins(5, 5, 20, 5);
                                final Button btn = new Button(getActivity());
                                final TimeSlots slot1 = slots[l];
                                btn.setLayoutParams(buttonLayoutParams);
                                String time = slot1.getTiming().substring(0, 5);
                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(time);
                                    btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                                } catch (final Exception e) {
                                    e.printStackTrace();
                                }
                                btn.setTextColor(Color.WHITE);
                                btn.setBackgroundResource(R.drawable.btn_reser_login);
                                btn.setPadding(5, 0, 5, 0);
                           /* btn.setFocusableInTouchMode(true);
                            btn.setFocusable(true);*/
                                linearSlots.addView(btn);
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
//                                    btn.setBackgroundResource(R.drawable.btn_bg_orange);
                                        btn.setPadding(5, 0, 5, 0);
                                        DataHolder.getInstance().tableTimeId = slot1.getSlotId();
                                        DataHolder.getInstance().time = btn.getText().toString();
                                        tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + btn.getText().toString());
                                        Log.e("slot id", DataHolder.getInstance().tableTimeId + " " + DataHolder.getInstance().time);

                                    }
                                });
                            }
                        }else{
                            txtNoSlot.setVisibility(View.VISIBLE);
                            txtNoSlot.setText("We 're sorry, nothing's available around this time.Try another date or time perhaps ?");
                        }

                    } else if (code == 0) {
                        horzscroll.setVisibility(View.GONE);
                        txtNoSlot.setVisibility(View.VISIBLE);
                        txtNoSlot.setText("We 're sorry, nothing's available around this time.Try another date or time perhaps ?");
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
            case Constants.REQ_RESTAURANT_MODIFY_BOOKING:
                try {
                    llayout.setVisibility(View.VISIBLE);
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been Updated Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                WithoutSignIn.withoutSignInback=false;
                                Fragment fragment = new RestaurantDetails();

//                                DataHolder.getInstance().date = dateToSend;
//                                DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
                                Log.e("time", DataHolder.getInstance().time + " " + DataHolder.getInstance().date+" "+ DataHolder.getInstance().noOfPeople);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.REQ_CANCEL_RESTAURANT_BOOKING:
                try {
                    llayout.setVisibility(View.VISIBLE);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been Cancelled Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                WithoutSignIn.withoutSignInback=false;
                                DataHolder.getInstance().date = dateToSend;
                                DataHolder.getInstance().noOfPeople = tvPeople.getText().toString().trim();
                                Log.e("time", DataHolder.getInstance().time + " " + DataHolder.getInstance().date);
                                Fragment fragment = new RestaurantDetails();
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        final long current = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        } else {
            monthOfYear = monthOfYear + 1;
            x = String.valueOf(monthOfYear);
        }
        if (calendar.getTimeInMillis() < current) {
            customDatePicker();
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            try {
                String setDate = dayOfMonth + "/" + x + "/" + year;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date1 = dateFormat.parse(setDate);
                SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date2 = newDateFormat.parse(setDate);
                dateToSend = CommonUtils.target.format(date1);
                DataHolder.getInstance().date = dateToSend;
                newDateFormat.applyPattern("EEEE MMMM d");
                String MyDate1 = newDateFormat.format(date2);
                StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                while (tokenizer.hasMoreTokens()) {
                    day = tokenizer.nextToken();
                    mnth = tokenizer.nextToken();
                    date = tokenizer.nextToken();
                    tvdte.setText(day + ", " + mnth + " " + date);
                    Log.e("day in picker", DataHolder.getInstance().date + "   " + dateToSend);
                    tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                    tvTick.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String AM_PM = " AM";
        String mm_precede = "";
        int hourOfDay1 = 0;
        if (minute < 10) {
            mm_precede = "0";
        }
        if (hourOfDay >= 12) {
            AM_PM = " PM";
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay1 = hourOfDay - 12;
            } else {
                hourOfDay1 = 12;
            }
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time = tvtime.getText().toString();
            tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        } else if (hourOfDay == 0) {
            hourOfDay1 = 12;
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time = tvtime.getText().toString();
            tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        } else {
            tvtime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
            DataHolder.getInstance().time = tvtime.getText().toString();
            tvdetails.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        }
        timeToSend = hourOfDay + ":" + minute;
        Log.e("24 hr", "" + hourOfDay + ",,,," + minute + "  timetoSend" + timeToSend);
    }
}
