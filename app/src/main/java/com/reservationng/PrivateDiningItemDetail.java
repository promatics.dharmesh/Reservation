package com.reservationng;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by promatics on 7/12/15.
 */
public class PrivateDiningItemDetail extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.private_dining_item_detail, container, false);
        getActivity().setTitle("Capacity");
        return v;
    }
}
