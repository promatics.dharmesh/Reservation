package com.reservationng.Hotels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.adapters.HotelRoomAdapter;
import com.reservationng.models.RoomDetail;
import com.reservationng.models.RoomFacilities;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HotelRooms extends Fragment implements View.OnClickListener, ServiceCallback {

    private RecyclerView mRecyclerView;
    private TextView txtNoData;
    private ArrayList<RoomDetail> list = new ArrayList<RoomDetail>();
    private boolean check1=false;
    private LinearLayout llayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.hotel_room_list, container, false);
        getActivity().setTitle("Rooms");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        roomDetail();
        return v;
    }

    private void roomDetail() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("hotel_id", getArguments().getString("id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_ROOM_DETAIL, map).execute(Constants.HOTEL_ROOM_DETAIL);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_HOTEL_ROOM_DETAIL:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        list.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            RoomDetail model = new RoomDetail();
                            JSONObject HotelRoom = info.getJSONObject(j).getJSONObject("HotelRoom");
                            model.setId(HotelRoom.getString("id"));
                            model.setName(HotelRoom.getString("room_name"));
                            model.setNumber(HotelRoom.getString("no_rooms"));
                            model.setDesc(HotelRoom.getString("room_description"));
                            model.setPrice(HotelRoom.getString("price_per_night"));
                            model.setFacilities(HotelRoom.getString("available_facility"));
                            model.setImage(HotelRoom.getString("image"));
                            model.setHotelId(HotelRoom.getString("hotel_id")); // or getArguments().getString("id")
                            check1 = getArguments().getBoolean("check1");
                            Log.e("check1 in hotel room", "" + check1);
                            if (!check1) {
                                model.setCheckinDate(getArguments().getString("checkin_date"));
                                model.setCheckoutdate(getArguments().getString("checkout_date"));
                                model.setNoOfRooms(getArguments().getString("no_of_rooms"));
                                model.setFlag("1");
                            } else {
                                model.setFlag("0");
                            }
                            if (object.getJSONArray("room_facility").length() > 0) {
                                JSONArray roomFacility = object.getJSONArray("room_facility");
                                if (roomFacility.length() > 0) {
                                    JSONObject object1 = roomFacility.getJSONObject(j);
                                    String splitFacilities[] = model.getFacilities().split(",");
                                    RoomFacilities room[] = new RoomFacilities[splitFacilities.length];
                                    for (int m = 0; m < splitFacilities.length; m++) {
                                        String name = object1.getString(splitFacilities[m]);
                                        RoomFacilities facilities = new RoomFacilities(name);
                                        facilities.setName(object1.getString(splitFacilities[m]));
                                        room[m] = facilities;
//                                    Log.e("room", +m + " " + room[m].getName());
                                    }
                                    model.setFacility(room);
                                }
                                list.add(model);
                            }
                        }
                        mRecyclerView.setAdapter(new HotelRoomAdapter(getActivity(), list));

                    } else if (code == 0) {
                        llayout.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No Data Found");                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;

        }
    }
}
