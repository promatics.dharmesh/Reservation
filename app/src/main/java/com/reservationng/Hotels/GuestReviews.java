package com.reservationng.Hotels;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.ReviewListAdapter;
import com.reservationng.models.ReviewDetails;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.DynamicHeight;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GuestReviews  extends Fragment implements ServiceCallback {

    private ListView listRestaurantReview;
    private TextView tvRating,tvFood,tvService,tvAmbiance,tvNoise,tvNoData;
    private RatingBar ratingbar;
    private ArrayList<ReviewDetails> list=new ArrayList<ReviewDetails>();
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.reiew_screen, container, false);
        getActivity().setTitle("Reviews");
        listRestaurantReview = (ListView) v.findViewById(R.id.listRestaurantReview);
        ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
        tvRating = (TextView) v.findViewById(R.id.tvRating);
        tvFood = (TextView) v.findViewById(R.id.tvFood);
        tvService = (TextView) v.findViewById(R.id.tvService);
        tvAmbiance = (TextView) v.findViewById(R.id.tvAmbiance);
        tvAmbiance.setVisibility(View.GONE);
        tvNoise = (TextView) v.findViewById(R.id.tvNoise);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        reviews();
        return v;
    }


    private void reviews() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("hotel_id", getArguments().getString("id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_FULL_REVIEW, map).execute(Constants.HOTEL_FULL_REVIEW);
    }
    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode){
            case Constants.REQ_HOTEL_FULL_REVIEW:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        JSONObject ratings = object.getJSONObject("overall_ratings");
                        ratingbar.setRating(Float.parseFloat(ratings.getString("overall")));
                        LayerDrawable layerDrawable=(LayerDrawable)ratingbar.getProgressDrawable();
                        layerDrawable.getDrawable(2).setColorFilter(getActivity().getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                        tvRating.setText("Overall Rating:    "+ratings.getString("overall"));
                        tvFood.setText(Html.fromHtml("<b>Food</b>"));
                        tvFood.append("\n"+ ratings.getString("food"));
                        tvService.setText(Html.fromHtml("<b>Service</b>"));
                        tvService.append("\n"+ ratings.getString("service"));
                        tvAmbiance.setText(Html.fromHtml("<b>Ambiance</b>"));
                        tvAmbiance.append("\n"+ ratings.getString("amb"));
                        if(ratings.getString("noise").equalsIgnoreCase("1")) {
                            tvNoise.setText(Html.fromHtml("<b>Noise level</b>"));
                            tvNoise.append("\n Low");
                        }else if(ratings.getString("noise").equalsIgnoreCase("2")) {
                            tvNoise.setText(Html.fromHtml("<b>Noise level</b>"));
                            tvNoise.append("\n Moderate");
                        }else if(ratings.getString("noise").equalsIgnoreCase("3")) {
                            tvNoise.setText(Html.fromHtml("<b>Noise level</b>"));
                            tvNoise.append("\n High");
                        }
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i <info.length() ; i++) {
                            JSONObject jsonObject=info.getJSONObject(i);
                            JSONObject HotelReview = jsonObject.getJSONObject("HotelReview");
                            ReviewDetails reviews=new ReviewDetails();
                            reviews.setRating(HotelReview.getString("overall_rating"));
                            reviews.setDate(HotelReview.getString("date_review"));
                            reviews.setReview(HotelReview.getString("review"));
                            JSONObject user=jsonObject.getJSONObject("User");
                            reviews.setUserName(user.getString("first_name")+" "+user.getString("last_name"));
//                            details[i]=reviews;
                            list.add(reviews);
                        }
                        listRestaurantReview.setAdapter(new ReviewListAdapter(getActivity(),list));
                        DynamicHeight.setListViewHeightBasedOnChildren1(listRestaurantReview);
                    }else if(code==0){
                        llayout.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
