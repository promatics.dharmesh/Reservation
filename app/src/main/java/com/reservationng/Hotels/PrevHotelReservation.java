package com.reservationng.Hotels;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.ReservationHotelAdapter;
import com.reservationng.models.ReservationHotel;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by android2 on 1/22/16.
 */
public class PrevHotelReservation extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView;
    private TextView tvNoData;
        private ReservationHotelAdapter adapter;
    private ArrayList<ReservationHotel> list = new ArrayList<ReservationHotel>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.rest_list, container, false);
        View v = inflater.inflate(R.layout.rest_list, container, false);
        getActivity().setTitle("Resorts");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        prevReservations();
        return v;
    }

    private void prevReservations(){
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "2");
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKING_HOTEL, map).execute(Constants.VIEW_BOOKING_HOTEL);
    }
    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode){

            case Constants.REQ_VIEW_BOOKING_HOTEL:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        list.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReservationHotel model = new ReservationHotel();
                            JSONObject HotelCalculate = info.getJSONObject(i).getJSONObject("HotelCalculate");
                            model.setId(HotelCalculate.getString("id"));  //booking id same as hotel_book_id in HotelReview
                            model.setHotelId(HotelCalculate.getString("hotel_id"));
                            model.setCheckIn(HotelCalculate.getString("check_in"));
                            model.setCheckOut(HotelCalculate.getString("check_out"));
                            model.setTotadult(HotelCalculate.getString("no_adult"));
                            model.setTotchild(HotelCalculate.getString("no_child"));
                            model.setTotRooms(HotelCalculate.getString("no_room"));
                            model.setRoomType(HotelCalculate.getString("room_type"));
                            model.setPrice(HotelCalculate.getString("total_price"));
                            JSONObject Hotel = info.getJSONObject(i).getJSONObject("Hotel");
                            model.setHotelImg(Hotel.getString("image1"));
                            model.setHotelName(Hotel.getString("hotel_name"));
                            model.setCuisines(Hotel.getString("cuisines"));
                            model.setMobileNo(Hotel.getString("mobile_number"));
                            model.setAddress(Hotel.getString("address1")+" "+Hotel.getString("address2"));

                            JSONArray HotelReview = info.getJSONObject(i).getJSONArray("HotelReview");
                            if (HotelReview.length() > 0) {
                                for (int j = 0; j < HotelReview.length(); j++) {
                                    model.setNoiseLvl(HotelReview.getJSONObject(j).getString("noise_level"));
                                    model.setReview(HotelReview.getJSONObject(j).getString("review"));
                                    model.setRating(HotelReview.getJSONObject(j).getString("overall_rating"));
                                    model.setFood(HotelReview.getJSONObject(j).getString("food_rating"));
                                    model.setService(HotelReview.getJSONObject(j).getString("service_rating"));
                                    model.setFlag(true);  //view a review
                                }
                            } else {
                                model.setFlag(false);  //write a review
                            }
                            list.add(model);
                        }
//                        adapter = new PrevHotelAdapter(getActivity(), list);
//                        mRecyclerView.setAdapter(adapter);
                        adapter = new ReservationHotelAdapter(getActivity(), list,this);
                        mRecyclerView.setAdapter(adapter);
                    } else if (code == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("No data Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
