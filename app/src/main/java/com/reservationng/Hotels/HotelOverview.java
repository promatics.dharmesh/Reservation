package com.reservationng.Hotels;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONObject;

import java.util.HashMap;

public class HotelOverview extends Fragment implements ServiceCallback, View.OnClickListener {

    private ImageView ivImg;
    private TextView tvName, tvMobileNo, tvDesc;
    private AQuery aq;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.hotel_overview, container, false);
        getActivity().setTitle("OverView");
        ivImg = (ImageView) v.findViewById(R.id.ivImg);
        tvName = (TextView) v.findViewById(R.id.tvName);
        tvMobileNo = (TextView) v.findViewById(R.id.tvMobileNo);
        tvDesc = (TextView) v.findViewById(R.id.tvDesc);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        aq = new AQuery(getActivity());
        hotelOverview();
        tvMobileNo.setOnClickListener(this);
        return v;
    }

    private void hotelOverview() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("hotel_id", getArguments().getString("id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_OVERVIEW, map).execute(Constants.HOTEL_OVERVIEW);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_HOTEL_OVERVIEW:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        JSONObject obj = object.getJSONObject("info").getJSONObject("Hotel");
                        tvName.setText(obj.getString("hotel_name"));
                        tvDesc.setText(obj.getString("description"));
                        tvMobileNo.setText(obj.getString("mobile_number"));
                        aq.id(ivImg).image(Constants.BASE_HOTEL_IMG + obj.getString("image1"));
                        Log.e("img url ", Constants.BASE_HOTEL_IMG + obj.getString("image1"));
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMobileNo:
                Intent intent = new Intent(Intent.ACTION_CALL);
                Log.e("mobile no", tvMobileNo.getText().toString());
                intent.setData(Uri.parse("tel:" + tvMobileNo.getText().toString()));
                startActivity(intent);
                break;
        }
    }
}
