package com.reservationng.Hotels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.adapters.HotelsListAdapter;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.models.HotelDetail;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Hotels extends Fragment implements ServiceCallback {
    private RecyclerView mRecyclerView;
    private HotelsListAdapter adapter;
    public static ArrayList<HotelDetail> hotelList = new ArrayList<HotelDetail>();
    private TextView txtNoData, tvTotalHotels, txtLoc, txtDuration, tvRoom;
    private String day, mnth, date, day1, mnth1, date1;
    private View v;
    private LinearLayout llayout;
    public static boolean checkResortLoc ;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.hotels_layout, container, false);
            getActivity().setTitle("Resorts");
            checkResortLoc=true;
            setHasOptionsMenu(true);
            mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
            txtNoData = (TextView) v.findViewById(R.id.txtNoData);
            tvTotalHotels = (TextView) v.findViewById(R.id.tvTotalHotels);
            txtDuration = (TextView) v.findViewById(R.id.txtDuration);
            txtLoc = (TextView) v.findViewById(R.id.txtLoc);
            tvRoom = (TextView) v.findViewById(R.id.tvRoom);
            llayout = (LinearLayout) v.findViewById(R.id.llayout);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date MyDate = newDateFormat.parse(getArguments().getString("checkin_date"));
            Date MyDate1 = newDateFormat.parse(getArguments().getString("checkout_date"));
            newDateFormat.applyPattern("EEEE MMMM d");
            StringTokenizer tokenizer = new StringTokenizer(newDateFormat.format(MyDate), " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            StringTokenizer tokenizer1 = new StringTokenizer(newDateFormat.format(MyDate1), " ");
            while (tokenizer1.hasMoreTokens()) {
                day1 = tokenizer1.nextToken();
                mnth1 = tokenizer1.nextToken();
                date1 = tokenizer1.nextToken();
            }
            txtDuration.setText(day + ", " + date + " " + mnth + " to " + day1 + ", " + date1 + " " + mnth1);
            tvRoom.setText(getArguments().getString("no_of_rooms"));
            searchHotel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    private void searchHotel() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("no_of_rooms", getArguments().getString("no_of_rooms"));
        map.put("checkout_date", getArguments().getString("checkout_date"));
        map.put("checkin_date", getArguments().getString("checkin_date"));
        if (getArguments().getString("search_type").equalsIgnoreCase("2")) {
            map.put("search_type", "2");
            map.put("long", getArguments().getString("long"));
            map.put("lat", getArguments().getString("lat"));
            map.put("city", "0");
            map.put("distance", String.valueOf(HotelFilter.dis));
        } else if (getArguments().getString("search_type").equalsIgnoreCase("3")) {
            map.put("search_type", "3");
            map.put("city", DataHolder.getInstance().hotelLocId);
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        } else if (getArguments().getString("search_type").equalsIgnoreCase("4")) {
            map.put("search_type", "4");
            map.put("city", "");
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        } else {
            map.put("search_type", "1");
            map.put("city", DataHolder.getInstance().hotelLocId);
            map.put("long", "");
            map.put("lat", "");
            map.put("distance", "0");
        }
        if (User.getInstance() != null) {
            map.put("login_user_id", User.getInstance().getUserId());
        } else {
            map.put("login_user_id", "");
        }
        map.put("short_type", String.valueOf(HotelFilter.sort));
        if (HotelFilter.priceRange > 0) {
            map.put("price_range", String.valueOf(HotelFilter.priceRange));
        } else {
            map.put("price_range", "0");
        }
        if (HotelFilter.specialOffers > 0) {
            map.put("special_offer", String.valueOf(HotelFilter.specialOffers));
        } else {
            map.put("special_offer", "0");
        }
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_SEARCH_HOTEL, map).execute(Constants.SEARCH_HOTEL);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.restaurant_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_map:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelsMap(), HotelsMap.class.getSimpleName());
                break;
            case R.id.ic_filter:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelFilter(), HotelFilter.class.getSimpleName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {

            case Constants.REQ_SEARCH_HOTEL:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    String message = object.getString("message");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        final int disInAdapter = HotelFilter.dis;
                        HotelFilter.cuisineId = 0;
                        HotelFilter.specFeaturesId = 0;
                        HotelFilter.priceRange = 0;
                        HotelFilter.specialOffers = 0;
                        HotelFilter.dis = 0;
                        HotelFilter.sort = 0;
                        hotelList.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            HotelDetail model = new HotelDetail();
                            JSONObject hotel = info.getJSONObject(j).getJSONObject("Hotel");
                            JSONObject City = info.getJSONObject(j).getJSONObject("City");
                            JSONObject State = info.getJSONObject(j).getJSONObject("State");
                            JSONArray HotelFavourite = info.getJSONObject(j).getJSONArray("HotelFavourite");
                            JSONObject overall = info.getJSONObject(j);
                            if (overall.isNull("overall")) {
                                model.setOverallRating("0");
                            } else {
                                model.setOverallRating(String.valueOf(overall.getInt("overall")));
                            }
                            if (overall.isNull("total_no_of_reviews")) {
                                model.setTotReview("0");
                            } else {
                                model.setTotReview(String.valueOf(overall.getInt("total_no_of_reviews")));
                            }


                          /*  JSONArray HotelReview = info.getJSONObject(j).getJSONArray("HotelReview");
                            if(HotelReview.length()>0){
                                model.setTotReview(String.valueOf(HotelReview.length()));
                            }else{
                                model.setTotReview("0");
                            }*/
                            if (HotelFavourite.length() > 0) {
                                for (int k = 0; k < HotelFavourite.length(); k++) {
                                    JSONObject userId = HotelFavourite.getJSONObject(k);
//                                model.setFavUserId(userId.getString("user_id"));
                                    if (User.getInstance().getUserId().equalsIgnoreCase(userId.getString("user_id"))) {
                                        model.setFavStatus("1");
                                    }
                                }
                            } else {
                                model.setFavStatus("0");
                            }

                            model.setId(hotel.getString("id"));
                            model.setName(hotel.getString("hotel_name"));
                            model.setLng(hotel.getString("long"));
                            model.setLat(hotel.getString("lat"));
                            model.setCityId(City.getString("id"));
                            model.setCityName(City.getString("city_name"));
                            model.setStateId(State.getString("id"));
                            model.setStateName(State.getString("state_name"));
                            model.setAddress1(hotel.getString("address1"));
                            model.setAddress2(hotel.getString("address2"));
                            model.setPhoneno(hotel.getString("phone_number"));
                            model.setImage(hotel.getString("image1"));
                            model.setCuisine(hotel.getString("cuisines"));
                            model.setPriceRange(hotel.getString("price_range"));
                            if (disInAdapter > 0) {
                                model.setDistance(String.valueOf(disInAdapter));
                            } else {
                                model.setDistance("0");
                            }

                            if (getArguments().getString("search_type").equalsIgnoreCase("2")) {
                                txtLoc.setText("All Resorts near " + model.getCityName() + ", " + model.getStateName());
                            } else if (!model.getStateName().equalsIgnoreCase("")) {
                                txtLoc.setText(model.getCityName());
                            } else if (getArguments().getString("search_type").equalsIgnoreCase("3")) {
                                txtLoc.setText(DataHolder.getInstance().loc);
                            } else {
                                txtLoc.setText(model.getCityName() + ", " + model.getStateName());
                            }

                            hotelList.add(model);
                            if (hotelList.size() == 1) {
                                tvTotalHotels.setText(hotelList.size() + " Resort Available");
                            } else {
                                tvTotalHotels.setText(hotelList.size() + " Resorts Available");
                            }
                        }
                        adapter = new HotelsListAdapter(getActivity(), hotelList);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new HotelsListAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Log.e("position clicked", "" + position + "id " + hotelList.get(position).getId());
                                Fragment fragment = new HotelDetails();

                                HotelDataHolder.getInstance().hotelId = hotelList.get(position).getId();
                                HotelDataHolder.getInstance().name = hotelList.get(position).getName();
                                HotelDataHolder.getInstance().address1 = hotelList.get(position).getAddress1();
                                HotelDataHolder.getInstance().address2 = hotelList.get(position).getAddress2();
                                HotelDataHolder.getInstance().city = hotelList.get(position).getCityName();
                                HotelDataHolder.getInstance().state = hotelList.get(position).getStateName();
                                HotelDataHolder.getInstance().mobile = hotelList.get(position).getPhoneno();
                                HotelDataHolder.getInstance().curLng = getArguments().getString("long");
                                HotelDataHolder.getInstance().curLat = getArguments().getString("lat");

//                                HotelDataHolder.getInstance().desLng= hotelList.get(position).getLng();
//                                HotelDataHolder.getInstance().desLat= hotelList.get(position).getLat();
//
                                HotelDataHolder.getInstance().desLng = getArguments().getString("long");
                                HotelDataHolder.getInstance().desLat = getArguments().getString("lat");

                                HotelDataHolder.getInstance().img = hotelList.get(position).getImage();
                                HotelDataHolder.getInstance().favStatus = hotelList.get(position).getFavStatus();
                                HotelDataHolder.getInstance().cityId = hotelList.get(position).getCityId();
                                HotelDataHolder.getInstance().checkOut = getArguments().getString("checkout_date");
                                HotelDataHolder.getInstance().checkIn = getArguments().getString("checkin_date");
                                HotelDataHolder.getInstance().room = getArguments().getString("no_of_rooms");
                                HotelDataHolder.getInstance().overallRating = hotelList.get(position).getOverallRating();
                                HotelDataHolder.getInstance().check = false;

                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelDetails.class.getSimpleName());
                            }
                        });

                    } else if (code == 0) {
                        HotelFilter.cuisineId = 0;
                        HotelFilter.specFeaturesId = 0;
                        HotelFilter.priceRange = 0;
                        HotelFilter.specialOffers = 0;
                        HotelFilter.dis = 0;
                        HotelFilter.sort = 0;
                        hotelList.clear();
                        llayout.setVisibility(View.VISIBLE);
                        Log.e("list size", "" + hotelList.size());
                        if (getArguments().getString("search_type").equalsIgnoreCase("2")) {
                            txtLoc.setText("All Restaurants near Current Location");
                        } else if (getArguments().getString("search_type").equalsIgnoreCase("4")) {
                            txtLoc.setText("All Restaurants available");
                        } else {
//                            tvSrchbyLoc.setText(getArguments().getString("citystateName"));
                            txtLoc.setText(DataHolder.getInstance().loc);
                        }


                       /* if (getArguments().getString("search_type").equalsIgnoreCase("2")) {
                            txtLoc.setText("All Resorts near " + getArguments().getString("citystateName"));
                        } else {
                            txtLoc.setText(getArguments().getString("citystateName"));
                        }*/
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("Resorts coming soon to RNG");
                        tvTotalHotels.setText("Resorts coming soon to RNG");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;

        }

    }
}