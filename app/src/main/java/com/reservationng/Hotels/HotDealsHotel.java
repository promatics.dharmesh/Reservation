package com.reservationng.Hotels;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.HotDealsResortLoc;
import com.reservationng.R;
import com.reservationng.adapters.HotDealsAdapterHotel;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDetail;
import com.reservationng.models.Offers;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HotDealsHotel  extends Fragment implements View.OnClickListener, ServiceCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private RecyclerView mRecyclerView;
    private HotDealsAdapterHotel adapter;
    private LinearLayout linearlayout,llayout;
    private TextView txtFindDeal,txtNoData;
    private ArrayList<HotelDetail> offerList=new ArrayList<HotelDetail>();
    private GoogleApiClient googleApiClient;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public  double latitude, longitude;
    public static boolean dealsFlagResort=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hot_deals, container, false);
        getActivity().setTitle("Hot Deals");
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        txtNoData = (TextView) view.findViewById(R.id.txtNoData);
        linearlayout = (LinearLayout) view.findViewById(R.id.linearlayout);
        llayout = (LinearLayout) view.findViewById(R.id.llayout);
        txtFindDeal = (TextView) view.findViewById(R.id.txtFindDeal);
        txtFindDeal.setVisibility(View.VISIBLE);
        txtFindDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard)getActivity()).beginTransactions(R.id.content_frame, new HotDealsResortLoc(), HotDealsResortLoc.class.getSimpleName());

            }
        });
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        hotelDeals();
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
        return view;
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                CommonUtils.showToast(getActivity(), "This device is not supported the Google Play Services");
            }
            return false;
        }
        return true;
    }


    private void fetchCurrentLoc() {
        try {
            Location location = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.e("Latitude", latitude + " Longitude" + longitude);
                hotelDeals();
            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                final String message = "Do you want open GPS setting?";
                builder.setMessage(message)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface d, int id) {
                                        startActivity(new Intent(action));
                                        d.dismiss();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface d, int id) {
                                        d.cancel();
                                    }
                                });
                builder.create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hotelDeals() {
        HashMap<String, String> map = new HashMap<>();
        if(!dealsFlagResort) {
            map.put("type", "1");
        }else {
            map.put("type", "2");
            map.put("city_id", DataHolder.getInstance().resLocId);
        }
        Log.e("map values are", map.toString()+dealsFlagResort);
        Log.e("map values in resort",map.values().toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_HOT_DEALS, map).execute(Constants.HOTEL_HOT_DEALS);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_HOTEL_HOT_DEALS:
                try {
                    Log.e(" response is.....", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                   /* if(object.has("message")){
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No Data Found");
                    }else{
                        txtNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }*/
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        txtNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        offerList.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            HotelDetail model = new HotelDetail();
                            JSONObject hotel = info.getJSONObject(j).getJSONObject("Hotel");
                            JSONObject City = info.getJSONObject(j).getJSONObject("City");
                            JSONObject State = info.getJSONObject(j).getJSONObject("State");
                            model.setId(hotel.getString("id"));
                            model.setName(hotel.getString("hotel_name"));
                            model.setCuisine(hotel.getString("cuisines"));
                            model.setStateId(State.getString("id"));
                            model.setCityId(City.getString("id"));
                            model.setCityName(City.getString("city_name"));
                            model.setStateName(State.getString("state_name"));
                            model.setLng(hotel.getString("long"));
                            model.setLat(hotel.getString("lat"));
                            model.setImage(hotel.getString("image1"));
                            model.setAddress1(hotel.getString("address1"));
                            model.setPhoneno(hotel.getString("phone_number"));
                            model.setAddress2(hotel.getString("address2"));
                            Log.e(" model.....", model.getName()+" "+model.getAddress1());
                            JSONArray HotelFavourite=info.getJSONObject(j).getJSONArray("HotelFavourite");
                            if(HotelFavourite.length()>0){
                                model.setFavStatus("1");
                            }else{
                                model.setFavStatus("0");
                            }
                            JSONObject overall = info.getJSONObject(j);
                            if(overall.isNull("overall")) {
                                model.setOverallRating("0");
                            }else{
                                model.setOverallRating(String.valueOf(overall.getInt("overall")));
                            }

                            JSONArray HotelOffer=info.getJSONObject(j).getJSONArray("HotelOffer");

                            Offers[] offers = new Offers[HotelOffer.length()];
                            for (int l = 0; l <HotelOffer.length() ; l++) {
                                JSONObject object1=HotelOffer.getJSONObject(l);
                                Offers offers1=new Offers();
                                offers1.setId(object1.getString("id"));
                                offers1.setName(object1.getString("offer_name"));
                                offers[l]=offers1;
                            }
                            model.setOffers(offers);
                            offerList.add(model);
                        }
                        Log.e("list size",""+offerList.size());
                        adapter = new HotDealsAdapterHotel(getActivity(),offerList);
                        mRecyclerView.setAdapter(adapter);

                    }else if (code == 0) {
                        llayout.setVisibility(View.VISIBLE);
                        Log.e("list size", "" + offerList.size());
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No data found");
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
//        fetchCurrentLoc();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("Connection failed", "" + connectionResult.getErrorCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DataHolder.getInstance().resLoc.equalsIgnoreCase("")){
            txtFindDeal.setText("Location");
        }else {
            txtFindDeal.setText(DataHolder.getInstance().resLoc);
        }
    }
}
