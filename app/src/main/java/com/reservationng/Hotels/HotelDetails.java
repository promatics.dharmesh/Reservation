package com.reservationng.Hotels;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutHotelSignIn;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class HotelDetails extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, ServiceCallback {
    private TextView tvNeedtoKnow, txtRooms, tvName, tvAddress, tvphone, tvhotelMap, tvOverView, tvHotelFacilities, tvNearByAct,tvGuestReview;
    private Button btnBookHotel;
    public static Menu menu;
    private AQuery aQuery = new AQuery(getActivity());
    private ImageView img_hotel;
    private String number;
//    private boolean check = false;
    private ArrayList<RestaurantLists> list = new ArrayList<RestaurantLists>();
    private RatingBar ratingbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.hotel_details, container, false);
        getActivity().setTitle(HotelDataHolder.getInstance().name);
        setHasOptionsMenu(true);
        txtRooms = (TextView) v.findViewById(R.id.txtRooms);
        tvName = (TextView) v.findViewById(R.id.tvName);
        tvphone = (TextView) v.findViewById(R.id.tvphone);
        txtRooms = (TextView) v.findViewById(R.id.txtRooms);
        tvAddress = (TextView) v.findViewById(R.id.tvAddress);
        tvhotelMap = (TextView) v.findViewById(R.id.tvhotelMap);
        btnBookHotel = (Button) v.findViewById(R.id.btnBookHotel);
        img_hotel = (ImageView) v.findViewById(R.id.img_hotel);
        tvOverView = (TextView) v.findViewById(R.id.tvOverView);
        ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
        tvHotelFacilities = (TextView) v.findViewById(R.id.tvHotelFacilities);
        tvNearByAct = (TextView) v.findViewById(R.id.tvNearByAct);
        tvNeedtoKnow = (TextView) v.findViewById(R.id.tvNeedtoKnow);
        tvGuestReview = (TextView) v.findViewById(R.id.tvGuestReview);
        txtRooms.setOnClickListener(this);
        btnBookHotel.setOnClickListener(this);
        tvhotelMap.setOnClickListener(this);
        tvOverView.setOnClickListener(this);
        tvHotelFacilities.setOnClickListener(this);
        tvNearByAct.setOnClickListener(this);
        tvNeedtoKnow.setOnClickListener(this);
        tvGuestReview.setOnClickListener(this);
//        check = getArguments().getBoolean("check");
//        Log.e("chk in details", "" + check);
        tvName.setText(HotelDataHolder.getInstance().name);
        StringBuilder ssb = new StringBuilder();
        if (!HotelDataHolder.getInstance().address1.equalsIgnoreCase("null") || !HotelDataHolder.getInstance().address1.isEmpty()) {
            ssb.append(HotelDataHolder.getInstance().address1);
        }
        if (!HotelDataHolder.getInstance().address2.equalsIgnoreCase("null") || !HotelDataHolder.getInstance().address2.isEmpty()) {
            if (ssb.length() > 0) {
                ssb.append(", " + HotelDataHolder.getInstance().address2);
            } else {
                ssb.append(HotelDataHolder.getInstance().address2);
            }

        }
      /*  if (!HotelDataHolder.getInstance().city.equalsIgnoreCase("null") || !HotelDataHolder.getInstance().city.isEmpty()) {
            if (ssb.length() > 0) {
                ssb.append(", " + HotelDataHolder.getInstance().city);
            } else {
                ssb.append(HotelDataHolder.getInstance().city);
            }

        }
        if (!HotelDataHolder.getInstance().state.equalsIgnoreCase("null") || !HotelDataHolder.getInstance().state.isEmpty()) {

            if (ssb.length() > 0) {
                ssb.append(", " + HotelDataHolder.getInstance().state);
            } else {
                ssb.append(HotelDataHolder.getInstance().state);
            }
        }*/
        tvAddress.setText(ssb.toString());

        number = HotelDataHolder.getInstance().mobile;
        if(!number.equalsIgnoreCase("")) {
            tvphone.setText("Tel: " + number);
        }else {
            tvphone.setText("Tel: " + "N/A");
        }

        //TODO for check true
//        if (!check) {
//            ratingbar.setVisibility(View.VISIBLE);
            ratingbar.setIsIndicator(true);
            Drawable stars = ratingbar.getProgressDrawable();
            DrawableCompat.setTint(stars, Color.rgb(162, 138, 60));
        if(!HotelDataHolder.getInstance().overallRating.equalsIgnoreCase("null")) {
            Log.e("ratig ", HotelDataHolder.getInstance().overallRating);
            ratingbar.setRating(Float.parseFloat(HotelDataHolder.getInstance().overallRating));
        }
//        } else {
//            ratingbar.setVisibility(View.GONE);
//        }
        aQuery.id(img_hotel).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().img);


        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        inflater.inflate(R.menu.fav_menu, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.add_to_fav);
            if (HotelDataHolder.getInstance().favStatus.equalsIgnoreCase("1")) {
                item.setIcon(R.drawable.favorite_fill);
            } else {
                item.setIcon(R.drawable.fav);
            }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_to_fav:
                if (User.getInstance() != null) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hotel_id", HotelDataHolder.getInstance().hotelId);
                    map.put("user_id", User.getInstance().getUserId());
                    Log.e("map values", map.toString());
                    new CallService(this, getActivity(), Constants.REQ_FAVORITE_HOTEL, map).execute(Constants.FAVORITE_HOTEL);
                } else {
                    CommonUtils.showDialog(getActivity(), "Please sign in first to favourite a resort.");
                }
                break;
            case R.id.makeCall:
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:" + number));
                startActivity(i);
                break;

            case R.id.sharing:
                try {
                    Uri bmpUri = CommonUtils.getLocalBitmapUri(img_hotel);
                    if (bmpUri != null) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, HotelDataHolder.getInstance().name);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, HotelDataHolder.getInstance().name);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.setType("image/*");
                        startActivity(Intent.createChooser(shareIntent, "Share Image"));
                    } else {
                        CommonUtils.showToast(getActivity(), "Sharing failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBookHotel:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelBooking(), "Hotel bookings");
                break;
            case R.id.txtRooms:
                WithoutHotelSignIn.withoutSignHotelInback = false;
                Fragment fragment = new HotelRooms();
                Bundle bundle = new Bundle();
                Log.e("check in hotel details", "" + HotelDataHolder.getInstance().check);
                if (!HotelDataHolder.getInstance().check) {
                    Log.e("on map", "src"+HotelDataHolder.getInstance().curLng+"..."+HotelDataHolder.getInstance().curLat);
                    //destination
                    Double dlati= Double.parseDouble(HotelDataHolder.getInstance().desLat);
                    Double dlng= Double.parseDouble(HotelDataHolder.getInstance().desLng);

                    Log.e("on map", "dest"+dlati+"..."+dlng);

                    bundle.putString("id", HotelDataHolder.getInstance().hotelId);
                    bundle.putString("checkout_date", HotelDataHolder.getInstance().checkOut);
                    bundle.putString("checkin_date", HotelDataHolder.getInstance().checkIn);
                    bundle.putString("no_of_rooms", HotelDataHolder.getInstance().room);
                    bundle.putBoolean("check1", false);
                } else {
                    // coming from hotdeals
                    bundle.putString("id", HotelDataHolder.getInstance().hotelId);
                    bundle.putBoolean("check1", true);
                }
                fragment.setArguments(bundle);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelRooms.class.getSimpleName());
                break;
            case R.id.tvhotelMap:
                try {
                    Log.e("on map", "src" + HotelDataHolder.getInstance().curLng + "..." + HotelDataHolder.getInstance().curLat);
                    //destination
                    Double dlati = Double.parseDouble(HotelDataHolder.getInstance().desLat);
                    Double dlng = Double.parseDouble(HotelDataHolder.getInstance().desLng);

                    Log.e("on map", "dest" + dlati + "..." + dlng);


                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", Double.parseDouble(HotelDataHolder.getInstance().curLat), Double.parseDouble(HotelDataHolder.getInstance().curLng), "Current Location", dlati, dlng, tvAddress.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }
//                getActivity().finish();


                /*Intent intent = new Intent(getActivity(), HotelFullMap.class);
                if (check) {
                    intent.putExtra("curlati", HotelDataHolder.getInstance().curLat);
                    intent.putExtra("curlong", HotelDataHolder.getInstance().curLng);
                } else {
                    intent.putExtra("curlati", getArguments().getString("curlat"));
                    intent.putExtra("curlong", getArguments().getString("curlong"));
                }
                intent.putExtra("lati", HotelDataHolder.getInstance().curLat);
                intent.putExtra("long", HotelDataHolder.getInstance().curLng);
                intent.putExtra("hotelName", HotelDataHolder.getInstance().name);
                startActivity(intent);*/
                break;
            case R.id.tvOverView:
                Fragment fragment1 = new HotelOverview();
                Bundle bundle1 = new Bundle();
                bundle1.putString("id", HotelDataHolder.getInstance().hotelId);
                fragment1.setArguments(bundle1);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, HotelOverview.class.getSimpleName());
                break;
            case R.id.tvHotelFacilities:
                Fragment fragment2 = new HotelFacilities();
                Bundle bundle2 = new Bundle();
                bundle2.putString("id", HotelDataHolder.getInstance().hotelId);
                fragment2.setArguments(bundle2);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment2, HotelFacilities.class.getSimpleName());
                break;
            case R.id.tvNearByAct:
                Fragment fragment3 = new HotelNearByActivities();
                Bundle bundle3 = new Bundle();
                bundle3.putString("id", HotelDataHolder.getInstance().hotelId);
//                bundle3.putString("hotel_city_id", getArguments().getString("hotel_city_id"));
                bundle3.putString("hotel_city_id",  HotelDataHolder.getInstance().cityId);
                fragment3.setArguments(bundle3);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment3, HotelNearByActivities.class.getSimpleName());
                break;
            case R.id.tvNeedtoKnow:
                Fragment fragment4 = new HotelQuery();
                Bundle bundle4 = new Bundle();
                bundle4.putString("id", HotelDataHolder.getInstance().hotelId);
                fragment4.setArguments(bundle4);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment4, HotelQuery.class.getSimpleName());
                break;

            case R.id.tvGuestReview:
                Fragment fragment5 = new GuestReviews();
                Bundle bundle5 = new Bundle();
                bundle5.putString("id", HotelDataHolder.getInstance().hotelId);
                fragment5.setArguments(bundle5);
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment5, GuestReviews.class.getSimpleName());
                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_FAVORITE_HOTEL:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 2) {
                        CommonUtils.showToast(getActivity(), "Added to Favorites");
                        menu.getItem(0).setIcon(R.drawable.favorite_fill);
                    } else if (code == 1) {
                        CommonUtils.showToast(getActivity(), "successfully unfavorite");
                        menu.getItem(0).setIcon(R.drawable.fav);
                    } else {
                        CommonUtils.showToast(getActivity(), "Something went wrong");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

}
