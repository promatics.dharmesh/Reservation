package com.reservationng.Hotels;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CommonUtils;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutHotelSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Vaishali Arora on 3/20/2016.
 */
public class HotelConfirmationPage extends Fragment implements ServiceCallback, NumberPicker.OnValueChangeListener, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener, View.OnClickListener, GoogleMap.OnMarkerClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {


    private ImageView ivResortImg;
    private TextView tvRooms, tvcheckIn, tvCheckOut, tvMobileNo, tvAddress, txtName, txtemail, txtCalendar,btnUpdate, btnCancel;
    private int count1;
    private Button btnAnother;
    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private String x,y, hotelName, checkIn, checkOut, rooms, mobile, day, mnth, date, checkInSend = "", checkOutSend = "";
    private AQuery aQuery = new AQuery(getActivity());
    View v;
    private ScrollView sv;
    CircleImageView ivRestImg;
    private LinearLayout llBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.hotel_confirmation_page, container, false);
            getActivity().setTitle("Reservation Confirmed!");
            sv = (ScrollView) v.findViewById(R.id.sv);
            ivResortImg = (ImageView) v.findViewById(R.id.ivResortImg);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            txtName = (TextView) v.findViewById(R.id.txtName);
            tvRooms = (TextView) v.findViewById(R.id.tvRooms);
            tvcheckIn = (TextView) v.findViewById(R.id.tvcheckIn);
            tvCheckOut = (TextView) v.findViewById(R.id.tvCheckOut);
            tvMobileNo = (TextView) v.findViewById(R.id.tvMobileNo);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            txtemail = (TextView) v.findViewById(R.id.txtemail);
            llBtn = (LinearLayout) v.findViewById(R.id.llBtn);
            btnUpdate = (TextView) v.findViewById(R.id.btnUpdate);
            btnCancel = (TextView) v.findViewById(R.id.btnCancel);
            btnAnother = (Button) v.findViewById(R.id.btnAnother);
            txtCalendar = (TextView) v.findViewById(R.id.txtCalendar);

            hotelName = HotelDataHolder.getInstance().name;
            checkIn = HotelDataHolder.getInstance().checkIn;
            checkOut = HotelDataHolder.getInstance().checkOut;
            rooms = HotelDataHolder.getInstance().room;
            mobile = HotelDataHolder.getInstance().mobile;

            txtName.setText(hotelName);
            tvcheckIn.setText(checkIn);
            tvCheckOut.setText(checkOut);
            tvRooms.setText(rooms);
            tvMobileNo.setText(mobile);
            tvAddress.setText(HotelDataHolder.getInstance().address);

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date checkin = newDateFormat.parse(checkIn);
            Date checkout = newDateFormat.parse(checkOut);
            newDateFormat.applyPattern("EEEE MMMM d");
            String MyDate1 = newDateFormat.format(checkin);
            String MyDate2 = newDateFormat.format(checkout);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
                tvcheckIn.setText(day + ", " + mnth + " " + date);
            }
            StringTokenizer tokenizer1 = new StringTokenizer(MyDate2, " ");
            while (tokenizer1.hasMoreTokens()) {
                day = tokenizer1.nextToken();
                mnth = tokenizer1.nextToken();
                date = tokenizer1.nextToken();
                tvCheckOut.setText(day + ", " + mnth + " " + date);
            }
            if (User.getInstance() != null) {
                btnCancel.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.VISIBLE);
                btnAnother.setVisibility(View.GONE);
                btnUpdate.setOnClickListener(this);
                btnCancel.setOnClickListener(this);
                tvMobileNo.setOnClickListener(this);
                tvRooms.setOnClickListener(this);
                tvcheckIn.setOnClickListener(this);
                tvCheckOut.setOnClickListener(this);
                txtCalendar.setOnClickListener(this);
                txtemail.setOnClickListener(this);
            } else {
                btnCancel.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
                btnAnother.setVisibility(View.VISIBLE);
                txtCalendar.setOnClickListener(this);
                txtemail.setOnClickListener(this);
                btnAnother.setOnClickListener(this);
            }

            aQuery.id(ivResortImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().img);
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().img);



            initMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }


    private void initMap() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == resultCode) {
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);
        }
    }

    private void showMarkers() {
//        map.clear();
        String slat = HotelDataHolder.getInstance().curLat;
        String slng = HotelDataHolder.getInstance().curLng;
        LatLng latLng = new LatLng(Double.valueOf(slat), Double.valueOf(slng));
        LatLng dlatLng = new LatLng(Double.parseDouble(HotelDataHolder.getInstance().desLat), Double.parseDouble(HotelDataHolder.getInstance().desLng));
//        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));
        // uncomment it to show route from source to destination
        // Navigator navigator = new Navigator(map, latLng, dlatLng);
        // navigator.findDirections(true);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 12.0f));
    }

    @Override
    public void onMapLoaded() {
        Log.e("cnf",HotelDataHolder.getInstance().curLat+HotelDataHolder.getInstance().curLng);
        String slat = HotelDataHolder.getInstance().curLat;
        String slng = HotelDataHolder.getInstance().curLng;
        LatLng latLng = new LatLng(Double.valueOf(slat), Double.valueOf(slng));
        LatLng dlatLng = new LatLng(Double.parseDouble(HotelDataHolder.getInstance().desLat), Double.parseDouble(HotelDataHolder.getInstance().desLng));
//        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 12.0f));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new
                ContextThemeWrapper(getActivity(), android.R.style.
                Theme_Holo_Light));
        builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
        builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMap();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equalsIgnoreCase(DataHolder.getInstance().name)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new
                    ContextThemeWrapper(getActivity(), android.R.style.
                    Theme_Holo_Light));
            builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
            builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showMap();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return false;
    }

    private void showMap() {

        Double slati = Double.valueOf(HotelDataHolder.getInstance().curLat);
        Double slng = Double.valueOf(HotelDataHolder.getInstance().curLng);
        Double dlati = Double.parseDouble(HotelDataHolder.getInstance().desLat);
        Double dlng = Double.parseDouble(HotelDataHolder.getInstance().desLng);

        Log.e("on map", "dest" + dlati + "..." + dlng);
        Log.e("on map", "src" + slati + "..." + slng);

        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", slati, slng, "Current Location", dlati, dlng, HotelDataHolder.getInstance().name);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);

    }

    private void updateBooking() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("hotel_cal_id", HotelDataHolder.getInstance().bookingId);
        map.put("room_type", HotelDataHolder.getInstance().room_type);
//        if (checkInSend.equalsIgnoreCase("")) {
        map.put("check_in", checkIn);
//       } else {
//        map.put("check_in", checkInSend);
//    }
//        if (checkOutSend.equalsIgnoreCase("")) {
        map.put("check_out", checkOut);
//        } else {
//            map.put("check_out", checkOutSend);
//        }
        map.put("no_room", tvRooms.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_BOOKING_MODIFY, map).execute(Constants.HOTEL_BOOKING_MODIFY);
    }

    private void showDatePickerDialog() {
        final long current = System.currentTimeMillis();
        final Calendar calendarTwoWeeksInFuture = Calendar.getInstance();
        int year = calendarTwoWeeksInFuture.get(Calendar.YEAR);
        int month = calendarTwoWeeksInFuture.get(Calendar.MONTH);
        int day1 = calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                if (monthOfYear < 10) {
                    monthOfYear = monthOfYear + 1;
                    x = "0" + monthOfYear;
                } else {
                    x = String.valueOf(monthOfYear);
                }
                if (calendar.getTimeInMillis() < current) {
                    showDatePickerDialog();
                    Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
                } else {
                    if (count1 == 1) {
                        try {
                            String setDate = dayOfMonth + "/" + x + "/" + year;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date1 = dateFormat.parse(setDate);
                            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date2 = newDateFormat.parse(setDate);
                            checkInSend = CommonUtils.target.format(date1);
                            checkIn = checkInSend;
                            newDateFormat.applyPattern("EEEE MMMM d");
                            String MyDate1 = newDateFormat.format(date2);
                            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                            while (tokenizer.hasMoreTokens()) {
                                day = tokenizer.nextToken();
                                mnth = tokenizer.nextToken();
                                date = tokenizer.nextToken();
                                tvcheckIn.setText(day + ", " + mnth + " " + date);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (count1 == 2) {
                        try {
                            String setDate = dayOfMonth + "/" + x + "/" + year;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date3 = dateFormat.parse(setDate);
                            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date2 = newDateFormat.parse(setDate);
                            checkOutSend = CommonUtils.target.format(date3);
                            checkOut = checkOutSend;
                            newDateFormat.applyPattern("EEEE MMMM d");
                            String MyDate1 = newDateFormat.format(date2);
                            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                            while (tokenizer.hasMoreTokens()) {
                                day = tokenizer.nextToken();
                                mnth = tokenizer.nextToken();
                                date = tokenizer.nextToken();
                                tvCheckOut.setText(day + ", " + mnth + " " + date);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, calendarTwoWeeksInFuture.get(Calendar.YEAR), calendarTwoWeeksInFuture.get(Calendar.MONTH), calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMinDate(current);
        mDatePickerDialog.show();
    }

    public void noOfRoom() {
        final Dialog d = new Dialog(getActivity());
        d.setTitle("Select Rooms");
        d.setContentView(R.layout.number_picker);
        Button btnSet = (Button) d.findViewById(R.id.btnSet);
        Button btncancel = (Button) d.findViewById(R.id.btncancel);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(20);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvRooms.setText(String.valueOf(np.getValue()));
                d.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvRooms.setText(tvRooms.getText().toString());
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnUpdate:
                try {
                    Date checkIn1 = CommonUtils.source.parse(checkIn);
                    Date checkOut1 = CommonUtils.source.parse(checkOut);
                    if (checkOut1.before(checkIn1) || checkOut1.equals(checkIn1)) {
                        CommonUtils.showToast(getActivity(), "CheckOut Date must be greater than CheckIn Date");
                    } else {
                        sv.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnUpdate.setVisibility(View.GONE);
                        updateBooking();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnCancel:
                cancelBooking();
                break;
            case R.id.tvRooms:
                noOfRoom();
                break;
            case R.id.tvcheckIn:
                count1 = 1;
                customDatePicker();
                break;
            case R.id.tvCheckOut:
                count1 = 2;
                customDatePicker();
                break;
            case R.id.tvMobileNo:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + mobile));
                startActivity(intent);
                break;


            case R.id.btnAnother:
                WithoutHotelSignIn.GuestFlag=false;
                WithoutHotelSignIn.withoutSignHotelInback = false;
                Fragment fragment = new HotelDetails();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelDetails.class.getSimpleName());
                break;
           /* case R.id.tvCall:
                Intent i = new Intent(Intent.ACTION_CALL);
                Log.e("mobile no in rest detl", HotelDataHolder.getInstance().mobile);
                i.setData(Uri.parse("tel:" + HotelDataHolder.getInstance().mobile));
                startActivity(i);
                break;*/

            case R.id.txtemail:
                try {
                    Intent testIntent = new Intent(Intent.ACTION_VIEW);
                    String msg = "Reserved " + HotelDataHolder.getInstance().room + " Rooms from " + CommonUtils.source1.format(CommonUtils.target.parse(HotelDataHolder.getInstance().checkIn)) + " to " + CommonUtils.source1.format(CommonUtils.target.parse(HotelDataHolder.getInstance().checkOut));
                    Uri data = Uri.parse("mailto:?subject=" + "Booking at " + HotelDataHolder.getInstance().name + "&body=" + msg);
                    testIntent.setData(data);
                    startActivity(testIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.txtCalendar:
                Log.e("click on calendar","checking.....!!!!!!");
                try{
                long calID = 1;
                long startMillis = 0;
                long endMillis = 0;

                int y = 0, m = 0, d = 0;
                int y1 = 0, m1 = 0, d1 = 0;
                Calendar beginTime = Calendar.getInstance();
                StringTokenizer tokenizer = new StringTokenizer(checkIn, "-");
                while (tokenizer.hasMoreTokens()) {
                    y = Integer.parseInt(tokenizer.nextToken());
                    m = Integer.parseInt(tokenizer.nextToken());
                    d = Integer.parseInt(tokenizer.nextToken());
                }

                StringTokenizer tokenizer1 = new StringTokenizer(checkOut, "-");
                while (tokenizer1.hasMoreTokens()) {
                    y1 = Integer.parseInt(tokenizer1.nextToken());
                    m1 = Integer.parseInt(tokenizer1.nextToken());
                    d1 = Integer.parseInt(tokenizer1.nextToken());
                }
                beginTime.set(y, m - 1, d);// set(int year, int month, int day, int hourOfDay, int minute)
                startMillis = beginTime.getTimeInMillis();
                Calendar endTime = Calendar.getInstance();
                endTime.set(y1, m1 - 1, d1);
                endMillis = endTime.getTimeInMillis();

                TimeZone tz = TimeZone.getDefault();
                String msg = "Reserved " + HotelDataHolder.getInstance().room + " Rooms from " + CommonUtils.source1.format(CommonUtils.target.parse(HotelDataHolder.getInstance().checkIn)) + " to " + CommonUtils.source1.format(CommonUtils.target.parse(HotelDataHolder.getInstance().checkOut));
                ContentResolver cr = getActivity().getContentResolver();
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, startMillis);
                values.put(CalendarContract.Events.DTEND, endMillis);
                values.put(CalendarContract.Events.TITLE, "RNG Booking at  "+HotelDataHolder.getInstance().name );
//                values.put(CalendarContract.Events.DESCRIPTION,msg);
                values.put(CalendarContract.Events.DESCRIPTION,msg);
//                values.put(CalendarContract.Events.ALL_DAY, true);
                values.put(CalendarContract.Events.CALENDAR_ID, calID);
                values.put(CalendarContract.Events.EVENT_TIMEZONE, tz.getID());
                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

                // get the event ID that is the last element in the Uri
                long eventID = Long.parseLong(uri.getLastPathSegment());
                Log.e("event id", "" + eventID);
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, startMillis);
                Intent intent1 = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                startActivity(intent1);
}catch (Exception e){
    e.printStackTrace();
}
                break;

        }
    }

    private void cancelBooking() {
        sv.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", HotelDataHolder.getInstance().bookingId);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_CANCEL_HOTEL_BOOKING, map).execute(Constants.CANCEL_HOTEL_BOOKING);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_CANCEL_HOTEL_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been Cancelled Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sv.setVisibility(View.VISIBLE);
//                                getActivity().onBackPressed();
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelDetails(), HotelDetails.class.getSimpleName());

                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.REQ_HOTEL_BOOKING_MODIFY:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been made Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sv.setVisibility(View.VISIBLE);
//                                getActivity().onBackPressed();
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelDetails(), HotelDetails.class.getSimpleName());
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void customDatePicker(){
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                HotelConfirmationPage.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        final long current = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        } else {
            x = String.valueOf(monthOfYear);
        }
        if (dayOfMonth < 10) {
            y = "0" + dayOfMonth;
        } else {
            y = String.valueOf(dayOfMonth);
        }
        if (calendar.getTimeInMillis() < current) {
            showDatePickerDialog();
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            if (count1 == 1) {
                try {
                    String setDate = dayOfMonth + "/" + x + "/" + year;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = dateFormat.parse(setDate);
                    SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date2 = newDateFormat.parse(setDate);
                    checkInSend = CommonUtils.target.format(date1);
                    checkIn = checkInSend;
                    newDateFormat.applyPattern("EEEE MMMM d");
                    String MyDate1 = newDateFormat.format(date2);
                    StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                    while (tokenizer.hasMoreTokens()) {
                        day = tokenizer.nextToken();
                        mnth = tokenizer.nextToken();
                        date = tokenizer.nextToken();
                        tvcheckIn.setText(day + ", " + mnth + " " + date);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (count1 == 2) {
                try {
                    String setDate = dayOfMonth + "/" + x + "/" + year;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date3 = dateFormat.parse(setDate);
                    SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date2 = newDateFormat.parse(setDate);
                    checkOutSend = CommonUtils.target.format(date3);
                    checkOut = checkOutSend;
                    newDateFormat.applyPattern("EEEE MMMM d");
                    String MyDate1 = newDateFormat.format(date2);
                    StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                    while (tokenizer.hasMoreTokens()) {
                        day = tokenizer.nextToken();
                        mnth = tokenizer.nextToken();
                        date = tokenizer.nextToken();
                        tvCheckOut.setText(day + ", " + mnth + " " + date);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
