package com.reservationng.Hotels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.NavigationMap.Navigator;
import com.reservationng.R;

public class HotelFullMap extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private GoogleMap map;
    private String dLati = "", dLng = "",curLati="",curLng="";
    private TouchableMapFragment mapFragment;
    LatLng latLng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on create", "oncreate");
        setContentView(R.layout.part_rest_map);
        Intent rcvIntent = getIntent();
        dLati = rcvIntent.getStringExtra("lati");
        dLng = rcvIntent.getStringExtra("long");
//        dLati = "30.7333148";
//        dLng = "76.7794179";
        curLati=rcvIntent.getStringExtra("curlati");
        curLng=rcvIntent.getStringExtra("curlong");

        Log.e("hotel lat n lng", dLati + "..." + dLng +"//"+curLati+"//"+curLng);
        mapFragment = (TouchableMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
        }
    }


    private void showMarkers() {
        map.clear();
        try {
            LatLng latLng = new LatLng(Double.parseDouble(curLati), Double.parseDouble(curLng));
            LatLng dlatLng = new LatLng(Double.parseDouble(dLati), Double.parseDouble(dLng));
            map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
            map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(getIntent().getStringExtra("hotelName")));
            Navigator navigator = new Navigator(map, latLng, dlatLng);
            navigator.findDirections(true);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 12.0f));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onMapLoaded() {
        showMarkers();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
