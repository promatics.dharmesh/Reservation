package com.reservationng.Hotels;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.adapters.ReservationHotelAdapter;
import com.reservationng.models.ReservationHotel;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UpcomingHotels extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView;
    private TextView tvNoData;
    private ReservationHotelAdapter adapter;
    private ArrayList<ReservationHotel> list = new ArrayList<ReservationHotel>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rest_list, container, false);
        getActivity().setTitle("Resorts");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        myReservations();

        return v;
    }


    public void myReservations() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "1");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKING_HOTEL, map).execute(Constants.VIEW_BOOKING_HOTEL);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_VIEW_BOOKING_HOTEL:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReservationHotel model = new ReservationHotel();
                            JSONObject HotelCalculate = info.getJSONObject(i).getJSONObject("HotelCalculate");
                            model.setId(HotelCalculate.getString("id"));  //booking id
                            model.setHotelId(HotelCalculate.getString("hotel_id"));
                            model.setCheckIn(HotelCalculate.getString("check_in"));
                            model.setCheckOut(HotelCalculate.getString("check_out"));
                            model.setTotadult(HotelCalculate.getString("no_adult"));
                            model.setTotchild(HotelCalculate.getString("no_child"));
                            model.setTotRooms(HotelCalculate.getString("no_room"));
                            model.setRoomType(HotelCalculate.getString("room_type"));
                            model.setPrice(HotelCalculate.getString("total_price"));
                            JSONObject Hotel = info.getJSONObject(i).getJSONObject("Hotel");
                            model.setHotelImg(Hotel.getString("image1"));
                            model.setHotelName(Hotel.getString("hotel_name"));
                            model.setCuisines(Hotel.getString("cuisines"));
                            model.setMobileNo(Hotel.getString("mobile_number"));
                            model.setAddress(Hotel.getString("address1")+" "+Hotel.getString("address2"));
                            list.add(model);
                        }
                        adapter = new ReservationHotelAdapter(getActivity(), list);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new ReservationHotelAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Fragment fragment=new ChangeHotelReservation();
                                Bundle bundle=new Bundle();
                                bundle.putString("bookingId",list.get(position).getId());
                                bundle.putString("image1",list.get(position).getHotelImg());
                                bundle.putString("hotel_name",list.get(position).getHotelName());
                                bundle.putString("check_in",list.get(position).getCheckIn());
                                bundle.putString("check_out",list.get(position).getCheckOut());
                                bundle.putString("no_room",list.get(position).getTotRooms());
                                bundle.putString("mobile_number",list.get(position).getMobileNo());
                                bundle.putString("address",list.get(position).getAddress());
                                bundle.putString("room_type",list.get(position).getRoomType());
                                fragment.setArguments(bundle);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ChangeHotelReservation.class.getSimpleName());
                            }
                        });
                    } else if (code == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("No data Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
