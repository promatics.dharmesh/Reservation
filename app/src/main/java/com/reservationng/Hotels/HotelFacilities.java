package com.reservationng.Hotels;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.adapters.HotelFacilityAdapter;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HotelFacilities extends Fragment implements ServiceCallback{

    private RecyclerView mRecyclerView;
    private ArrayList<String> list=new ArrayList<>();
    private TextView txtNoData;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.hotel_room_list,container,false);
        getActivity().setTitle("Resort Facilities");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                size(10).build());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        hotelFacilities();
        return v;
    }

    private void hotelFacilities() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("hotel_id", getArguments().getString("id"));
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_FACILITIES, map).execute(Constants.HOTEL_FACILITIES);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode){
            case Constants.REQ_HOTEL_FACILITIES:
            try {
                Log.e(" response", response);
                JSONObject object = new JSONObject(response);
                int code = object.getInt("code");
                if (code == 1) {
                    llayout.setVisibility(View.VISIBLE);
                    JSONObject info=object.getJSONObject("info");
                    JSONArray HotelAmenitie=info.getJSONArray("HotelAmenitie");
                    if(HotelAmenitie.length()>0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        for (int i = 0; i < HotelAmenitie.length(); i++) {
                            JSONObject object1 = HotelAmenitie.getJSONObject(i);
                            String id = object1.getString("id");
                            list.add(object1.getString("amenity_name"));
                        }
                    }else{
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No Data Found");
                    }
                    mRecyclerView.setAdapter(new HotelFacilityAdapter(getActivity(),list));
                } else if (code == 0) {
                    txtNoData.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setText("No Data Found");
                }
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtils.showDialog(getActivity(), "Invalid response from server");
            }
            break;
        }
    }
}
