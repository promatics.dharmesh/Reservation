package com.reservationng.Hotels;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONObject;

import java.util.HashMap;

public class HotelQuery extends Fragment implements ServiceCallback, View.OnClickListener {

    private EditText etName, etSub, etQuery, etEmail;
    private TextView tvSubmit, tvCancel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hotel_query, container, false);
        etName = (EditText) view.findViewById(R.id.etName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etSub = (EditText) view.findViewById(R.id.etSub);
        etQuery = (EditText) view.findViewById(R.id.etQuery);
        tvSubmit = (TextView) view.findViewById(R.id.tvSubmit);
        tvCancel = (TextView) view.findViewById(R.id.tvCancel);
        tvSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        return view;
    }

    private void hotelQuery() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", etName.getText().toString().trim());
        map.put("email", etEmail.getText().toString().trim());
        map.put("hotel_id", getArguments().getString("id"));
        map.put("subject", etSub.getText().toString().trim());
        map.put("query", etQuery.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_QUERY, map).execute(Constants.HOTEL_QUERY);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_HOTEL_QUERY:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("Query submitted successfully");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().onBackPressed();
                            }
                        });
                        builder.show();
                    } else if (code == 0) {
                        CommonUtils.showToast(getActivity(), "Query not submitted successfully");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                if (etName.getText().toString().trim().isEmpty()) {
                    etName.setError("Please enter name");
                    etName.requestFocus();
                } else if (!etEmail.getText().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter valid email");
                    etEmail.requestFocus();
                } else if (etSub.getText().toString().trim().isEmpty()) {
                    etSub.setError("Please enter subject");
                    etSub.requestFocus();
                } else if (etQuery.getText().toString().trim().isEmpty()) {
                    etQuery.setError("Please enter query");
                    etQuery.requestFocus();
                } else {
                    etName.setError(null);
                    etEmail.setError(null);
                    etSub.setError(null);
                    etQuery.setError(null);
                    hotelQuery();
                }
                break;
            case R.id.tvCancel:
                getActivity().onBackPressed();
                break;
        }
    }

}
