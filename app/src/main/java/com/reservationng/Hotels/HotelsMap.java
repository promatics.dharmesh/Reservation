package com.reservationng.Hotels;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CommonUtils;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.R;
import com.reservationng.models.HotelDetail;

public class HotelsMap extends Fragment implements OnMapReadyCallback,GoogleMap.OnMapLoadedCallback{

    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final  int  CONNECTION_FAILURE_RESOLUTION_REQUEST=9000;
    private Double myLat, myLng;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_hotels, container, false);
        return v;
    }

    private void initMap(){
        int resultCode= GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if(ConnectionResult.SUCCESS==resultCode){
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
        }
    }

    private void setUpMap(){
        try {
            showMarkers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMarkers() {
        map.clear();
        for (HotelDetail lists : Hotels.hotelList) {
            Log.e("res lat n lng", lists.getLat() + ".." + lists.getLng() + " name" + lists.getName());
            myLat = Double.valueOf(lists.getLat());
            myLng = Double.valueOf(lists.getLng());
            map.addMarker(new MarkerOptions()
                    .position(
                            new LatLng(Double.valueOf(lists.getLat()), Double
                                    .valueOf(lists.getLng())))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker))
                    .title(lists.getName())
                    .snippet(lists.getCityName() + ", " + lists.getStateName()));
        }
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLat, myLng), 10.0f));
        //map.setOnMarkerClickListener(this);
        //map.setOnInfoWindowClickListener(this);
    }
    @Override
    public void onMapLoaded() {
        setUpMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        initMap();
    }
}
