package com.reservationng.Hotels;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.reservationng.CMS.PrivacyPolicy;
import com.reservationng.CMS.TermsnConditions1;
import com.reservationng.ClickSpan;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.WithoutHotelSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class HotelBooking extends Fragment implements ServiceCallback, View.OnClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private TextView tvName, tvCheckInDate, tvCheckOutdate, tvPrice, tvRewardPt, tvPrivacy, tvTerms;
    private String day, mnth, date, x;
    private ImageView imgRoom;
    private AQuery aQuery = new AQuery(getActivity());
    private EditText etFname, etLname, etPhone, etEmail, etPwd, etCnfPwd;
    private Button btnBook;
    private Spinner spAdults, spChild, spNoOfRooms;
    private ArrayList<String> adults = new ArrayList<>();
    private ArrayList<String> child = new ArrayList<>();
    private ArrayList<String> rooms = new ArrayList<>();
    private boolean check = false;
    private static int count1;
    private int diffInDays, totPrice;
    Dialog d1;
    private RelativeLayout rlayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.hotel_booking, container, false);
        getActivity().setTitle("Reservation");
        tvName = (TextView) v.findViewById(R.id.tvName);
        tvCheckInDate = (TextView) v.findViewById(R.id.tvCheckInDate);
        tvCheckOutdate = (TextView) v.findViewById(R.id.tvCheckOutdate);
        tvRewardPt = (TextView) v.findViewById(R.id.tvRewardPt);
        tvTerms = (TextView) v.findViewById(R.id.tvTerms);
        tvPrivacy = (TextView) v.findViewById(R.id.tvPrivacy);
        spNoOfRooms = (Spinner) v.findViewById(R.id.spNoOfRooms);
        btnBook = (Button) v.findViewById(R.id.btnBook);
        imgRoom = (ImageView) v.findViewById(R.id.imgRoom);
        tvPrice = (TextView) v.findViewById(R.id.tvPrice);
        etFname = (EditText) v.findViewById(R.id.etFname);
        etLname = (EditText) v.findViewById(R.id.etLname);
        etPhone = (EditText) v.findViewById(R.id.etPhone);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etPwd = (EditText) v.findViewById(R.id.etPwd);
        etCnfPwd = (EditText) v.findViewById(R.id.etCnfPwd);
        btnBook = (Button) v.findViewById(R.id.btnBook);
        spAdults = (Spinner) v.findViewById(R.id.spAdults);
        spChild = (Spinner) v.findViewById(R.id.spChild);
        rlayout = (RelativeLayout) v.findViewById(R.id.rlayout);
        check = getArguments().getBoolean("check");

        if (check) {
            tvCheckInDate.setTextColor(Color.BLACK);
            tvCheckOutdate.setTextColor(Color.BLACK);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) tvCheckOutdate.getLayoutParams();
            layoutParams.setMargins(0, 10, 0, 10);
            tvCheckOutdate.setPadding(15, 10, 10, 10);
            tvCheckInDate.setPadding(15, 10, 10, 10);
            tvCheckInDate.setGravity(Gravity.LEFT | Gravity.CENTER);
            tvCheckOutdate.setGravity(Gravity.LEFT | Gravity.CENTER);
            tvCheckInDate.setText("CheckIn Date");
            tvCheckOutdate.setText("CheckOut Date");
            tvCheckInDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sp_down_arrow, 0);
            tvCheckOutdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sp_down_arrow, 0);
            tvCheckInDate.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_whitebg));
            tvCheckOutdate.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.btn_whitebg));
            spNoOfRooms.setVisibility(View.VISIBLE);
            for (int k = 1; k <= 20; k++) {
                rooms.add(String.valueOf(k));
            }
            rooms.add(0, "Number of Rooms");
            spNoOfRooms.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_layout, rooms));
            spNoOfRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    datePrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            tvCheckInDate.setOnClickListener(this);
            tvCheckOutdate.setOnClickListener(this);
        } else {
            spNoOfRooms.setVisibility(View.GONE);
            tvPrice.setText("₦ " + getArguments().getString("tot_price"));
            Log.e("price", getArguments().getString("tot_price"));

            try {
                SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date checkin = newDateFormat.parse(getArguments().getString("checkin_date"));
                Date checkout = newDateFormat.parse(getArguments().getString("checkout_date"));
                newDateFormat.applyPattern("EEE MMM d");
                String MyDate1 = newDateFormat.format(checkin);
                String MyDate2 = newDateFormat.format(checkout);
                Log.e("date", MyDate1 + MyDate2);
                StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                while (tokenizer.hasMoreTokens()) {
                    day = tokenizer.nextToken();
                    mnth = tokenizer.nextToken();
                    date = tokenizer.nextToken();
                    tvCheckInDate.setText("Check in, " + day + ", " + mnth + " " + date);
                }
                StringTokenizer tokenizer1 = new StringTokenizer(MyDate2, " ");
                while (tokenizer1.hasMoreTokens()) {
                    day = tokenizer1.nextToken();
                    mnth = tokenizer1.nextToken();
                    date = tokenizer1.nextToken();
                    tvCheckOutdate.setText("Check out, " + day + ", " + mnth + " " + date);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        tvName.setText(getArguments().getString("room_name"));
        HotelDataHolder.getInstance().RoomImg = getArguments().getString("room_img");
        aQuery.id(imgRoom).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + HotelDataHolder.getInstance().RoomImg);
        if (User.getInstance() != null) {
            etFname.setText(User.getInstance().getFname());
            etLname.setText(User.getInstance().getLname());
            etPhone.setText(User.getInstance().getMobileNo().substring(3, User.getInstance().getMobileNo().length()));
            etEmail.setFocusableInTouchMode(false);
            etEmail.setText(User.getInstance().getEmail());
            tvRewardPt.setVisibility(View.GONE);
            etPwd.setVisibility(View.GONE);
            etCnfPwd.setVisibility(View.GONE);
        } else {
            etEmail.setFocusableInTouchMode(true);
            tvRewardPt.setVisibility(View.VISIBLE);
            etPwd.setVisibility(View.VISIBLE);
            etCnfPwd.setVisibility(View.VISIBLE);
        }
        for (int i = 1; i <= 30; i++) {
            adults.add(String.valueOf(i));
        }
        for (int j = 0; j <= 30; j++) {
            child.add(String.valueOf(j));
        }
        adults.add(0, "Please Select");
        child.add(0, "Please Select");
        spAdults.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, adults));
        spChild.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, child));


        com.reservationng.utils.CommonUtils.clickify(tvTerms, "Terms and Conditions", new ClickSpan.OnClickListener() {
            @Override
            public void onClick() {
                Intent i = new Intent(getActivity(), TermsnConditions1.class);
                startActivity(i);
//                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new TermsnConditions(), TermsnConditions.class.getSimpleName());
            }
        });

        com.reservationng.utils.CommonUtils.clickify(tvPrivacy, "Privacy Statements", new ClickSpan.OnClickListener() {
            @Override
            public void onClick() {
                Intent i = new Intent(getActivity(), PrivacyPolicy.class);
                startActivity(i);
//                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new PrivacyPolicy(), PrivacyPolicy.class.getSimpleName());
            }
        });


        btnBook.setOnClickListener(this);
        tvPrivacy.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
        return v;
    }

    private void datePrice() {
        try {
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date checkIn = newDateFormat.parse(tvCheckInDate.getText().toString());
            Date checkOut = newDateFormat.parse(tvCheckOutdate.getText().toString());
            if (checkOut.before(checkIn)) {
                CommonUtils.showToast(getActivity(), "CheckOut Date must be greater than CheckIn Date");
                spNoOfRooms.setSelection(0);
                return;
            }
            if (checkOut.equals(checkIn)) {
                int price = Integer.parseInt(getArguments().getString("room_price"));
                totPrice = spNoOfRooms.getSelectedItemPosition() * price * 1;
                tvPrice.setText("₦ " + totPrice);
                return;
            }
            long diff = checkOut.getTime() - checkIn.getTime();
            diffInDays = (int) diff / (1000 * 60 * 60 * 24);
            int price = Integer.parseInt(getArguments().getString("room_price"));
            //comes from hot deals adapter hotel
            totPrice = spNoOfRooms.getSelectedItemPosition() * price * diffInDays;
            tvPrice.setText("₦ " + totPrice);
            Log.e("tot price in adapter", "" + totPrice + "dayz diff" + diffInDays);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hotelBooking() {
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            if (check) {
                map.put("check_in", String.valueOf(CommonUtils.target.format(CommonUtils.source2.parse(tvCheckInDate.getText().toString().trim()))));
                map.put("check_out", String.valueOf(CommonUtils.target.format(CommonUtils.source2.parse(tvCheckOutdate.getText().toString().trim()))));
                map.put("no_room", String.valueOf(spNoOfRooms.getSelectedItem()));
            } else {
                map.put("check_in", getArguments().getString("checkin_date"));
                map.put("check_out", getArguments().getString("checkout_date"));
                map.put("no_room", getArguments().getString("no_room"));
            }
            map.put("room_type", getArguments().getString("room_id"));

            map.put("hotel_id", getArguments().getString("hotel_id"));
            map.put("no_adult", String.valueOf(spAdults.getSelectedItemPosition()));
            map.put("no_child", String.valueOf(spChild.getSelectedItemPosition()));
            map.put("first_name", etFname.getText().toString().trim());
            map.put("last_name", etLname.getText().toString().trim());
            map.put("phone_number", "234" + etPhone.getText().toString().trim());
            map.put("email", etEmail.getText().toString().trim());
            if (User.getInstance() != null) {
                map.put("login", "1");
                map.put("password", "");
            } else {
                map.put("login", "2");
                map.put("password", etPwd.getText().toString().trim());
            }
            Log.e("map values", map.toString());
            new CallService(this, getActivity(), Constants.REQ_HOTEL_BOOKING, map).execute(Constants.HOTEL_BOOKING);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Validation() {

        if (etFname.getText().toString().trim().isEmpty()) {
            etFname.setError("Please enter first name");
            etFname.requestFocus();
        } else if (etLname.getText().toString().trim().isEmpty()) {
            etLname.setError("Please enter last name");
            etLname.requestFocus();
        } else if (etPhone.getText().toString().trim().isEmpty() || etPhone.getText().toString().trim().length() != 10) {
            etPhone.setError("Please enter 10 digits phone number");
            etPhone.requestFocus();
        } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            etEmail.setError("Please enter valid email");
            etEmail.requestFocus();
        } else if (spAdults.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please select number of Adults");
        } /*else if (spChild.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please select number of Children");
        }*/ else if (User.getInstance() == null) {
            if (etPwd.getText().toString().trim().isEmpty()) {
                etPwd.setError("Please enter password");
                etPwd.requestFocus();
            } else if (!etCnfPwd.getText().toString().trim().equals(etPwd.getText().toString().trim())) {
                etCnfPwd.setError("Password and Confirm password must be equal");
                etCnfPwd.requestFocus();
            } else {
                etFname.setError(null);
                etLname.setError(null);
                etPhone.setError(null);
                etEmail.setError(null);
                etPwd.setError(null);
                etCnfPwd.setError(null);
                rlayout.setVisibility(View.GONE);
                hotelBooking();
            }
        } else {
            etFname.setError(null);
            etLname.setError(null);
            etPhone.setError(null);
            etEmail.setError(null);
            rlayout.setVisibility(View.GONE);
            hotelBooking();
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_HOTEL_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");

                    if (code == 1) {

                        rlayout.setVisibility(View.VISIBLE);
                        JSONObject booking_info = object.getJSONObject("booking_info");
                        JSONObject rating = object.getJSONObject("rating");
                        JSONObject HotelBooking = booking_info.getJSONObject("HotelBooking");
                        JSONObject HotelCalculate = booking_info.getJSONObject("HotelCalculate");
                        JSONObject Hotel = booking_info.getJSONObject("Hotel");

                        if (rating.getString("overall").isEmpty()) {
                            HotelDataHolder.getInstance().overallRating = "0";
                        } else {
                            HotelDataHolder.getInstance().overallRating = rating.getString("overall");
                        }

                        Fragment fragment = new HotelConfirmationPage();

                        HotelDataHolder.getInstance().bookingId = HotelBooking.getString("id");
                        HotelDataHolder.getInstance().checkIn = HotelCalculate.getString("check_in");
                        HotelDataHolder.getInstance().checkOut = HotelCalculate.getString("check_out");
                        HotelDataHolder.getInstance().hotelId = HotelCalculate.getString("hotel_id");
                        HotelDataHolder.getInstance().room = HotelCalculate.getString("no_room");
                        HotelDataHolder.getInstance().room_type = HotelCalculate.getString("room_type");

                        StringBuilder ssb = new StringBuilder();
                        if (!Hotel.getString("address1").equalsIgnoreCase("null") || !Hotel.getString("address1").isEmpty()) {
                            ssb.append(Hotel.getString("address1"));
                        }
                        if (!Hotel.getString("address2").equalsIgnoreCase("null") || !Hotel.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Hotel.getString("address2"));
                            } else {
                                ssb.append(Hotel.getString("address2"));
                            }
                        }
                        HotelDataHolder.getInstance().name = Hotel.getString("hotel_name");
                        HotelDataHolder.getInstance().img = Hotel.getString("image1");
                        HotelDataHolder.getInstance().mobile = Hotel.getString("mobile_number");
                        HotelDataHolder.getInstance().address = ssb.toString();
//                        DataHolder.getInstance().desc= Restaurant.getString("description");
//                        DataHolder.getInstance().curLng= getArguments().getString("curlong");
//                        DataHolder.getInstance().curLati= getArguments().getString("curlat");
                        Log.e("with login", HotelDataHolder.getInstance().hotelId);
                        if (User.getInstance() != null) {
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelConfirmationPage.class.getSimpleName());
                        } else {
                            if (WithoutHotelSignIn.GuestFlag) {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelConfirmationPage.class.getSimpleName());
                            } else {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new WithoutHotelSignIn(), WithoutHotelSignIn.class.getSimpleName());

                            }
                        }

                    } else if (code == 2) {
                        rlayout.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
                                View vvv = LayoutInflater.from(getActivity()).inflate(R.layout.signin_dialog, null);
                                d.setView(vvv);
                                d1 = d.create();
                                final EditText etEmail = (EditText) vvv.findViewById(R.id.etEmail);
                                final EditText etPwd = (EditText) vvv.findViewById(R.id.etPwd);
                                TextView txtSignin = (TextView) vvv.findViewById(R.id.txtSignin);
                                TextView txtCancel = (TextView) vvv.findViewById(R.id.txtCancel);
                                TextView txtForgotPwd = (TextView) vvv.findViewById(R.id.txtForgotPwd);
                                CheckBox chkshowPwd = (CheckBox) vvv.findViewById(R.id.chkshowPwd);
                                etEmail.setText(DataHolder.getInstance().email);
                                txtForgotPwd.setVisibility(View.GONE);
                                chkshowPwd.setVisibility(View.GONE);
                                txtSignin.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put("email", etEmail.getText().toString().trim());
                                        map.put("password", etPwd.getText().toString().trim());
                                        Log.e("map values", map.toString());
                                        new CallService(HotelBooking.this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
                                    }
                                });
                                txtCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        d1.dismiss();
                                    }
                                });
                                d1.show();
                            }
                        });
                        builder.show();
                    }
                        /*JSONObject hotel_info=object.getJSONObject("hotel_info");

                        JSONObject Hotel=hotel_info.getJSONObject("Hotel");
                        StringBuilder ssb = new StringBuilder();
                        if (!Hotel.getString("address1").equalsIgnoreCase("null") || !Hotel.getString("address1").isEmpty()) {
                            ssb.append(Hotel.getString("address1"));
                        }
                        if (!Hotel.getString("address2").equalsIgnoreCase("null") || !Hotel.getString("address2").isEmpty()) {
                            if (ssb.length() > 0 ) {
                                ssb.append(", " + Hotel.getString("address2"));
                            } else {
                                ssb.append(Hotel.getString("address2"));
                            }
                        }

                        HotelDataHolder.getInstance().bookingId=getArguments().getString("hotel_id");
                        HotelDataHolder.getInstance().checkIn= getArguments().getString("checkin_date");
                        HotelDataHolder.getInstance().checkOut= getArguments().getString("checkout_date");
                        HotelDataHolder.getInstance().hotelId=getArguments().getString("hotel_id");
                        HotelDataHolder.getInstance().room= getArguments().getString("no_room");
                        HotelDataHolder.getInstance().room_type=getArguments().getString("room_id");
                        HotelDataHolder.getInstance().name= Hotel.getString("hotel_name");
                        HotelDataHolder.getInstance().img= Hotel.getString("image1");
//                        HotelDataHolder.getInstance().mobile= Hotel.getString("mobile_number");
//                        HotelDataHolder.getInstance().address= ssb.toString();
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment fragment1=new WithoutHotelSignIn();
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment1, WithoutHotelSignIn.class.getSimpleName());
                            }
                        });
                        builder.show();*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode = "234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName, countryCode);
                        d1.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private void customDatePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                HotelBooking.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnBook:
                if (check) {
                    if (spNoOfRooms.getSelectedItemPosition() == 0) {
                        if (tvCheckInDate.getText().toString().equalsIgnoreCase("CheckIn Date")) {
                            CommonUtils.showToast(getActivity(), "Please select CheckIn Date");
                            spNoOfRooms.setSelection(0);
                            return;
                        }
                        if (tvCheckOutdate.getText().toString().equalsIgnoreCase("CheckOut Date")) {
                            CommonUtils.showToast(getActivity(), "Please select CheckOut Date");
                            spNoOfRooms.setSelection(0);
                            return;
                        }
                        CommonUtils.showToast(getActivity(), "Please select number of Rooms");
                    } else {
                        datePrice();

                        Validation();
                    }
                } else {
                    Validation();
                }
                break;

            case R.id.tvCheckInDate:
                count1 = 1;
                customDatePicker();
                break;
            case R.id.tvCheckOutdate:
                count1 = 2;
                customDatePicker();
                break;

            case R.id.tvTerms:
                Intent i = new Intent(getActivity(), TermsnConditions1.class);
                startActivity(i);
                break;

            case R.id.tvPrivacy:
                Intent i1 = new Intent(getActivity(), PrivacyPolicy.class);
                startActivity(i1);

                break;


        }
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        final long current = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        }
        if (calendar.getTimeInMillis() < current) {
            customDatePicker();
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            if (count1 == 1) {
                tvCheckInDate.setText(dayOfMonth + "/" + x + "/" + year);
            }
            if (count1 == 2) {
                tvCheckOutdate.setText(dayOfMonth + "/" + x + "/" + year);
//                        datePrice();
//                        spNoOfRooms.setSelection(0);
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2, int i3) {

    }
}
