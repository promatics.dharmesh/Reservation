package com.reservationng.Hotels;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.adapters.HotelsListAdapter;
import com.reservationng.models.HotelDetail;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HotelNearByActivities extends Fragment implements ServiceCallback{

    public static ArrayList<HotelDetail> hotelList = new ArrayList<HotelDetail>();
    private RecyclerView mRecyclerView;
    private TextView txtNoData;
    private LinearLayout llayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.hotel_room_list,container,false);
        getActivity().setTitle("Near By Activites");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                size(10).build());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        nearbyActivities();
        return v;
    }

    private void nearbyActivities() {
        HashMap<String,String> map=new HashMap<>();
        map.put("hotel_id",getArguments().getString("id"));
        map.put("city_id",getArguments().getString("hotel_city_id"));
        Log.e("map values",map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_NEAR_BY_ACTIVITIES, map).execute(Constants.HOTEL_NEAR_BY_ACTIVITIES);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode){
            case Constants.REQ_HOTEL_NEAR_BY_ACTIVITIES:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        hotelList.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int j = 0; j < info.length(); j++) {
                            HotelDetail model = new HotelDetail();
                            JSONObject hotel = info.getJSONObject(j).getJSONObject("Hotel");
                            JSONObject City = info.getJSONObject(j).getJSONObject("City");
                            JSONObject State = info.getJSONObject(j).getJSONObject("State");
                            model.setId(hotel.getString("id"));
                            model.setName(hotel.getString("hotel_name"));
                            model.setCityId(City.getString("id"));
                            model.setCityName(City.getString("city_name"));
                            model.setStateId(State.getString("id"));
                            model.setStateName(State.getString("state_name"));
                            model.setCuisine(hotel.getString("cuisines"));
                            model.setImage(hotel.getString("image1"));
                            model.setAddress1(hotel.getString("address1"));
                            model.setAddress2(hotel.getString("address2"));
                            model.setPhoneno(hotel.getString("mobile_number"));
                            hotelList.add(model);
                            Log.e("model contains", model.getId() + model.getName() + model.getImage());

                        }
                        mRecyclerView.setAdapter(new HotelsListAdapter(getActivity(), hotelList,this));
                    } else if (code == 0) {
                        llayout.setVisibility(View.VISIBLE);
                        Log.e("list size", "" + hotelList.size());
                        // if (restaurantLists.size() == 0) {
                        txtNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoData.setText("No Data Found");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }

                break;

        }
    }
}
