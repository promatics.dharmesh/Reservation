package com.reservationng;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.reservationng.Hotels.Hotels;
import com.reservationng.Restaurants.Restaurants;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.User;

public class Dashboard extends AppCompatActivity {
    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see

//    String TITLES[] = {"Search", "About", "Mail"};
//    int ICONS[] = {R.drawable.ic_action_action_home,R.drawable.ic_action_action_home,R.drawable.ic_action_action_home};

    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

//    String NAME = "Ankit Verma";
//    String EMAIL = "abc@gmail.com";
//    int PROFILE = R.drawable.dummy_photo;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    //    RecyclerView mRecyclerView;                           // Declaring RecyclerView
//    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    public static DrawerLayout Drawer;
    ActionBarDrawerToggle mDrawerToggle;                  // Declaring DrawerLayout

    private FragmentManager fm;
    private String title;
    private Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.dashboard);
            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
            fm = getSupportFragmentManager();
            mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.app_name, R.string.app_name) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    supportInvalidateOptionsMenu();
                    if (User.getInstance() != null) {
                        com.reservationng.Drawer drawerObj = new com.reservationng.Drawer();
                        drawerObj.abc();
//                        com.reservation.Drawer.qwerty();
                    } else {
                        com.reservationng.Drawer drawerObj = new com.reservationng.Drawer();
                        drawerObj.abc();
//                        com.reservation.Drawer.qwerty();
                    }
//                    setTitle(R.string.app_name);
                }
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    supportInvalidateOptionsMenu();
                }
            }; // Drawer Toggle Object Made
            Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
            mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
            try {
                if (savedInstanceState == null) {
                    DataHolder.getInstance().loc = "";
                    DataHolder.getInstance().locId = "";
                    DataHolder.getInstance().hotelLoc = "";
                    DataHolder.getInstance().hotelLocId = "";
                    beginTransactions(R.id.content_frame, new HomeScreen(),
                            "homescreen");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isOpen = Drawer.isDrawerOpen(Gravity.LEFT);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(!isOpen);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Drawer.isDrawerOpen(Gravity.LEFT))
                    Drawer.closeDrawers();
                else
                    Drawer.openDrawer(Gravity.LEFT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        try {

            if (Drawer.isDrawerOpen(Gravity.LEFT)) {
                Drawer.closeDrawers();
                return;
            }
            if(WithoutSignIn.withoutSignInback || WithoutHotelSignIn.withoutSignHotelInback) {
                return;
            }else if(Restaurants.checkLocHome){ //to clear loc text on home screen of restaurants
                DataHolder.getInstance().loc="";
                DataHolder.getInstance().locId="";
                Restaurants.checkLocHome=false;
                super.onBackPressed();
            }else if(Hotels.checkResortLoc){ //to clear loc text on home screen of restaurants
                DataHolder.getInstance().hotelLoc="";
                DataHolder.getInstance().hotelLocId="";
                Hotels.checkResortLoc=false;
                super.onBackPressed();
            }
            else {
                if (fm.getBackStackEntryCount() == 1) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    super.onBackPressed();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void beginTransactions(int id, Fragment frag, String tag) {
        Fragment prevfrag = getSupportFragmentManager().findFragmentByTag(tag);
        fm = getSupportFragmentManager();
        if (null == prevfrag) {
            FragmentTransaction ft = fm.beginTransaction();
            // ft.setCustomAnimations(R.anim.left_to_right, 0, 0,
            // R.anim.right_to_left);
            ft.replace(id, frag, tag);
            ft.addToBackStack(tag);
            ft.commit();
        } else {
            fm.popBackStack(tag, 0);
        }
    }

    /**
     * Hides the soft keyboard.
     */
    public void hidekeyboard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            if (((Activity) context).getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(((Activity) context)
                        .getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

   /* @Override
    protected void onResume() {
        try {
            super.onResume();
            if (User.getInstance() != null) {
            } else {
                Intent intentLog = new Intent(Dashboard.this, MainActivity.class);
                intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentLog);
                finish();
            }
        } catch (Exception e) {
            CommonUtils.showDialog(this, "there is some problem");
            e.printStackTrace();
        }
    }*/
}