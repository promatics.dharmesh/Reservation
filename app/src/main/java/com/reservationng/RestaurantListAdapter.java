package com.reservationng;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.reservationng.Restaurants.RestaurantDetails;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.CustomViewHolder> {

    private Context mContext;

    public RestaurantListAdapter(FragmentActivity activity) {
        this.mContext = activity;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_list_item, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.sublayout_hotel1.setTag(holder);
        holder.sublayout_hotel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, new RestaurantDetails(), RestaurantDetails.class.getSimpleName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout sublayout_hotel1;
        CardView container;

        public CustomViewHolder(View view) {
            super(view);
            this.container = (CardView) view.findViewById(R.id.item_layout_container);
            this.sublayout_hotel1 = (RelativeLayout) view.findViewById(R.id.sublayout_hotel1);
        }
    }
}
