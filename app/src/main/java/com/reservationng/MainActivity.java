package com.reservationng;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.facebook.FacebookSdk;
import com.reservationng.utils.User;

public class MainActivity extends AppCompatActivity {

    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            if (User.getInstance() != null) {
                Intent i = new Intent(this, Dashboard.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();

            } else {
                getSupportFragmentManager().beginTransaction().add(R.id.container, new SplashScreen()).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.showToast(MainActivity.this, "main act Error Occurred!");
        }
    }

    public void addNewFragment(int id, Fragment frag, String tag) {
        Fragment prevFrag = getSupportFragmentManager().findFragmentByTag(tag);
        fm = getSupportFragmentManager();
        if (null == prevFrag) {
            FragmentTransaction ft = fm.beginTransaction();
            // ft.setCustomAnimations(R.anim.left_to_right, 0, 0,
            // R.anim.right_to_left);
            ft.replace(id, frag, tag);
            ft.addToBackStack(tag);
            ft.commit();
        } else {
            fm.popBackStack(tag, 0);
        }
    }

    public void hidekeyboard() {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(
                    this.getCurrentFocus().getWindowToken(), 0);
        }
    }

}