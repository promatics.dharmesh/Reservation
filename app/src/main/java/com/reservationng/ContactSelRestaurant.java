package com.reservationng;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ContactSelRestaurant extends Fragment implements View.OnClickListener {

    private TextView tvdate,txttime;
    private int year,month,day;
    private LinearLayout llAddDetails;
    private CheckBox chkDateFlex;
    private Spinner spEvent;
    private String eventType[]={"EventType 1","EventType 2"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.contact_selected_restaurants,container,false);
        getActivity().setTitle("Contact Selected Restaurants");
        txttime = (TextView) v.findViewById(R.id.txttime);
        tvdate = (TextView) v.findViewById(R.id.tvdate);
        chkDateFlex = (CheckBox) v.findViewById(R.id.chkDateFlex);
        llAddDetails = (LinearLayout) v.findViewById(R.id.llAddDetails);
        spEvent = (Spinner) v.findViewById(R.id.spEvent);

        tvdate.setOnClickListener(this);
        txttime.setOnClickListener(this);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        tvdate.setText(day+ "/" + (month + 1) + "/" +year);
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txttime.setText(sdf.format(calendar.getTime()));

        chkDateFlex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    llAddDetails.setVisibility(View.GONE);
                }else{
                    llAddDetails.setVisibility(View.VISIBLE);
                }
            }
        });

        spEvent.setAdapter(new ArrayAdapter<String>(getActivity() ,R.layout.spinner_layout,eventType));
        return v;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tvdate:

                try {
                    final Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @SuppressLint("SimpleDateFormat")
                                @Override
                                public void onDateSet(DatePicker arg0, int Year,
                                                      int Month, int Day) {
                                    tvdate.setText(Day+ "/" + (Month + 1) + "/" +Year);
                                    String date = tvdate.getText().toString();
                                    try {
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                        Date dateObj = sdf.parse(date);
                                        tvdate.setText(new SimpleDateFormat("dd/MM/yyyy").format(dateObj));
                                    } catch (final Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, year, month, day);
                    dpd.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
                    dpd.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txttime:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                        String AM_PM = " AM";
                        String mm_precede = "";
                        if (hourOfDay >= 12) {
                            AM_PM = " PM";
                            if (hourOfDay >=13 && hourOfDay < 24) {
                                hourOfDay -= 12;
                            }
                            else {
                                hourOfDay = 12;
                            }
                        } else if (hourOfDay == 0) {
                            hourOfDay = 12;
                        }
                        if (minute < 10) {
                            mm_precede = "0";
                        }
                        txttime.setText( hourOfDay + ":" + mm_precede + minute + AM_PM);
                    }
                }, hour, minute, false);
                mTimePicker.show();
                break;
        }
    }
}
