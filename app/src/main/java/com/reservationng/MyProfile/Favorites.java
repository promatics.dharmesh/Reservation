package com.reservationng.MyProfile;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.Hotels.HotelDetails;
import com.reservationng.R;
import com.reservationng.Restaurants.RestaurantDetails;
import com.reservationng.adapters.FavHotelAdapter;
import com.reservationng.adapters.FavoritesRestAdapter;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.models.HotelDetail;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Favorites extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView, mRecyclerView2;
    private FavoritesRestAdapter adapter;
    private FavHotelAdapter adapter1;
    private ArrayList<RestaurantLists> list = new ArrayList<RestaurantLists>();
    private ArrayList<HotelDetail> hotellist = new ArrayList<HotelDetail>();
    private TextView tvNoData, tvHotel, tvRest;
    private View v;
    private LinearLayout llfavorite;
    private boolean flag=false;
    private boolean flag1=false;
    RestaurantLists model;
    private Double latitude,longitude;
    String curDate = "", curTime = "",timeTosend="";
    int curHour = 0, curMin = 0;
    public static boolean hotel;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            v = inflater.inflate(R.layout.favorites, container, false);
              Log.e("fav  flag",""+flag);
            mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
            mRecyclerView2 = (RecyclerView) v.findViewById(R.id.mRecyclerView2);
            tvNoData = (TextView) v.findViewById(R.id.tvNoData);
            tvHotel = (TextView) v.findViewById(R.id.tvHotel);
            tvRest = (TextView) v.findViewById(R.id.tvRest);
            llfavorite = (LinearLayout) v.findViewById(R.id.llfavorite);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                    size(10).build());
            mRecyclerView2.setHasFixedSize(true);
            mRecyclerView2.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            mRecyclerView2.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                    size(10).build());

        StringTokenizer stringTokenizer = new StringTokenizer(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime()), " ");
        while (stringTokenizer.hasMoreTokens()) {
            curDate = stringTokenizer.nextToken();
            curTime = stringTokenizer.nextToken();
        }
        Log.e("cur dt n time",  curDate + ".." + curTime);
        StringTokenizer s1 = new StringTokenizer(curTime, ":");
        while (s1.hasMoreTokens()) {
            curHour = Integer.parseInt(s1.nextToken());
            curMin = Integer.parseInt(s1.nextToken());
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                final Date dateObj = sdf.parse(curHour+":"+curMin);
                timeTosend= new SimpleDateFormat("hh:mm a").format(dateObj);
            } catch (final Exception e) {
                e.printStackTrace();
            }
            Log.e("a1  n a2", curHour + ".." + curMin+" "+timeTosend);
        }
        favresortList();
        favrestList();
        return v;
    }




    public void favresortList() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_HOTEL_FAVORITE_LIST, map,false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.HOTEL_FAVORITE_LIST);
    }

    public void favrestList() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_FAVOURITE_LIST, map,false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,Constants.FAVOURITE_LIST);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_HOTEL_FAVORITE_LIST:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        Log.e("hot fav if", "hot fav if");
                        hotellist.clear();
                        tvHotel.setVisibility(View.VISIBLE);
                        mRecyclerView2.setVisibility(View.VISIBLE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            HotelDetail model = new HotelDetail();
                            JSONObject HotelFavourite = info.getJSONObject(i).getJSONObject("HotelFavourite");
                            String id = HotelFavourite.getString("id");
                            model.setId(HotelFavourite.getString("hotel_id"));
                            String user_id = HotelFavourite.getString("user_id");
                            JSONObject Hotel = info.getJSONObject(i).getJSONObject("Hotel");
                            JSONObject overall = info.getJSONObject(i);
                            if (overall.isNull("overall")) {
                                model.setOverallRating("0.0");
                            } else {
                                model.setOverallRating(String.valueOf(overall.getInt("overall")));
                            }
                            model.setTotReview(overall.getString("total_no_of_reviews"));
                            model.setDescription(Hotel.getString("description"));
                            model.setCuisine(Hotel.getString("cuisines"));
                            model.setImage(Hotel.getString("image1"));
                            model.setName(Hotel.getString("hotel_name"));
                            model.setLng(Hotel.getString("long"));
                            model.setLat(Hotel.getString("lat"));
                            model.setPhoneno(Hotel.getString("mobile_number"));
                            model.setAddress1(Hotel.getString("address1"));
                            model.setAddress2(Hotel.getString("address2"));
                            model.setCityId(Hotel.getString("city"));  //hotel city id
                            hotellist.add(model);
                        }
                        adapter1 = new FavHotelAdapter(getActivity(), hotellist, this);
                        mRecyclerView2.setAdapter(adapter1);

                        adapter1.setOnItemClickListener(new FavHotelAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
//                                hotel=true;
                                Fragment fragment=new HotelDetails();
                                Bundle bundle = new Bundle();
//                                bundle.putString("id", hotellist.get(position).getId());
//                                bundle.putString("hotelName", hotellist.get(position).getName());
                                HotelDataHolder.getInstance().hotelId=hotellist.get(position).getId();
                                HotelDataHolder.getInstance().name=hotellist.get(position).getName();
                                HotelDataHolder.getInstance().address1=hotellist.get(position).getAddress1();
                                HotelDataHolder.getInstance().address2=hotellist.get(position).getAddress2();
//                                bundle.putString("address1", hotellist.get(position).getAddress1());
//                                bundle.putString("address2", hotellist.get(position).getAddress2());
//                                bundle.putString("city", "null");
//                                bundle.putString("state", "null");
                                HotelDataHolder.getInstance().city="null";
                                HotelDataHolder.getInstance().state="null";
                                HotelDataHolder.getInstance().mobile=hotellist.get(position).getPhoneno();
                                HotelDataHolder.getInstance().mobile=hotellist.get(position).getPhoneno();
//                                bundle.putString("phone_number", hotellist.get(position).getPhoneno());
//                                bundle.putString("curlong", "0.0");
//                                bundle.putString("curlat", "0.0");

                                HotelDataHolder.getInstance().curLng="0.0";
                                HotelDataHolder.getInstance().curLat="0.0";

//                                bundle.putString("curlong", String.valueOf(longitude));
//                                bundle.putString("curlat", String.valueOf(latitude));
//                                bundle.putString("long", hotellist.get(position).getLng());
//                                bundle.putString("lat", hotellist.get(position).getLat());
//                                bundle.putString("image1", hotellist.get(position).getImage());

//                                bundle.putString("FavStatus", "1");
//                                bundle.putString("hotel_city_id", hotellist.get(position).getCityId());
                                HotelDataHolder.getInstance().desLat=hotellist.get(position).getLat();
                                HotelDataHolder.getInstance().desLng=hotellist.get(position).getLng();
                                HotelDataHolder.getInstance().img=hotellist.get(position).getImage();
                                HotelDataHolder.getInstance().cityId=hotellist.get(position).getCityId();
                                HotelDataHolder.getInstance().favStatus="1";
                                HotelDataHolder.getInstance().checkOut=curDate;
                                HotelDataHolder.getInstance().checkIn=curDate;
                                HotelDataHolder.getInstance().room="1";
//                                bundle.putString("checkout_date", curDate);
//                                bundle.putString("checkin_date", curDate);

//                                bundle.putString("no_of_rooms", "1");
                                HotelDataHolder.getInstance().overallRating=hotellist.get(position).getOverallRating();
//                                bundle.putString("overall_rating", hotellist.get(position).getOverallRating());
//                                fragment.setArguments(bundle);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, HotelDetails.class.getSimpleName());
//                                flag=false;
                            }
                        });


                    } else if (code == 0) {
                        Log.e("hot fav else", "hot fav else");
                        hotellist.clear();
                        tvHotel.setVisibility(View.GONE);
                        mRecyclerView2.setVisibility(View.GONE);
                    }
                    if (list.size()==0)
                    {
                        flag=true;
                    }
//                        Log.e("hot fav both if", "hot fav both if");
//                        mRecyclerView.setVisibility(View.GONE);
//                        mRecyclerView2.setVisibility(View.GONE);
//                        tvHotel.setVisibility(View.GONE);
//                        tvRest.setVisibility(View.GONE);
//                        tvNoData.setVisibility(View.VISIBLE);
//                        tvNoData.setText("Add a favourite restaurant/resort When it comes to food, it's okay to play favourites. Tap the heart in the upper right of any restaurant profile. we'll save your favourites here for you.");
//                    } else {
//                        Log.e("hot fav both else", "hot fav both else");
//                       /* llfavorite.setVisibility(View.VISIBLE);
//                        mRecyclerView.setVisibility(View.VISIBLE);
//                        mRecyclerView2.setVisibility(View.VISIBLE);
//                        tvRest.setVisibility(View.VISIBLE);
//                        tvHotel.setVisibility(View.VISIBLE);
//                        tvNoData.setVisibility(View.GONE);*/
//                        Log.e("hotel", "inside else hotel" + hotellist.size());
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case Constants.REQ_FAVOURITE_LIST:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        Log.e(" fav if", " fav if");
                        list.clear();
                        tvRest.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                           model = new RestaurantLists();
                            JSONObject RestaurantFavourite = info.getJSONObject(i).getJSONObject("RestaurantFavourite");
                            String id = RestaurantFavourite.getString("id");
                            model.setResId(RestaurantFavourite.getString("restaurant_id"));
                            String user_id = RestaurantFavourite.getString("user_id");
                            JSONObject Restaurant = info.getJSONObject(i).getJSONObject("Restaurant");
                            JSONObject overall = info.getJSONObject(i);
                            if (overall.isNull("overall")) {
                                model.setResOverallRating("0");
                            } else {
                                model.setResOverallRating(String.valueOf(overall.getInt("overall")));
                            }
                            model.setResTotReviews(overall.getString("total_no_of_reviews"));
                            if(info.getJSONObject(i).has("Cuisine")) {
                                JSONObject Cuisine = info.getJSONObject(i).getJSONObject("Cuisine");
                                model.setResCuisine(Cuisine.getString("cuisine_name"));
                            }
                            model.setResImg(Restaurant.getString("image1"));
                            model.setResName(Restaurant.getString("rest_name"));
                            list.add(model);
                        }
                        adapter = new FavoritesRestAdapter(getActivity(), list, this);
                        mRecyclerView.setAdapter(adapter);

                        adapter.setOnItemClickListener(new FavoritesRestAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                 Fragment fragment = new RestaurantDetails();
                                 DataHolder.getInstance().id=list.get(position).getResId();
                                 DataHolder.getInstance().date=curDate;
                                 DataHolder.getInstance().time=timeTosend;
                                 DataHolder.getInstance().curLng= "0.0";
                                 DataHolder.getInstance().curLati= "0.0";
//                                 DataHolder.getInstance().curLng= String.valueOf(latitude);
//                                 DataHolder.getInstance().curLati= String.valueOf(longitude);
                                 DataHolder.getInstance().noOfPeople="1";
                                 DataHolder.getInstance().img=list.get(position).getResImg();
                                 Log.e("time",DataHolder.getInstance().time);
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, "restDetails");
//                                flag=false;
                            }
                        });

                    } else if (code == 0) {
                        Log.e(" fav else", " fav else");
                        list.clear();
                        tvRest.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    if (list.size() == 0 && hotellist.size() == 0) {
                        flag1=true;
                    }
//                        Log.e(" fav both if", " fav both if");
//                        mRecyclerView.setVisibility(View.GONE);
//                        mRecyclerView2.setVisibility(View.GONE);
//                        tvRest.setVisibility(View.GONE);
//                        tvHotel.setVisibility(View.GONE);
//                        tvNoData.setVisibility(View.VISIBLE);
//                        tvNoData.setText("Add a favourite restaurant/resort When it comes to food, it's okay to play favourites. Tap the heart in the upper right of any restaurant profile. we'll save your favourites here for you.");
//                    } else {
//                        Log.e(" fav both else", " fav both else");
//                       /* llfavorite.setVisibility(View.VISIBLE);
//                        mRecyclerView.setVisibility(View.VISIBLE);
//                        mRecyclerView2.setVisibility(View.VISIBLE);
//                        tvRest.setVisibility(View.VISIBLE);
//                        tvHotel.setVisibility(View.VISIBLE);
//                        tvNoData.setVisibility(View.GONE);*/
//                        Log.e("Restaurant", "inside else restaurant" + list.size());
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        if (flag1 && flag)
        {
            mRecyclerView.setVisibility(View.GONE);
                        mRecyclerView2.setVisibility(View.GONE);
                        tvRest.setVisibility(View.GONE);
                        tvHotel.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("Add a favourite restaurant/resort When it comes to food, it's okay to play favourites. Tap the heart in the upper right of any restaurant profile. we'll save your favourites here for you.");
        }
    }



   /* @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !flag ) {
            favresortList();
            favrestList();
            flag = true;
        }
    }*/
}
