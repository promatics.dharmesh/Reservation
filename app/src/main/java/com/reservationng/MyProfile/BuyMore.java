package com.reservationng.MyProfile;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reservationng.R;

import java.util.Calendar;

/**
 * Created by android2 on 11/30/15.
 */
public class BuyMore extends Fragment implements View.OnClickListener{


    private RadioButton rbRecipient,rbPrintable;
    private LinearLayout llrecipient,lldiffTimevalue,lldiffTime;
    private TextView txtprintable,txtPickDiff,txtsendToday,txtDate;
    private Spinner spAmt,spTime,spTimeZone;
    private String amt []={"100","200","300"};
    private String time []={"1:00 AM","2:00 AM","3:00 AM"};
    private String timezone []={"Alaska","Hawaii","Samoa"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.buy_more, container, false);
        getActivity().setTitle("Buy Gift");
        rbRecipient=(RadioButton)v.findViewById(R.id.rbRecipient);
        rbPrintable=(RadioButton)v.findViewById(R.id.rbPrintable);
        llrecipient=(LinearLayout)v.findViewById(R.id.llrecipient);
        lldiffTimevalue=(LinearLayout)v.findViewById(R.id.lldiffTimevalue);
        lldiffTime=(LinearLayout)v.findViewById(R.id.lldiffTime);
        txtprintable=(TextView)v.findViewById(R.id.txtprintable);
        txtsendToday=(TextView)v.findViewById(R.id.txtsendToday);
        txtPickDiff=(TextView)v.findViewById(R.id.txtPickDiff);
        txtDate=(TextView)v.findViewById(R.id.txtDate);
        spAmt=(Spinner)v.findViewById(R.id.spAmt);
        spTime=(Spinner)v.findViewById(R.id.spTime);
        spTimeZone=(Spinner)v.findViewById(R.id.spTimeZone);
        txtPickDiff.setOnClickListener(this);
        txtsendToday.setOnClickListener(this);
        txtDate.setOnClickListener(this);
        rbRecipient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    llrecipient.setVisibility(View.VISIBLE);
                    txtprintable.setVisibility(View.GONE);
                }
            }
        });
        rbPrintable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    llrecipient.setVisibility(View.GONE);
                    txtprintable.setVisibility(View.VISIBLE);
                    lldiffTime.setVisibility(View.GONE);
                    lldiffTimevalue.setVisibility(View.GONE);
                    txtprintable.setText(Html.fromHtml("<b>" +"Got it."+"</b>"+" We'll email you the gift card, or you can choose to print after checkout."));
                }
            }
        });
        spAmt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout,amt));
        spTime.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout,time));
        spTimeZone.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout,timezone));
        return v;
    }

    private void showDatePickerDialog() {
        final long current = System.currentTimeMillis();
        final Calendar calendarTwoWeeksInFuture = Calendar.getInstance();
        int year = calendarTwoWeeksInFuture.get(Calendar.YEAR);
        int month = calendarTwoWeeksInFuture.get(Calendar.MONTH);
        int day = calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                if (calendar.getTimeInMillis() < current) {
                    showDatePickerDialog();
                    Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
                } else {
                    txtDate.setText(dayOfMonth+ "/" + (monthOfYear + 1) + "/" +year);
                }
            }
        }, calendarTwoWeeksInFuture.get(Calendar.YEAR), calendarTwoWeeksInFuture.get(Calendar.MONTH), calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMinDate(current);
        mDatePickerDialog.show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtPickDiff:
                lldiffTime.setVisibility(View.VISIBLE);
                lldiffTimevalue.setVisibility(View.VISIBLE);
                txtsendToday.setTextColor(getResources().getColor(R.color.red));
                txtPickDiff.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.txtsendToday:
                lldiffTime.setVisibility(View.GONE);
                lldiffTimevalue.setVisibility(View.GONE);
                txtsendToday.setTextColor(getResources().getColor(R.color.black));
                txtPickDiff.setTextColor(getResources().getColor(R.color.red));
                break;

            case R.id.txtDate:
                showDatePickerDialog();
                break;
        }
    }
}
