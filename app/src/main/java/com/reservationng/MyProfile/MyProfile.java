package com.reservationng.MyProfile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.HomeScreen;
import com.reservationng.R;
import com.reservationng.models.City;
import com.reservationng.models.State;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyProfile extends Fragment implements ServiceCallback, View.OnClickListener {
    private Spinner spTitle, spCity, spState;
    private Button btnUpdate;
    private TextView txtEmail;
    private EditText etFname, etLname, etAddress1, etAddress2, etMobile, etPass, etCnfPass;
    private String[] title = {"Mr.", "Mrs.", "Ms."};
    private String[] city = {"Select Area"};
    private String[] state = {"Select State"};
    private ArrayList<City> cityList = new ArrayList<>();
    private ArrayList<State> stateList = new ArrayList<>();
    private String cityId = "", stateId = "";
    private int titleId;
    private LinearLayout llayout;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
             v = inflater.inflate(R.layout.my_profile, container, false);
            getActivity().setTitle("Personal Details");
            spCity = (Spinner) v.findViewById(R.id.spCity);
            spTitle = (Spinner) v.findViewById(R.id.spTitle);
            spState = (Spinner) v.findViewById(R.id.spState);
            btnUpdate = (Button) v.findViewById(R.id.btnUpdate);
            txtEmail = (TextView) v.findViewById(R.id.txtEmail);
            etFname = (EditText) v.findViewById(R.id.etFirst);
            etLname = (EditText) v.findViewById(R.id.etLast);
            etAddress1 = (EditText) v.findViewById(R.id.etAddressLine1);
            etAddress2 = (EditText) v.findViewById(R.id.etAddressLine2);
            etMobile = (EditText) v.findViewById(R.id.etMobile);
            etPass = (EditText) v.findViewById(R.id.etPass);
            etCnfPass = (EditText) v.findViewById(R.id.etCnfPass);
            llayout = (LinearLayout) v.findViewById(R.id.llayout);
            new CallService(this, getActivity(), Constants.REQ_ALL_STATE).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.ALL_STATE);

            spTitle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, title));
            spCity.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, city));
            spState.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, state));
            btnUpdate.setOnClickListener(this);
            etFname.setText(User.getInstance().getFname());
            etLname.setText(User.getInstance().getLname());
            txtEmail.setText(User.getInstance().getEmail());

            if (!User.getInstance().getAddress1().equalsIgnoreCase("null") || User.getInstance().getAddress1() != null || !User.getInstance().getAddress1().isEmpty() || !User.getInstance().getAddress1().equals("")) {
                etAddress1.setText(User.getInstance().getAddress1());
            } else {

                etAddress1.setText("");

            }
            if (!User.getInstance().getAddress2().equalsIgnoreCase("null") || User.getInstance().getAddress2() != null || !User.getInstance().getAddress2().isEmpty() || !User.getInstance().getAddress2().equalsIgnoreCase("")) {
                etAddress2.setText(User.getInstance().getAddress2());

            } else {
                etAddress2.setText("");


            }
            Log.e("mobile", User.getInstance().getMobileNo() + "title " + User.getInstance().getTitle()+User.getInstance().getStateId());
            Log.e("address", User.getInstance().getAddress1() + "adress2 " + User.getInstance().getAddress2());

            if(User.getInstance().getMobileNo().length()>10) {
//                etMobile.setText(User.getInstance().getMobileNo());
                etMobile.setText(User.getInstance().getMobileNo().substring(3, User.getInstance().getMobileNo().length()));
            }else{
                etMobile.setText(User.getInstance().getMobileNo());
            }
            if (User.getInstance().getTitle() != null || !User.getInstance().getTitle().equalsIgnoreCase("null")) {
                spTitle.setSelection(0);
            } else {
                spTitle.setSelection(Integer.parseInt(User.getInstance().getTitle()) - 1);
            }
            spCity.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, city));
            spState.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, state));
            spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    titleId = spTitle.getSelectedItemPosition() + 1;
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
            return v;
    }

    private void updateProfile() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", User.getInstance().getUserId());
        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("address1", etAddress1.getText().toString().trim());
        map.put("address2", etAddress2.getText().toString().trim());
        if (!cityId.equalsIgnoreCase("")) {
            map.put("city", cityId);
        }
        if (!stateId.equalsIgnoreCase("")) {
            map.put("state", stateId);
        }
        if (!etPass.getText().toString().isEmpty()) {
            map.put("password", etPass.getText().toString().trim());
        }
        map.put("title", String.valueOf(titleId));
        map.put("mobile_number", etMobile.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_UPDATE_PROFILE, map).execute(Constants.UPDATE_PROFILE);
    }

    private void validation() {
        if (etFname.getText().toString().isEmpty()) {
            etFname.setError("Please enter first name");
            etFname.requestFocus();
        } else if (etLname.getText().toString().isEmpty()) {
            etLname.setError("Please enter last name");
            etLname.requestFocus();
        } else if (etAddress1.getText().toString().isEmpty()) {
            etAddress1.setError("Please enter address (line 1)");
            etAddress1.requestFocus();
        } else if (etAddress2.getText().toString().isEmpty()) {
            etAddress2.setError("Please enter address (line 2)");
            etAddress2.requestFocus();
        } else if (spState.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please select state");
        } else if (spCity.getSelectedItemPosition() == 0) {
            CommonUtils.showToast(getActivity(), "Please select Area");
        } else if (etMobile.getText().toString().isEmpty() || etMobile.getText().toString().length() != 10) {
            etMobile.setError("Please enter 10 digit mobile number");
            etMobile.requestFocus();
        } else if (!etPass.getText().toString().isEmpty()) {
            if (!etCnfPass.getText().toString().equalsIgnoreCase(etPass.getText().toString())) {
                etCnfPass.setError("Password and Confirm Password must be equal");
                etCnfPass.requestFocus();
            } else {
                updateProfile();
            }
        } else {
            updateProfile();
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String result) {
        switch (requestcode) {
            case Constants.REQ_ALL_STATE:
                try {
                    JSONObject response = new JSONObject(result);
                    int code = response.getInt("code");
                    if (code == 1) {
                        spState.setSelection(0);
                        stateList.clear();
                        JSONArray cityArr = response.getJSONArray("state");
                        state = new String[cityArr.length() + 1];
                        for (int i = 0; i < cityArr.length() + 1; i++) {
                            State statemodel = new State();
                            if (i == 0) {
                                state[0] = "Select state";
                                statemodel.setStateName("Select state");
                            } else {
                                JSONObject ob = cityArr.getJSONObject(i - 1).getJSONObject("State");
                                statemodel.setStateId(ob.getString("id"));
                                statemodel.setStateName(ob.getString("state_name"));
                                state[i] = ob.getString("state_name");
                            }
                            stateList.add(statemodel);
                        }
                        spState.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_layout, state));
                        for (int j = 0; j < stateList.size(); j++) {
                            if (stateList.get(j).getStateId().equalsIgnoreCase(User.getInstance().getStateId())) {
                                spState.setSelection(j);
                            }
                        }
                        //TODO
                     /*   if (User.getInstance().getStateId() != null || !User.getInstance().getStateId().equalsIgnoreCase("null") || !User.getInstance().getStateId().equalsIgnoreCase("")) {
                        spState.setSelection(Integer.parseInt(User.getInstance().getStateId()));
                        }else{
                        spState.setSelection(0);
                        }*/

                        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (spState.getSelectedItemPosition() == 0) {
                                    // do nothiing
                                } else {
                                    if (!stateList.get(i).getStateId().equalsIgnoreCase("")) {
                                        Log.e("if",User.getInstance().getStateId());
                                        stateId = stateList.get(i).getStateId();
                                    }
                                    else {
                                        Log.e("else",User.getInstance().getStateId());
                                        stateId = User.getInstance().getStateId();
                                    }
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put("state_id", stateId);
                                    Log.e("map values", map.toString());
                                    new CallService(MyProfile.this, getActivity(), Constants.REQ_CITY_ACC_STATE, map).execute(Constants.CITY_ACC_STATE);
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (ArrayIndexOutOfBoundsException ae) {
                    ae.printStackTrace();
                }
                break;
            case Constants.REQ_CITY_ACC_STATE:
                try {
                    JSONObject response = new JSONObject(result);
                    int code = response.getInt("code");
                    if (code == 1) {
                        spCity.setSelection(0);
                        cityList.clear();
                        JSONArray cityArr = response.getJSONArray("city");
                        city = new String[cityArr.length() + 1];
                        for (int i = 0; i < cityArr.length() + 1; i++) {
                            City citymodel = new City();
                            if (i == 0) {
                                city[0] = "Select Area";
                                citymodel.setCityName("Select Area");
                            } else {
                                JSONObject ob = cityArr.getJSONObject(i - 1).getJSONObject("City");
                                citymodel.setCityId(ob.getString("id"));
                                citymodel.setCityName(ob.getString("city_name"));
                                citymodel.setStateId(ob.getString("state_id"));
                                city[i] = ob.getString("city_name");
                            }
                            cityList.add(citymodel);
                        }
                        spCity.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_layout, city));
                        for (int j = 0; j < cityList.size(); j++) {
                            if (cityList.get(j).getCityId().equalsIgnoreCase(User.getInstance().getCityId())) {
                                spCity.setSelection(j);
                            }
                        }
                        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (spCity.getSelectedItemPosition() == 0) {
                                    // do nothiing
                                } else {
                                    cityId = cityList.get(i).getCityId();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_UPDATE_PROFILE:
                try {
                    Log.e(" response", result);
                    JSONObject object = new JSONObject(result);
                    int code = object.getInt("code");
                    llayout.setVisibility(View.VISIBLE);
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //       JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //   String countryId = user.getString("location");
                        //  String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode = "234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName, countryCode);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage("Profile updated Successfully.");
                        alert.setTitle("Reservation");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                etPass.setText("");
                                etCnfPass.setText("");
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HomeScreen(), "home screen");
                            }
                        });
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:
                etFname.setError(null);
                etLname.setError(null);
                etAddress1.setError(null);
                etAddress2.setError(null);
                etMobile.setError(null);
                etCnfPass.setError(null);
                validation();
                break;
        }
    }
}
//city_keyword_list