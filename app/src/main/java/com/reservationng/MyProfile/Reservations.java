package com.reservationng.MyProfile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.Hotels.ChangeHotelReservation;
import com.reservationng.R;
import com.reservationng.adapters.CombineBookingsAdapter;
import com.reservationng.adapters.CombineBookingsPrevAdapter;
import com.reservationng.models.CombineReservations;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Reservations extends Fragment implements View.OnClickListener, ServiceCallback {

    private TextView tvPrevRest, tvPrevResort, upComing, prev;
    private RecyclerView mRecyclerView, mRecyclerViewPrev;
    private TextView tvNoData;
    private CombineBookingsAdapter adapter;
    private CombineBookingsPrevAdapter adapter1;
    private ArrayList<CombineReservations> list = new ArrayList<CombineReservations>();
    CombineReservations model;
    private boolean flag = false, mmg, mmg1;
    public static LinearLayout llayout;
    public static Double lat, lng;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("resv flag", "" + flag);
        View v = inflater.inflate(R.layout.reservations, container, false);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        tvPrevRest = (TextView) v.findViewById(R.id.tvPrevRest);
        tvPrevResort = (TextView) v.findViewById(R.id.tvPrevResort);
        prev = (TextView) v.findViewById(R.id.prev);
        upComing = (TextView) v.findViewById(R.id.upComing);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        mRecyclerViewPrev = (RecyclerView) v.findViewById(R.id.mRecyclerViewPrev);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerViewPrev.setHasFixedSize(true);
        mRecyclerViewPrev.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        tvPrevRest.setOnClickListener(this);
        tvPrevResort.setOnClickListener(this);
        prev.setOnClickListener(this);
        upComing.setOnClickListener(this);

        upcomingBookings();

        return v;
    }


    public void upcomingBookings() {
//        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "1");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKINGS_COMBINE, map, false).execute(Constants.VIEW_BOOKINGS_COMBINE);

    }

    private void prevBookings() {
//        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        map.put("type", "2");
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_VIEW_BOOKINGS_COMBINE_PREV, map, false).execute(Constants.VIEW_BOOKINGS_COMBINE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.upComing:
                mRecyclerView.setVisibility(View.VISIBLE);
                mRecyclerViewPrev.setVisibility(View.GONE);
                upcomingBookings();
                break;

            case R.id.prev:
                mRecyclerView.setVisibility(View.GONE);
                mRecyclerViewPrev.setVisibility(View.VISIBLE);
                prevBookings();
                break;
        }
    }


    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_VIEW_BOOKINGS_COMBINE:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
                        JSONObject info = object.getJSONObject("info");
                        JSONArray restaurant = info.getJSONArray("restaurant");
                        JSONArray hotel = info.getJSONArray("hotel");

                        if (restaurant.length() > 0) {
                            mmg = false;
                            mRecyclerView.setVisibility(View.VISIBLE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.GONE);
                            for (int i = 0; i < restaurant.length(); i++) {
                                model = new CombineReservations();
                                JSONObject RestaurantBooking = restaurant.getJSONObject(i).getJSONObject("RestaurantBooking");
                                model.setBookingId(RestaurantBooking.getString("id"));
                                if (RestaurantBooking.isNull("time")) {
                                    model.setTimings("0");
                                } else {
                                    model.setTimings(RestaurantBooking.getString("time").substring(0, 5));
                                }
                                model.setDate(RestaurantBooking.getString("date"));
                                model.setNoOfPeople(RestaurantBooking.getString("no_of_person"));
                                model.setSpcReq(RestaurantBooking.getString("special_request"));
                                model.setTabletimeId(RestaurantBooking.getString("table_time_id"));
                                JSONObject Restaurant = restaurant.getJSONObject(i).getJSONObject("Restaurant");
                                model.setResid(Restaurant.getString("id"));
                                model.setImg(Restaurant.getString("image1"));
                                model.setName(Restaurant.getString("rest_name"));
                                model.setPhoneNo(Restaurant.getString("mobile_number"));

                                if (Restaurant.isNull("lat")) {
                                    model.setLat("0.0");

                                } else {
                                    model.setLat(Restaurant.getString("lat"));
                                }

                                if (Restaurant.isNull("long")) {
                                    model.setLng("0.0");

                                } else {
                                    model.setLng(Restaurant.getString("long"));
                                }
                                String add = "";

                                if (Restaurant.getString("address1").equalsIgnoreCase("")) {
                                    add = Restaurant.getString("address1");
                                } else if (Restaurant.getString("address2").equalsIgnoreCase("")) {
                                    add = Restaurant.getString("address2");
                                } else {
                                    add = Restaurant.getString("address1") + ", " + Restaurant.getString("address2");
                                }
                                if (Restaurant.getString("address1").equalsIgnoreCase("") && Restaurant.getString("address1").equalsIgnoreCase("")) {
                                    add = "N/A";
                                }
                                model.setAddress(add);
                                model.setDesc(Restaurant.getString("description"));
                                model.setRewardPt(Restaurant.getString("reward_point"));
                                model.setType("rest");
                                list.add(model);
                            }
                        }
                        if (hotel.length() > 0) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.GONE);
                            mmg1 = false;
                            for (int i = 0; i < hotel.length(); i++) {

                                model = new CombineReservations();
                                JSONObject HotelCalculate = hotel.getJSONObject(i).getJSONObject("HotelCalculate");
                                model.setId(HotelCalculate.getString("id"));  //booking id
                                model.setHotelId(HotelCalculate.getString("hotel_id"));
                                model.setCheckIn(HotelCalculate.getString("check_in"));
                                model.setCheckOut(HotelCalculate.getString("check_out"));
                                model.setTotadult(HotelCalculate.getString("no_adult"));
                                model.setTotchild(HotelCalculate.getString("no_child"));
                                model.setTotRooms(HotelCalculate.getString("no_room"));
                                model.setRoomType(HotelCalculate.getString("room_type"));
                                model.setPrice(HotelCalculate.getString("total_price"));
                                JSONObject Hotel = hotel.getJSONObject(i).getJSONObject("Hotel");
                                model.setHotelImg(Hotel.getString("image1"));
                                model.setHotelName(Hotel.getString("hotel_name"));
                                model.setCuisines(Hotel.getString("cuisines"));
                                model.setMobileNo(Hotel.getString("mobile_number"));

                                String add = "";

                                if (Hotel.getString("address1").equalsIgnoreCase("")) {
                                    add = Hotel.getString("address1");
                                } else if (Hotel.getString("address2").equalsIgnoreCase("")) {
                                    add = Hotel.getString("address2");
                                } else {
                                    add = Hotel.getString("address1") + ", " + Hotel.getString("address2");
                                }
                                if (Hotel.getString("address1").equalsIgnoreCase("") && Hotel.getString("address1").equalsIgnoreCase("")) {
                                    add = "N/A";
                                }

                                model.setAddress(add);

                                if (Hotel.isNull("lat")) {
                                    model.setLat("0.0");
                                } else {
                                    model.setLat(Hotel.getString("lat"));
                                }
                                if (Hotel.isNull("lat")) {
                                    model.setLng("0.0");
                                } else {
                                    model.setLng(Hotel.getString("long"));
                                }
                                model.setType("resort");
                                list.add(model);
                            }
                        }
                        Log.e("list size >>>>>",""+list.size()+ mmg+ "   "+mmg1);
                        if(list.size()>0) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.GONE);
                            upComing.setText("UPCOMING RESERVATIONS (" + list.size() + " )");
                        }else{
                            mRecyclerView.setVisibility(View.GONE);
                            upComing.setVisibility(View.GONE);
                            prev.setVisibility(View.GONE);
                            mRecyclerViewPrev.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setGravity(Gravity.CENTER);
                            tvNoData.setTextColor(getResources().getColor(R.color.light_grey));
                            tvNoData.setText("Book a table\n" +
                                    "Start earning points and get closer to your free meal.");

                        }
                        adapter = new CombineBookingsAdapter(getActivity(), list);
                        mRecyclerView.setAdapter(adapter);
                        adapter.setOnItemClickListener(new CombineBookingsAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                if (list.get(position).getType().equalsIgnoreCase("rest")) {
                                    Fragment fragment = new RestResvListingUpdate();
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("check", true);
                                    bundle.putString("table_time_id", list.get(position).getTabletimeId());
                                    bundle.putString("rest_name", list.get(position).getName());
                                    bundle.putString("noOfPeople", list.get(position).getNoOfPeople());
                                    bundle.putString("date", list.get(position).getDate());
                                    bundle.putString("image1", list.get(position).getImg());
                                    bundle.putString("booking_id", list.get(position).getBookingId());
                                    bundle.putString("time", list.get(position).getTimings());
                                    bundle.putString("mobile_number", list.get(position).getPhoneNo());
                                    bundle.putString("address", list.get(position).getAddress());
                                    bundle.putString("rest_id", list.get(position).getResid());
                                    bundle.putString("desc", list.get(position).getDesc());
                                    bundle.putString("special_request", list.get(position).getSpcReq());
                                    DataHolder.getInstance().destLati = list.get(position).getLat();
                                    DataHolder.getInstance().destLng = list.get(position).getLng();
                                    fragment.setArguments(bundle);
                                    Log.e("upd rest resv", bundle.toString());
                                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestResvListingUpdate.class.getSimpleName());
                                }
                                if (list.get(position).getType().equalsIgnoreCase("resort")) {
                                    Fragment fragment = new ChangeHotelReservation();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("bookingId", list.get(position).getId());
                                    bundle.putString("image1", list.get(position).getHotelImg());
                                    bundle.putString("hotel_name", list.get(position).getHotelName());
                                    bundle.putString("check_in", list.get(position).getCheckIn());
                                    bundle.putString("check_out", list.get(position).getCheckOut());
                                    bundle.putString("no_room", list.get(position).getTotRooms());
                                    bundle.putString("mobile_number", list.get(position).getMobileNo());
                                    bundle.putString("address", list.get(position).getAddress());
                                    bundle.putString("room_type", list.get(position).getRoomType());
                                    HotelDataHolder.getInstance().desLat = list.get(position).getLat();
                                    HotelDataHolder.getInstance().desLng = list.get(position).getLng();
                                    fragment.setArguments(bundle);
                                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ChangeHotelReservation.class.getSimpleName());
                                }
                            }
                        });
                    } else if (code == 0) {
                        prev.setVisibility(View.GONE);
                        upComing.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                        mRecyclerViewPrev.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setGravity(Gravity.CENTER);
                        tvNoData.setTextColor(getResources().getColor(R.color.light_grey));
                        tvNoData.setText("Book a table\n" + "Start earning points and get closer to your free meal.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_VIEW_BOOKINGS_COMBINE_PREV:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        list.clear();
//                        mRecyclerViewPrev.setVisibility(View.VISIBLE);
//                        mRecyclerView.setVisibility(View.GONE);
//                        tvNoData.setVisibility(View.GONE);
                        JSONObject info = object.getJSONObject("info");
                        JSONArray restaurant = info.getJSONArray("restaurant");
                        JSONArray hotel = info.getJSONArray("hotel");

                        if (restaurant.length() > 0) {

                            mRecyclerView.setVisibility(View.GONE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);

                            for (int i = 0; i < restaurant.length(); i++) {
                                model = new CombineReservations();
                                JSONObject RestaurantBooking = restaurant.getJSONObject(i).getJSONObject("RestaurantBooking");
                                model.setBookingId(RestaurantBooking.getString("id"));
                                if (RestaurantBooking.isNull("time")) {
                                    model.setTimings("0");
                                } else {
                                    model.setTimings(RestaurantBooking.getString("time").substring(0, 5));
                                }
                                model.setDate(RestaurantBooking.getString("date"));
                                model.setNoOfPeople(RestaurantBooking.getString("no_of_person"));
                                model.setCustomer_arrival_confirm(RestaurantBooking.getString("customer_arrival_confirm"));

                                JSONObject Restaurant = restaurant.getJSONObject(i).getJSONObject("Restaurant");
                                model.setResid(RestaurantBooking.getString("restaurant_id"));
                                model.setImg(Restaurant.getString("image1"));
                                model.setName(Restaurant.getString("rest_name"));
                                model.setRewardPt(Restaurant.getString("reward_point"));

                                JSONArray RestaurantReview = restaurant.getJSONObject(i).getJSONArray("RestaurantReview");
                                if (RestaurantReview.length() > 0) {
                                    for (int j = 0; j < RestaurantReview.length(); j++) {
                                        model.setNoiseLvl(RestaurantReview.getJSONObject(j).getString("noise_level"));
                                        model.setReview(RestaurantReview.getJSONObject(j).getString("review"));
                                        model.setRating(RestaurantReview.getJSONObject(j).getString("overall_rating"));
                                        model.setFood(RestaurantReview.getJSONObject(j).getString("food_rating"));
                                        model.setService(RestaurantReview.getJSONObject(j).getString("service_rating"));
                                        model.setAmbience(RestaurantReview.getJSONObject(j).getString("ambience_rating"));
                                        model.setFlag(true);  //view a review
                                    }
                                } else {
                                    model.setFlag(false);  //write a review
                                }
                                model.setType("rest");
                                list.add(model);
                            }
                        }

                        if (hotel.length() > 0) {
                            mRecyclerView.setVisibility(View.GONE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);

                            for (int i = 0; i < hotel.length(); i++) {


                                model = new CombineReservations();
                                JSONObject HotelCalculate = hotel.getJSONObject(i).getJSONObject("HotelCalculate");
                                model.setId(HotelCalculate.getString("id"));  //booking id same as hotel_book_id in HotelReview
                                model.setHotelId(HotelCalculate.getString("hotel_id"));
                                model.setCheckIn(HotelCalculate.getString("check_in"));
                                model.setCheckOut(HotelCalculate.getString("check_out"));
                                model.setTotadult(HotelCalculate.getString("no_adult"));
                                model.setTotchild(HotelCalculate.getString("no_child"));
                                model.setTotRooms(HotelCalculate.getString("no_room"));
                                model.setRoomType(HotelCalculate.getString("room_type"));
                                model.setPrice(HotelCalculate.getString("total_price"));

                                JSONObject Hotel = hotel.getJSONObject(i).getJSONObject("Hotel");
                                model.setHotelImg(Hotel.getString("image1"));
                                model.setHotelName(Hotel.getString("hotel_name"));
                                model.setCuisines(Hotel.getString("cuisines"));
                                model.setMobileNo(Hotel.getString("mobile_number"));
                                model.setAddress(Hotel.getString("address1") + " " + Hotel.getString("address2"));
                                JSONArray HotelReview = hotel.getJSONObject(i).getJSONArray("HotelReview");
                                if (HotelReview.length() > 0) {
                                    for (int j = 0; j < HotelReview.length(); j++) {
                                        model.setNoiseLvl(HotelReview.getJSONObject(j).getString("noise_level"));
                                        model.setReview(HotelReview.getJSONObject(j).getString("review"));
                                        model.setRating(HotelReview.getJSONObject(j).getString("overall_rating"));
                                        model.setFood(HotelReview.getJSONObject(j).getString("food_rating"));
                                        model.setService(HotelReview.getJSONObject(j).getString("service_rating"));
                                        model.setFlag(true);  //view a review
                                    }
                                } else {
                                    model.setFlag(false);  //write a review
                                }
                                model.setType("resort");
                                list.add(model);
                            }
                        }

                        if(list.size()>0) {
                            mRecyclerView.setVisibility(View.GONE);
                            upComing.setVisibility(View.VISIBLE);
                            prev.setVisibility(View.VISIBLE);
                            mRecyclerViewPrev.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);
                            prev.setText("PREVIOUS RESERVATIONS (" + list.size() + " )");
                        }else{
                            mRecyclerView.setVisibility(View.GONE);
                            upComing.setVisibility(View.GONE);
                            prev.setVisibility(View.GONE);
                            mRecyclerViewPrev.setVisibility(View.GONE);

                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setGravity(Gravity.CENTER);
                            tvNoData.setTextColor(getResources().getColor(R.color.light_grey));
                            tvNoData.setText("Book a table\n" +
                                    "Start earning points and get closer to your free meal.");

                        }
                        adapter1 = new CombineBookingsPrevAdapter(getActivity(), list);
                        mRecyclerViewPrev.setAdapter(adapter1);
                    } else if (code == 0) {
                        prev.setVisibility(View.GONE);
                        upComing.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                        mRecyclerViewPrev.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("Book a table\n" + "Start earning points and get closer to your free meal.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !flag ) {
            fetchCurrentLoc();
            upcomingBookings();
            flag = true;
        }
    }*/


}

