package com.reservationng.MyProfile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.ReviewResortAdapter;
import com.reservationng.adapters.ReviewRestAdapter;
import com.reservationng.models.ReviewDetails;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Reviews extends Fragment implements ServiceCallback {

    private RecyclerView rvRest, rvResort;
    private TextView tvNoData, tvHotel, tvRest;
    private ArrayList<ReviewDetails> list = new ArrayList<ReviewDetails>();
    private ArrayList<ReviewDetails> resortlist = new ArrayList<ReviewDetails>();
    private ReviewRestAdapter adapter;
    private ReviewResortAdapter resortAdapter;
    private LinearLayout llayout;
    View v;
    private boolean flag = false;
    private  boolean flag1=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.reviews_list, container, false);
        rvRest = (RecyclerView) v.findViewById(R.id.rvRest);
        rvResort = (RecyclerView) v.findViewById(R.id.rvResort);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        tvHotel = (TextView) v.findViewById(R.id.tvHotel);
        tvRest = (TextView) v.findViewById(R.id.tvRest);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        rvRest.setHasFixedSize(true);
        rvRest.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvResort.setHasFixedSize(true);
        rvResort.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        restReviews();
        resortReviews();

        return v;
    }

    public void restReviews() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_SHOW_USER_REVIEW_RESTAURANT, map,false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.SHOW_USER_REVIEW_RESTAURANT);
    }

    public void resortReviews() {
        llayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_SHOW_USER_REVIEW_HOTEL, map,false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.SHOW_USER_REVIEW_HOTEL);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_SHOW_USER_REVIEW_RESTAURANT:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);

                    int code = object.getInt("code");
                    if (code == 1) {
                        Log.e("Enter in 1","yesss");

                        llayout.setVisibility(View.VISIBLE);
                        rvRest.setVisibility(View.VISIBLE);
                        tvRest.setVisibility(View.VISIBLE);
                        list.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReviewDetails model = new ReviewDetails();
                            JSONObject HotelReview = info.getJSONObject(i).getJSONObject("RestaurantReview");
                            JSONObject Restaurant = info.getJSONObject(i).getJSONObject("Restaurant");
                            model.setId(HotelReview.getString("id")); //review id
                            model.setNoiseLvl(HotelReview.getString("noise_level"));
                            model.setReview(HotelReview.getString("review"));
                            model.setRating(HotelReview.getString("overall_rating"));
                            model.setFood(HotelReview.getString("food_rating"));
                            model.setService(HotelReview.getString("service_rating"));
                            model.setAmbience(HotelReview.getString("ambience_rating"));
                            model.setRestName(Restaurant.getString("rest_name"));
                            list.add(model);
                        }
                        adapter = new ReviewRestAdapter(getActivity(), list);
                        rvRest.setAdapter(adapter);
                    } else if (code == 0) {
                        Log.e("Enter in 1,0","yesss");

                        list.clear();
                        rvRest.setVisibility(View.GONE);
                        tvRest.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    if (list.size() == 0) {
                        flag=true;
                    }
//                        Log.e("Restaurant", "inside restaurant");
//                        Log.e("LISTINFIRT",list.size()+" "+resortlist.size());
//                        llayout.setVisibility(View.VISIBLE);
//                        rvRest.setVisibility(View.GONE);
//                        rvResort.setVisibility(View.GONE);
//                        tvRest.setVisibility(View.GONE);
//                        tvHotel.setVisibility(View.GONE);
//                        tvNoData.setVisibility(View.VISIBLE);
//                        tvNoData.setText("Make a booking, write a review and earn points towards a free meal.>>>>>restaurant");
//                    } else {
//                        Log.e("Restaurant", "inside else restaurant" + list.size());
//                    }
//                    Reservations.llayout.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_SHOW_USER_REVIEW_HOTEL:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");

                    if (code == 1) {
                        Log.e("Enter in 2","yesss");

                        resortlist.clear();
                        llayout.setVisibility(View.VISIBLE);
                        tvHotel.setVisibility(View.VISIBLE);
                        rvResort.setVisibility(View.VISIBLE);
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            ReviewDetails model = new ReviewDetails();
                            JSONObject HotelReview = info.getJSONObject(i).getJSONObject("HotelReview");
                            JSONObject Hotel = info.getJSONObject(i).getJSONObject("Hotel");
                            model.setId(HotelReview.getString("id")); //review id
                            model.setNoiseLvl(HotelReview.getString("noise_level"));
                            model.setReview(HotelReview.getString("review"));
                            model.setRating(HotelReview.getString("overall_rating"));
                            model.setFood(HotelReview.getString("food_rating"));
                            model.setService(HotelReview.getString("service_rating"));
                            model.setRestName(Hotel.getString("hotel_name"));
                            resortlist.add(model);
                        }
                        resortAdapter = new ReviewResortAdapter(getActivity(), resortlist);
                        rvResort.setAdapter(resortAdapter);
                    } else if (code == 0) {
                        Log.e("Enter in 2,0","yesss");

                        resortlist.clear();
                        rvResort.setVisibility(View.GONE);
                        tvHotel.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.GONE);
                    }
                    if (list.size() == 0) {
                        flag1=true;
                    }
//                        Log.e("LISTINSECOND",list.size()+" "+resortlist.size());
//                        Log.e("hotel", "inside hotel");
//                        llayout.setVisibility(View.VISIBLE);
//                        rvRest.setVisibility(View.GONE);
//                        rvResort.setVisibility(View.GONE);
//                        tvHotel.setVisibility(View.GONE);
//                        tvRest.setVisibility(View.GONE);
//                        tvNoData.setVisibility(View.VISIBLE);
//                        tvNoData.setText("Make a booking, write a review and earn points towards a free meal.>>>>resort");
//                    } else {
////                        tvNoData.setText("No data Found !!!");
//                        Log.e("Resort", "inside else hotel" + resortlist.size());
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
        if (flag && flag1)
        {
            llayout.setVisibility(View.VISIBLE);
                        rvRest.setVisibility(View.GONE);
                        rvResort.setVisibility(View.GONE);
                        tvHotel.setVisibility(View.GONE);
                        tvRest.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("Make a booking, write a review and earn points towards a free meal.>>>>resort");
        }
    }

   /* @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !flag ) {
            restReviews();
            resortReviews();
            flag = true;
        }
    }*/
}
