package com.reservationng.MyProfile;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.reservationng.CMS.AboutPoints;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;

public class Points extends Fragment implements View.OnClickListener, ServiceCallback {

    private TextView tvAboutPt, tvdetails, tvEarnedPt, tvNextReward, tvperc, tvpoints;

    private ProgressBar progress;
    private LinearLayout llayout, llpoints;
    View v;
    private boolean flag = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.points, container, false);
        Log.e("points flag",""+flag);
        tvdetails = (TextView) v.findViewById(R.id.tvdetails);
        tvEarnedPt = (TextView) v.findViewById(R.id.tvEarnedPt);
        tvNextReward = (TextView) v.findViewById(R.id.tvNextReward);
        tvperc = (TextView) v.findViewById(R.id.tvperc);
        tvAboutPt = (TextView) v.findViewById(R.id.tvAboutPt);
        tvpoints = (TextView) v.findViewById(R.id.tvpoints);
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        llpoints = (LinearLayout) v.findViewById(R.id.llpoints);
        progress = (ProgressBar) v.findViewById(R.id.progress);
//          to change color of the progress bar
//          progress.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        progress.setMax(100);
        tvAboutPt.setOnClickListener(this);
        points();

        return v;
    }

    private void points() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", User.getInstance().getUserId());
        Log.e("values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_POINTS, map).execute(Constants.POINTS);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAboutPt:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new AboutPoints(), AboutPoints.class.getSimpleName());
                break;
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_POINTS:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        tvpoints.setVisibility(View.VISIBLE);
                        llayout.setVisibility(View.VISIBLE);
                        tvpoints.setVisibility(View.VISIBLE);
                        tvEarnedPt.setVisibility(View.VISIBLE);
                        tvNextReward.setVisibility(View.VISIBLE);
                        progress.setVisibility(View.VISIBLE);
                        tvperc.setVisibility(View.VISIBLE);
                        tvdetails.setVisibility(View.VISIBLE);
                        if (object.getString("points_gain").equalsIgnoreCase("0")) {
                            tvEarnedPt.setText("Earned Points\n0 point");
                        } else {
                            tvEarnedPt.setText("Earned Points" + "\n" + object.getString("points_gain") + " points");
                        }
                        tvNextReward.setText("Next Reward\n15000 points");
                        progress.setProgress(object.getInt("perctange"));
                        int pointDiff = 15000 - object.getInt("points_gain");
                        String perc = object.getString("perctange");
                        NumberFormat numberFormat = NumberFormat.getInstance();
                        numberFormat.setMinimumFractionDigits(2);
                        numberFormat.setMaximumFractionDigits(2);
                        Number number = numberFormat.parse(perc);
                        tvperc.setVisibility(View.GONE);
                        tvperc.setText(numberFormat.format(number) + "%");
                        tvdetails.setText("You are " + String.valueOf(pointDiff) + " points away from a free meal.");                    }
                    else if (code == 0) {
                        llayout.setVisibility(View.VISIBLE);
                        tvpoints.setVisibility(View.GONE);
                        tvEarnedPt.setVisibility(View.GONE);
                        tvNextReward.setVisibility(View.GONE);
                        progress.setVisibility(View.GONE);
                        tvperc.setVisibility(View.GONE);
                        tvdetails.setVisibility(View.VISIBLE);
                        tvdetails.setGravity(Gravity.CENTER);
                        llpoints.setBackground(getResources().getDrawable(R.color.light_grey));
                        tvdetails.setBackground(getResources().getDrawable(R.color.light_grey));
                        tvdetails.setTextSize((float) 18.0);
                        tvdetails.setText("You haven't earned any points...yet.\n No worries, just make and honour an \n Reservation.ng booking, we 'll\n reward you for your tasteful behaviour.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !flag) {
//            points();
            flag = true;
        }
    }
}
