package com.reservationng.MyProfile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.reservationng.CommonUtils;
import com.reservationng.CustomMapClasses.TouchableMapFragment;
import com.reservationng.R;
import com.reservationng.models.DataHolder;
import com.reservationng.models.TimeSlots;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

public class RestResvListingUpdate extends Fragment implements  NumberPicker.OnValueChangeListener,ServiceCallback,OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener, View.OnClickListener, GoogleMap.OnMarkerClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener, com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener {


    private TextView txtRestName, tvMobileNo, tvAddress,tvdate,tvdte,tvtime,tvPeople,tvTick,txtNoSlot;
    private String MyDate1,day,mnth,date,noOfpeople,dt,dateToSend,timeToSend,x,y,showTime,slotId="";
    private CircleImageView ivRestImg;
    private ImageView ivImg;
    private AQuery aQuery=new AQuery(getActivity());
    private ArrayList<String> peopleList=new ArrayList<>();
    private LinearLayout linearSlots;
    private boolean check=false,flag=false;
    private HorizontalScrollView horzscroll;
    private Button btnUpdate,btnCancel;
    private EditText etSpecialReq;
    private ScrollView sv;
    private FrameLayout framelayout;
    private GoogleMap map;
    private TouchableMapFragment mapFragment;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.rest_resv_listing, container, false);
        getActivity().setTitle("Reservation Confirmed !");
        try {
            framelayout = (FrameLayout) v.findViewById(R.id.framelayout);
            ivImg = (ImageView) v.findViewById(R.id.ivImg);
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            txtNoSlot = (TextView) v.findViewById(R.id.txtNoSlot);
            tvMobileNo = (TextView) v.findViewById(R.id.tvMobileNo);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            tvdate = (TextView) v.findViewById(R.id.tvdate);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            tvTick = (TextView) v.findViewById(R.id.tvTick);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);
            linearSlots = (LinearLayout) v.findViewById(R.id.linearSlots);
            horzscroll = (HorizontalScrollView) v.findViewById(R.id.horzscroll);
            btnCancel = (Button) v.findViewById(R.id.btnCancel);
            btnUpdate = (Button) v.findViewById(R.id.btnUpdate);
            etSpecialReq = (EditText) v.findViewById(R.id.etSpecialReq);
            sv = (ScrollView) v.findViewById(R.id.sv);
            txtRestName.setText(getArguments().getString("rest_name"));
            tvMobileNo.setText(getArguments().getString("mobile_number"));
            tvAddress.setText(getArguments().getString("address"));
            etSpecialReq.setText(getArguments().getString("special_request"));

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dt=getArguments().getString("date");
            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMM d");
            MyDate1 = newDateFormat.format(MyDate);
            StringTokenizer tokenizer=new StringTokenizer(MyDate1," ");
            while (tokenizer.hasMoreTokens()){
                day=tokenizer.nextToken();
                mnth=tokenizer.nextToken();
                date=tokenizer.nextToken();
            }
            for (int i = 1; i <=20 ; i++) {
                peopleList.add(""+i);
            }
            check=getArguments().getBoolean("check");
            tvPeople.setText( getArguments().getString("noOfPeople"));
            tvdte.setText(day+", "+mnth+" "+date);
            showTime=getArguments().getString("time");
            SimpleDateFormat dateFormat=new SimpleDateFormat("H:mm");
            Date date1=dateFormat.parse(showTime);
            tvtime.setText(new SimpleDateFormat("hh:mm a").format(date1));
            if(check){
                dateToSend=getArguments().getString("date");
                timeToSend=getArguments().getString("time");
                tvdate.setText("Table for " + getArguments().getString("noOfPeople") + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            }

            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("image1"));
            aQuery.id(ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + getArguments().getString("image1"));

            tvdte.setOnClickListener(this);
            tvtime.setOnClickListener(this);
            tvPeople.setOnClickListener(this);
            tvTick.setOnClickListener(this);
            btnUpdate.setOnClickListener(this);
            btnCancel.setOnClickListener(this);
            tvMobileNo.setOnClickListener(this);
            initMap();
        }catch (Exception e){
            e.printStackTrace();
        }
        return v;
    }

    private void initMap() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == resultCode) {
            mapFragment = (TouchableMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode, getActivity(),
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                CommonUtils.showDialog(getActivity(),
                        "Google Play Services not available");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map == null) {
            map = googleMap;
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.setOnMapLoadedCallback(this);
            map.setOnMapClickListener(this);
            map.setOnMarkerClickListener(this);
        }
    }

    private void showMarkers() {
        LatLng latLng = new LatLng(Double.valueOf(DataHolder.getInstance().curLati), Double.valueOf(DataHolder.getInstance().curLng));
        LatLng dlatLng = new LatLng(Double.parseDouble(DataHolder.getInstance().destLati), Double.parseDouble(DataHolder.getInstance().destLng));
        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_marker)).title("Its You"));
        map.addMarker(new MarkerOptions().position(dlatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(DataHolder.getInstance().name));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(dlatLng, 10.0f));
    }

    @Override
    public void onMapLoaded() {
        try {
            String slng = String.valueOf(map.getMyLocation().getLongitude());
            String slat = String.valueOf(map.getMyLocation().getLatitude());
            Log.e("loc >>",""+map.getMyLocation().getLongitude());
            Log.e("loc >>",""+map.getMyLocation().getLatitude());
            if(slat.equalsIgnoreCase("")){
                DataHolder.getInstance().curLati = "0.0";
            }else {
                DataHolder.getInstance().curLati = slat;
            }

            if(slng.equalsIgnoreCase("")){
                DataHolder.getInstance().curLng = "0.0";
            }else {
                DataHolder.getInstance().curLng = slng;
            }

            showMarkers();
        }catch (NullPointerException e){
            e.printStackTrace();
        }catch (NumberFormatException ne){
            ne.printStackTrace();
        }

    }

    @Override
    public void onMapClick(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new
                ContextThemeWrapper(getActivity(), android.R.style.
                Theme_Holo_Light));
        builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
        builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMap();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equalsIgnoreCase(DataHolder.getInstance().name)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new
                    ContextThemeWrapper(getActivity(), android.R.style.
                    Theme_Holo_Light));
            builder.setMessage("\"Reservation.ng\" wants to open \"Google Maps\"");
            builder.setPositiveButton("Open", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showMap();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return false;
    }

    private void showMap() {
        Double slati=0.0;
        Double slng=0.0;
        if(DataHolder.getInstance().curLati.equalsIgnoreCase("")){
            slati = 0.0;
        }else {
            slati = Double.valueOf(DataHolder.getInstance().curLati);
        }

        if(DataHolder.getInstance().curLng.equalsIgnoreCase("")){
            slng = 0.0;
        }else {
            slng = Double.valueOf(DataHolder.getInstance().curLng);
        }

        Double dlati = Double.parseDouble(DataHolder.getInstance().destLati);
        Double dlng = Double.parseDouble(DataHolder.getInstance().destLng);
        Log.e("on map", "dest" + dlati + "..." + dlng);
        Log.e("on map", "src" + slati + "..." + slng);
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", slati, slng, "Current Location", dlati, dlng, DataHolder.getInstance().address);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);

    }

    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                RestResvListingUpdate.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.e("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }


    private void customDatePicker(){
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                RestResvListingUpdate.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void showDatePickerDialog() {
        final long current = System.currentTimeMillis();
        final Calendar calendarTwoWeeksInFuture = Calendar.getInstance();
        int year = calendarTwoWeeksInFuture.get(Calendar.YEAR);
        int month = calendarTwoWeeksInFuture.get(Calendar.MONTH);
        int day1 = calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                check=false;
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                if (monthOfYear < 10) {
                    monthOfYear = monthOfYear + 1;
                    x = "0" + monthOfYear;
                }else{
                    monthOfYear = monthOfYear + 1;
                    x= String.valueOf(monthOfYear);
                }
                if (calendar.getTimeInMillis() < current) {
                    showDatePickerDialog();
                    Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        String setDate=dayOfMonth + "/" + x + "/" + year;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date1 = dateFormat.parse(setDate);
                        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date2 = newDateFormat.parse(setDate);
                        dateToSend= CommonUtils.target.format(date1);
                        newDateFormat.applyPattern("EEE MMM d");
                        String MyDate1 = newDateFormat.format(date2);
                        StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                        while (tokenizer.hasMoreTokens()) {
                            day = tokenizer.nextToken();
                            mnth = tokenizer.nextToken();
                            date = tokenizer.nextToken();
                            tvdte.setText( day + ", " + mnth + " " + date);
                            tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                            tvTick.setVisibility(View.VISIBLE);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }, calendarTwoWeeksInFuture.get(Calendar.YEAR), calendarTwoWeeksInFuture.get(Calendar.MONTH), calendarTwoWeeksInFuture.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMinDate(current);
        mDatePickerDialog.show();
    }


    public void peopleShow(){
        final Dialog d = new Dialog(getActivity());
        d.setTitle("Select People");
        d.setContentView(R.layout.number_picker);
        Button btnSet = (Button) d.findViewById(R.id.btnSet);
        Button btncancel = (Button) d.findViewById(R.id.btncancel);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(20);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        btnSet.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                check=false;
                tvPeople.setText(String.valueOf(np.getValue()));
                tvdate.setText("Table for " +  tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + getArguments().getString("time"));
                tvTick.setVisibility(View.VISIBLE);
                d.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tvPeople.setText(tvPeople.getText().toString());
                d.dismiss();
            }
        });
        d.show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tvMobileNo:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getArguments().getString("mobile_number")));
                startActivity(intent);
                break;
            case R.id.tvTick:
                timeSlot();
                break;
            case R.id.tvdte:
                customDatePicker();
                break;

            case R.id.tvtime:
                customTimePicker();
                break;

            case R.id.tvPeople:
                peopleShow();
                break;
            case R.id.btnCancel:
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", getArguments().getString("booking_id"));
                sv.setVisibility(View.GONE);
                new CallService(this, getActivity(), Constants.REQ_CANCEL_RESTAURANT_BOOKING, map).execute(Constants.CANCEL_RESTAURANT_BOOKING);
                break;

            case R.id.btnUpdate:
                updateBooking();
                break;
        }
    }

    private void updateBooking() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("id", getArguments().getString("booking_id"));
        map.put("date", dateToSend);
        if(slotId.equalsIgnoreCase("")) {
            map.put("table_time_id", getArguments().getString("table_time_id"));
        }else{
            map.put("table_time_id",slotId);
        }
        if(tvPeople.getText().toString().trim().equalsIgnoreCase(getArguments().getString("noOfPeople"))) {
            map.put("no_of_person", getArguments().getString("noOfPeople"));
        }else{
            map.put("no_of_person", tvPeople.getText().toString().trim());
        }
        map.put("special_request", etSpecialReq.getText().toString().trim());
        Log.e("map values", map.toString());
        sv.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_MODIFY_BOOKING, map).execute(Constants.RESTAURANT_MODIFY_BOOKING);
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void timeSlot(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("rest_id", getArguments().getString("rest_id"));
        map.put("date", dateToSend);
        map.put("time", timeToSend);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REST_TIME_SLOT, map).execute(Constants.REST_TIME_SLOT);
    }
    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode){
            case Constants.REQ_REST_TIME_SLOT:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        linearSlots.removeAllViews();
                        TimeSlots[] slots;
                        horzscroll.setVisibility(View.VISIBLE);
                        txtNoSlot.setVisibility(View.GONE);
                        tvTick.setVisibility(View.GONE);
                        JSONObject info = object.getJSONObject("info");
                        JSONArray TableTime = info.getJSONArray("TableTime");
                        slots= new TimeSlots[TableTime.length()];
                        for (int l = 0; l < TableTime.length(); l++) {
                            JSONObject ob = TableTime.getJSONObject(l);
                            TimeSlots slot = new TimeSlots();
                            slot.setRestTableId(ob.getString("restaurant_table_id"));
                            slot.setTiming(ob.getString("timing"));
                            slot.setSlotId(ob.getString("id"));
                            slots[l] = slot;
                            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            buttonLayoutParams.setMargins(5, 5, 20, 5);
                            final Button  btn = new Button(getActivity());
                            final TimeSlots slot1 = slots[l];
                            btn.setLayoutParams(buttonLayoutParams);
                            String time = slot1.getTiming().substring(0, 5);
                            try {
                                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                final Date dateObj = sdf.parse(time);
                                btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
                            } catch (final Exception e) {
                                e.printStackTrace();
                            }
                            btn.setTextColor(Color.WHITE);
                            btn.setBackgroundResource(R.drawable.btn_reser_login);
                            btn.setPadding(5,0,5,0);
                            btn.setFocusableInTouchMode(true);
                            btn.setFocusable(true);
                            btn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if(hasFocus){
                                        btn.setPadding(5,0,5,0);
                                        btn.setBackgroundResource(R.drawable.btn_bg_orange);
                                        slotId=slot1.getSlotId();
                                        Log.e("slot id", slot1.getSlotId());
                                    }else{
                                        btn.setPadding(5,0,5,0);
                                        btn.setBackgroundResource(R.drawable.btn_reser_login);
                                    }
                                }
                            });

                            linearSlots.addView(btn);
                        }
                        Log.e("tme slts in rsrve ", "" + slots.length);
                    }else if(code==0){
                        horzscroll.setVisibility(View.GONE);
                        txtNoSlot.setVisibility(View.VISIBLE);
                        txtNoSlot.setText("We're sorry, nothing's available around this time.Try another date or time perhaps ?");
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;

            case Constants.REQ_CANCEL_RESTAURANT_BOOKING:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been Cancelled Successfully");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sv.setVisibility(View.VISIBLE);
                                getActivity().onBackPressed();
//                                this.finish();
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_RESTAURANT_MODIFY_BOOKING:
                try {
                    Log.e("response", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Thank you");
                        builder.setMessage("Your Booking has been Updated Successfully.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sv.setVisibility(View.VISIBLE);
                                getActivity().onBackPressed();
//                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new UpcomingRestaurants(), UpcomingRestaurants.class.getSimpleName());
//                               finish();

                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog,int year, int monthOfYear, int dayOfMonth) {
        check=false;
        final long current = System.currentTimeMillis();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (monthOfYear < 10) {
            monthOfYear = monthOfYear + 1;
            x = "0" + monthOfYear;
        }else{
            monthOfYear = monthOfYear + 1;
            x= String.valueOf(monthOfYear);
        }
        if (dayOfMonth < 10) {
            y = "0" + dayOfMonth;
        } else {
            y = String.valueOf(dayOfMonth);
        }
        if (calendar.getTimeInMillis() < current) {
            showDatePickerDialog();
            Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
        } else {
            try {
                String setDate=dayOfMonth + "/" + x + "/" + year;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date1 = dateFormat.parse(setDate);
                SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date2 = newDateFormat.parse(setDate);
                dateToSend= CommonUtils.target.format(date1);
                newDateFormat.applyPattern("EEE MMM d");
                String MyDate1 = newDateFormat.format(date2);
                StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                while (tokenizer.hasMoreTokens()) {
                    day = tokenizer.nextToken();
                    mnth = tokenizer.nextToken();
                    date = tokenizer.nextToken();
                    tvdte.setText( day + ", " + mnth + " " + date);
                    tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
                    tvTick.setVisibility(View.VISIBLE);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        check=false;
        String AM_PM = " AM";
        String mm_precede = "";
        int hourOfDay1=0;
        if (minute < 10) {
            mm_precede = "0";
        }
        if (hourOfDay >= 12) {
            AM_PM = " PM";
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay1 =hourOfDay- 12;
            } else {
                hourOfDay1 = 12;
            }
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        } else if (hourOfDay == 0) {
            hourOfDay1 = 12;
            tvtime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        }else{
            tvtime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
            tvdate.setText("Table for " + tvPeople.getText().toString() + ", " + day + ", " + mnth + " " + date + " at " + tvtime.getText().toString());
            tvTick.setVisibility(View.VISIBLE);
        }
        Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
        timeToSend = hourOfDay + ":" + minute;
    }
}
