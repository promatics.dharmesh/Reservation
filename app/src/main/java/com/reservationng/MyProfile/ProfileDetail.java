package com.reservationng.MyProfile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.Drawer;
import com.reservationng.HomeScreen;
import com.reservationng.R;
import com.reservationng.adapters.TabAdapter;
import com.reservationng.models.ReviewHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.reservationng.utils.WriteReviewRestaurant1;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ProfileDetail extends Fragment implements View.OnClickListener, ServiceCallback {
    TabLayout tabLayout;
    ViewPager viewPager;
    private TextView txtusername;
    private TabAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_details, container, false);
        txtusername = (TextView) v.findViewById(R.id.txtusername);
        txtusername.setText(User.getInstance().getFname() + " " + User.getInstance().getLname());
        tabLayout = (TabLayout) v.findViewById(R.id.tab1);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager1);
        getActivity().setTitle("Profile Details");
        tabLayout.addTab(tabLayout.newTab().setText("Points"));
        tabLayout.addTab(tabLayout.newTab().setText("Reservations"));
        tabLayout.addTab(tabLayout.newTab().setText("Reviews"));
        tabLayout.addTab(tabLayout.newTab().setText("Favorites"));
        tabLayout.addTab(tabLayout.newTab().setText("Voucher"));
        adapter = new TabAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                try {
                    viewPager.setCurrentItem(tab.getPosition());
                    Log.e("viewpager", "" + tab.getPosition() + " " + viewPager.getCurrentItem());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

            return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    private void checkReview() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REVIEW_ON_LAST_BOOKING, map, false).execute(Constants.REVIEW_ON_LAST_BOOKING);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_REVIEW_ON_LAST_BOOKING:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        Drawer.review.setVisibility(View.VISIBLE);
                        JSONObject info = object.getJSONObject("info");
                        final JSONObject RestaurantBooking = info.getJSONObject("RestaurantBooking");
                        final JSONObject Restaurant = info.getJSONObject("Restaurant");
                        final String id = RestaurantBooking.getString("id");
                        final String restId = RestaurantBooking.getString("restaurant_id");
//                        AlertDialog.Builder builder = new AlertDialog.Builder(new
//                                ContextThemeWrapper(getActivity(), android.R.style.
//                                Theme_Holo_Light));
//                        builder.setTitle("Reservation");
//                        builder.setMessage("Please give review for your last restaurant booking.");
//                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
                        Fragment fragment = new WriteReviewRestaurant1();
                        Bundle bundle = new Bundle();
                        bundle.putString("booking_id", id);
                        bundle.putString("rest_id", restId);
                        bundle.putString("rest_name", Restaurant.getString("rest_name"));
                        fragment.setArguments(bundle);
                        ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, WriteReviewRestaurant1.class.getSimpleName());
//                            }
//                        });
//                        builder.show();

                    } else {
                        Drawer.review.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(HomeScreen.review){
            AlertDialog.Builder builder = new AlertDialog.Builder(new
                    ContextThemeWrapper(getActivity(), android.R.style.
                    Theme_Holo_Light));
            builder.setTitle("Reservation");
            builder.setMessage("Please rate your last visit to [ "+ ReviewHolder.getInstance().restName+" ]");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Fragment fragment = new WriteReviewRestaurant1();
                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, WriteReviewRestaurant1.class.getSimpleName());

                }
            });
            builder.show();

        }

    }
}
