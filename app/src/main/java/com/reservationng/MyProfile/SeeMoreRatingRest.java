package com.reservationng.MyProfile;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.reservationng.R;

/**
 * Created by android2 on 1/23/16.
 */
public class SeeMoreRatingRest extends Fragment {

    RatingBar ratingbar;
    private TextView tvFood,tvService,tvAmbience,tvrview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.see_more_rating, container, false);
        getActivity().setTitle(getArguments().getString("name"));
        tvFood = (TextView) v.findViewById(R.id.tvFood);
        tvService = (TextView) v.findViewById(R.id.tvService);
        tvAmbience = (TextView) v.findViewById(R.id.tvAmbience);
        tvrview = (TextView) v.findViewById(R.id.tvrview);
        ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
        ratingbar.setRating(Float.parseFloat(getArguments().getString("overall")));
        LayerDrawable layerDrawable = (LayerDrawable) ratingbar.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(getActivity().getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
        tvAmbience.setVisibility(View.VISIBLE);
        tvFood.setText("Food Rating : "+getArguments().getString("food"));
        tvService.setText("Service Rating : "+getArguments().getString("service"));
        tvAmbience.setText("Ambience Rating : "+getArguments().getString("amb"));
        tvrview.setText(getArguments().getString("desc"));
        return v;
    }
}
