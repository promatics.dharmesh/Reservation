package com.reservationng.MyProfile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.adapters.PurGiftVoucherAdapter;
import com.reservationng.models.PurGiftVouchers;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.MyLinearLayoutManager;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PurchasedGiftVoucher extends Fragment implements ServiceCallback {

    private RecyclerView mRecyclerView;
    private ArrayList<PurGiftVouchers> list = new ArrayList<PurGiftVouchers>();
    private PurGiftVoucherAdapter adapter;
    private TextView tvNoData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rest_list, container, false);
        getActivity().setTitle("Purchased gift Vouchers");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        tvNoData = (TextView) v.findViewById(R.id.tvNoData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        purchaseGiftVoucher();
        return v;
    }

    private void purchaseGiftVoucher() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", User.getInstance().getUserId());
//        map.put("user_id", "69");
        new CallService(this, getActivity(), Constants.REQ_PURCHASED_GIFT_VOUCHER, map).execute(Constants.PURCHASED_GIFT_VOUCHER);
    }


    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_PURCHASED_GIFT_VOUCHER:
                try {
                    Log.e("response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tvNoData.setVisibility(View.GONE);
                        list.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            PurGiftVouchers model = new PurGiftVouchers();
                            JSONObject PurchaseGiftVoucher = info.getJSONObject(i).getJSONObject("PurchaseGiftVoucher");
                            model.setId(PurchaseGiftVoucher.getString("id"));
                            model.setUserId(PurchaseGiftVoucher.getString("user_id"));
                            model.setGiftVoucherId(PurchaseGiftVoucher.getString("gift_voucher_id"));
                            model.setVoucherAmt(PurchaseGiftVoucher.getString("voucher_amount"));
                            model.setVoucherCode(PurchaseGiftVoucher.getString("voucher_code"));
                            model.setTransNo(PurchaseGiftVoucher.getString("transaction_no"));
                            model.setRefNo(PurchaseGiftVoucher.getString("reference_no"));
                            model.setPaymentDate(PurchaseGiftVoucher.getString("payment_date"));
                            model.setExpirydate(PurchaseGiftVoucher.getString("voucher_expiring_date"));
                            list.add(model);
                        }
                        adapter = new PurGiftVoucherAdapter(getActivity(), list);
                        mRecyclerView.setAdapter(adapter);
                    } else if (code == 0) {
                        mRecyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText("No Gift Vouchers yet.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
