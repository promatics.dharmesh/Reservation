package com.reservationng.MyProfile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.adapters.VoucherListAdapter;
import com.reservationng.models.voucherDetails;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class VoucherList extends Fragment implements View.OnClickListener, ServiceCallback {

    private RecyclerView mRecyclerView;
    private VoucherListAdapter adapter;
    private Button btnBuyMore;
    private ArrayList<voucherDetails> list = new ArrayList<voucherDetails>();
    private LinearLayout llayout;
    private boolean flag=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.voucher_list, container, false);
//        getActivity().setTitle("Vouchers");
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        llayout=(LinearLayout)v.findViewById(R.id.llayout);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                size(8).build());
        btnBuyMore = (Button) v.findViewById(R.id.btnBuyMore);
        btnBuyMore.setOnClickListener(this);
//        giftCards();
        return v;
    }

    private void giftCards() {
        new CallService(this, getActivity(), Constants.REQ_GIFT_CARD_LIST).execute(Constants.GIFT_CARD_LIST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBuyMore:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new BuyMore(), "buyMore");
                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_GIFT_CARD_LIST:
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        llayout.setVisibility(View.VISIBLE);
                        list.clear();
                        JSONArray info = object.getJSONArray("info");
                        for (int i = 0; i < info.length(); i++) {
                            voucherDetails model = new voucherDetails();
                            JSONObject jsonObject = info.getJSONObject(i).getJSONObject("GiftVoucher");
                            model.setId(jsonObject.getString("id")); //gift voucher id
                            int amount=0;
                            amount=Integer.parseInt(jsonObject.getString("voucher_amount"))*1000;

                            model.setPrice(NumberFormat.getNumberInstance(Locale.US).format(amount));
                            model.setCreatedDate(jsonObject.getString("created_date"));
                            model.setExpiryDate(jsonObject.getString("voucher_expire_date"));
                            list.add(model);
                        }
                        adapter = new VoucherListAdapter(getActivity(), list);
                        mRecyclerView.setAdapter(adapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {
            if (isVisibleToUser && !flag) {
                giftCards();
                flag = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
