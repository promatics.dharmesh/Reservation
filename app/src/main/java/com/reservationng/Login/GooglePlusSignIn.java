package com.reservationng.Login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.util.HashMap;

public class GooglePlusSignIn extends Activity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ServiceCallback {

    public static GoogleApiClient mClient;
    public static final int RC_SIGN_IN = 0;
    private String Gplus_id,
            Gplus_name, Gplus_email, Gplus_fname, Gplus_lname;
    private String[] name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("act res", "" + data + "...." + requestCode + resultCode);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                mClient.connect();
                Log.e("act res for google", "" + data + "...." + requestCode + resultCode);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        Log.e("on conn","gplus");

        if (Plus.PeopleApi.getCurrentPerson(mClient) != null) {
            Person curperson = Plus.PeopleApi.getCurrentPerson(mClient);
            Gplus_name = curperson.getDisplayName();
            Gplus_email = Plus.AccountApi.getAccountName(mClient);
            name = Gplus_name.split(" ");
            for (int i = 0; i < name.length; i++) {
                Log.e("name length", "" + name.length);
                Log.e("name is 0", name[0]);
                Log.e("name is 1", name[name.length - 1]);
                Gplus_fname = name[0];
                Gplus_lname = name[name.length - 1];
            }
            Gplus_id = curperson.getId();
            Log.e("data", Gplus_fname + " " + Gplus_lname + "  " + Gplus_id + "..." + Gplus_email);
            GooglePlusLogin();
        } else {
            CommonUtils.showToast(this, "client is null");
            finish();
        }
    }

    private void GooglePlusLogin() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("google_id", Gplus_id);
        map.put("first_name", Gplus_fname);
        map.put("last_name", Gplus_lname);
        map.put("email", Gplus_email);
        Log.e("map values", map.toString());
        new CallService(this, this, Constants.REQ_GOOGLE_LOGIN, map).execute(Constants.GOOGLE_LOGIN);
    }

    @Override
    public void onConnectionSuspended(int i)
    {
        mClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
                Log.e("catch", "excp catched");
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_GOOGLE_LOGIN:
                try {
                    String cityName = "", stateId = "", stateName = "";
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");

                        if (info.has("City")) {
                            JSONObject City = info.getJSONObject("City");
                            cityName = City.getString("city_name");
                            stateId = City.getString("state_id");
                        } else {
                            stateId = "0";
                        }
                        if (info.has("State")) {
                            JSONObject State = info.getJSONObject("State");
                            stateName = State.getString("state_name");
                        }
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);
                        CommonUtils.showToast(GooglePlusSignIn.this,"Login successfully.");
                        Intent i = new Intent(this, Dashboard.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(this, android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Something went wrong");
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
