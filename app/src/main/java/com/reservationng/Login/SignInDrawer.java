package com.reservationng.Login;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;


public class SignInDrawer extends Fragment implements View.OnClickListener, ServiceCallback {


    private Button btn_facebook, btn_SignInRes, btn_google_plus,btn_CreateAccount;
    private CallbackManager callbackManager;
    private String Fname, Lname, FEmail, FB_id, Gplus_id,
            Gplus_name, Gplus_email, Gplus_fname, Gplus_lname;
    private String TAG = "facebook_data";
    private String[] name;
    private RelativeLayout rlayout;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sign_in_drawer, container, false);
        getActivity().setTitle("Sign In");
        btn_SignInRes = (Button) v.findViewById(R.id.btn_SignInRes);
        btn_facebook = (Button) v.findViewById(R.id.btn_facebook);
        btn_google_plus = (Button) v.findViewById(R.id.btn_google_plus);
        btn_CreateAccount = (Button) v.findViewById(R.id.btn_CreateAccount);
        rlayout = (RelativeLayout) v.findViewById(R.id.rlayout);
        rlayout.setVisibility(View.VISIBLE);
        btn_SignInRes.setOnClickListener(this);
        btn_facebook.setOnClickListener(this);
        btn_google_plus.setOnClickListener(this);
        btn_CreateAccount.setOnClickListener(this);


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("response", response.toString());
                        Log.e("Object", object.toString());
                        rlayout.setVisibility(View.GONE);
                        try {
                            FB_id = object.getString("id");
                            Log.e(TAG, "User ID : " + FB_id);
                            Fname = object.getString("first_name");
                            Log.e(TAG, "First Name : " + Fname);
                            Lname = object.getString("last_name");
                            Log.e(TAG, "Last Name : " + Lname);
                            FEmail = object.getString("email");
                            Log.e(TAG, "Email : " + FEmail);
                            facebookLogin();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                CommonUtils.showToast(getActivity(), "You are not logged in");
            }
            @Override
            public void onError(FacebookException e) {
            }
        });
        return v;
    }

    private void facebookLogin() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("facebook_id", FB_id);
        map.put("first_name", Fname);
        map.put("last_name", Lname);
        map.put("email", FEmail);
        Log.e("map values", map.toString());
        rlayout.setVisibility(View.GONE);
        new CallService(this, getActivity(), Constants.REQ_FB_LOGIN, map).execute(Constants.FB_LOGIN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_CreateAccount:
            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new CreateAccount(), "Create Account");
              break;

            case R.id.btn_SignInRes:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInScreen(), SignInScreen.class.getSimpleName());
                break;
            case R.id.btn_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
                break;
            case R.id.btn_google_plus:
//                Toast.makeText(getActivity(),"Connecting with Googles Plus",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(getActivity(),GooglePlusSignIn.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        rlayout.setVisibility(View.GONE);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.e("act res", "" + data + "...." + requestCode + resultCode);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_FB_LOGIN:
                try {
                    String cityName = "", stateId = "", stateName = "";
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        rlayout.setVisibility(View.VISIBLE);
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");

                        if (info.has("City")) {
                            JSONObject City = info.getJSONObject("City");
                            cityName = City.getString("city_name");
                            stateId = City.getString("state_id");
                        } else {
                            stateId = "0";
                        }
                        if (info.has("State")) {
                            JSONObject State = info.getJSONObject("State");
                            stateName = State.getString("state_name");
                        }
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("user id", id);
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);
                        CommonUtils.showToast(getActivity(),"Login Successfully.");
                        Intent i = new Intent(getActivity(), Dashboard.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    } else {
                        rlayout.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


        }
    }
}
