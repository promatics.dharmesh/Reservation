package com.reservationng.Login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.models.City;
import com.reservationng.models.State;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CreateAccount extends Fragment implements ServiceCallback, View.OnClickListener {
    private Spinner spCity, spState,spCode;
    private String[] city = {"Select City"};
    private String[] state = {"Select State"};
    private ArrayList<City> cityList = new ArrayList<>();
    private ArrayList<State> stateList = new ArrayList<>();
    private ArrayList<String> spcodeList = new ArrayList<>();
    private TextView txtContinue, txtCancel,tvGenerate;
    private EditText etFname, etLname, etEmail, etPwd, etCnfPwd,etMobile,etOtp;
    private String cityId = "", stateId = "",otpRecvd="";
    private RelativeLayout rlayout;
    public static boolean createFlag=true;
    String mobileNumber="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.create_account, container, false);
        getActivity().setTitle("Create Account");
        etFname = (EditText) v.findViewById(R.id.etFname);
        etLname = (EditText) v.findViewById(R.id.etLname);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etPwd = (EditText) v.findViewById(R.id.etPwd);
        etMobile = (EditText) v.findViewById(R.id.etMobile);
        etOtp = (EditText) v.findViewById(R.id.etOtp);
        etCnfPwd = (EditText) v.findViewById(R.id.etCnfPwd);
        txtContinue = (TextView) v.findViewById(R.id.txtContinue);
        txtCancel = (TextView) v.findViewById(R.id.txtCancel);
        tvGenerate = (TextView) v.findViewById(R.id.tvGenerate);
        spCity = (Spinner) v.findViewById(R.id.spCity);
        spState = (Spinner) v.findViewById(R.id.spState);
        spCode = (Spinner) v.findViewById(R.id.spCode);
        rlayout = (RelativeLayout) v.findViewById(R.id.rlayout);
        txtContinue.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        spcodeList.add(0,"+234");
        /*for (int i = 1; i <=99 ; i++) {
            spcodeList.add(""+i);
        }*/
        spState.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, state));
        spCity.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, city));
        spCode.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, spcodeList));
        new CallService(this, getActivity(), Constants.REQ_ALL_STATE,false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.ALL_STATE);
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("spcode value",""+spCode.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

            tvGenerate.setOnClickListener(this);
        return v;
    }

    void register() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("first_name", etFname.getText().toString().trim());
        map.put("last_name", etLname.getText().toString().trim());
        map.put("mobile_number", mobileNumber.substring(1,mobileNumber.length()));
        map.put("otp",etOtp.getText().toString().toString() );
        map.put("email", etEmail.getText().toString().trim());
        map.put("password", etPwd.getText().toString().trim());
        map.put("state", stateId);
        map.put("city", cityId);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REGISTER, map).execute(Constants.REGISTER);
    }

    private void generateOtp(){
        rlayout.setVisibility(View.GONE);
        HashMap<String, String> map = new HashMap<String, String>();
        mobileNumber=spCode.getSelectedItem()+etMobile.getText().toString().trim();
        map.put("mobile_number", mobileNumber.substring(1,mobileNumber.length()));
        Log.e("map values", map.toString());
            new CallService(this, getActivity(), Constants.REQ_GENERATE_OTP, map).execute(Constants.GENERATE_OTP);
    }
    @Override
    public void onServiceResponse(int requestcode, String result) {
        switch (requestcode) {
            case Constants.REQ_GENERATE_OTP:
                rlayout.setVisibility(View.VISIBLE);
                Log.e(" response", result);
                try {
                    JSONObject object = new JSONObject(result);
                    int code = object.getInt("code");
                    if (code == 1) {

                        otpRecvd=object.getString("otp");
                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage("Your Sign up code has been sent to your mobile number.");
                        alert.setCancelable(false);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                tvGenerate.setBackgroundColor(getResources().getColor(R.color.middle_color));
                                tvGenerate.setOnClickListener(null);
                            }
                        });
                        alert.show();
                    }else{
                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage("Your SignUp code is wrong. Please enter the correct SignUp Code.");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

            case Constants.REQ_ALL_STATE:
                try {
                    rlayout.setVisibility(View.VISIBLE);
                    JSONObject response = new JSONObject(result);
                    int code = response.getInt("code");
                    if (code == 1) {
                        spState.setSelection(0);
                        stateList.clear();
                        JSONArray cityArr = response.getJSONArray("state");
                        state = new String[cityArr.length() + 1];
                        for (int i = 0; i < cityArr.length() + 1; i++) {
                            State statemodel = new State();
                            if (i == 0) {
                                state[0] = "Select State";
                                statemodel.setStateName("Select State");
                            } else {
                                JSONObject ob = cityArr.getJSONObject(i - 1).getJSONObject("State");
                                statemodel.setStateId(ob.getString("id"));
                                statemodel.setStateName(ob.getString("state_name"));
                            //    statemodel.setCountryId(ob.getString("country_id"));
                                state[i] = ob.getString("state_name");
                            }
                            stateList.add(statemodel);
                            spState.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_layout, state));
                            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    if (spState.getSelectedItemPosition() == 0) {
                                        ((TextView) adapterView.getChildAt(0)).setTextColor(Color.GRAY);
                                    } else {
                                        stateId = stateList.get(i).getStateId();
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put("state_id", stateId);
                                        Log.e("map values", map.toString());
                                        new CallService(CreateAccount.this, getActivity(), Constants.REQ_CITY_ACC_STATE, map).execute(Constants.CITY_ACC_STATE);

                                        // Toast.makeText(getActivity(), stateId + ".." + stateList.get(i).getStateId(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


            case Constants.REQ_CITY_ACC_STATE:
                try {
                    JSONObject response = new JSONObject(result);
                    int code = response.getInt("code");
                    if (code == 1) {
                        spCity.setSelection(0);
                        cityList.clear();
                        JSONArray cityArr = response.getJSONArray("city");
                        city = new String[cityArr.length() + 1];
                        for (int i = 0; i < cityArr.length() + 1; i++) {
                            City citymodel = new City();
                            if (i == 0) {
                                city[0] = "Select City";
                                citymodel.setCityName("Select City");
                            } else {
                                JSONObject ob = cityArr.getJSONObject(i - 1).getJSONObject("City");
                                citymodel.setCityId(ob.getString("id"));
                                citymodel.setCityName(ob.getString("city_name"));
                                citymodel.setStateId(ob.getString("state_id"));
                                city[i] = ob.getString("city_name");
                            }
                            cityList.add(citymodel);
                            spCity.setAdapter(new ArrayAdapter(getActivity(), R.layout.spinner_layout, city));
                            spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    if (spCity.getSelectedItemPosition() == 0) {
                                        ((TextView) adapterView.getChildAt(0)).setTextColor(Color.GRAY);
                                    } else {
                                        cityId = cityList.get(i).getCityId();
                                        //Toast.makeText(getActivity(),cityId+".."+ cityList.get(i).getCityId(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.REQ_REGISTER:
                Log.e(" response", result);
                try {
                    JSONObject object = new JSONObject(result);
                    int code = object.getInt("code");
                    if (code == 1) {
                        rlayout.setVisibility(View.VISIBLE);
                        Log.e("createFlag",""+createFlag);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage("User Registered Successfully. Check Email for the activation.");
                        alert.setTitle("Reservation");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                etFname.setText("");
                                etLname.setText("");
                                etEmail.setText("");
                                etPwd.setText("");
                                etMobile.setText("");
                                etOtp.setText("");
                                etCnfPwd.setText("");

                                spCity.setSelection(0);
                                spState.setSelection(0);
                                spCode.setSelection(0);
                                if(!createFlag) {
                                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInDrawer(), SignInDrawer.class.getSimpleName());
                                    createFlag=true;
                                }else{
                                    Intent i = new Intent(getActivity(), Dashboard.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                }
                            }
                        });

                        alert.show();
                    }
                    else if (code == 2) {
                        rlayout.setVisibility(View.VISIBLE);
                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                        alert.setMessage("Sign up code is wrong. Please enter the correct Sign up code");
                        alert.setMessage(object.getString("message"));
                        alert.setTitle("Reservation");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        alert.show();
                    }
                    else if (code == 0) {
                        rlayout.setVisibility(View.VISIBLE);
                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage("Email Already Exists.");
                        alert.setTitle("Reservation");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               dialog.dismiss();
                            }
                        });

                        alert.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txtContinue:
                if (etFname.getText().toString().trim().isEmpty()) {
                    etFname.setError("Please enter first name");
                    etFname.requestFocus();
                } else if (etLname.getText().toString().trim().isEmpty()) {
                    etLname.setError("Please enter last name");
                    etLname.requestFocus();
                } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter valid email");
                    etEmail.requestFocus();
                } else if (etPwd.getText().toString().trim().isEmpty()) {
                    etPwd.setError("Please enter password");
                    etPwd.requestFocus();
                } else if (!etCnfPwd.getText().toString().trim().equals(etPwd.getText().toString().trim())) {
                    etCnfPwd.setError("Password and Confirm password must be equal");
                    etCnfPwd.requestFocus();
                } else if(etMobile.getText().toString().trim().isEmpty() || etMobile.getText().toString().trim().length()!=10){
                    etMobile.setError("Please enter 10 digits mobile number");
                    etMobile.requestFocus();
                }
                else if(etMobile.getText().toString().startsWith("0")){
                    etMobile.setError("Please donot start mobile number with zero");
                    etMobile.requestFocus();
                }
                else if(etOtp.getText().toString().trim().length()==0 || !etOtp.getText().toString().trim().equalsIgnoreCase(otpRecvd)){
                    etOtp.setError("Please enter sign up code you received");
                    etOtp.requestFocus();
                }
                else if (spState.getSelectedItemPosition() == 0) {
                    CommonUtils.showToast(getActivity(), "Please select State");
                } else if (spCity.getSelectedItemPosition() == 0) {
                    CommonUtils.showToast(getActivity(), "Please select Area");
                } else {
                    etFname.setError(null);
                    etLname.setError(null);
                    etEmail.setError(null);
                    etPwd.setError(null);
                    etCnfPwd.setError(null);etMobile.setError(null);
                    etOtp.setError(null);
                    rlayout.setVisibility(View.GONE);
                    register();
                }
                break;

            case R.id.txtCancel:
                getActivity().onBackPressed();
                break;

            case R.id.tvGenerate:
               if(etMobile.getText().toString().trim().isEmpty() || etMobile.getText().toString().trim().length()!=10){
                etMobile.setError("Please enter 10 digits mobile number");
                etMobile.requestFocus();
            }else if(etMobile.getText().toString().startsWith("0")){
                etMobile.setError("Please donot start mobile number with zero");
                etMobile.requestFocus();
            }
               else {
                    generateOtp();
                }
                break;
        }

    }


}
