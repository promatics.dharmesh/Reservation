package com.reservationng.Login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.Restaurants.FutureTimeBooking;
import com.reservationng.Restaurants.SpecialOfferConfirm;
import com.reservationng.WithoutSignIn;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SignInScreen extends Fragment implements View.OnClickListener, ServiceCallback {

    private TextView txtCancel, txtSignin, txtForgotPwd;
    private EditText etEmail, etPhn, etPwd;
    private CheckBox chkshowPwd;
    private Button btn_CreateAccount;
    private RelativeLayout rlayout;
    public static boolean Bookflag=false;
    public static boolean Bookflag2Success=false;
    public static boolean vivekJugad=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sign_in, container, false);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        getActivity().setTitle("Sign In");
        // etPhn = (EditText) v.findViewById(R.id.etPhn);
        etPwd = (EditText) v.findViewById(R.id.etPwd);
        txtSignin = (TextView) v.findViewById(R.id.txtSignin);
        txtCancel = (TextView) v.findViewById(R.id.txtCancel);
        txtForgotPwd = (TextView) v.findViewById(R.id.txtForgotPwd);
        chkshowPwd = (CheckBox) v.findViewById(R.id.chkshowPwd);
        btn_CreateAccount = (Button) v.findViewById(R.id.btn_CreateAccount);
        rlayout = (RelativeLayout) v.findViewById(R.id.rlayout);
        // btn_signin = (Button) v.findViewById(R.id.btn_signin);
        String htmlString = "<u>Forgot Password ?</u>";
        txtForgotPwd.setText(Html.fromHtml(htmlString));
        txtSignin.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        txtForgotPwd.setOnClickListener(this);
        if(SignInScreen.Bookflag){
            etEmail.setText(DataHolder.getInstance().email);
        }
        if(FutureTimeBooking.FutureBookFlag){
            etEmail.setText(DataHolder.getInstance().email);
        }
        if(SpecialOfferConfirm.OffersBookFlag){
            etEmail.setText(DataHolder.getInstance().email);
        }
        btn_CreateAccount.setOnClickListener(this);
        // btn_signin.setOnClickListener(this);

        chkshowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etPwd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    etPwd.setInputType(129);
                }
            }
        });

        return v;
    }

    void SignIn() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", etEmail.getText().toString().trim());
        map.put("password", etPwd.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_CreateAccount:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new CreateAccount(), CreateAccount.class.getSimpleName());
                break;
            case R.id.txtForgotPwd:
                etEmail.setText("");
                etPwd.setText("");
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new ForgotPassword(), ForgotPassword.class.getSimpleName());
                break;
            case R.id.txtCancel:
                getActivity().onBackPressed();
                break;

            case R.id.txtSignin:
                if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter valid email");
                    etEmail.requestFocus();
                } else if (etPwd.getText().toString().isEmpty()) {
                    etPwd.setError("Please enter password");
                    etPwd.requestFocus();
                } else {
                    etEmail.setError(null);
                    etPwd.setError(null);
                    rlayout.setVisibility(View.GONE);
                    SignIn();
                }
                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");

                        String cityId = user.getString("city");

                        String cityName = City.getString("city_name");

                        String stateId = user.getString("state");

                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";
                        Log.e("val", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);
                        Log.e("WithoutSignIn.BookFlag", "" + WithoutSignIn.BookFlag);
                        Log.e("FutureTimeBooking.Flag", "" + FutureTimeBooking.FutureBookFlag);
                        Log.e("SpecialOferCnfirm.Flag",""+SpecialOfferConfirm.OffersBookFlag);

                       if(Bookflag){
                            getActivity().onBackPressed();
                        }else{
                            CommonUtils.showToast(getActivity(),"Login Successfully");
                            Intent i = new Intent(getActivity(), Dashboard.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    } else {
                        rlayout.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                etEmail.setText("");
                                etPwd.setText("");
                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }
}
