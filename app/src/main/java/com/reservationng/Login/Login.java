package com.reservationng.Login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.reservationng.Dashboard;
import com.reservationng.R;

public class Login extends Fragment implements View.OnClickListener {
    private Button btnCraete, btnGooglePlus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_layout, container, false);
        getActivity().setTitle("Sign Up");

        btnCraete = (Button) v.findViewById(R.id.btn_CreateAccount);
        btnGooglePlus = (Button) v.findViewById(R.id.btnGooglePlus);
        btnCraete.setOnClickListener(this);

        return v;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_CreateAccount:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new CreateAccount(), "Create Account");
                break;
        }
    }
}
