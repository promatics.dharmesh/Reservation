package com.reservationng.Login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by android2 on 12/2/15.
 */
public class ForgotPassword extends Fragment implements View.OnClickListener,ServiceCallback {

    private EditText etEmail;
    private Button btnResetPwd;
    private RelativeLayout rlayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.forgot_password, container, false);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        btnResetPwd = (Button) v.findViewById(R.id.btnResetPwd);
        rlayout = (RelativeLayout) v.findViewById(R.id.rlayout);
        getActivity().setTitle("Forgot password");
        btnResetPwd.setOnClickListener(this);

        return v;
    }

    private void forgotPassword(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", etEmail.getText().toString().trim());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_FORGOT_PASSWORD, map).execute(Constants.FORGOT_PASSWORD);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btnResetPwd:
                if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter valid email");
                    etEmail.requestFocus();
                } else {
                    etEmail.setError(null);
                    rlayout.setVisibility(View.GONE);
                    forgotPassword();
                }
                break;
        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_FORGOT_PASSWORD:
                try {
                    Log.e(" response",  response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        rlayout.setVisibility(View.VISIBLE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("A link to reset your password has been sent to email.");
                        builder.setPositiveButton("Ok",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new SignInScreen(), SignInScreen.class.getSimpleName());

                            }
                        });
                        builder.show();
                    }else if(code==0){
                        rlayout.setVisibility(View.VISIBLE);
                        etEmail.setText("");
                        CommonUtils.showDialog(getActivity(),"Email Not Registered.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
