package com.reservationng;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.reservationng.Hotels.Hotels;
import com.reservationng.Login.SignInScreen;
import com.reservationng.Restaurants.RestaurantBookScreen;
import com.reservationng.Restaurants.RestaurantFilter;
import com.reservationng.Restaurants.Restaurants;
import com.reservationng.Restaurants.SpecialOffersDetails;
import com.reservationng.models.DataHolder;
import com.reservationng.models.ReviewHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.CheckNetworkStatus;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;
import com.reservationng.utils.Webview;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

public class HomeScreen extends Fragment implements OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ServiceCallback, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    private View v;
    private TextView tvtable, tvroom, tvdate, tvcheckin, tvcheckOut, txttime;
    private String[] people = {"1 people", "2 people", "3 people", "4 people", "5 people", "6 people", "7 people", "8 people", "9 people", "10 people", "11 people", "12 people", "13 people", "14 people", "15 people", "16 people", "17 people", "18 people", "19 people", "20 people", "Larger Party"};
    private String[] rooms = {"1 Room", "2 Rooms", "3 Rooms", "4 Rooms", "5 Rooms", "6 Rooms", "7 Rooms", "8 Rooms", "9 Rooms", "10 Rooms", "11 Rooms", "12 Rooms", "13 Rooms", "14 Rooms", "15 Rooms", "16 Rooms", "17 Rooms", "18 Rooms", "19 Rooms", "20 Rooms",};
    private Spinner spPeople, spRooms;
    private int year, month, day;
    private GoogleApiClient googleApiClient;
    private EditText etlocation, ethotel;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private String citystateId, time, cityStateName, x, y, currentTime, currentTime1, time1,DATETOSND="";
    private double latitude, longitude;
    private boolean  flag1 = false, flagLoc = false;
    private static int check;
    public static boolean chkGPS=false, review=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.homescreen, container, false);
            if (SignInScreen.Bookflag2Success) {
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new RestaurantBookScreen(), "RestaurantBookScreen");
            }

            SpecialOffersDetails.SprcOfferGuestFlag=false;
            WithoutSignIn.ResvGuestFlag=false;
            WithoutSignIn.SpecGuestFlag=false;
            WithoutSignIn.withoutSignInback=false;
            WithoutHotelSignIn.withoutSignHotelInback=false;

            //Todo on apr 29
            RestaurantFilter.cuisineId = 0;
            RestaurantFilter.specFeaturesId = 0;
            RestaurantFilter.priceRange = 0;
            RestaurantFilter.specialOffers = 0;
            RestaurantFilter.dis = 0;
            RestaurantFilter.sort = 0;

            getActivity().setTitle("Reservation");
            tvtable = (TextView) v.findViewById(R.id.tvsearch);
            tvroom = (TextView) v.findViewById(R.id.tvroom);
            spPeople = (Spinner) v.findViewById(R.id.spPeople);
            spRooms = (Spinner) v.findViewById(R.id.spRooms);
            tvdate = (TextView) v.findViewById(R.id.tvdate);
            txttime = (TextView) v.findViewById(R.id.txttime);
            tvcheckOut = (TextView) v.findViewById(R.id.tvcheckOut);
            tvcheckin = (TextView) v.findViewById(R.id.tvcheckin);
            etlocation = (EditText) v.findViewById(R.id.etlocation);
            ethotel = (EditText) v.findViewById(R.id.ethotel);
            tvcheckin.setOnClickListener(this);
            tvcheckOut.setOnClickListener(this);
            tvdate.setOnClickListener(this);
            txttime.setOnClickListener(this);
            tvtable.setOnClickListener(this);
            tvroom.setOnClickListener(this);
            etlocation.setOnClickListener(this);
            ethotel.setOnClickListener(this);
            Log.e("HomeScreen.review",""+HomeScreen.review);
            if(User.getInstance()!=null) {
                checkReview();
              /*  if (HomeScreen.review) {
                    setHasOptionsMenu(false);
                } else {
                    setHasOptionsMenu(true);
                }*/
            }
            getActivity().invalidateOptionsMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
        etlocation.setInputType(InputType.TYPE_NULL);
        ethotel.setInputType(InputType.TYPE_NULL);
        showDefaultDetails();
        return v;
    }



    private void checkReview() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", User.getInstance().getEmail());
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_REVIEW_ON_LAST_BOOKING, map, false).execute(Constants.REVIEW_ON_LAST_BOOKING);
    }

    private void showDefaultDetails() {
        try {
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            String date="",de="",dy="",mnth="";
                date=day + "/" + (month + 1) + "/" + year;
            DATETOSND=date;
            Log.e("DATETOSND",DATETOSND);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date1 = dateFormat.parse(date);
                SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                newDateFormat.applyPattern("EEE MMM d");
                String MyDate1 = newDateFormat.format(date1);
                StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                while (tokenizer.hasMoreTokens()) {
                    dy = tokenizer.nextToken();
                    mnth = tokenizer.nextToken();
                    de = tokenizer.nextToken();
                }

            String myFormat = "hh:mm a";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            int hr=0,min1=0;
            String qm="",min="";
            StringTokenizer s1 = new StringTokenizer(sdf.format(calendar.getTime()), ":");
            while (s1.hasMoreTokens()) {
                hr = Integer.parseInt(s1.nextToken());
                min = s1.nextToken();
                Log.e("hr  n min >>>>>", hr + ".." + min+" ???"+sdf.format(calendar.getTime()));
            }

            StringTokenizer s2 = new StringTokenizer(String.valueOf(min), " ");
            while (s2.hasMoreTokens()) {
                min1 = Integer.parseInt(s2.nextToken());
                qm = s2.nextToken();
                if(min1>0 && min1<=15){
                    currentTime=hr+":"+"15"+" "+qm;
                }
                else  if(min1>15 && min1<=30){
                    currentTime=hr+":"+"30"+" "+qm;
                }else  if(min1>30 && min1<=45){
                    currentTime=hr+":"+"45"+" "+qm;
                }else if(min1>45 && min1<=59){
                    if(hr==12){
                        currentTime="1"+":"+"00"+" "+qm;
                    }else{
                        currentTime=hr+1+":"+"00"+" "+qm;
                    }
                }else if(min1==00){
                    currentTime=hr+":"+"00"+" "+qm;
                }
            }
            spPeople.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, people));
            spRooms.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, rooms));
            spPeople.setSelection(1);
            spRooms.setSelection(1);
            final Date dateObj = sdf.parse(sdf.format(calendar.getTime()));
            time = new SimpleDateFormat("H:mm").format(dateObj);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void customTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                HomeScreen.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(Color.parseColor("#c0a756"));
        tpd.setTimeInterval(1, 15, 1);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.e("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void customDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                HomeScreen.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                CommonUtils.showToast(getActivity(), "This device is not supported the Google Play Services");
            }
            return false;
        }
        return true;
    }

    private void fetchCurrentLoc() {
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

//                Geocoder geocoder = new Geocoder(getActivity());
//                List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
//                if (addressList.size() <= 0) {
//                    CommonUtils.showToast(getActivity(), "Invalid Location");
//                } else {
//                    cityStateName = addressList.get(0).getLocality() + ", " + addressList.get(0).getAdminArea();
//                }

                Log.e("Latitude", latitude + " Longitude" + longitude + chkGPS);

            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                final String message = "Do you want to open GPS Settings?";
                builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface d, int id) {
                        chkGPS = true;
                        getActivity().startActivity(new Intent(action));
                        d.dismiss();
                    }
                })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                chkGPS = false;
                                d.cancel();
                            }
                        });
                builder.create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txttime:
                currentTime1 = currentTime;
                customTimePicker();
                break;
            case R.id.tvdate:
                check = 1;
                customDatePicker();
                break;
            case R.id.tvcheckin:
                check = 2;
                customDatePicker();
                break;
            case R.id.tvcheckOut:
                check = 3;
                customDatePicker();
                break;
            case R.id.tvsearch:
                searchRestaurantData();
                break;
            case R.id.tvroom:
                searchHotelData();
                break;

            case R.id.etlocation:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new LocResult(), LocResult.class.getSimpleName());
                break;
            case R.id.ethotel:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HotelResult(), HotelResult.class.getSimpleName());
                break;
        }
    }

    private void searchRestaurantData() {
        try {
            if (String.valueOf(spPeople.getSelectedItemPosition() + 1).equalsIgnoreCase("21")) {
                String url = Constants.BASE_PVT_DINING;
                Log.e("url is", url);
                Intent i = new Intent(getActivity(), Webview.class);
                i.putExtra("url", url);
                getActivity().startActivity(i);
            } else {
                Fragment fragment = new Restaurants();
                Bundle bundle = new Bundle();
                bundle.putBoolean("check", true);
                bundle.putString("city", DataHolder.getInstance().locId);
                DataHolder.getInstance().noOfPeople = String.valueOf(spPeople.getSelectedItemPosition() + 1);
                bundle.putString("people", DataHolder.getInstance().noOfPeople);
                    Log.e("DATETOSND",DATETOSND);
                DataHolder.getInstance().date = CommonUtils.target.format(CommonUtils.source2.parse(DATETOSND));

                bundle.putString("date", DataHolder.getInstance().date);
                if (flag1 == true) {
                    time = time1;
                    DataHolder.getInstance().time24 = time;
                    bundle.putString("time", DataHolder.getInstance().time24);
                } else {
                    DataHolder.getInstance().time24 = time;
                    bundle.putString("time", DataHolder.getInstance().time24);
                }
                bundle.putString("long", String.valueOf(longitude));
                bundle.putString("lat", String.valueOf(latitude));
                Log.e("chkgps",""+chkGPS);
                if (etlocation.getText().toString().isEmpty() && chkGPS) {
                    DataHolder.getInstance().searchtype="2";
                    bundle.putString("citystateName", cityStateName);
                } else if (etlocation.getText().toString().isEmpty() && !chkGPS) {
                    DataHolder.getInstance().searchtype="4";
                    bundle.putString("citystateName", cityStateName);
                } else {
                    String val1 = "", val2 = "";
                    //to check whether search of Restaurant is by loc or Restaurant name
                    StringTokenizer tokenizer = new StringTokenizer(DataHolder.getInstance().locId, "|");
                    while (tokenizer.hasMoreTokens()) {
                        val1 = tokenizer.nextToken();
                        val2 = tokenizer.nextToken();
                    }
                    Log.e("val1 n val2", val1 + "  " + val2);
                    if (val2.equalsIgnoreCase("0")) {
                        DataHolder.getInstance().searchtype="3";
                    } else if (val2.equalsIgnoreCase("C")) {
                        DataHolder.getInstance().searchtype="5";
                    } else {
                        DataHolder.getInstance().searchtype="1";
                    }
                    bundle.putString("citystateName", etlocation.getText().toString().trim());
                }
                fragment.setArguments(bundle);
                Log.e("bundle", bundle.toString());
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, Restaurants.class.getSimpleName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchHotelData() {
        try {
            Fragment fragment = new Hotels();
            Bundle bundle = new Bundle();
            bundle.putString("city", DataHolder.getInstance().hotelLocId);
            bundle.putString("no_of_rooms", String.valueOf(spRooms.getSelectedItemPosition() + 1));
//            bundle.putString("checkout_date", CommonUtils.target.format(CommonUtils.source2.parse(tvcheckOut.getText().toString())));
//            bundle.putString("checkin_date", CommonUtils.target.format(CommonUtils.source2.parse(tvcheckin.getText().toString())));
            bundle.putString("checkout_date", CommonUtils.target.format(CommonUtils.source2.parse(DATETOSND)));
            bundle.putString("checkin_date", CommonUtils.target.format(CommonUtils.source2.parse(DATETOSND)));
            bundle.putString("long", String.valueOf(longitude));
            bundle.putString("lat", String.valueOf(latitude));
            if (ethotel.getText().toString().isEmpty() && chkGPS) {
                bundle.putString("search_type", "2");
                bundle.putString("citystateName", cityStateName);
            } else if (ethotel.getText().toString().isEmpty() && !chkGPS) {
                bundle.putString("search_type", "4");
                bundle.putString("citystateName", cityStateName);
            } else {
                String val1 = "", val2 = "";
                //to check whether search of Resort is by loc or Resort name
                StringTokenizer tokenizer = new StringTokenizer(DataHolder.getInstance().hotelLocId, "|");
                while (tokenizer.hasMoreTokens()) {
                    val1 = tokenizer.nextToken();
                    val2 = tokenizer.nextToken();
                }
                Log.e("val1 n val2", val1 + "  " + val2);
                if (val2.equalsIgnoreCase("0")) {
                    bundle.putString("search_type", "3");
                } else {
                    bundle.putString("search_type", "1");
                }
                bundle.putString("citystateName", ethotel.getText().toString().trim());
            }
            fragment.setArguments(bundle);
            Log.e("hotel data", bundle.toString());
            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, Hotels.class.getSimpleName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (CheckNetworkStatus.getInstance(getActivity()).isOnline()) {
            fetchCurrentLoc();
        } else {
            CommonUtils.showToast(getActivity(), "Please check your internet connection");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {

            case Constants.REQ_REVIEW_ON_LAST_BOOKING:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        Drawer.review.setVisibility(View.VISIBLE);
                        HomeScreen.review=true;
                        JSONObject info = object.getJSONObject("info");
                        final JSONObject RestaurantBooking = info.getJSONObject("RestaurantBooking");
                        final JSONObject Restaurant = info.getJSONObject("Restaurant");
                        final String id = RestaurantBooking.getString("id");
                        final String restId = RestaurantBooking.getString("restaurant_id");
                        ReviewHolder.getInstance().bookingid= id;
                        ReviewHolder.getInstance().restId= restId;
                        ReviewHolder.getInstance().restName= Restaurant.getString("rest_name");
                    } else {
                        Drawer.review.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                ethotel.getWindowToken(), 0);
        InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                etlocation.getWindowToken(), 0);

        Log.e("on homescreen", DataHolder.getInstance().loc + " " + DataHolder.getInstance().locId + Webview.pvtDining);
        etlocation.setText(DataHolder.getInstance().loc);
        ethotel.setText(DataHolder.getInstance().hotelLoc);

        if (Webview.pvtDining) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Reservation");
            builder.setMessage("Your Private Dining has reserved successfully.Please check your email.");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Webview.pvtDining = false;
                    showDefaultDetails();
                    Log.e("Webview.pvtDining", "" + Webview.pvtDining);
                }
            }).create().show();
        }
    }


    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

        try {
            String AM_PM = " AM";
            String mm_precede = "";
            String minute1 = "";
            int hourOfDay1 = 0;
            if (minute < 10) {
                mm_precede = "0";
            }

            if (hourOfDay >= 12) {
                AM_PM = " PM";
                if (hourOfDay >= 13 && hourOfDay < 24) {
                    hourOfDay1 = hourOfDay - 12;
                } else {
                    hourOfDay1 = 12;
                }
                txttime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            } else if (hourOfDay == 0) {
                hourOfDay1 = 12;
                txttime.setText(hourOfDay1 + ":" + mm_precede + minute + AM_PM);
            } else {
                txttime.setText(hourOfDay + ":" + mm_precede + minute + AM_PM);
            }
            String curDate = "", curTime = "";
            int curHour = 0, curMin = 0;
            StringTokenizer stringTokenizer = new StringTokenizer(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()), " ");
            while (stringTokenizer.hasMoreTokens()) {
                curDate = stringTokenizer.nextToken();
                curTime = stringTokenizer.nextToken();
            }
            flag1 = false;
            if (tvdate.getText().toString().equalsIgnoreCase(curDate)) {
                Log.e("cur dt n time", tvdate.getText().toString() + curDate + ".." + curTime);
                StringTokenizer s1 = new StringTokenizer(curTime, ":");
                while (s1.hasMoreTokens()) {
                    curHour = Integer.parseInt(s1.nextToken());
                    curMin = Integer.parseInt(s1.nextToken());

                    Log.e("a1  n a2", curHour + ".." + curMin);
                }
                if (hourOfDay < curHour) {
                    flag1 = true;
                    customTimePicker();
                    CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
                    txttime.setText(currentTime1);

                    //convert curtime to 24 hr format

                    String myFormat = "hh:mm a";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    final Date dateObj = sdf.parse(txttime.getText().toString());
                    time1 = new SimpleDateFormat("H:mm").format(dateObj);

                    Log.e("curr time", time1);
                } else if (hourOfDay == curHour) {
                    flag1 = true;
                    if (minute < curMin) {
                        customTimePicker();
                        CommonUtils.showToast(getActivity(), "Today, you can't select the time before the current time");
                        txttime.setText(currentTime1);


                        String myFormat = "hh:mm a";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        final Date dateObj = sdf.parse(txttime.getText().toString());
                        time1 = new SimpleDateFormat("H:mm").format(dateObj);
//                        time1 = curTime;
                        Log.e("cur time", time1);
                    }
                }
            }
            Log.e("24 hr", "" + hourOfDay + ",,,," + minute);
            time = hourOfDay + ":" + minute;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        try {
            final long current = System.currentTimeMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            if (monthOfYear < 10) {
                monthOfYear = monthOfYear + 1;
                x = "0" + monthOfYear;
            } else {
                x = String.valueOf(monthOfYear);
            }
            if (dayOfMonth < 10) {
                y = "0" + dayOfMonth;
            } else {
                y = String.valueOf(dayOfMonth);
            }



            if (calendar.getTimeInMillis() < current) {
                customDatePicker();
                Toast.makeText(getActivity(), "Invalid date, please try again", Toast.LENGTH_LONG).show();
            } else {

                String date = "", de = "", dy = "", mnth = "";
                date = y + "/" + x + "/" + year;
                DATETOSND=date;

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date1 = dateFormat.parse(date);
                SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                newDateFormat.applyPattern("EEE MMM d");
                String MyDate1 = newDateFormat.format(date1);
                StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
                while (tokenizer.hasMoreTokens()) {
                    dy = tokenizer.nextToken();
                    mnth = tokenizer.nextToken();
                    de = tokenizer.nextToken();
                }

                if (check == 1) {
                    tvdate.setText(dy + ", " + de + " " + mnth);
                }
                if (check == 2) {
                    tvcheckin.setText(dy + ", " + de + " " + mnth);
                }
                if (check == 3) {
                    try {
                        tvcheckOut.setText(dy + ", " + de + " " + mnth);
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/M/yyyy");
                        Date date11 = dateFormat1.parse(tvcheckin.getText().toString());
                        Date date2 = dateFormat1.parse(tvcheckOut.getText().toString());
                        if (date2.before(date11)) {
                            customDatePicker();
                            Toast.makeText(getActivity(), "you can't select the date before the checkin date", Toast.LENGTH_LONG).show();
                        } else {
                            tvcheckOut.setText(dy + ", " + de + " " + mnth);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (User.getInstance() != null) {
            inflater.inflate(R.menu.text_badges, menu);
            MenuItem bell = menu.findItem(R.id.review);
            if (menu != null) {

                if (HomeScreen.review) {
                    bell.setVisible(false);
                } else {
                    bell.setVisible(true);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.review:
                checkReview();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
