package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.Hotels.WriteReviewResort;
import com.reservationng.R;
import com.reservationng.Restaurants.WriteReviewRestaurant;
import com.reservationng.models.CombineReservations;
import com.reservationng.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CombineBookingsPrevAdapter extends RecyclerView.Adapter<CombineBookingsPrevAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<CombineReservations> list;
    private AQuery aQuery = new AQuery(mContext);
    public static MyClickListener myClickListener;
    private String type;
    CombineReservations data;

    public CombineBookingsPrevAdapter(FragmentActivity activity, ArrayList<CombineReservations> list) {
        this.mContext = activity;
        this.list = list;
        type = "1";
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.previous_reservations, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CombineBookingsPrevAdapter.CustomViewHolder holder, int position) {
        data = list.get(position);

        if (data.getType().equalsIgnoreCase("resort")) {

            holder.tvCuisine.setVisibility(View.VISIBLE);
            holder.tvDetails.setVisibility(View.VISIBLE);
            holder.tvRewardPt.setVisibility(View.GONE);
            holder.tvDate.setVisibility(View.VISIBLE);

            holder.tvName.setText(data.getHotelName());
            try {
                holder.tvDetails.setText(CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckIn())) + " to " + CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckOut())));
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tvCuisine.setText(data.getCuisines());
            holder.tvDate.setText("₦" + data.getPrice() + " for " + data.getTotRooms() + " rooms ");
            aQuery.id(holder.ivImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getHotelImg());

            //TODO for Resort Reviews
//            if (!data.isFlag()) {
//                holder.tvReview.setText("Write A Review");
//            } else {
//                holder.tvReview.setText("View A Review");
//            }
//            if (type.equalsIgnoreCase("2")) {
//            holder.llReview.setVisibility(View.VISIBLE);
//            } else {
//                holder.llReview.setVisibility(View.GONE);
//            }
        }

        if (data.getType().equalsIgnoreCase("rest")) {
            holder.tvDate.setVisibility(View.GONE);
            holder.tvCuisine.setVisibility(View.GONE);
            holder.tvDetails.setVisibility(View.VISIBLE);
            holder.tvRewardPt.setVisibility(View.VISIBLE);

            try {
                if (data.getCustomer_arrival_confirm().equalsIgnoreCase("1")) { //review written by user.
                        //flag true-->view
                    //else -->write
                    holder.llReview.setVisibility(View.VISIBLE);
                    if (data.isFlag()) {
                        holder.tvReview.setText("View A Review");
                    } else {
                        holder.tvReview.setText("Write A Review");
                    }
                } else {
                    holder.llReview.setVisibility(View.VISIBLE);
                    holder.tvReview.setText("Write A Review");
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            holder.tvName.setText(data.getName());
            holder.tvRewardPt.setText("+" + data.getRewardPt());
            try {
                if (!data.getTimings().equalsIgnoreCase("0")) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    final Date dateObj = sdf.parse(data.getTimings());
                    holder.tvDetails.setText("Table for " + data.getNoOfPeople() + " people at " + new SimpleDateFormat("hh:mm a").format(dateObj));
                } else {
                    holder.tvDetails.setText("Table for " + data.getNoOfPeople() + " people at " + data.getTimings());
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
            aQuery.id(holder.ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getImg());

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivImg;
        private TextView tvDetails, tvName, tvDate, tvCuisine, tvReview, tvRewardPt;
        private LinearLayout llReview;

        public CustomViewHolder(View view) {
            super(view);
            this.ivImg = (ImageView) view.findViewById(R.id.ivImg);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            this.tvDetails = (TextView) view.findViewById(R.id.tvDetails);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvCuisine = (TextView) view.findViewById(R.id.tvCuisine);
            this.tvRewardPt = (TextView) view.findViewById(R.id.tvRewardPt);
            this.tvReview = (TextView) view.findViewById(R.id.tvReview);
            this.llReview = (LinearLayout) view.findViewById(R.id.llReview);
//            if(type.equalsIgnoreCase("1")) {
//                view.setOnClickListener(this);
//            }else if(type.equalsIgnoreCase("2")){
            this.llReview.setOnClickListener(this);
//            }
        }

        @Override
        public void onClick(View v) {
//            if(type.equalsIgnoreCase("1")) {
//                myClickListener.onItemClick(getPosition(), v);
//            }else {
            int pos = getAdapterPosition();
            if (v.getId() == R.id.llReview) {
                if (list.get(pos).getType().equalsIgnoreCase("resort")) {

                    Fragment fragment = new WriteReviewResort();
                    Bundle bundle = new Bundle();
                    if (list.get(pos).isFlag()) {  //view review
                        bundle.putBoolean("check", true);
                        bundle.putString("hotel_id", list.get(pos).getHotelId());
                        bundle.putString("hotel_book_id", list.get(pos).getId());
                        bundle.putString("noise_level", list.get(pos).getNoiseLvl());
                        bundle.putString("review", list.get(pos).getReview());
                        bundle.putString("overall_rating", list.get(pos).getRating());
                        bundle.putString("food_rating", list.get(pos).getFood());
                        bundle.putString("service_rating", list.get(pos).getService());
                    } else {
                        bundle.putString("hotel_id", list.get(pos).getHotelId());
                        bundle.putString("hotel_book_id", list.get(pos).getId());
                    }
                    fragment.setArguments(bundle);
                    Log.e("bundle contains", "" + bundle.toString());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, WriteReviewResort.class.getSimpleName());
                }


                if (list.get(pos).getType().equalsIgnoreCase("rest")) {

                    Fragment fragment = new WriteReviewRestaurant();
                    Bundle bundle = new Bundle();
                    if (list.get(pos).isFlag()) {  //view review
                        bundle.putBoolean("check", true);
                        bundle.putString("rest_id", list.get(pos).getResid());
                        bundle.putString("booking_id", list.get(pos).getBookingId());
                        bundle.putString("noise_level", list.get(pos).getNoiseLvl());
                        bundle.putString("review", list.get(pos).getReview());
                        bundle.putString("overall_rating", list.get(pos).getRating());
                        bundle.putString("food_rating", list.get(pos).getFood());
                        bundle.putString("service_rating", list.get(pos).getService());
                        bundle.putString("ambience_rating", list.get(pos).getAmbience());
                    } else {
                        bundle.putString("rest_id", list.get(pos).getResid());
                        bundle.putString("booking_id", list.get(pos).getBookingId());
                    }
                    fragment.setArguments(bundle);
                    Log.e("bundle contains", "" + bundle.toString());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, WriteReviewRestaurant.class.getSimpleName());

                }


            }
//            }
        }
    }
}
