package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.SpecOfferDetails1;
import com.reservationng.models.DataHolder;
import com.reservationng.models.Offers;

/**
 * Created by android2 on 1/9/16.
 */
public class MoreHotDealsAdapter extends RecyclerView.Adapter<MoreHotDealsAdapter.CustomViewHolder>  {

    private Context mContext;
    Offers arr[];

    public MoreHotDealsAdapter(FragmentActivity activity, Offers arr[]) {
        this.mContext = activity;
        this.arr =arr;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_room_facility_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.tvRoomFacility.setText(arr[position].getName());

        Log.e("DataHolder",DataHolder.getInstance().chkDeal);

        //val4 click resort
        if(!DataHolder.getInstance().chkDeal.equalsIgnoreCase("val")) {
            holder.tvRoomFacility.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment fragment = new SpecOfferDetails1();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", arr[position].getId());
//                   bundle.putString("name", list.get(position).getOfferName());
//                   bundle.putString("desc", list.get(position).getOfferDesc());
                    fragment.setArguments(bundle);
                    Log.e("bundle ofr ", "" + bundle.toString() + DataHolder.getInstance().curLati + " " +
                            DataHolder.getInstance().curLng);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, SpecOfferDetails1.class.getSimpleName());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arr.length;
    }



    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView tvRoomFacility;

        public CustomViewHolder(View view) {
            super(view);
            this.tvRoomFacility = (TextView) view.findViewById(R.id.tvRoomFacility);
        }
    }
}
