package com.reservationng.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Hotels.HotelNearByActivities;
import com.reservationng.R;
import com.reservationng.models.HotelDetail;
import com.reservationng.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class HotelsListAdapter extends RecyclerView.Adapter<HotelsListAdapter.CustomViewHolder> {

    private Context mContext;
    private List<HotelDetail> list;
    public static MyClickListener myClickListener;
    private AQuery aQuery = new AQuery(mContext);
    private HotelNearByActivities hotelNearByActivities;
    private int type;

    public HotelsListAdapter(FragmentActivity activity, List<HotelDetail> list) {
        this.mContext = activity;
        this.list = list;
        type = 1;
    }

    public HotelsListAdapter(FragmentActivity activity, ArrayList<HotelDetail> list, HotelNearByActivities hotelNearByActivities) {
        this.mContext = activity;
        this.list = list;
        this.hotelNearByActivities = hotelNearByActivities;
        type = 2;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotels_list_item, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        HotelDetail data = list.get(position);
        if (type == 2) { //for nearby activities of hotel
            holder.ratingbar.setVisibility(View.GONE);
            holder.tvDis.setVisibility(View.GONE);
            holder.tvReviews.setVisibility(View.GONE);
            holder.txtMobile.setVisibility(View.VISIBLE);
        } else {  //for hotel listing
            holder.ratingbar.setVisibility(View.VISIBLE);

            holder.tvReviews.setVisibility(View.VISIBLE);
            holder.txtMobile.setVisibility(View.GONE);

            holder.tvReviews.setVisibility(View.VISIBLE);
            if (data.getTotReview().equalsIgnoreCase("0")) {
                holder.tvReviews.setText("No Reviews");
            } else {
                holder.tvReviews.setText(data.getTotReview() + " Reviews");
            }
            holder.ratingbar.setRating(Float.parseFloat(data.getOverallRating()));
            LayerDrawable layerDrawable = (LayerDrawable) holder.ratingbar.getProgressDrawable();
            layerDrawable.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

            Log.e("dis", data.getDistance());
            if (!data.getDistance().equalsIgnoreCase("0")) {
                holder.tvDis.setVisibility(View.VISIBLE);
                holder.tvDis.setText(data.getDistance()+" miles");
            } else {
                holder.tvDis.setVisibility(View.GONE);
            }
        }
        holder.tvhotelname.setText(data.getName());
        holder.txtCuisine.setText(data.getCuisine());
        if (type == 1) {
            if (data.getPriceRange().equalsIgnoreCase("1")) {
                holder.txtPrice.setText("₦");
            } else if (data.getPriceRange().equalsIgnoreCase("2")) {
                holder.txtPrice.setText("₦₦");
            } else if (data.getPriceRange().equalsIgnoreCase("3")) {
                holder.txtPrice.setText("₦₦₦");
            } else if (data.getPriceRange().equalsIgnoreCase("4")) {
                holder.txtPrice.setText("₦₦₦₦");
            }
        } else {
            holder.txtPrice.setText(data.getAddress1() + ", " + data.getAddress2() + " " + data.getCityName() + ", " + data.getStateName());
        }
        holder.txtMobile.setText(data.getPhoneno());
        aQuery.id(holder.img_hotel).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getImage());
        holder.tvhotelname.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvhotelname, txtPrice, tvDis, txtCuisine, txtMobile, tvReviews;
        private ImageView img_hotel;
        RatingBar ratingbar;

        public CustomViewHolder(View view) {
            super(view);
            this.img_hotel = (ImageView) view.findViewById(R.id.img_hotel);
            this.ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            this.tvhotelname = (TextView) view.findViewById(R.id.tvhotelname);
            this.txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            this.txtMobile = (TextView) view.findViewById(R.id.txtMobile);
            this.txtCuisine = (TextView) view.findViewById(R.id.txtCuisine);
            this.tvDis = (TextView) view.findViewById(R.id.tvDis);
            this.tvReviews = (TextView) view.findViewById(R.id.tvReviews);
            if (type == 1) {
                view.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(), v);
        }
    }
}
