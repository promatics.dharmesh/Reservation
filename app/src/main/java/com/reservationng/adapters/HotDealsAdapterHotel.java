package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Dashboard;
import com.reservationng.Hotels.HotelFacilities;
import com.reservationng.R;
import com.reservationng.Restaurants.MoreHotDeals;
import com.reservationng.models.DataHolder;
import com.reservationng.models.HotelDetail;
import com.reservationng.utils.Constants;

import java.util.ArrayList;

public class HotDealsAdapterHotel extends RecyclerView.Adapter<HotDealsAdapterHotel.CustomViewHolder> {

    private Context mContext;
    private ArrayList<HotelDetail> offerList;
    private AQuery aQuery=new AQuery(mContext);

    public HotDealsAdapterHotel(FragmentActivity activity,ArrayList<HotelDetail> offerList) {
        this.mContext = activity;
        this.offerList=offerList;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hot_deals_adapter,parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HotDealsAdapterHotel.CustomViewHolder holder, int position) {

        final HotelDetail data=offerList.get(position);
        holder.btnMenu.setText("Facilities");
        holder.btnBook.setText("Details");
        holder.txtName.setText(data.getName());

        String addresss="";
        if(data.getCityName().equalsIgnoreCase("") || data.getCityName().equalsIgnoreCase("null")){
            addresss=data.getAddress1()+", "+data.getAddress2()+", "+data.getStateName();
        }else if(data.getAddress1().equalsIgnoreCase("")){
            addresss=data.getAddress2()+", "+data.getCityName()+", "+data.getStateName();
        }else if(data.getAddress2().equalsIgnoreCase("")){
            addresss=  data.getAddress1()+", "+data.getCityName()+", "+data.getStateName();
        }else if(data.getStateName().equalsIgnoreCase("") || data.getStateName().equalsIgnoreCase("null")){
            addresss= data.getAddress1()+", "+data.getAddress2()+", "+data.getCityName();
        }
        else{
            addresss= data.getAddress1()+", "+data.getAddress2()+", "+data.getCityName()+", "+data.getStateName();
        }
        if(data.getAddress1().equalsIgnoreCase("") && data.getAddress2().equalsIgnoreCase("")){
            addresss= data.getCityName()+", "+data.getStateName();
        }
        holder.txtAddress.setText(addresss);
        if(data.getCuisine().equalsIgnoreCase("null")){
            holder.txtCuisine.setText("N/A");
        }else{
            holder.txtCuisine.setText(data.getCuisine());
        }

        StringBuilder builder=new StringBuilder();
        for (int i = 0; i <data.getOffers().length ; i++) {
            builder.append(data.getOffers()[i].getName());
            builder.append(", ");
        }
        holder.txtDeals.setText(builder.toString().substring(0,builder.toString().length()-2));
        aQuery.id(holder.imgView).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getImage());

        /*holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new HotelFacilities();
                Bundle bundle=new Bundle();
                bundle.putString("id",data.getId());
                fragment.setArguments(bundle);
                Log.e("hotel id",data.getId());
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelFacilities.class.getSimpleName());
            }
        });*/
       /* holder.btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new HotelDetails();
                Bundle bundle=new Bundle();
                bundle.putBoolean("check",true);
                bundle.putString("id", data.getId()); //hotel id
                bundle.putString("hotelName",data.getName());
                bundle.putString("address1", data.getAddress1());
                bundle.putString("address2", data.getAddress2());
                bundle.putString("phone_number", data.getPhoneno());
                bundle.putString("long", data.getLng());
                bundle.putString("lat", data.getLat());
                bundle.putString("image1", data.getImage());
                bundle.putString("FavStatus", hotelList.get(position).getFavStatus());
                bundle.putString("hotel_city_id", data.getCityId());
                fragment.setArguments(bundle);
                Log.e("from hotdeals adapter",bundle.toString());
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelDetails.class.getSimpleName());
            }
        });*/
       /* holder.txtDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new MoreHotDeals();
                Bundle bundle=new Bundle();
                bundle.putParcelableArray("array", data.getOffers());
                fragment.setArguments(bundle);
                Log.e("arr contains",  bundle.toString());
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, MoreHotDeals.class.getSimpleName());
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView imgView;
        protected TextView txtName,txtCuisine,txtAddress,txtDeals;
        private Button btnMenu,btnBook;

        public CustomViewHolder(View view) {
            super(view);
            this.txtName = (TextView) view.findViewById(R.id.txtName);
            this.txtCuisine = (TextView) view.findViewById(R.id.txtCuisine);
            this.txtAddress = (TextView) view.findViewById(R.id.txtAddress);
            this.txtDeals = (TextView) view.findViewById(R.id.txtDeals);
            this.imgView = (ImageView) view.findViewById(R.id.imgView);
            this.btnMenu = (Button) view.findViewById(R.id.btnMenu);
            this.btnBook=(Button)view.findViewById(R.id.btnBook);
            this.btnBook.setOnClickListener(this);
            this.btnMenu.setOnClickListener(this);
            this.txtDeals.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos=getAdapterPosition();
            switch (v.getId()){
                case R.id.btnBook:

                    Fragment fragment=new MoreHotDeals();
                    Bundle bundle=new Bundle();
                    bundle.putParcelableArray("array", offerList.get(pos).getOffers());
                    fragment.setArguments(bundle);
                    DataHolder.getInstance().chkDeal="val";
                    Log.e("arr contains",  bundle.toString());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, MoreHotDeals.class.getSimpleName());
                /*    Fragment fragment=new HotelDetails();
//                    Bundle bundle=new Bundle();
//                    bundle.putBoolean("check",true);
                    HotelDataHolder.getInstance().hotelId= offerList.get(pos).getId(); //hotel id
                    HotelDataHolder.getInstance().name=offerList.get(pos).getName();Hot
                    HotelDataHolder.getInstance().address1= offerList.get(pos).getAddress1();
                    HotelDataHolder.getInstance().address2= offerList.get(pos).getAddress2();
                    HotelDataHolder.getInstance().mobile=offerList.get(pos).getPhoneno();
                    //cur n dest lat n lng same here......
                    HotelDataHolder.getInstance().desLng= offerList.get(pos).getLng();
                    HotelDataHolder.getInstance().desLat=offerList.get(pos).getLat();
                    HotelDataHolder.getInstance().curLng= offerList.get(pos).getLng();
                    HotelDataHolder.getInstance().curLat=offerList.get(pos).getLat();
                    HotelDataHolder.getInstance().img= offerList.get(pos).getImage();
                    HotelDataHolder.getInstance().favStatus= offerList.get(pos).getFavStatus();
                    HotelDataHolder.getInstance().cityId= offerList.get(pos).getCityId();
                    HotelDataHolder.getInstance().city=offerList.get(pos).getCityName();
                    HotelDataHolder.getInstance().state= offerList.get(pos).getStateName();
                    HotelDataHolder.getInstance().overallRating= offerList.get(pos).getOverallRating();
                    HotelDataHolder.getInstance().check= true;
                    Log.e("From hot deals adapter",HotelDataHolder.getInstance().curLng+"..."+HotelDataHolder.getInstance().curLat);
                    Log.e("From hot deals adapter",HotelDataHolder.getInstance().desLng+"..."+HotelDataHolder.getInstance().desLat);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelDetails.class.getSimpleName());
                    break;*/
                    break;
                case R.id.btnMenu:
                    Fragment fragment1=new HotelFacilities();
                    Bundle bundle1=new Bundle();
                    bundle1.putString("id", offerList.get(pos).getId());
                    fragment1.setArguments(bundle1);
                    Log.e("hotel id", offerList.get(pos).getId());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment1, HotelFacilities.class.getSimpleName());
                    break;

                case R.id.txtDeals:
                    Fragment fragment2=new MoreHotDeals();
                    Bundle bundle2=new Bundle();
                    bundle2.putParcelableArray("array", offerList.get(pos).getOffers());
                    fragment2.setArguments(bundle2);
                    DataHolder.getInstance().chkDeal="val";
                    Log.e("arr contains",  bundle2.toString());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment2, MoreHotDeals.class.getSimpleName());
                    break;
            }

        }
    }
}
