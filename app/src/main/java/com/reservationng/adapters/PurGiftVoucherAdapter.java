package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.models.PurGiftVouchers;
import com.reservationng.utils.ServiceCallback;

import java.util.ArrayList;

public class PurGiftVoucherAdapter extends RecyclerView.Adapter<PurGiftVoucherAdapter.CustomViewHolder> implements ServiceCallback {

    private Context mContext;
    private ArrayList<PurGiftVouchers> list;
    public static MyClickListener myClickListener;
    private PurGiftVouchers data;

    public PurGiftVoucherAdapter(FragmentActivity activity, ArrayList<PurGiftVouchers> list) {
        this.mContext = activity;
        this.list = list;
    }


    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_gift_voucher, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PurGiftVoucherAdapter.CustomViewHolder holder, final int position) {
        data = list.get(position);
        holder.tvAmt.setText("₦" + data.getVoucherAmt());
        holder.tvCode.setText(data.getVoucherCode());
        holder.tvTrans.setText(data.getTransNo());
        holder.tvRef.setText(data.getRefNo());
        try {
            holder.tvPurDate.setText(CommonUtils.target4.format(CommonUtils.target.parse(data.getPaymentDate())));
            holder.tvExpDate.setText(CommonUtils.target4.format(CommonUtils.target.parse(data.getExpirydate())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
        }
    }


    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvAmt, tvCode, tvTrans, tvRef, tvPurDate, tvExpDate;

        public CustomViewHolder(View view) {
            super(view);
            this.tvAmt = (TextView) view.findViewById(R.id.tvAmt);
            this.tvCode = (TextView) view.findViewById(R.id.tvCode);
            this.tvTrans = (TextView) view.findViewById(R.id.tvTrans);
            this.tvRef = (TextView) view.findViewById(R.id.tvRef);
            this.tvPurDate = (TextView) view.findViewById(R.id.tvPurDate);
            this.tvExpDate = (TextView) view.findViewById(R.id.tvExpDate);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(), v);
        }
    }
}
