package com.reservationng.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.models.voucherDetails;
import com.reservationng.utils.User;
import com.reservationng.utils.Webview;

import java.util.ArrayList;

public class VoucherListAdapter extends RecyclerView.Adapter<VoucherListAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<voucherDetails> list;

    public VoucherListAdapter(FragmentActivity activity, ArrayList<voucherDetails> list) {
        this.mContext = activity;
        this.list = list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.voucher_list_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VoucherListAdapter.CustomViewHolder holder, int position) {
        final voucherDetails data = list.get(position);
        holder.tvName.setText("Gift Voucher");
        holder.tvPrice.setText("₦" + data.getPrice());
        try {
            holder.tvDate.setText("Expiry Date : " + CommonUtils.target4.format(CommonUtils.target.parse(data.getExpiryDate())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvPrice, tvName, tvDate;
        private Button btnBuyMore;

        public CustomViewHolder(View view) {
            super(view);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            this.btnBuyMore = (Button) view.findViewById(R.id.btnBuyMore);
            this.btnBuyMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            if (v.getId() == R.id.btnBuyMore) {
                Log.e(" click pos", "" + pos + "id is" + list.get(pos).getId());
//              username--->Lekugo
//              pwd-->voustu88
//              String url=  "http://mercury.promaticstechnologies.com/Reservation/WebServices/buy_voucher_apps/user_id/gift_voucher_id?user_id="+ User.getInstance().getUserId()+"&"+"gift_voucher_id="+list.get(pos).getId();
//                String url = "http://mercury.promaticstechnologies.com/Reservation/WebServices/buy_voucher_android/" + User.getInstance().getUserId() + "/" + list.get(pos).getId();
                String url = "http://reservation.ng/dev/WebServices/buy_voucher_apps/" + User.getInstance().getUserId() + "/" + list.get(pos).getId();
                Log.e("url is", url);
//              Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));  //to view in browser
                Intent i = new Intent(mContext, Webview.class);
                i.putExtra("url", url);
                mContext.startActivity(i);
            }
        }
    }


}
