package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.Restaurants.MoreHotDeals;
import com.reservationng.Restaurants.RestaurantMenu;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.Constants;

import java.util.ArrayList;

public class HotDealsAdapterRestaurant extends RecyclerView.Adapter<HotDealsAdapterRestaurant.CustomViewHolder> {

    private Context mContext;
    private ArrayList<RestaurantLists> offerList;
    private AQuery aQuery=new AQuery(mContext);
    private String  day,mnth,date;
    private Fragment fragment;

    public HotDealsAdapterRestaurant(FragmentActivity activity, ArrayList<RestaurantLists> offerList) {
        this.mContext = activity;
        this.offerList=offerList;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hot_deals_adapter,parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HotDealsAdapterRestaurant.CustomViewHolder holder, int position) {

        final RestaurantLists data=offerList.get(position);
        holder.btnMenu.setText("Menu");
        holder.txtName.setText(data.getResName());
        String addresss="";
        if(data.getResCityName().equalsIgnoreCase("") || data.getResCityName().equalsIgnoreCase("null")){
            addresss=data.getResAddress1()+", "+data.getResAddress2()+", "+data.getResStateName();
        }else if(data.getResAddress1().equalsIgnoreCase("")){
            addresss=data.getResAddress2()+", "+data.getResCityName()+", "+data.getResStateName();
        }else if(data.getResAddress2().equalsIgnoreCase("")){
            addresss=  data.getResAddress1()+", "+data.getResCityName()+", "+data.getResStateName();
        }else if(data.getResStateName().equalsIgnoreCase("") || data.getResStateName().equalsIgnoreCase("null")){
            addresss= data.getResAddress1()+", "+data.getResAddress2()+", "+data.getResCityName();
        }
        else{
            addresss= data.getResAddress1()+", "+data.getResAddress2()+", "+data.getResCityName()+", "+data.getResStateName();
        }
        if(data.getResAddress1().equalsIgnoreCase("") && data.getResAddress2().equalsIgnoreCase("")){
            addresss= data.getResCityName()+", "+data.getResStateName();
        }
        holder.txtAddress.setText(addresss);

        if(data.getResCuisine().equalsIgnoreCase("null")){
            holder.txtCuisine.setText("N/A");
        }else {
            holder.txtCuisine.setText(data.getResCuisine());
        }
            StringBuilder builder=new StringBuilder();

        try {
            for (int i = 0; i < data.getOffers().length; i++) {
                builder.append(data.getOffers()[i].getName());
                builder.append(", ");
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        holder.txtDeals.setText(builder.toString().substring(0,builder.toString().length()-2));
        aQuery.id(holder.imgView).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getResImg());

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new RestaurantMenu();
                Bundle bundle=new Bundle();
                bundle.putString("restaurant_id",data.getResId());
                fragment.setArguments(bundle);
                Log.e("restaurant_id",data.getResId());
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, RestaurantMenu.class.getSimpleName());
            }
        });
      /*  try {
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dt = data.getResDate();
            Date MyDate = newDateFormat.parse(CommonUtils.target.format(CommonUtils.source2.parse(dt)));
            newDateFormat.applyPattern("EEEE MMM d");
            String MyDate1 = newDateFormat.format(MyDate);
            Log.e("date", MyDate1 + dt);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
        }catch(Exception e){
            e.printStackTrace();
        }*/
        holder.btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment=new MoreHotDeals();
                Bundle bundle=new Bundle();
//                Log.e("arr contains", data.getOffers()[0].getName());
                bundle.putString("restaurant_id", data.getResId());
                DataHolder.getInstance().chkDeal="";
                bundle.putParcelableArray("array", data.getOffers());
                fragment.setArguments(bundle);
                Log.e("arr contains",  bundle.toString());
                Log.e("offers..",bundle.toString()+" "+  DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng+" "+DataHolder.getInstance().destLati+" "+DataHolder.getInstance().destLng);

                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, MoreHotDeals.class.getSimpleName());


//                Fragment fragment1 = new SpecialOffersHotDeals();
//                Bundle bundle1 = new Bundle();
//                bundle1.putString("restaurant_id", data.getResId());
//                fragment1.setArguments(bundle1);
//                Log.e("offers..",bundle1.toString()+" "+  DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng+" "+DataHolder.getInstance().destLati+" "+DataHolder.getInstance().destLng);
//                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment1, SpecialOffersHotDeals.class.getSimpleName());

               /* Fragment fragment = new RestaurantBookScreen();
                Bundle bundle = new Bundle();
                DataHolder.getInstance().id=data.getResId();
                bundle.putString("rest_id", DataHolder.getInstance().id);
                bundle.putString("rest_name", data.getResName());
                bundle.putString("rest_desc", data.getResDesc());

                bundle.putString("date", DataHolder.getInstance().date);
                DataHolder.getInstance().slotId=data.getHotdealsTimingId();
                DataHolder.getInstance().slottiming=data.getResTableTiming();
                bundle.putString("slotId", DataHolder.getInstance().slotId );
                bundle.putString("slotTiming", DataHolder.getInstance().slottiming);
//                                                    bundle.putString("slotId", slot1.getSlotId());
//                                                    bundle.putString("slotTiming", btn.getText().toString());
                bundle.putString("restTableId", "111");
                bundle.putString("mobile_number", data.getResMobileNo());
                DataHolder.getInstance().noOfPeople=data.getNoOfPeople();
                bundle.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
                bundle.putString("img", data.getResImg());
                bundle.putString("reward_point", data.getRewardPt());
                DataHolder.getInstance().curLati=data.getResLati();
                DataHolder.getInstance().curLng=data.getResLng();
                DataHolder.getInstance().destLati=data.getResLati();
                DataHolder.getInstance().destLng=data.getResLng();
                bundle.putString("curlat", data.getResLati());
                bundle.putString("curlong", data.getResLng());
                fragment.setArguments(bundle);
                Log.e("rest book screen", bundle.toString());
//                                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment,"55").commit();

                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, RestaurantBookScreen.class.getSimpleName());*/
            }
        });

        holder.txtDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new MoreHotDeals();
                Bundle bundle=new Bundle();
//                Log.e("arr contains", data.getOffers()[0].getName());
                DataHolder.getInstance().chkDeal="val";
                bundle.putString("restaurant_id", data.getResId());
                bundle.putParcelableArray("array", data.getOffers());
                fragment.setArguments(bundle);
                Log.e("arr contains",  bundle.toString());
                Log.e("offers..",bundle.toString()+" "+  DataHolder.getInstance().curLati+" "+DataHolder.getInstance().curLng+" "+DataHolder.getInstance().destLati+" "+DataHolder.getInstance().destLng);
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, MoreHotDeals.class.getSimpleName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imgView;
        protected TextView txtName,txtCuisine,txtAddress,txtDeals;
        private Button btnMenu,btnBook;

        public CustomViewHolder(View view) {
            super(view);
            this.txtName = (TextView) view.findViewById(R.id.txtName);
            this.txtCuisine = (TextView) view.findViewById(R.id.txtCuisine);
            this.txtAddress = (TextView) view.findViewById(R.id.txtAddress);
            this.txtDeals = (TextView) view.findViewById(R.id.txtDeals);
            this.imgView = (ImageView) view.findViewById(R.id.imgView);
            this.btnMenu = (Button) view.findViewById(R.id.btnMenu);
            this.btnBook=(Button)view.findViewById(R.id.btnBook);
        }
    }
}
