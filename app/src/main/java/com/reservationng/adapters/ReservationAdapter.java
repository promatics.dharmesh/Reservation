package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.R;
import com.reservationng.Restaurants.UpcomingRestaurants;
import com.reservationng.models.ReservationRestaurants;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.CustomViewHolder> implements ServiceCallback {

    private Context mContext;
    private ArrayList<ReservationRestaurants> list;
    private AQuery aQuery = new AQuery(mContext);
    private UpcomingRestaurants upcomingRestaurants;
    private int type;
    public static MyClickListener myClickListener;
    private ReservationRestaurants data;
    View view;
    CustomViewHolder viewHolder;

    public ReservationAdapter(FragmentActivity activity, ArrayList<ReservationRestaurants> list) {
        this.mContext = activity;
        this.list = list;
        type = 1; //for previous reservations
    }

    public ReservationAdapter(FragmentActivity activity, ArrayList<ReservationRestaurants> list, UpcomingRestaurants upcomingRestaurants) {
        this.mContext = activity;
        this.list = list;
        this.upcomingRestaurants = upcomingRestaurants;
        type = 2;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservations_adapter, parent, false);
        viewHolder = new CustomViewHolder(view);
//         viewHolder.llReview.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReservationAdapter.CustomViewHolder holder, final int position) {
        data = list.get(position);
        if (type == 2) {
            holder.llReview.setVisibility(View.GONE);
            holder.view.setVisibility(View.GONE);
            holder.view1.setVisibility(View.GONE);
        } else {
            holder.llReview.setVisibility(View.VISIBLE);
            holder.view.setVisibility(View.VISIBLE);
            holder.view1.setVisibility(View.VISIBLE);
            Log.e("flag value", "" + data.isFlag() + data.getRewardPt());
            if (!data.isFlag()) {
                holder.tvReview.setText("Write A Review");
            } else {
                holder.tvReview.setText("View Review");
            }
        }
        holder.tvRewardPt.setText("+" + data.getRewardPt());
        holder.tvRestName.setText(data.getName());
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(data.getTimings());
            holder.tvDetails.setText("Table for " + data.getNoOfPeople() + " people at " + new SimpleDateFormat("hh:mm a").format(dateObj));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        aQuery.id(holder.ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getImg());
        holder.ivRestImg.setTag(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
        }
    }


    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivRestImg;
        private TextView tvDetails, tvRestName, tvReview, tvRewardPt;
        private LinearLayout llReview;
        private View view, view1;

        public CustomViewHolder(View view) {
            super(view);
            this.view = (View) view.findViewById(R.id.view);
            this.view1 = (View) view.findViewById(R.id.view1);
            this.ivRestImg = (ImageView) view.findViewById(R.id.ivRestImg);
            this.tvReview = (TextView) view.findViewById(R.id.tvReview);
            this.tvDetails = (TextView) view.findViewById(R.id.tvDetails);
            this.tvRestName = (TextView) view.findViewById(R.id.tvRestName);
            this.tvRewardPt = (TextView) view.findViewById(R.id.tvRewardPt);
            this.llReview = (LinearLayout) view.findViewById(R.id.llReview);
            if (type == 2) {
                view.setOnClickListener(this);
//                this.tvRestName.setOnClickListener(this);
            }
            this.llReview.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(), v);
            if (v.getId() == R.id.tvRestName) {
                int pos = getAdapterPosition();
                Log.e("pos on restname click", "" + pos + "rest id" + list.get(pos).getName() + "id.." + list.get(pos).getBookingId());
            }
        }
    }
}




