package com.reservationng.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.Restaurants.ReviewScreen;
import com.reservationng.models.ReviewDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class ReviewListAdapter extends BaseAdapter {
    private Context ctx;
    private ReviewScreen reviewScreen;
    private ArrayList<ReviewDetails> list;

    public ReviewListAdapter(FragmentActivity activity, ReviewScreen reviewScreen,ArrayList<ReviewDetails> list) {
        this.ctx = activity;
        this.reviewScreen = reviewScreen;
        this.list=list;
    }

    public ReviewListAdapter(FragmentActivity activity, ArrayList<ReviewDetails> list) {
        this.ctx=activity;
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolder holder;
        if (v == null) {
            v = LayoutInflater.from(ctx).inflate(R.layout.review_list_adapter, parent, false);
            holder = new ViewHolder();
            holder.ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
            holder.tvName = (TextView) v.findViewById(R.id.tvName);
            holder.tvReviews = (TextView) v.findViewById(R.id.tvReviews);
            v.setTag(holder);
        }
        holder = (ViewHolder) v.getTag();
        ReviewDetails data=list.get(position);
        String date[]=data.getDate().split(" ");
        try {
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date MyDate = newDateFormat.parse(date[0]);
            newDateFormat.applyPattern("MMM d");
           String MyDate1 = newDateFormat.format(MyDate);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
               String mnth = tokenizer.nextToken();
               String dte = tokenizer.nextToken();
                holder.tvName.setText("by "+data.getUserName()+" on "+ mnth+" "+dte);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        holder.tvReviews.setText(data.getReview());
        holder.ratingbar.setRating(Float.parseFloat(data.getRating()));
        LayerDrawable layerDrawable=(LayerDrawable)holder.ratingbar.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(ctx.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

        return v;
    }

    static class ViewHolder {
        RatingBar ratingbar;
        TextView tvName, tvReviews;

    }
}
