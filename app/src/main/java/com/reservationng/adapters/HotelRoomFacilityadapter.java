package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.R;
import com.reservationng.models.RoomFacilities;

public class HotelRoomFacilityadapter extends RecyclerView.Adapter<HotelRoomFacilityadapter.CustomViewHolder> {

    private Context mContext;
    RoomFacilities arr[];
    private AQuery aQuery = new AQuery(mContext);

    public HotelRoomFacilityadapter(FragmentActivity activity,RoomFacilities arr[]) {
        this.mContext = activity;
        this.arr =arr;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_room_facility_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
            holder.tvRoomFacility.setText(arr[position].getName());
    }

    @Override
    public int getItemCount() {
        return arr.length;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView tvRoomFacility;

        public CustomViewHolder(View view) {
            super(view);
            this.tvRoomFacility = (TextView) view.findViewById(R.id.tvRoomFacility);
        }
    }
}
