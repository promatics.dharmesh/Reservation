package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.Hotels.PrevHotelReservation;
import com.reservationng.Hotels.WriteReviewResort;
import com.reservationng.R;
import com.reservationng.models.ReservationHotel;
import com.reservationng.utils.Constants;

import java.util.ArrayList;

public class ReservationHotelAdapter extends RecyclerView.Adapter<ReservationHotelAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<ReservationHotel> list;
    private AQuery aQuery = new AQuery(mContext);
    public static MyClickListener myClickListener;
    private PrevHotelReservation prevHotelReservation;
//    private PrevHotelReservation prevHotelReservation;
    private String type;

    public ReservationHotelAdapter(FragmentActivity activity,ArrayList<ReservationHotel> list ) {
        this.mContext = activity;
        this.list = list;
        type="1";
    }

    public ReservationHotelAdapter(FragmentActivity activity, ArrayList<ReservationHotel> list, PrevHotelReservation prevHotelReservation) {
        this.mContext=activity;
        this.list=list;
        this.prevHotelReservation=prevHotelReservation;
        type="2";
    }


    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_hotel_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReservationHotelAdapter.CustomViewHolder holder, int position) {
        final ReservationHotel data = list.get(position);
        holder.tvName.setText(data.getHotelName());
        try {
            holder.tvDate.setText(CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckIn())) + " to " + CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckOut())));
        }catch (Exception e){
            e.printStackTrace();
        }
            holder.tvCuisine.setText(data.getCuisines());
        holder.tvDetails.setText("₦" + data.getPrice() + " for " + data.getTotRooms() + " rooms ");
        aQuery.id(holder.ivImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getHotelImg());

        if (!data.isFlag()) {
            holder.tvReview.setText("Write A Review");
        } else {
            holder.tvReview.setText("View A Review");
        }
        if(type.equalsIgnoreCase("2")){
            holder.llReview.setVisibility(View.VISIBLE);
        }else{
            holder.llReview.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivImg;
        private TextView tvDetails,tvName,tvDate,tvCuisine,tvReview;
        private LinearLayout llReview;

        public CustomViewHolder(View view) {
            super(view);
            this.ivImg = (ImageView) view.findViewById(R.id.ivImg);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            this.tvDetails = (TextView) view.findViewById(R.id.tvDetails);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvCuisine = (TextView) view.findViewById(R.id.tvCuisine);
            this.tvReview = (TextView) view.findViewById(R.id.tvReview);
            this.llReview = (LinearLayout) view.findViewById(R.id.llReview);
            if(type.equalsIgnoreCase("1")) {
                view.setOnClickListener(this);
            }else if(type.equalsIgnoreCase("2")){
                this.llReview.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if(type.equalsIgnoreCase("1")) {
                myClickListener.onItemClick(getPosition(), v);
            }else {
                int pos = getAdapterPosition();
                if (v.getId() == R.id.llReview) {
                    Log.e("name click pos", "" + pos + "tvname is" + list.get(pos).getHotelName());
                    Log.e("flag val", "" + list.get(pos).isFlag());
                    Fragment fragment = new WriteReviewResort();
                    Bundle bundle = new Bundle();
                    if (list.get(pos).isFlag()) {  //view review
                        bundle.putBoolean("check", true);
                        bundle.putString("hotel_id", list.get(pos).getHotelId());
                        bundle.putString("hotel_book_id", list.get(pos).getId());
                        bundle.putString("noise_level", list.get(pos).getNoiseLvl());
                        bundle.putString("review",list.get(pos).getReview());
                        bundle.putString("overall_rating", list.get(pos).getRating());
                        bundle.putString("food_rating", list.get(pos).getFood());
                        bundle.putString("service_rating",list.get(pos).getService());
                    } else {
                        bundle.putString("hotel_id", list.get(pos).getHotelId());
                        bundle.putString("hotel_book_id", list.get(pos).getId());
                    }
                    fragment.setArguments(bundle);
                    Log.e("bundle contains", "" +bundle.toString());
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, WriteReviewResort.class.getSimpleName());
                }
            }
        }
    }
}
