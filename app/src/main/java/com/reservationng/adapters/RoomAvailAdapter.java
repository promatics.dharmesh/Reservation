package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.reservationng.Hotels.HotelDetails;
import com.reservationng.R;

public class RoomAvailAdapter extends BaseAdapter {
    private Context ctx;
    private HotelDetails hotelDetails;

    public RoomAvailAdapter(FragmentActivity activity, HotelDetails hotelDetails) {
        this.ctx=activity;
        this.hotelDetails=hotelDetails;
    }
    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        v= LayoutInflater.from(ctx).inflate(R.layout.room_avail_list_item,parent,false);
        return v;
    }
}
