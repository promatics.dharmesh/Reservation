package com.reservationng.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.reservationng.MyProfile.Favorites;
import com.reservationng.MyProfile.Points;
import com.reservationng.MyProfile.Reservations;
import com.reservationng.MyProfile.Reviews;
import com.reservationng.MyProfile.VoucherList;

public class TabAdapter extends FragmentStatePagerAdapter {
    int numtabs;

    public TabAdapter(FragmentManager fr, int numtabs) {
        super(fr);
        this.numtabs = numtabs;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Points();
        } else if (position == 1) {
            return new Reservations();
        } else if (position == 2) {
            return new Reviews();
        } else if (position == 3) {
            return new Favorites();
        } else if (position == 4) {
            return new VoucherList();
        }
        return null;
    }

    @Override
    public int getCount() {
        return numtabs;
    }
}
