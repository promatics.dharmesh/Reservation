package com.reservationng.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.reservationng.Hotels.HotDealsHotel;
import com.reservationng.Restaurants.HotDealsRestaurant;

public class TabAdapter1 extends FragmentStatePagerAdapter {
    int numtabs;

    public TabAdapter1(FragmentManager fr, int numtabs) {
        super(fr);
        this.numtabs = numtabs;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new HotDealsRestaurant();
        } else if (position == 1) {
            return new HotDealsHotel();
        }
        return null;
    }

    @Override
    public int getCount() {
        return numtabs;
    }
}
