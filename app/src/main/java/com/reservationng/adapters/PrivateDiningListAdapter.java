package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.PrivateDiningItemDetail;
import com.reservationng.R;

public class PrivateDiningListAdapter extends RecyclerView.Adapter<PrivateDiningListAdapter.CustomViewHolder> {

    private Context mContext;

    public PrivateDiningListAdapter(FragmentActivity activity) {
        this.mContext = activity;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.private_dining_list_item, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.tvhotelname.setTag(holder);
        holder.imageView.setTag(holder);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, new PrivateDiningItemDetail(), PrivateDiningItemDetail.class.getSimpleName());
            }
        });
    }


    @Override
    public int getItemCount() {
        return 5;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView tvhotelname;
        CardView container;

        public CustomViewHolder(View view) {
            super(view);
            this.container = (CardView) view.findViewById(R.id.dining_container);
            this.tvhotelname = (TextView) view.findViewById(R.id.tvhotelname);
            this.imageView = (ImageView) view.findViewById(R.id.img_dining);
        }
    }
}
