package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.models.RestMenuCategory;

import java.util.List;

/**
 * Created by android2 on 12/22/15.
 */
public class RestMenuAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<RestMenuCategory> listHeader;

    public RestMenuAdapter(FragmentActivity activity, List<RestMenuCategory> listHeader) {
        this.context = activity;
        this.listHeader = listHeader;

    }

    @Override
    public int getGroupCount() {
        return listHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Log.e("child size", "" + listHeader.get(groupPosition).getMenuItems().length);
        return listHeader.get(groupPosition).getMenuItems().length;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int arg0, boolean arg1, View v, ViewGroup arg3) {
        v = LayoutInflater.from(context).inflate(
                R.layout.expandable_lv_header, arg3, false);
        TextView txt = (TextView) v.findViewById(R.id.text1);
        txt.setText(listHeader.get(arg0).getCatName());
        txt.setText(listHeader.get(arg0).getCatName());
        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(
                R.layout.rest_menuitem, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        TextView tvCom = (TextView) convertView.findViewById(R.id.tvCom);
        tvName.setText(listHeader.get(groupPosition).getMenuItems()[childPosition].getMenuItemName());
        tvPrice.setText("₦ " + listHeader.get(groupPosition).getMenuItems()[childPosition].getMenuItemPrice());
        tvCom.setText(listHeader.get(groupPosition).getMenuItems()[childPosition].getMenuItemCom());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
