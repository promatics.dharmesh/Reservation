package com.reservationng.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.MyProfile.Favorites;
import com.reservationng.R;
import com.reservationng.models.HotelDetail;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FavoritesRestAdapter extends RecyclerView.Adapter<FavoritesRestAdapter.CustomViewHolder> implements ServiceCallback {

    private Context mContext;
    private ArrayList<RestaurantLists> list;
    private ArrayList<HotelDetail> hotellist;
    private AQuery aQuery=new AQuery(mContext);
    private Favorites favorites;
    public static MyClickListener myClickListener;

    public FavoritesRestAdapter(FragmentActivity activity, ArrayList<RestaurantLists> list, Favorites favorites) {
        this.mContext = activity;
        this.list=list;
        this.favorites=favorites;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_adapter,parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FavoritesRestAdapter.CustomViewHolder holder, int position) {
        final RestaurantLists data=list.get(position);
        holder.txtName.setText(data.getResName());
        holder.tvCuisine.setText(data.getResCuisine());
        holder.tvReviews.setText(data.getResTotReviews()+" Reviews");
        aQuery.id(holder.ivRest).progress(R.id.progressbar).image(Constants.BASE_REST_IMG+data.getResImg());

        holder.ratingbar.setRating(Float.parseFloat(data.getResOverallRating()));
        LayerDrawable layerDrawable=(LayerDrawable)holder.ratingbar.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

        Log.e("rest img",Constants.BASE_REST_IMG+data.getResImg());
        holder.ivFav.setTag(position);
        holder.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("rest_id",data.getResId());
                map.put("user_id", User.getInstance().getUserId());
                Log.e("map values", map.toString());
                new CallService(FavoritesRestAdapter.this,mContext,Constants.REQ_ADD_FAVOURITE,map).execute(Constants.ADD_FAVOURITE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode)
        {
            case Constants.REQ_ADD_FAVOURITE:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        favorites.favrestList();
                        notifyDataSetChanged();
                    } else {
                        CommonUtils.showToast(mContext, "Something went wrong");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivRest,ivFav;
        private TextView txtName,tvCuisine,tvReviews;
        private RatingBar ratingbar;

        public CustomViewHolder(View view) {
            super(view);
            this.ivRest = (ImageView) view.findViewById(R.id.ivRest);
            this.ivFav = (ImageView) view.findViewById(R.id.ivFav);
            this.txtName = (TextView) view.findViewById(R.id.txtName);
            this.tvCuisine = (TextView) view.findViewById(R.id.tvCuisine);
            this.tvReviews = (TextView) view.findViewById(R.id.tvReviews);
            this.ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(), v);
        }
    }
}
