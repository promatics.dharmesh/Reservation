package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.Hotels.HotelBooking;
import com.reservationng.Hotels.HotelRoomFacilities;
import com.reservationng.R;
import com.reservationng.WithoutHotelSignIn;
import com.reservationng.models.HotelDataHolder;
import com.reservationng.models.RoomDetail;
import com.reservationng.models.RoomFacilities;
import com.reservationng.utils.Constants;
import com.reservationng.utils.User;

import java.util.Date;
import java.util.List;

public class HotelRoomAdapter extends RecyclerView.Adapter<HotelRoomAdapter.CustomViewHolder> {

    private Context mContext;
    private List<RoomDetail> list;
    private AQuery aQuery = new AQuery(mContext);
    private int totPrice;
    private long diff,diffInDays;

    public HotelRoomAdapter(FragmentActivity activity, List<RoomDetail> list) {
        this.mContext = activity;
        this.list = list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_room_info, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        final RoomDetail data = list.get(position);
        holder.tvRoomName.setText(data.getName());
        holder.tvprice.setText( "₦"+data.getPrice());
        aQuery.id(holder.ivRoomImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getImage());

        if (data.getFacility().length >= 1) {
            for (int i = 0; i < data.getFacility().length; i++) {
                RoomFacilities facilities = data.getFacility()[i];
//                Log.e("facilities list", facilities.getName());
                if (data.getFacility().length == 1) {
                    holder.tvfac1.setVisibility(View.VISIBLE);
                    holder.tvfac1.setText(data.getFacility()[0].getName());
                }
                if (data.getFacility().length == 2) {
                    holder.tvfac1.setVisibility(View.VISIBLE);
                    holder.tvfac2.setVisibility(View.VISIBLE);
                    holder.tvfac1.setText(data.getFacility()[0].getName());
                    holder.tvfac2.setText(data.getFacility()[1].getName());
                }
                if (data.getFacility().length == 3) {
                    holder.tvfac1.setVisibility(View.VISIBLE);
                    holder.tvfac2.setVisibility(View.VISIBLE);
                    holder.tvfac3.setVisibility(View.VISIBLE);
                    holder.tvfac1.setText(data.getFacility()[0].getName());
                    holder.tvfac2.setText(data.getFacility()[1].getName());
                    holder.tvfac3.setText(data.getFacility()[2].getName());
                }
                if (data.getFacility().length > 3) {
                    holder.tvViewMore.setVisibility(View.VISIBLE);
                    holder.tvfac1.setVisibility(View.VISIBLE);
                    holder.tvfac2.setVisibility(View.VISIBLE);
                    holder.tvfac3.setVisibility(View.VISIBLE);
                    holder.tvfac4.setVisibility(View.VISIBLE);
                    holder.tvfac1.setText(data.getFacility()[0].getName());
                    holder.tvfac2.setText(data.getFacility()[1].getName());
                    holder.tvfac3.setText(data.getFacility()[2].getName());
                    holder.tvfac4.setText(data.getFacility()[3].getName());
                }
            }
        } else {
            holder.tvfac1.setVisibility(View.GONE);
            holder.tvfac2.setVisibility(View.GONE);
            holder.tvfac3.setVisibility(View.GONE);
            holder.tvfac4.setVisibility(View.GONE);
            holder.tvViewMore.setVisibility(View.GONE);
        }
        holder.btnBookRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             try {
                 if(User.getInstance()!=null) {

                     if (!data.getFlag().equalsIgnoreCase("0")) {
                         Log.e("flag is ", data.getFlag());
                         Date checkIn = CommonUtils.source.parse(data.getCheckinDate());
                         Date checkOut = CommonUtils.source.parse(data.getCheckoutdate());
                         diff = checkOut.getTime() - checkIn.getTime();
                         int diffInDays = (int) diff / (1000 * 60 * 60 * 24);
                         Fragment fragment = new HotelBooking();
                         Bundle bundle = new Bundle();
                         bundle.putBoolean("check", false);
                         bundle.putString("checkout_date", data.getCheckoutdate());
                         bundle.putString("checkin_date", data.getCheckinDate());
                         if (diffInDays > 0) {
                             totPrice = Integer.parseInt(data.getNoOfRooms()) * Integer.parseInt(data.getPrice()) * (diffInDays);
                             bundle.putString("tot_price", String.valueOf(totPrice));
                         } else {
                             totPrice = Integer.parseInt(data.getNoOfRooms()) * Integer.parseInt(data.getPrice()) * 1;
                             bundle.putString("tot_price", String.valueOf(totPrice));
                         }
                         bundle.putString("room_img", data.getImage());
                         bundle.putString("room_id", data.getId());
                         bundle.putString("no_room", data.getNoOfRooms());
                         bundle.putString("hotel_id", data.getHotelId());
                         bundle.putString("room_name", data.getName());
                         fragment.setArguments(bundle);
                         Log.e("bundle contains", bundle.toString() + "days diff" + diffInDays);
                         ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelBooking.class.getSimpleName());
                     } else {
                         //comes from hotdeals
                         Log.e("flag is ", data.getFlag());
                         Fragment fragment = new HotelBooking();
                         Bundle bundle = new Bundle();
                         bundle.putBoolean("check", true);
                         bundle.putString("room_img", data.getImage());
                         bundle.putString("room_id", data.getId());
                         bundle.putString("hotel_id", data.getHotelId());
                         bundle.putString("room_name", data.getName());
                         bundle.putString("room_price", data.getPrice());
                         fragment.setArguments(bundle);
                         Log.e("bundle for check true", bundle.toString());
                         ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelBooking.class.getSimpleName());

                     }
                 }else{
                     Date checkIn = CommonUtils.source.parse(data.getCheckinDate());
                     Date checkOut = CommonUtils.source.parse(data.getCheckoutdate());
                     diff = checkOut.getTime() - checkIn.getTime();
                     int diffInDays = (int) diff / (1000 * 60 * 60 * 24);

                     if (diffInDays > 0) {
                         totPrice = Integer.parseInt(data.getNoOfRooms()) * Integer.parseInt(data.getPrice()) * (diffInDays);
                        HotelDataHolder.getInstance().price= String.valueOf(totPrice);
                     } else {
                         totPrice = Integer.parseInt(data.getNoOfRooms()) * Integer.parseInt(data.getPrice()) * 1;
                         HotelDataHolder.getInstance().price= String.valueOf(totPrice);
                     }

                     HotelDataHolder.getInstance().bookingId=data.getId();
                     HotelDataHolder.getInstance().checkIn= data.getCheckinDate();
                     HotelDataHolder.getInstance().checkOut= data.getCheckoutdate();
                     HotelDataHolder.getInstance().hotelId=data.getHotelId();
                     HotelDataHolder.getInstance().room= data.getNoOfRooms();
//                     HotelDataHolder.getInstance().room_type=getArguments().getString("room_id");
                     HotelDataHolder.getInstance().roomName= data.getName();
                     HotelDataHolder.getInstance().RoomImg= data.getImage();

                     Log.e("without login bookingId",  HotelDataHolder.getInstance().bookingId);
                     Log.e("without login checkIn",  HotelDataHolder.getInstance().checkIn);
                     Log.e("without login checkOut",  HotelDataHolder.getInstance().checkOut);
                     Log.e("without login hotelId",  HotelDataHolder.getInstance().hotelId);
                     Log.e("without login room",  HotelDataHolder.getInstance().room);
                     Log.e("without login roomName",  HotelDataHolder.getInstance().roomName);
                     Log.e("without login RoomImg",  HotelDataHolder.getInstance().RoomImg);
                     Log.e("without login price",  HotelDataHolder.getInstance().price);
                     ((Dashboard) mContext).beginTransactions(R.id.content_frame, new WithoutHotelSignIn(), WithoutHotelSignIn.class.getSimpleName());
                 }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        holder.tvViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HotelRoomFacilities();
                Bundle bundle = new Bundle();
                bundle.putParcelableArray("array", data.getFacility());
                fragment.setArguments(bundle);
                Log.e("view more",bundle.toString());
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, HotelRoomFacilities.class.getSimpleName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView tvfac1, tvfac2, tvfac3, tvfac4, tvRoomName, tvViewMore, tvprice;
        private ImageView ivRoomImg;
        private Button btnBookRoom;

        public CustomViewHolder(View view) {
            super(view);
            this.tvfac1 = (TextView) view.findViewById(R.id.tvfac1);
            this.tvfac2 = (TextView) view.findViewById(R.id.tvfac2);
            this.tvfac3 = (TextView) view.findViewById(R.id.tvfac3);
            this.tvfac4 = (TextView) view.findViewById(R.id.tvfac4);
            this.tvRoomName = (TextView) view.findViewById(R.id.tvRoomName);
            this.tvViewMore = (TextView) view.findViewById(R.id.tvViewMore);
            this.tvprice = (TextView) view.findViewById(R.id.tvprice);
            this.ivRoomImg = (ImageView) view.findViewById(R.id.ivRoomImg);
            this.btnBookRoom = (Button) view.findViewById(R.id.btnBookRoom);
        }
    }
}
