package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.models.CombineReservations;
import com.reservationng.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class CombineBookingsAdapter extends RecyclerView.Adapter<CombineBookingsAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<CombineReservations> list;
    private AQuery aQuery = new AQuery(mContext);
    public static MyClickListener myClickListener;
    private String type;

    public CombineBookingsAdapter(FragmentActivity activity,ArrayList<CombineReservations> list ) {
        this.mContext = activity;
        this.list = list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_reservations, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CombineBookingsAdapter.CustomViewHolder holder, int position) {
        final CombineReservations data = list.get(position);

        if(data.getType().equalsIgnoreCase("resort")){
            holder.tvCuisine.setVisibility(View.VISIBLE);
            holder.tvCuisine.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            holder.tvDetails.setVisibility(View.VISIBLE);
            holder.tvName.setText(data.getHotelName());
            try {
                holder.tvDate.setText(CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckIn())) + " to " + CommonUtils.target4.format(CommonUtils.source.parse(data.getCheckOut())));
            }catch (Exception e){
                e.printStackTrace();
            }
            holder.tvCuisine.setText(data.getCuisines());
            holder.tvDetails.setText("₦" + data.getPrice() + " for " + data.getTotRooms() + " rooms ");
            aQuery.id(holder.ivImg).progress(R.id.progressbar).image(Constants.BASE_HOTEL_IMG + data.getHotelImg());

        }
        if(data.getType().equalsIgnoreCase("rest"))
        {
            holder.tvDetails.setVisibility(View.GONE);
            holder.tvCuisine.setVisibility(View.VISIBLE);
            holder.tvCuisine.setCompoundDrawablesWithIntrinsicBounds(R.drawable.diamond,0,0,0);
            holder.tvCuisine.setText("+" + data.getRewardPt());
            holder.tvName.setText(data.getName());
//            Log.e("timings",data.getTimings());
            try {
                if(!data.getTimings().equalsIgnoreCase("0")) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                    final Date dateObj = sdf.parse(data.getTimings());
                    holder.tvDate.setText("Table for " + data.getNoOfPeople() + " people at " + new SimpleDateFormat("hh:mm a").format(dateObj));
                }else{
                    holder.tvDate.setText("Table for " + data.getNoOfPeople() + " people at " + data.getTimings());
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
            aQuery.id(holder.ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getImg());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivImg;
        private TextView tvDetails,tvName,tvDate,tvCuisine,tvReview;
        private LinearLayout llReview;

        public CustomViewHolder(View view) {
            super(view);
            this.ivImg = (ImageView) view.findViewById(R.id.ivImg);
            this.tvDate = (TextView) view.findViewById(R.id.tvDate);
            this.tvDetails = (TextView) view.findViewById(R.id.tvDetails);
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvCuisine = (TextView) view.findViewById(R.id.tvCuisine);
            this.tvReview = (TextView) view.findViewById(R.id.tvReview);
            this.llReview = (LinearLayout) view.findViewById(R.id.llReview);
                view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
                myClickListener.onItemClick(getPosition(), v);
        }
    }
}
