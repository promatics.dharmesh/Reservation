package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.Restaurants.HotDealsRestaurant;

/**
 * Created by android2 on 12/4/15.
 */
public class RestaurantCategoryAdapter extends RecyclerView.Adapter<RestaurantCategoryAdapter.CustomViewHolder> {

    private Context mContext;

    public RestaurantCategoryAdapter(FragmentActivity activity) {
        this.mContext = activity;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_category_adapter, parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RestaurantCategoryAdapter.CustomViewHolder holder, int position) {
        holder.txtName.setTag(holder);
        holder.imageView.setTag(holder);
        if(position==0){
        holder.txtName.setText("Best Deals");
            holder.imageView.setImageResource(R.drawable.bestdeals);
        }
        else if(position==1){
            holder.txtName.setText("Best Breakfasts");
            holder.imageView.setImageResource(R.drawable.bestdeals);
        }else if(position==2){
            holder.txtName.setText("Spectacular View");
            holder.imageView.setImageResource(R.drawable.bestdeals);
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard) mContext).beginTransactions(R.id.content_frame, new HotDealsRestaurant(), HotDealsRestaurant.class.getSimpleName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView txtName;
        CardView container;

        public CustomViewHolder(View view) {
            super(view);
            this.container = (CardView) view.findViewById(R.id.item_layout_container);
            this.txtName = (TextView) view.findViewById(R.id.txtName);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
        }
    }
}
//http://mercury.promaticstechnologies.com/Reservation/WebServices/city_keyword_list