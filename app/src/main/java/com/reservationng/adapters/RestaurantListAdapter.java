package com.reservationng.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.Restaurants.RestaurantDetails;
import com.reservationng.models.DataHolder;
import com.reservationng.models.RestaurantLists;
import com.reservationng.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.CustomViewHolder> {

    private Context mContext;
    private List<RestaurantLists> list;
    public static MyClickListener myClickListener;
    private AQuery aQuery=new AQuery(mContext);

    public RestaurantListAdapter(FragmentActivity activity, List<RestaurantLists> list) {
        this.mContext = activity;
        this.list = list;
    }
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_list_item, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        RestaurantLists data = list.get(position);
        holder.tvhotelname.setText(data.getResName());
        if(!data.getResCuisine().equalsIgnoreCase("null")) {
            holder.txtCuisines.setText(data.getResCuisine());
        }else{
            holder.txtCuisines.setText("N/A");
        }

        if(!data.getResStateName().equalsIgnoreCase("null")) {
            holder.txtLoc.setText(data.getResCityName() + ", " + data.getResStateName());
        }else {
            holder.txtLoc.setText(data.getResCityName());
        }
        if(data.getPriceRange().equalsIgnoreCase("1")){
            holder.txtPriceRange.setText("₦");
        }else if(data.getPriceRange().equalsIgnoreCase("2")){
            holder.txtPriceRange.setText("₦₦");
        }else if(data.getPriceRange().equalsIgnoreCase("3")){
            holder.txtPriceRange.setText("₦₦₦");
        }else if(data.getPriceRange().equalsIgnoreCase("4")){
            holder.txtPriceRange.setText("₦₦₦₦");
        }

        holder.tvReviews.setVisibility(View.VISIBLE);

        if (!data.getDistance().equalsIgnoreCase("0")) {
            holder.txtdis.setVisibility(View.VISIBLE);
            holder.txtdis.setText(data.getDistance()+" miles");
        } else {
            holder.txtdis.setVisibility(View.GONE);
        }

        if(data.getResTotReviews().equalsIgnoreCase("0")){
            holder.tvReviews.setText("No Reviews");
        }else {
            holder.tvReviews.setText(data.getResTotReviews() + " Reviews");
        }
        if (!data.getResImg().equalsIgnoreCase("null")) {
            aQuery.id(holder.img_hotel).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getResImg());
        }else{
            aQuery.id(holder.img_hotel).progress(R.id.progressbar).image(R.drawable.img1);
        }

        holder.ratingbar.setRating(Float.parseFloat(data.getResOverallRating()));
        LayerDrawable layerDrawable=(LayerDrawable)holder.ratingbar.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
        try {
            if(data.getIsSlotsAvail().equalsIgnoreCase("1"))
            {
                holder.linearSlots.setVisibility(View.VISIBLE);
                holder.btn1.setVisibility(View.VISIBLE);
                holder.btn2.setVisibility(View.VISIBLE);
                holder.btn3.setVisibility(View.VISIBLE);
                holder.tvAvailability.setVisibility(View.GONE);
                if (data.getSlots().length > 0) {
                    for (int i = 0; i < data.getSlots().length; i++) {
                        String time1 = data.getSlots()[0].getTiming().substring(0, 5);
                        String time2 = data.getSlots()[1].getTiming().substring(0, 5);
                        String time3 = data.getSlots()[2].getTiming().substring(0, 5);
                        try {
                            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                            final Date dateObj1 = sdf.parse(time1);
                            final Date dateObj2 = sdf.parse(time2);
                            final Date dateObj3 = sdf.parse(time3);
                            holder.btn1.setText(new SimpleDateFormat("hh:mm a").format(dateObj1));
                            holder.btn2.setText(new SimpleDateFormat("hh:mm a").format(dateObj2));
                            holder.btn3.setText(new SimpleDateFormat("hh:mm a").format(dateObj3));
                        } catch (final Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }else {
//                if (data.getSlots().length == 0) {
                    holder.btn1.setVisibility(View.GONE);
                    holder.btn2.setVisibility(View.GONE);
                    holder.btn3.setVisibility(View.GONE);
                    holder.linearSlots.setVisibility(View.GONE);
                    holder.tvAvailability.setVisibility(View.VISIBLE);
//                    }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        holder.sublayout_hotel1.setTag(holder);
        holder.tvhotelname.setTag(holder);
        holder.txtLoc.setTag(holder);
        holder.txtCuisines.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }
    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


    public   class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private RelativeLayout sublayout_hotel1;
        private TextView tvhotelname, txtCuisines,txtLoc,tvReviews,txtPriceRange,txtdis,tvAvailability;
        CardView container;
        private LinearLayout linearSlots;
        private ImageView img_hotel;
        private HorizontalScrollView horizontalScroll;
        private RatingBar ratingbar;
        private  Button btn1,btn2,btn3;

        public CustomViewHolder(View view) {
            super(view);
            this.container = (CardView) view.findViewById(R.id.item_layout_container);
            this.img_hotel = (ImageView) view.findViewById(R.id.img_hotel);
            this.tvhotelname = (TextView) view.findViewById(R.id.tvhotelname);
            this.txtCuisines = (TextView) view.findViewById(R.id.txtCuisines);
            this.tvReviews = (TextView) view.findViewById(R.id.tvReviews);
            this.tvAvailability = (TextView) view.findViewById(R.id.tvAvailability);
            this.txtLoc = (TextView) view.findViewById(R.id.txtLoc);
            this.txtdis = (TextView) view.findViewById(R.id.txtdis);
            this.txtPriceRange = (TextView) view.findViewById(R.id.txtPriceRange);
            this.sublayout_hotel1 = (RelativeLayout) view.findViewById(R.id.sublayout_hotel1);
            this.linearSlots = (LinearLayout) view.findViewById(R.id.linearSlots);
            this.btn1 = (Button) view.findViewById(R.id.btn1);
            this.btn2 = (Button) view.findViewById(R.id.btn2);
            this.btn3 = (Button) view.findViewById(R.id.btn3);
            this.ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            this.tvAvailability.setOnClickListener(this);
            this.btn1.setOnClickListener(this);
            this.btn2.setOnClickListener(this);
            this.btn3.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(),v);
            int pos = getAdapterPosition();
            switch (v.getId())
            {
                case R.id.tvAvailability:

                    Fragment fragment = new RestaurantDetails();
                    DataHolder.getInstance().id=list.get(pos).getResId();
                    DataHolder.getInstance().name=list.get(pos).getResName();
                  /*  DataHolder.getInstance().date=getArguments().getString("date");
                    DataHolder.getInstance().time=twelvehrtime;
                    DataHolder.getInstance().curLati=getArguments().getString("lat");
                    DataHolder.getInstance().curLng=getArguments().getString("long");
                    DataHolder.getInstance().noOfPeople=noOfPeople;*/
                    DataHolder.getInstance().img=list.get(pos).getResImg();
                    Log.e("details are...",DataHolder.getInstance().id+"\n "+DataHolder.getInstance().name+"\n"+DataHolder.getInstance().date+"\n "+DataHolder.getInstance().time+"\n"+DataHolder.getInstance().curLati+"\n "+DataHolder.getInstance().curLng+"\n"+DataHolder.getInstance().noOfPeople+"\n "+DataHolder.getInstance().img);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, RestaurantDetails.class.getSimpleName());
                    break;

                case R.id.btn1:

                    Fragment fragment1 = new RestaurantDetails();
                    DataHolder.getInstance().id=list.get(pos).getResId();
                    DataHolder.getInstance().name=list.get(pos).getResName();
                    DataHolder.getInstance().time=btn1.getText().toString().trim();

                  /*  DataHolder.getInstance().curLati=getArguments().getString("lat");
                    DataHolder.getInstance().curLng=getArguments().getString("long");
                    DataHolder.getInstance().noOfPeople=noOfPeople;
                    DataHolder.getInstance().date=getArguments().getString("date");*/

                    DataHolder.getInstance().img=list.get(pos).getResImg();
                    Log.e("details are...",DataHolder.getInstance().id+"\n "+DataHolder.getInstance().name+"\n"+DataHolder.getInstance().date+"\n "+DataHolder.getInstance().time+"\n"+DataHolder.getInstance().curLati+"\n "+DataHolder.getInstance().curLng+"\n"+DataHolder.getInstance().noOfPeople+"\n "+DataHolder.getInstance().img);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment1, RestaurantDetails.class.getSimpleName());
                    break;

                case R.id.btn2:

                    Fragment fragment2 = new RestaurantDetails();
                    DataHolder.getInstance().id=list.get(pos).getResId();
                    DataHolder.getInstance().name=list.get(pos).getResName();
                    DataHolder.getInstance().time=btn2.getText().toString().trim();
                  /*  DataHolder.getInstance().date=getArguments().getString("date");
                    DataHolder.getInstance().curLati=getArguments().getString("lat");
                    DataHolder.getInstance().curLng=getArguments().getString("long");
                    DataHolder.getInstance().noOfPeople=noOfPeople;*/
                    DataHolder.getInstance().img=list.get(pos).getResImg();
                    Log.e("details are...",DataHolder.getInstance().id+"\n "+DataHolder.getInstance().name+"\n"+DataHolder.getInstance().date+"\n "+DataHolder.getInstance().time+"\n"+DataHolder.getInstance().curLati+"\n "+DataHolder.getInstance().curLng+"\n"+DataHolder.getInstance().noOfPeople+"\n "+DataHolder.getInstance().img);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment2, RestaurantDetails.class.getSimpleName());
                    break;


                case R.id.btn3:

                    Fragment fragment3 = new RestaurantDetails();
                    DataHolder.getInstance().id=list.get(pos).getResId();
                    DataHolder.getInstance().name=list.get(pos).getResName();
                    DataHolder.getInstance().time=btn3.getText().toString().trim();
                   /* DataHolder.getInstance().date=getArguments().getString("date");
                    DataHolder.getInstance().curLati=getArguments().getString("lat");
                    DataHolder.getInstance().curLng=getArguments().getString("long");
                    DataHolder.getInstance().noOfPeople=noOfPeople;*/
                    DataHolder.getInstance().img=list.get(pos).getResImg();
                    Log.e("details are...",DataHolder.getInstance().id+"\n "+DataHolder.getInstance().name+"\n"+DataHolder.getInstance().date+"\n "+DataHolder.getInstance().time+"\n"+DataHolder.getInstance().curLati+"\n "+DataHolder.getInstance().curLng+"\n"+DataHolder.getInstance().noOfPeople+"\n "+DataHolder.getInstance().img);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment3, RestaurantDetails.class.getSimpleName());
                    break;




            }

        }
    }
}
