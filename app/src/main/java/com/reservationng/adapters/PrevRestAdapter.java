package com.reservationng.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Dashboard;
import com.reservationng.R;
import com.reservationng.Restaurants.WriteReviewRestaurant;
import com.reservationng.models.ReservationRestaurants;
import com.reservationng.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by android2 on 1/20/16.
 */
public class PrevRestAdapter extends BaseAdapter {
    private Context ctx;
    private ArrayList<ReservationRestaurants> list;
    private AQuery aQuery = new AQuery(ctx);
    private ReservationRestaurants data;

    public PrevRestAdapter(FragmentActivity activity, ArrayList<ReservationRestaurants> list) {
        this.ctx = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolder holder;

        if (v == null) {
            v = LayoutInflater.from(ctx).inflate(R.layout.reservations_adapter, parent, false);
            holder = new ViewHolder();
            holder.view = (View) v.findViewById(R.id.view);
            holder.view1 = (View) v.findViewById(R.id.view1);
            holder.llReview = (LinearLayout) v.findViewById(R.id.llReview);
            holder.tvReview = (TextView) v.findViewById(R.id.tvReview);
            holder.tvRestName = (TextView) v.findViewById(R.id.tvRestName);
            holder.tvDetails = (TextView) v.findViewById(R.id.tvDetails);
            holder.tvRewardPt = (TextView) v.findViewById(R.id.tvRewardPt);
            holder.ivRestImg = (ImageView) v.findViewById(R.id.ivRestImg);
            holder.llReview.setVisibility(View.VISIBLE);
            holder.view.setVisibility(View.VISIBLE);
            holder.view1.setVisibility(View.VISIBLE);
            holder.llReview.setTag(position);
            v.setTag(holder);
        }
        holder = (ViewHolder) v.getTag();
        data = list.get(position);

        Log.e("flag value", "" + data.isFlag());
        if (!data.isFlag()) {
            holder.tvReview.setText("Write A Review");
        } else {
            holder.tvReview.setText("View A Review");
        }
        holder.tvRestName.setText(data.getName());
        holder.tvRewardPt.setText("+"+data.getRewardPt());
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(data.getTimings());
            holder.tvDetails.setText("Table for " + data.getNoOfPeople() + " people at " + new SimpleDateFormat("hh:mm a").format(dateObj));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        aQuery.id(holder.ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + data.getImg());
        holder.llReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tag = (int) v.getTag();
                Fragment fragment = new WriteReviewRestaurant();
                Bundle bundle = new Bundle();
                if (list.get(tag).isFlag()) {  //view review
                    bundle.putBoolean("check", true);
                    bundle.putString("rest_id", list.get(tag).getResid());
                    bundle.putString("booking_id", list.get(tag).getBookingId());
                    bundle.putString("noise_level", list.get(tag).getNoiseLvl());
                    bundle.putString("review",list.get(tag).getReview());
                    bundle.putString("overall_rating", list.get(tag).getRating());
                    bundle.putString("food_rating", list.get(tag).getFood());
                    bundle.putString("service_rating",list.get(tag).getService());
                    bundle.putString("ambience_rating", list.get(tag).getAmbience());
                } else {
                    bundle.putString("rest_id", list.get(tag).getResid());
                    bundle.putString("booking_id", list.get(tag).getBookingId());
                }
                fragment.setArguments(bundle);
                Log.e("bundle contains", "" +bundle.toString());
                ((Dashboard) ctx).beginTransactions(R.id.content_frame, fragment, WriteReviewRestaurant.class.getSimpleName());
            }
        });
        return v;
    }

    static class ViewHolder {
        ImageView ivRestImg;
        TextView tvDetails, tvRestName, tvReview,tvRewardPt;
        LinearLayout llReview;
        View view,view1;

    }
}
