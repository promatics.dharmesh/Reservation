package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.R;

import java.util.ArrayList;

/**
 * Created by android2 on 1/7/16.
 */
public class HotelFacilityAdapter extends RecyclerView.Adapter<HotelFacilityAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<String> list;

    public HotelFacilityAdapter(FragmentActivity activity,ArrayList<String> list) {
        this.mContext = activity;
        this.list =list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_room_facility_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.tvRoomFacility.setText(list.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView tvRoomFacility;

        public CustomViewHolder(View view) {
            super(view);
            this.tvRoomFacility = (TextView) view.findViewById(R.id.tvRoomFacility);
        }
    }
}
