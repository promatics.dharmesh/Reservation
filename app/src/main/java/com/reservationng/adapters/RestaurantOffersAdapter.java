package com.reservationng.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.R;
import com.reservationng.models.RestOffers;

import java.util.List;


public class RestaurantOffersAdapter extends RecyclerView.Adapter<RestaurantOffersAdapter.CustomViewHolder> {

    private Context mContext;
    private List<RestOffers> list;
    public static MyClickListener myClickListener;

    public RestaurantOffersAdapter(FragmentActivity activity,List<RestOffers> list) {
        this.mContext = activity;
        this.list=list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rest_offers_adapter,parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RestaurantOffersAdapter.CustomViewHolder holder, int position) {
        holder.tvOfferName.setTag(holder);
        holder.tvOfferDesc.setTag(holder);
        RestOffers data=list.get(position);
        holder.tvOfferName.setText(data.getOfferName());
        holder.tvOfferDesc.setText(data.getOfferDesc());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }
    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView tvOfferName,tvOfferDesc;

        public CustomViewHolder(View view) {
            super(view);
            this.tvOfferName = (TextView) view.findViewById(R.id.tvOfferName);
            this.tvOfferDesc = (TextView) view.findViewById(R.id.tvOfferDesc);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(),v);
        }
    }
}
