package com.reservationng.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.Dashboard;
import com.reservationng.MyProfile.SeeMoreRating;
import com.reservationng.R;
import com.reservationng.models.ReviewDetails;

import java.util.ArrayList;

public class ReviewResortAdapter extends RecyclerView.Adapter<ReviewResortAdapter.CustomViewHolder> {

    private Context mContext;
    private ArrayList<ReviewDetails> list;

    public ReviewResortAdapter(FragmentActivity activity, ArrayList<ReviewDetails> list) {
        this.mContext = activity;
        this.list = list;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_adapter, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReviewResortAdapter.CustomViewHolder holder, int position) {
        final ReviewDetails data = list.get(position);
        holder.txtfood.setText(CommonUtils.changeTextColor(mContext, "FOOD "));
        holder.txtfood.append("(" + data.getFood() + ") ");
        holder.txtservice.setVisibility(View.INVISIBLE);
        holder.txtambience.setText(CommonUtils.changeTextColor(mContext, "SERVICE "));
        holder.txtambience.append("(" + data.getService() + ") ");
        holder.txtfoodName.setText(data.getRestName());

        holder.ratingbar.setRating(Float.parseFloat(data.getRating()));
        LayerDrawable layerDrawable = (LayerDrawable) holder.ratingbar.getProgressDrawable();
        layerDrawable.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

        if(data.getReview().length()>35){
        holder.txtdetail.setText(data.getReview().substring(0,35)+"...");
        holder.tvSeeMore.setVisibility(View.VISIBLE);
        }else{
            holder.txtdetail.setText(data.getReview());
            holder.tvSeeMore.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RatingBar ratingbar;
        private TextView txtfood, txtambience, txtservice, txtfoodName, txtdetail,tvSeeMore;
        public CustomViewHolder(View v) {
            super(v);
            this.txtfood = (TextView) v.findViewById(R.id.txtfood);
            this.txtambience = (TextView) v.findViewById(R.id.txtambience);
            this.txtservice = (TextView) v.findViewById(R.id.txtservice);
            this.txtfoodName = (TextView) v.findViewById(R.id.txtfoodName);
            this.txtdetail = (TextView) v.findViewById(R.id.txtdetail);
            this.tvSeeMore = (TextView) v.findViewById(R.id.tvSeeMore);
            this.ratingbar = (RatingBar) v.findViewById(R.id.ratingbar);
            this.tvSeeMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos=getAdapterPosition();
            switch (v.getId()){
                case R.id.tvSeeMore:
                    Fragment fragment=new SeeMoreRating();
                    Bundle bundle=new Bundle();
                    bundle.putString("overall",list.get(pos).getRating());
                    bundle.putString("food",list.get(pos).getFood());
                    bundle.putString("service",list.get(pos).getService());
                    bundle.putString("amb",list.get(pos).getAmbience());
                    bundle.putString("desc",list.get(pos).getReview());
                    bundle.putString("name",list.get(pos).getRestName());
                    fragment.setArguments(bundle);
                    ((Dashboard) mContext).beginTransactions(R.id.content_frame, fragment, SeeMoreRating.class.getSimpleName());

                    break;
            }

        }
    }
}
