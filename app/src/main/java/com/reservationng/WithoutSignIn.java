package com.reservationng;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.reservationng.Login.CreateAccount;
import com.reservationng.Login.SignInScreen;
import com.reservationng.Restaurants.RestaurantBookScreen;
import com.reservationng.Restaurants.SpecialOfferConfirm;
import com.reservationng.Restaurants.SpecialOffersDetails;
import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;
import com.reservationng.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

public class WithoutSignIn extends Fragment implements View.OnClickListener, ServiceCallback {


    private TextView txtRestName, tvPeople, tvdte, tvtime;
    private Button btnSignIn, btnGuest, btnCreateAcc;
    private AQuery aQuery = new AQuery(getActivity());
    private ImageView ivImg;
    private CircleImageView ivRestImg;
    public static boolean BookFlag, ResvGuestFlag, SpecGuestFlag;
    private String MyDate1, dt, day, mnth, date, noOfpeople, showDate;

    Dialog d1;
    public static boolean withoutSignInback = false;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.without_signin, container, false);

        try {
            txtRestName = (TextView) v.findViewById(R.id.txtRestName);
            tvPeople = (TextView) v.findViewById(R.id.tvPeople);
            tvdte = (TextView) v.findViewById(R.id.tvdte);
            tvtime = (TextView) v.findViewById(R.id.tvtime);
            btnSignIn = (Button) v.findViewById(R.id.btnSignIn);
            btnGuest = (Button) v.findViewById(R.id.btnGuest);
            btnCreateAcc = (Button) v.findViewById(R.id.btnCreateAcc);
            ivImg = (ImageView) v.findViewById(R.id.ivImg);
            ivRestImg = (CircleImageView) v.findViewById(R.id.ivRestImg);

            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            dt = DataHolder.getInstance().date;

            Date MyDate = newDateFormat.parse(dt);
            newDateFormat.applyPattern("EEEE MMMM d");
            MyDate1 = newDateFormat.format(MyDate);
            Log.e("date", MyDate1 + dt);
            StringTokenizer tokenizer = new StringTokenizer(MyDate1, " ");
            while (tokenizer.hasMoreTokens()) {
                day = tokenizer.nextToken();
                mnth = tokenizer.nextToken();
                date = tokenizer.nextToken();
            }
            txtRestName.setText(DataHolder.getInstance().name);
            tvPeople.setText(DataHolder.getInstance().noOfPeople);
            tvdte.setText(day + ", " + mnth + " " + date);
            tvtime.setText(DataHolder.getInstance().time);
            aQuery.id(ivImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + DataHolder.getInstance().img);
            aQuery.id(ivRestImg).progress(R.id.progressbar).image(Constants.BASE_REST_IMG + DataHolder.getInstance().img);
            btnSignIn.setOnClickListener(this);
            btnGuest.setOnClickListener(this);
            btnCreateAcc.setOnClickListener(this);

            Log.e("name", DataHolder.getInstance().name + "\n");
            Log.e("dte", DataHolder.getInstance().date + "\n");
            Log.e("tym", DataHolder.getInstance().time + "\n");
            Log.e("img", DataHolder.getInstance().img + "\n");
            Log.e("people", DataHolder.getInstance().noOfPeople + "\n");
            Log.e("tableTimeId", DataHolder.getInstance().tableTimeId + "\n");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignIn:
                SignInScreen.Bookflag = true;

                final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
//                d.setTitle("Check-In");
                View vvv = LayoutInflater.from(getActivity()).inflate(R.layout.signin_dialog, null);
                d.setView(vvv);
                d1 = d.create();
                final EditText etEmail = (EditText) vvv.findViewById(R.id.etEmail);
                final EditText etPwd = (EditText) vvv.findViewById(R.id.etPwd);
                TextView txtSignin = (TextView) vvv.findViewById(R.id.txtSignin);
                TextView txtCancel = (TextView) vvv.findViewById(R.id.txtCancel);
                TextView txtForgotPwd = (TextView) vvv.findViewById(R.id.txtForgotPwd);
                CheckBox chkshowPwd = (CheckBox) vvv.findViewById(R.id.chkshowPwd);
                etEmail.setText(DataHolder.getInstance().email);
                txtForgotPwd.setVisibility(View.GONE);
                chkshowPwd.setVisibility(View.GONE);
                txtSignin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("email", etEmail.getText().toString().trim());
                        map.put("password", etPwd.getText().toString().trim());
                        Log.e("map values", map.toString());
                        new CallService(WithoutSignIn.this, getActivity(), Constants.REQ_LOGIN, map).execute(Constants.LOGIN);
                    }
                });
                txtCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d1.dismiss();
                    }
                });
                d1.show();
                break;
            case R.id.btnGuest:

                Log.e("Spec offr guest flag",""+SpecialOffersDetails.SprcOfferGuestFlag);
                if(User.getInstance()!=null) {
                    withoutSignInback = false;
                }else{
                    withoutSignInback = true;
                }
                if (SpecialOffersDetails.SprcOfferGuestFlag) {
                    SpecGuestFlag = true;

                    Fragment fragment = new SpecialOfferConfirm();
                    Bundle bundle = new Bundle();
                    bundle.putString("restaurant_id", DataHolder.getInstance().id);
                    bundle.putString("book_time_slot_id", DataHolder.getInstance().slotId);
                    try {
                        bundle.putString("book_date", CommonUtils.target2.format(CommonUtils.target.parse(DataHolder.getInstance().date)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    bundle.putString("offer_id", DataHolder.getInstance().offer_id);
                    bundle.putString("rest_name", DataHolder.getInstance().name);
                    bundle.putString("offer_name", DataHolder.getInstance().offerName);
                    bundle.putString("no_of_person", DataHolder.getInstance().noOfPeople);
                    bundle.putString("timee", DataHolder.getInstance().time);
                    fragment.setArguments(bundle);
                    Log.e(" book screen offers..", bundle.toString() + DataHolder.getInstance().curLng + " " + DataHolder.getInstance().curLati + DataHolder.getInstance().img);
                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, SpecialOfferConfirm.class.getSimpleName());
                } else {
                    ResvGuestFlag = true;
                    Fragment fragment = new RestaurantBookScreen();
                    Bundle bundle = new Bundle();
                    bundle.putString("rest_id", DataHolder.getInstance().id);
                    bundle.putString("rest_name", DataHolder.getInstance().name);
                    bundle.putString("rest_desc", DataHolder.getInstance().desc);
                    bundle.putString("date", DataHolder.getInstance().date);
                    bundle.putString("slotId", DataHolder.getInstance().tableTimeId);
                    DataHolder.getInstance().slotId = DataHolder.getInstance().tableTimeId;
                    DataHolder.getInstance().slottiming = DataHolder.getInstance().time;
                    bundle.putString("slotTiming", DataHolder.getInstance().slottiming);
                    bundle.putString("restTableId", "");
                    bundle.putString("mobile_number", DataHolder.getInstance().mobile);
                    bundle.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
                    bundle.putString("img", DataHolder.getInstance().img);
                    bundle.putString("reward_point", DataHolder.getInstance().rewardPt);
                    bundle.putString("curlat", DataHolder.getInstance().curLati);
                    bundle.putString("curlong", DataHolder.getInstance().curLng);
                    fragment.setArguments(bundle);
                    Log.e("rest book screen", bundle.toString());
                    ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantBookScreen.class.getSimpleName());
                }
                break;

            case R.id.btnCreateAcc:
                CreateAccount.createFlag = true;
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new CreateAccount(), "Create Account");
                break;
        }
    }

    private void booking() {
        DataHolder.getInstance().time = DataHolder.getInstance().slottiming;
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("restaurant_id", DataHolder.getInstance().id);
        map.put("timing_id", DataHolder.getInstance().tableTimeId);
        map.put("no_of_person", DataHolder.getInstance().noOfPeople);
        map.put("member_id", "");

        map.put("first_name", DataHolder.getInstance().efname);
        map.put("last_name", DataHolder.getInstance().elname);
        map.put("email", DataHolder.getInstance().emailuser);
        map.put("mobile_number", DataHolder.getInstance().ephn);
        map.put("special_request", DataHolder.getInstance().specReq);
        Log.e("map values", map.toString());
        new CallService(this, getActivity(), Constants.REQ_RESTAURANT_TABLE_BOOKING, map).execute(Constants.RESTAURANT_TABLE_BOOKING);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_LOGIN:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        withoutSignInback=false;
                        Log.e("WithoutSignninback",""+withoutSignInback);
                        JSONObject info = object.getJSONObject("info");
                        JSONObject user = info.getJSONObject("User");
                        //     JSONObject Country = info.getJSONObject("Country");
                        JSONObject City = info.getJSONObject("City");
                        JSONObject State = info.getJSONObject("State");
                        String id = user.getString("id");
                        String email = user.getString("email");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String cityId = user.getString("city");
                        String cityName = City.getString("city_name");
                        String stateId = City.getString("state_id");
                        String stateName = State.getString("state_name");
                        //      String countryId = user.getString("location");
                        // String countryName = Country.getString("country_name");
                        String address1 = user.getString("address1");
                        String address2 = user.getString("address2");
                        String mobileNo = user.getString("mobile_number");
                        String title = user.getString("title");
                        String countryCode="234";

                        Log.e("val >>", cityId + "\n" + cityName + "\n" + title + "\n" + address1 + "\n" + address2 + "\n" + mobileNo + "\n" + stateId + "\n" + stateName);
                        Log.e("SpecialOff det GuestFla", "" + SpecialOffersDetails.SprcOfferGuestFlag+" "+withoutSignInback);

                        User.storeUserData(id, fname, lname, email, cityId, cityName, address1, address2, mobileNo, title, stateId, stateName,countryCode);
                        d1.dismiss();

                        if (SpecialOffersDetails.SprcOfferGuestFlag) {
                            Fragment fragment = new SpecialOfferConfirm();
                            Bundle bundle = new Bundle();
                            bundle.putString("restaurant_id", DataHolder.getInstance().id);
                            bundle.putString("book_time_slot_id", DataHolder.getInstance().slotId);
                            try {
                                bundle.putString("book_date", CommonUtils.target2.format(CommonUtils.target.parse(DataHolder.getInstance().date)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            bundle.putString("offer_id", DataHolder.getInstance().offer_id);
                            bundle.putString("rest_name", DataHolder.getInstance().name);
                            bundle.putString("offer_name", DataHolder.getInstance().offerName);
                            bundle.putString("no_of_person", DataHolder.getInstance().noOfPeople);
                            bundle.putString("timee", DataHolder.getInstance().time);
                            fragment.setArguments(bundle);
                            Log.e(" book screen offers..", bundle.toString() + DataHolder.getInstance().curLng + " " + DataHolder.getInstance().curLati + DataHolder.getInstance().img);
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, SpecialOfferConfirm.class.getSimpleName());
                        } else {
                            Fragment fragment = new RestaurantBookScreen();
                            Bundle bundle = new Bundle();
                            bundle.putString("rest_id", DataHolder.getInstance().id);
                            bundle.putString("rest_name", DataHolder.getInstance().name);
                            bundle.putString("rest_desc", DataHolder.getInstance().desc);
                            bundle.putString("date", DataHolder.getInstance().date);
                            bundle.putString("slotId", DataHolder.getInstance().tableTimeId);
                            DataHolder.getInstance().slotId = DataHolder.getInstance().tableTimeId;
                            DataHolder.getInstance().slottiming = DataHolder.getInstance().time;
                            bundle.putString("slotTiming", DataHolder.getInstance().slottiming);
                            bundle.putString("restTableId", "");
                            bundle.putString("mobile_number", DataHolder.getInstance().mobile);
                            bundle.putString("noOfPeople", DataHolder.getInstance().noOfPeople);
                            bundle.putString("img", DataHolder.getInstance().img);
                            bundle.putString("reward_point", DataHolder.getInstance().rewardPt);
                            bundle.putString("curlat", DataHolder.getInstance().curLati);
                            bundle.putString("curlong", DataHolder.getInstance().curLng);
                            fragment.setArguments(bundle);
                            Log.e("rest book screen", bundle.toString());
                            ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, RestaurantBookScreen.class.getSimpleName());
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Sign In Error");
                        builder.setMessage("Wrong Credentials");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case Constants.REQ_RESTAURANT_TABLE_BOOKING:
                try {
                    Log.e("response booking", response.toString());
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        JSONObject booking_info = object.getJSONObject("booking_info");
                        JSONObject RestaurantBooking = booking_info.getJSONObject("RestaurantBooking");
                        JSONObject Restaurant = booking_info.getJSONObject("Restaurant");

                        Fragment fragment = new ConfirmationPage();
                        DataHolder.getInstance().bookingId = RestaurantBooking.getString("id");
                        DataHolder.getInstance().tableTimeId = RestaurantBooking.getString("table_time_id");
                        DataHolder.getInstance().id = RestaurantBooking.getString("restaurant_id");
                        DataHolder.getInstance().noOfPeople = RestaurantBooking.getString("no_of_person");
                        DataHolder.getInstance().date = RestaurantBooking.getString("date");
                        DataHolder.getInstance().specReq = RestaurantBooking.getString("special_request");
                        StringBuilder ssb = new StringBuilder();
                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().name = Restaurant.getString("rest_name");
                        DataHolder.getInstance().img = Restaurant.getString("image1");
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
                        Log.e("with login", DataHolder.getInstance().id + DataHolder.getInstance().mobile + DataHolder.getInstance().address);
                        DataHolder.getInstance().address = ssb.toString();
                        DataHolder.getInstance().desc = Restaurant.getString("description");
                        DataHolder.getInstance().curLng = getArguments().getString("curlong");
                        DataHolder.getInstance().curLati = getArguments().getString("curlat");
                        ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, fragment, ConfirmationPage.class.getSimpleName());
                    } else if (code == 2) {
                        DataHolder.getInstance().email = object.getString("email");
                        JSONObject restaurant_info = object.getJSONObject("restaurant_info");
                        JSONObject Restaurant = restaurant_info.getJSONObject("Restaurant");
                        StringBuilder ssb = new StringBuilder();
                        if (!Restaurant.getString("address1").equalsIgnoreCase("null") || !Restaurant.getString("address1").isEmpty()) {
                            ssb.append(Restaurant.getString("address1"));
                        }
                        if (!Restaurant.getString("address2").equalsIgnoreCase("null") || !Restaurant.getString("address2").isEmpty()) {
                            if (ssb.length() > 0) {
                                ssb.append(", " + Restaurant.getString("address2"));
                            } else {
                                ssb.append(Restaurant.getString("address2"));
                            }
                        }
                        DataHolder.getInstance().mobile = Restaurant.getString("mobile_number");
//                                Log.e("with login", DataHolder.getInstance().id+DataHolder.getInstance().mobile+DataHolder.getInstance().address);
                        DataHolder.getInstance().address = ssb.toString();
                        DataHolder.getInstance().desc = Restaurant.getString("description");
                        DataHolder.getInstance().curLng = getArguments().getString("curlong");
                        DataHolder.getInstance().curLati = getArguments().getString("curlat");
                        AlertDialog.Builder builder = new AlertDialog.Builder(new
                                ContextThemeWrapper(getActivity(), android.R.style.
                                Theme_Holo_Light));
                        builder.setTitle("Reservation");
                        builder.setMessage("You are registered with Reservation.ng, Please login first and proceed for the booking.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                SpecialOffersDetails.SprcOfferGuestFlag=false;
                            }
                        });
                        builder.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
