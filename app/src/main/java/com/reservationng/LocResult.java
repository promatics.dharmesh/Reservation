package com.reservationng;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.reservationng.models.DataHolder;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

public class LocResult extends Fragment implements ServiceCallback{

        private ListView lvCountry;
        private EditText edtSearch;
        CountryAdapter adapter;
        ArrayList<Item> countryList = new ArrayList<Item>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.lv, container, false);
        edtSearch = (EditText)v.findViewById(R.id.edtSearch);
        lvCountry = (ListView)v.findViewById(R.id.lvCountry);
        cityKeywordList();
        edtSearch.setHint("Location or Cuisines or Restaurants");
        // filter on text change
//        lvCountry.setTextFilterEnabled(true);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.filter(edtSearch.getText().toString().trim().toLowerCase());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return v;
    }


    private void cityKeywordList() {
        HashMap<String, String> map = new HashMap<String, String>();
//        map.put("keyword", etlocation.getText().toString().trim());
        new CallService(this, getActivity(), Constants.REQ_CITY_KEYWORD_LIST, map, true).execute(Constants.CITY_KEYWORD_LIST);
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_CITY_KEYWORD_LIST:
                try {
                    Log.e(" response", response);
                    JSONObject object = new JSONObject(response);
                    int code = object.getInt("code");
                    if (code == 1) {
                        countryList.clear();
                        JSONArray locArr = object.getJSONArray("city_list");
                        countryList.add(new SectionItem("Locations"));
                        for (int i = 0; i < locArr.length(); i++) {
                            JSONObject ob = locArr.getJSONObject(i);
                            countryList.add(new EntryItem(ob.getString("label"),ob.getString("value")));
                        }

                        JSONArray cusine_list = object.getJSONArray("cusine_list");
                        countryList.add(new SectionItem("Cuisines"));
                        for (int i = 0; i < cusine_list.length(); i++) {
                            JSONObject ob = cusine_list.getJSONObject(i);
                            countryList.add(new EntryItem(ob.getString("label"),ob.getString("value")));
                        }
                        JSONArray rest_list = object.getJSONArray("rest_list");
                        countryList.add(new SectionItem("Restaurants"));
                        for (int i = 0; i < rest_list.length(); i++) {
                            JSONObject ob = rest_list.getJSONObject(i);
                            countryList.add(new EntryItem(ob.getString("label"),ob.getString("value")));
                        }

                        // set adapter
                        adapter = new CountryAdapter(getActivity(), countryList);
                        lvCountry.setAdapter(adapter);

                        lvCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                DataHolder.getInstance().loc=((EntryItem) countryList.get(position)).getTitle();
                                DataHolder.getInstance().locId=((EntryItem) countryList.get(position)).getValue();

                                StringTokenizer tokenizer = new StringTokenizer(DataHolder.getInstance().locId, "|");
                                String val1 = "", val2 = "";
                                while (tokenizer.hasMoreTokens()) {
                                    val1 = tokenizer.nextToken();
                                    val2 = tokenizer.nextToken();
                                }
                                Log.e("val1 n val2", val1 + "  " + val2);
                                if (val2.equalsIgnoreCase("0")) {
                                    DataHolder.getInstance().searchtype="3";
                                } else if (val2.equalsIgnoreCase("C")) {
                                    DataHolder.getInstance().searchtype="5";
                                } else {
                                    DataHolder.getInstance().searchtype="1";
                                }
                                getActivity().onBackPressed();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;
        }
    }

        public interface Item {
            public boolean isSection();
            public String getTitle();
        }

        public class SectionItem implements Item {
            private final String title;

            public SectionItem(String title) {
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            @Override
            public boolean isSection() {
                return true;
            }
        }

        public class EntryItem implements Item {
            public final String title,value;

            public EntryItem(String title,String value) {
                this.title = title;
                this.value = value;
            }

            public String getTitle() {
                return title;
            }

            public String getValue() {
                return value;
            }

            @Override
            public boolean isSection() {
                return false;
            }
        }

        /**
         * Adapter
         */
        public class CountryAdapter extends BaseAdapter {
            private Context context;
            private ArrayList<Item> item;
            private ArrayList<Item> originalItem;

            public CountryAdapter() {
                super();
            }

            public CountryAdapter(Context context, ArrayList<Item> item) {
                this.context = context;
                this.item = item;
                originalItem = new ArrayList<Item>();
                originalItem.addAll(item);
//                this.originalItem = item;
            }

            @Override
            public int getCount() {
                return item.size();
            }

            @Override
            public Object getItem(int position) {
                return item.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (item.get(position).isSection()) {
                    // if section header
                    convertView = inflater.inflate(R.layout.snippet_item1, parent, false);
                    TextView tvSectionTitle = (TextView) convertView.findViewById(R.id.tvSectionTitle);
                    tvSectionTitle.setText(((SectionItem) item.get(position)).getTitle());
                } else {
                    // if item
                    convertView = inflater.inflate(R.layout.snippet_item2, parent, false);
                    TextView tvItemTitle = (TextView) convertView.findViewById(R.id.tvItemTitle);
                    tvItemTitle.setText(((EntryItem) item.get(position)).getTitle());
                }

                return convertView;
            }
            public void filter(String str) {
                item.clear();
                if (str.length() == 0) {
                    item.addAll(originalItem);
                } else {
                    for (int i = 0; i < originalItem.size(); i++) {
                        String title = originalItem.get(i).getTitle().toLowerCase(Locale.ENGLISH);
                        Log.e("title", title);
                        if (title.startsWith(str)) {
                            item.add(originalItem.get(i));
                        }
                    }

                }
                notifyDataSetChanged();
            }
        }
}
