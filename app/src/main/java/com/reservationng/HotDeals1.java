package com.reservationng;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservationng.adapters.TabAdapter1;
import com.reservationng.utils.ServiceCallback;

public class HotDeals1 extends Fragment implements View.OnClickListener, ServiceCallback {
    TabLayout tabLayout;
    ViewPager viewPager;
    private TextView txtusername;
    private TabAdapter1 adapter;
    private LinearLayout llayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.restaurant_details, container, false);
        tabLayout = (TabLayout) v.findViewById(R.id.tab1);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager1);
        getActivity().setTitle("Hot Deals");
        llayout = (LinearLayout) v.findViewById(R.id.llayout);
        llayout.setVisibility(View.VISIBLE);
        tabLayout.addTab(tabLayout.newTab().setText("Restaurants"));
        tabLayout.addTab(tabLayout.newTab().setText("Resorts"));

        adapter = new TabAdapter1(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {

                } else if (tab.getPosition() == 1) {
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

    }
}
