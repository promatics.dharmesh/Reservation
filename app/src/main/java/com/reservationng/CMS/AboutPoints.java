package com.reservationng.CMS;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONObject;

public class AboutPoints  extends Fragment implements ServiceCallback {

    private TextView tvDesc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.cms_layout1, container, false);
        getActivity().setTitle("About Points");
        tvDesc = (TextView) view.findViewById(R.id.tvDesc);
        new CallService(AboutPoints.this, getActivity(), Constants.REQ_BASE_CMS).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.BASE_CMS+"about_point");
        return view;
    }

    @Override
    public void onServiceResponse(int requestcode, String response) {

        switch (requestcode) {
            case Constants.REQ_BASE_CMS:
                Log.e("Response is", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("code")==1) {
                        String description = jsonObject.getString("description");
                        tvDesc.setText(Html.fromHtml("<p>" + description + "</p>"));
                    } else {
                        CommonUtils.showToast(getActivity(),"Something went wrong");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }
}
