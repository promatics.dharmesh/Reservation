package com.reservationng.CMS;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.reservationng.CommonUtils;
import com.reservationng.R;
import com.reservationng.utils.CallService;
import com.reservationng.utils.Constants;
import com.reservationng.utils.ServiceCallback;

import org.json.JSONObject;

/**
 * Created by android2 on 4/23/16.
 */
public class TermsnConditions1 extends AppCompatActivity implements ServiceCallback{

    private TextView tvDesc;
    private ScrollView sv;
    private Toolbar toolbar;                              // Declaring the Toolbar Object

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cms_layout);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        this.setSupportActionBar(toolbar);
        setTitle("Terms and Conditions");

        tvDesc = (TextView) findViewById(R.id.tvDesc);
        sv = (ScrollView) findViewById(R.id.sv);
        new CallService(TermsnConditions1.this, TermsnConditions1.this, Constants.REQ_BASE_CMS).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.BASE_CMS + "agreement");

    }

    @Override
    public void onServiceResponse(int requestcode, String response) {
        switch (requestcode) {
            case Constants.REQ_BASE_CMS:
                Log.e("Response is", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("code") == 1) {
                        sv.setVisibility(View.VISIBLE);
                        String description = jsonObject.getString("description");
                        tvDesc.setText(Html.fromHtml("<p>" + description + "</p>"));
                    } else {
                        CommonUtils.showToast(TermsnConditions1.this, "Something went wrong");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}