package com.reservationng;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.reservationng.adapters.PrivateDiningListAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

public class PrivateDiningList extends Fragment {
    private RecyclerView mRecyclerView;
    private PrivateDiningListAdapter adapter;
    private ArrayList<String> pvtDining=new ArrayList<String>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View v = inflater.inflate(R.layout.private_dining_list, container, false);
        getActivity().setTitle("Private Dining");
        pvtDining.clear();
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).color(Color.TRANSPARENT).
                size(10).build());
        adapter = new PrivateDiningListAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);
        pvtDining.add("All Categories");
        pvtDining.add("Event Size");
        pvtDining.add("Location");
        pvtDining.add("Cuisine");
        pvtDining.add("Price");
        return v;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.private_dining_menu, menu);
    }

    void PvtDining() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Private Dining");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vi = inflater.inflate(R.layout.listview_layout, null, false);
        dialog.setContentView(vi);
        ListView lv = (ListView) vi.findViewById(R.id.lvView);
        lv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pvtDining));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Price selected", "" + pvtDining.get(i).toString());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_add:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new ContactSelRestaurant(), "ContactSelRestaurant");
                break;
            case R.id.ic_filter:
               //pvtDining.clear();
                PvtDining();
                //((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new RestaurantFilter(), "Restaurants filetr");
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
