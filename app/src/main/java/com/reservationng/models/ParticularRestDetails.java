package com.reservationng.models;

/**
 * Created by android2 on 12/17/15.

 */
public class ParticularRestDetails {

    private String resName,resAddress1,resAddress2,resDesc;

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public String getResAddress1() {
        return resAddress1;
    }

    public void setResAddress1(String resAddress1) {
        this.resAddress1 = resAddress1;
    }

    public String getResAddress2() {
        return resAddress2;
    }

    public void setResAddress2(String resAddress2) {
        this.resAddress2 = resAddress2;
    }

    public String getResDesc() {
        return resDesc;
    }

    public void setResDesc(String resDesc) {
        this.resDesc = resDesc;
    }
}
