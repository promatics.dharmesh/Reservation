package com.reservationng.models;

import android.os.Parcel;
import android.os.Parcelable;

public class RoomFacilities implements Parcelable{

    public static final Parcelable.Creator<RoomFacilities> CREATOR=new Parcelable.Creator<RoomFacilities>() {
        @Override
        public RoomFacilities createFromParcel(Parcel parcel) {
            return new RoomFacilities(parcel);
        }

        @Override
        public RoomFacilities[] newArray(int i) {
            return new RoomFacilities[i];
        }
    };

    public RoomFacilities(String name){
        super();
        this.name=name;
    }
    public RoomFacilities(Parcel parcel) {
        name=parcel.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
