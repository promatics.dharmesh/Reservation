package com.reservationng.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Offers implements Parcelable{

    public static final Parcelable.Creator<Offers> CREATOR=new Parcelable.Creator<Offers>() {
        @Override
        public Offers createFromParcel(Parcel parcel) {
            return new Offers(parcel);
        }

        @Override
        public Offers[] newArray(int i) {
            return new Offers[i];
        }
    };

    public Offers(){
        super();
//        this.id=id;
//        this.name=name;
    }

    public Offers(Parcel parcel)
    {
        id=parcel.readString();
        name=parcel.readString();
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String id,name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
