package com.reservationng.models;

/**
 * Created by android2 on 12/18/15.
 */
public class MapLatLng {

    private com.google.android.gms.maps.model.LatLng LatLng;

    public com.google.android.gms.maps.model.LatLng getLatLng() {
        return LatLng;
    }

    public void setLatLng(com.google.android.gms.maps.model.LatLng latLng) {
        LatLng = latLng;
    }
}
