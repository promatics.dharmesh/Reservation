package com.reservationng.models;

/**
 * Created by android2 on 1/19/16.
 */

public class ReviewDetails {
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String ambience,service,food,noiseLvl,review,
            date,rating,id,restName,userName;

    public String getRestName() {
        return restName;
    }

    public void setRestName(String restName) {
        this.restName = restName;
    }

    public String getAmbience() {
        return ambience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAmbience(String ambience) {
        this.ambience = ambience;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getNoiseLvl() {
        return noiseLvl;
    }

    public void setNoiseLvl(String noiseLvl) {
        this.noiseLvl = noiseLvl;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
