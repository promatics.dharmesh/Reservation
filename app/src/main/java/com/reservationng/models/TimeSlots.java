package com.reservationng.models;

/**
 * Created by promatics on 17/12/15.
 */
public class TimeSlots {
    private String timing;
    private String restTableId;

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    private String slotId;

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getRestTableId() {
        return restTableId;
    }

    public void setRestTableId(String restTableId) {
        this.restTableId = restTableId;
    }
}
