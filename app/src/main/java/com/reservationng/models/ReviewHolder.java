package com.reservationng.models;

/**
 * Created by Vaishali Arora on 4/20/2016.
 */
public class ReviewHolder {


    public static ReviewHolder object = null;
    public String bookingid="",restId="",restName="";
    public ReviewHolder() {
        super();
        object = null;

    }

    public static ReviewHolder getInstance() {
        if (object == null)
            object = new ReviewHolder();
        return object;
    }

}
