package com.reservationng.models;

/**

 * Created by android2 on 12/22/15.
 */

public class RestMenuCategory {

    private String catId,catName;
    private RestMenuItems  menuItems[];

    public String getCatId() {
        return catId;
    }
    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public RestMenuItems[] getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(RestMenuItems[] menuItems) {
        this.menuItems = menuItems;
    }
}
