package com.reservationng.models;

public class ReservationRestaurants {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    public String getDesc() {
        return desc;
    }

    public String getSpcReq() {
        return spcReq;
    }

    public void setSpcReq(String spcReq) {
        this.spcReq = spcReq;
    }

    public String getAmbience() {
        return ambience;
    }

    public void setAmbience(String ambience) {
        this.ambience = ambience;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getNoiseLvl() {
        return noiseLvl;
    }

    public void setNoiseLvl(String noiseLvl) {
        this.noiseLvl = noiseLvl;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setDesc(String desc) {
        this.desc = desc;

    }

    public String getBookingId() {
        return bookingId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public boolean isFlag() {
        return flag;
    }

    public String getRewardPt() {
        return rewardPt;
    }

    public void setRewardPt(String rewardPt) {
        this.rewardPt = rewardPt;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getTabletimeId() {
        return tabletimeId;
    }

    public void setTabletimeId(String tabletimeId) {
        this.tabletimeId = tabletimeId;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNoOfPeople() {
        return noOfPeople;
    }

    public void setNoOfPeople(String noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    private String name, img, resid, noOfPeople, timings,date,
            ambience,service,food,noiseLvl,review,rating,rewardPt,
            bookingId,phoneNo,address,desc,spcReq,tabletimeId;
    private boolean flag;
}
