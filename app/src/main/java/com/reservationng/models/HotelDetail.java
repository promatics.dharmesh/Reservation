package com.reservationng.models;

/**
 * Created by android2 on 1/4/16.
 */
public class HotelDetail {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCityId() {
        return cityId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Offers[] getOffers() {
        return offers;
    }

    public void setOffers(Offers[] offers) {
        this.offers = offers;
    }

    public String getFavUserId() {
        return favUserId;
    }

    public String getFavStatus() {
        return favStatus;
    }

    public void setFavStatus(String favStatus) {
        this.favStatus = favStatus;
    }

    public void setFavUserId(String favUserId) {
        this.favUserId = favUserId;
    }

    public String getCuisine() {
        return cuisine;
    }

    public String getFirstroomId() {
        return firstroomId;
    }

    public void setFirstroomId(String firstroomId) {
        this.firstroomId = firstroomId;
    }

    public String getFirstroomName() {
        return firstroomName;
    }

    public String getTotReview() {
        return totReview;
    }

    public void setTotReview(String totReview) {
        this.totReview = totReview;
    }

    public void setFirstroomName(String firstroomName) {
        this.firstroomName = firstroomName;

    }

    public String getFirstRoomPrice() {
        return firstRoomPrice;
    }

    public void setFirstRoomPrice(String firstRoomPrice) {
        this.firstRoomPrice = firstRoomPrice;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOverallRating() {
        return OverallRating;
    }

    public void setOverallRating(String overallRating) {
        OverallRating = overallRating;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    private String id,name,image,price,reviews,lat,lng,cityId,stateId,cuisine,distance,
    cityName,stateName,address1,address2,phoneno,description,favUserId,favStatus,
    firstroomId,firstroomName,firstRoomPrice,OverallRating,totReview,priceRange;

    private Offers offers[];

}
