package com.reservationng.models;

public class DataHolder {

    public String id="",time="",date="",name="",noOfPeople="",img="",curLati="",curLng="",efname="",elname="",emailuser="",ephn="",searchtype="",
    bookingId="",tableTimeId="",slotId="",slottiming="",time24="",loc="",hotelLoc="",locId="",hotelLocId="",rewardPt="",offer_id="",
    specReq="",mobile="",desc="",address="",destLati="",destLng="",email="",hotDealLoc="",hotDealLocId="",resLoc="",resLocId="",offerName="",chkDeal="";

    public static DataHolder object = null;

    public DataHolder() {
        super();
        object = null;

    }

    public static DataHolder getInstance() {
        if (object == null)
            object = new DataHolder();
        return object;
    }

}
