package com.reservationng.models;

/**
 * Created by android2 on 12/16/15.
 */
public class RestaurantLists {



    public TimeSlots[] getSlots() {
        return slots;
    }

    public void setSlots(TimeSlots[] slots) {
        this.slots = slots;
    }

    private TimeSlots[] slots;

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public String getResAddress1() {
        return resAddress1;
    }

    public void setResAddress1(String resAddress1) {
        this.resAddress1 = resAddress1;
    }

    public String getResAddress2() {
        return resAddress2;
    }

    public void setResAddress2(String resAddress2) {
        this.resAddress2 = resAddress2;
    }

    public String getResCityId() {
        return resCityId;
    }

    public void setResCityId(String resCityId) {
        this.resCityId = resCityId;
    }

    public String getResStateId() {
        return resStateId;
    }

    public String getResTableDate() {
        return resTableDate;
    }

    public void setResTableDate(String resTableDate) {
        this.resTableDate = resTableDate;
    }

    public String getResTableNumber() {
        return resTableNumber;
    }

    public void setResTableNumber(String resTableNumber) {
        this.resTableNumber = resTableNumber;
    }

    public void setResStateId(String resStateId) {
        this.resStateId = resStateId;
    }

    public String getResPos() {
        return resPos;
    }

    public void setResPos(String resPos) {
        this.resPos = resPos;
    }

    public String getResTableTiming() {
        return resTableTiming;
    }

    public String getResCityName() {
        return resCityName;
    }

    public void setResCityName(String resCityName) {
        this.resCityName = resCityName;
    }

    public String getResStateName() {
        return resStateName;
    }

    public void setResStateName(String resStateName) {
        this.resStateName = resStateName;
    }

    public String getResTableId() {
        return resTableId;
    }

    public void setResTableId(String resTableId) {
        this.resTableId = resTableId;
    }

    public void setResTableTiming(String resTableTiming) {
        this.resTableTiming = resTableTiming;
    }

    public String getResDesc() {
        return resDesc;
    }

    public String getResMobileNo() {
        return resMobileNo;
    }

    public void setResMobileNo(String resMobileNo) {
        this.resMobileNo = resMobileNo;
    }

    public String getResLati() {
        return resLati;
    }

    public String getResDate() {
        return resDate;
    }

    public void setResDate(String resDate) {
        this.resDate = resDate;
    }

    public String getResFavStaus() {
        return resFavStaus;
    }

    public void setResFavStaus(String resFavStaus) {
        this.resFavStaus = resFavStaus;
    }

    public void setResLati(String resLati) {
        this.resLati = resLati;
    }

    public String getResLng() {
        return resLng;
    }

    public String getResImg() {
        return resImg;
    }

    public void setResImg(String resImg) {
        this.resImg = resImg;
    }

    public String getResFavId() {
        return resFavId;
    }

    public String getNoOfPeople() {
        return noOfPeople;
    }

    public void setNoOfPeople(String noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

    public void setResFavId(String resFavId) {
        this.resFavId = resFavId;
    }

    public String getResOverallRating() {
        return resOverallRating;
    }

    public void setResOverallRating(String resOverallRating) {
        this.resOverallRating = resOverallRating;
    }

    public String getResTotReviews() {
        return resTotReviews;
    }

    public void setResTotReviews(String resTotReviews) {
        this.resTotReviews = resTotReviews;
    }

    public Offers[] getOffers() {
        return offers;
    }

    public String getHotdealsTimingId() {
        return hotdealsTimingId;
    }

    public void setHotdealsTimingId(String hotdealsTimingId) {
        this.hotdealsTimingId = hotdealsTimingId;
    }

    public void setOffers(Offers[] offers) {
        this.offers = offers;
    }

    public void setResLng(String resLng) {
        this.resLng = resLng;
    }

    public ReviewDetails[] getReviewDetails() {
        return reviewDetails;
    }

    public String getFoodrating() {
        return foodrating;
    }

    public void setFoodrating(String foodrating) {
        this.foodrating = foodrating;
    }

    public String getNoise() {
        return noise;
    }

    public void setNoise(String noise) {
        this.noise = noise;
    }

    public String getAmb() {
        return amb;
    }

    public void setAmb(String amb) {
        this.amb = amb;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public void setReviewDetails(ReviewDetails[] reviewDetails) {
        this.reviewDetails = reviewDetails;

    }

    public String getRewardPt() {
        return rewardPt;
    }

    public void setRewardPt(String rewardPt) {
        this.rewardPt = rewardPt;
    }

    public String getDistance() {
        return distance;
    }


    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getSpecReq() {
        return specReq;
    }

    public void setSpecReq(String specReq) {
        this.specReq = specReq;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }


    public void setResDesc(String resDesc) {
        this.resDesc = resDesc;
    }

    public String getResCuisine() {
        return resCuisine;
    }

    public void setResCuisine(String resCuisine) {
        this.resCuisine = resCuisine;
    }

    private String resId, resName, resAddress1, resAddress2, resCityId, resCityName, resImg, noOfPeople,
            priceRange, cuisineId,isSlotsAvail,
            resStateId, resStateName, resDesc, resCuisine, resPos, resTableDate, resTableNumber,
            hotdealsTimingId,
            resTableTiming, resTableId, resLati, resLng, resDate, resMobileNo, resFavStaus,
            rewardPt, distance,openingHours,
            resFavId, resOverallRating, resTotReviews, foodrating, noise, amb, service,bookingId,specReq,resWebSite,setResAdditional
            ,resPaymentOptions,resPhoneno,resCuisines,resDining;

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getIsSlotsAvail() {
        return isSlotsAvail;
    }

    public void setIsSlotsAvail(String isSlotsAvail) {
        this.isSlotsAvail = isSlotsAvail;
    }

    private Offers offers[];

    private ReviewDetails reviewDetails[];

    public String getResWebSite() {
        return resWebSite;
    }

    public void setResWebSite(String resWebSite) {
        this.resWebSite = resWebSite;
    }

    public String getSetResAdditional() {
        return setResAdditional;
    }

    public void setSetResAdditional(String setResAdditional) {
        this.setResAdditional = setResAdditional;
    }

    public String getResPaymentOptions() {
        return resPaymentOptions;
    }

    public void setResPaymentOptions(String resPaymentOptions) {
        this.resPaymentOptions = resPaymentOptions;
    }

    public String getResPhoneno() {
        return resPhoneno;
    }

    public void setResPhoneno(String resPhoneno) {
        this.resPhoneno = resPhoneno;
    }

    public String getResCuisines() {
        return resCuisines;
    }

    public void setResCuisines(String resCuisines) {
        this.resCuisines = resCuisines;
    }

    public String getResDining() {
        return resDining;
    }

    public void setResDining(String resDining) {
        this.resDining = resDining;
    }
}
