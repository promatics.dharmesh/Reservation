package com.reservationng.models;

/**
 * Created by android2 on 12/22/15.
 */
public class RestMenuItems {

    private String menuItemId,menuItemName,menuItemPrice,menuItemCom;

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public String getMenuItemPrice() {
        return menuItemPrice;
    }

    public void setMenuItemPrice(String menuItemPrice) {
        this.menuItemPrice = menuItemPrice;
    }

    public String getMenuItemCom() {
        return menuItemCom;
    }

    public void setMenuItemCom(String menuItemCom) {
        this.menuItemCom = menuItemCom;
    }
}
