package com.reservationng.models;

/**
 * Created by android2 on 2/20/16.
 */
public class CombineReservations {
    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getTimings() {
        return Timings;
    }

    public void setTimings(String timings) {
        Timings = timings;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getSpcReq() {
        return SpcReq;
    }

    public void setSpcReq(String spcReq) {
        SpcReq = spcReq;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getNoOfPeople() {
        return NoOfPeople;

    }

    public void setNoOfPeople(String noOfPeople) {
        NoOfPeople = noOfPeople;
    }


    public String getTabletimeId() {
        return TabletimeId;
    }

    public void setTabletimeId(String tabletimeId) {
        TabletimeId = tabletimeId;
    }

    public String getResid() {
        return Resid;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getRewardPt() {
        return RewardPt;
    }

    public void setRewardPt(String rewardPt) {
        RewardPt = rewardPt;
    }

    public void setResid(String resid) {
        Resid = resid;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotRooms() {
        return totRooms;
    }

    public void setTotRooms(String totRooms) {
        this.totRooms = totRooms;
    }

    public String getTotadult() {
        return totadult;
    }

    public void setTotadult(String totadult) {
        this.totadult = totadult;
    }

    public String getTotchild() {
        return totchild;
    }

    public void setTotchild(String totchild) {
        this.totchild = totchild;
    }

    public String getAmbience() {
        return ambience;
    }

    public void setAmbience(String ambience) {
        this.ambience = ambience;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getNoiseLvl() {
        return noiseLvl;
    }

    public void setNoiseLvl(String noiseLvl) {
        this.noiseLvl = noiseLvl;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelImg() {
        return hotelImg;
    }

    public void setHotelImg(String hotelImg) {
        this.hotelImg = hotelImg;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getImg() {
        return img;

    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getCustomer_arrival_confirm() {
        return customer_arrival_confirm;
    }

    public void setCustomer_arrival_confirm(String customer_arrival_confirm) {
        this.customer_arrival_confirm = customer_arrival_confirm;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    private String BookingId, Timings, Date, NoOfPeople, SpcReq, TabletimeId,Resid,img,name,PhoneNo,
    Address,Desc,RewardPt,type;


    private String id, checkIn, checkOut, price, totRooms, totadult, totchild,
            ambience, service, food, noiseLvl, review, rating,lat,lng,
            roomType, hotelId, hotelImg, hotelName, cuisines, mobileNo, address,customer_arrival_confirm;
    private boolean flag;



}
